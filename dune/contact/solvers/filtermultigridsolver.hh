// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_SOLVERS_FILTER_MULTIGRID_CONTACT_SOLVER_HH
#define DUNE_CONTACT_SOLVERS_FILTER_MULTIGRID_CONTACT_SOLVER_HH

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/contact/common/filter.hh>
#include <dune/contact/solvers/tnnmmgcontactstep.hh>

#include <dune/solvers/solvers/quadraticipopt.hh>
#include <dune/solvers/iterationsteps/trustregiongsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/norms/twonorm.hh>
#include <dune/solvers/common/wrapownshare.hh>

namespace Dune {
namespace Contact {

/** \brief Filter Trust-Region SQP solver for large deformation contact problems
 *         with the infinity-norm as trust region norm.
 *         The local linearly constrained problems are solved using IpOpt.
 *
 *  The filter method ensures global convergence, by using the trust--region mechanism
 *  to achieve a sufficient reduction and the 'acceptable to the filter' criterion
 *  to enforce asymptotic convergence towards the feasible set. See
 *
 *  Fletcher, Gould, Toint (2002)
 *  "Global convergence of a trust-region SQP-filter algorithm for general nonlinear programming"
 *
 *  for a detailed description.
*/
template<class ProblemType>
class FilterMultigridContactSolver : public Dune::Solvers::IterativeSolver<typename ProblemType::VectorType>
{

  using VectorType = typename ProblemType::VectorType;
  using MatrixType = typename ProblemType::MatrixType;
  using field_type = typename VectorType::field_type;

  static const int dim = VectorType::block_type::dimension;
  using Base = Dune::Solvers::IterativeSolver<VectorType>;

public:

  template <class Norm1, class Norm2>
  FilterMultigridContactSolver(const Dune::ParameterTree& config,
                      std::vector<VectorType>& x,
                      ProblemType& problem,
                      std::vector<Norm1>& errorNorms,
                      Norm2&& mgErrorNorm) :
    Base(config.get<field_type>("tolerance"),config.get<int>("maxIterations"),
         config.get("verbosity", Solver::FULL))
  {
    setupParameter(config);
    setProblem(x, problem);
    setErrorNorms(errorNorms);
    Base::setErrorNorm(std::forward<Norm2>(mgErrorNorm));

    size_t totalSize(0);
    for (size_t i=0; i<x.size(); i++)
      totalSize += x[i].size();

    dirichletNodes_.resize(totalSize, false);
    dirichletValues_.resize(totalSize);
    dirichletValues_ = 0;
    correction_.resize(totalSize);
    correction_ = 0;

    hasDirichlet_.resize(x.size(), false);

    setupTnnmmg(config);
  }

  //! Setup the trust region parameter from a config file
  void setupParameter(const Dune::ParameterTree& config) {
    filter_ = Filter<field_type>(config.get("filterTolerance",1e-4));
    initTR_ = config.get<field_type>("initialTrustRegionRadius");
    maxTR_ = config.get("maxTrustRegionRadius",10*initTR_);
    localTol_ = config.get<field_type>("localTolerance");
    localIt_ = config.get<field_type>("localIterations");
    critTol_ = config.get<field_type>("critTol");
    trScaling_ = config.get<bool>("trScaling");
    ipoptOnly_ = config.get<bool>("ipoptOnly");
  }

  //! Setup the tnnmmg step
  void setupTnnmmg(const Dune::ParameterTree& config) {

    tnnmmgStep_.setMGType(1,3,3);

    // set either IpOpt or a non-convex Gauss-Seidel loop-solver as base solver
    if (config.get<bool>("ipoptBaseSolver")) {
      tnnmmgStep_.setBaseSolver(Dune::Solvers::QuadraticIPOptSolver<MatrixType,VectorType>(
                            config.get<field_type>("baseSolverTol"),
                            config.get<field_type>("baseSolverIterations"),
                            config.get<Solver::VerbosityMode>("baseSolverVerbosity", Solver::QUIET),
                            config.get("linearSolverType","")));
    } else
      tnnmmgStep_.setBaseSolver(Dune::Solvers::LoopSolver<VectorType>(
                            TrustRegionGSStep<MatrixType, VectorType>(),
                            config.get<field_type>("baseSolverIterations"),
                            config.get<field_type>("baseSolverTol"),
                            TwoNorm<VectorType>(),
                            config.get<Solver::VerbosityMode>("baseSolverVerbosity", Solver::QUIET)));

    tnnmmgStep_.setIgnore(dirichletNodes_);
    tnnmmgStep_.setVerbosity(config.get<Solver::VerbosityMode>("localVerbosity"));
  }

  /** \brief Set the Dirichlet values.
     *
     * The Dirichlet conditions are enforced during the solution
     * of the first linearised problem rather than modifying the
     * initial iterate of the filter scheme. This avoids possible problems
     *in the energy evaluation due to inverted elements.
     * Hence Dirichlet values for the correction must be handed over!
     */
  void setDirichletValues(const std::vector<VectorType>& dirichletValues,
                          const std::vector<Dune::BitSetVector<dim> >& dirichletNodes)
  {
    hasDirichlet_.resize(dirichletValues.size());
    int counter(0);
    for (size_t i=0; i<dirichletNodes.size(); i++) {
      hasDirichlet_[i] = (dirichletNodes[i].count() > 0);
      for (size_t j=0; j<dirichletNodes[i].size(); counter++,j++)
        if (dirichletNodes[i][j].any()) {
          dirichletNodes_[counter] = dirichletNodes[i][j];
          dirichletValues_[counter] = dirichletValues[i][j];
        }
    }
  }

  /** \brief Set the Dirichlet values.
     *
     * The Dirichlet conditions are enforced during the solution
     * of the first linearised problem rather than modifying the
     * initial iterate of the filter scheme. This avoids possible problems
     *in the energy evaluation due to inverted elements.
     * Hence Dirichlet values for the correction must be handed over!
     */
  void setDirichletValues(const VectorType& totalDirichletValues,
                          const Dune::BitSetVector<dim>& totalDirichletNodes)
  {
    dirichletNodes_ = totalDirichletNodes;
    dirichletValues_ = totalDirichletValues;
  }


  //! Set initial iterate and the problem class
  void setProblem(std::vector<VectorType>& x, ProblemType& problem)
  {
    iterates_ = Dune::stackobject_to_shared_ptr<std::vector<VectorType> >(x);
    problem_ = Dune::stackobject_to_shared_ptr<ProblemType>(problem);
  }

  //! Set the error norms to be used within the filter method
  template <class ErrorNorm>
  void setErrorNorms(std::vector<ErrorNorm>& errorNorms)
  {
    errorNorms_.resize(errorNorms.size());
    for (size_t i=0; i<errorNorms.size(); i++)
      errorNorms_[i] = Dune::Solvers::wrap_own_share<Norm<VectorType> >(errorNorms[i]);
  }

  //! Solve the problem
  void solve();

  //! Return solution object
  std::vector<VectorType> getSol() const {return *iterates_;}

  //! Return final energy
  field_type getEnergy() const {return energy_;}

  //! Return the final infeasibility
  field_type getInfeasibility() const {return infeasibility_;}

protected:

  //! Compute criticality measure for the current sub-problem
  field_type criticality() const {

    if (dirichletValues_.infinity_norm()>1e-10)
      return dirichletValues_.infinity_norm();

    const auto& obstacles = problem_->constraints();
    const auto& f = problem_->f();

    VectorType x(f.size());
    for (size_t i=0; i<f.size();i++)
      for (int k=0; k<dim; k++)
        if (dirichletNodes_[i][k])
          x[i][k] = 0;
        else if (f[i][k]>0)
          x[i][k] = std::min(1.0, obstacles[i].upper(k));
        else if  (f[i][k]<0)
          x[i][k] = -1;
    return std::fabs(f*x);

  }

  //! Compute criticality measure for the current sub-problem
  field_type criticality2() const {

    BoxConstraint<field_type,1> oneConstraint;
    oneConstraint[0] = {-1, 1};
    // the criticality measure is given by computing the projected gradient path
    const auto& obstacles = problem_->constraints();

    auto gradient = problem_->f();
    for (size_t j=0; j<gradient.size(); j++) {
      for (int k=0; k<dim; k++) {
          gradient[j][k] = obstacles[j][k].projectIn(gradient[j][k]);
          gradient[j][k] = oneConstraint[0].projectIn(gradient[j][k]);
        if (dirichletNodes_[j][k])
          gradient[j][k] = -correction_[j][k];
      }
    }
    return gradient.two_norm();
  }

  //! flags if no Dirichlet boundaries are given
  std::vector<bool> hasDirichlet_;

  //! The iterates of the global scheme
  std::shared_ptr<std::vector<VectorType> > iterates_;

  //! Dirichlet degrees of freedom
  Dune::BitSetVector<dim> dirichletNodes_;

  //! The Dirichlet values given in terms of a correction
  VectorType dirichletValues_;

  //! The iterate for the local problems
  VectorType correction_;

  //! The problem type, providing methods to assemble the nonlinear and linearised
  //! energy and constraints
  std::shared_ptr<ProblemType> problem_;

  //! The norms to estimate the errors
  std::vector<std::shared_ptr<const Norm<VectorType> > > errorNorms_;

  //! The filter set
  Filter<field_type> filter_;

  //! Initial trust region radius
  field_type initTR_;

  //! Maximal trust region radius
  field_type maxTR_;

  //! Tolerance for the solution of the local problem
  field_type localTol_;

  //! Number of iterations to solve the local problem
  field_type localIt_;

  //! Tolerance for the criticality measure
  field_type critTol_;

  //! Flag to determine whether the trust-region norm in transformed coordinates should
  //! be scaled to match the norms of the basis vectors (which can be larger than one)
  bool trScaling_;

  //! The TNNMMG step for the solution of the local problems
  Dune::Contact::TnnmmgContactStep<MatrixType, VectorType> tnnmmgStep_;

  //! The current infeasibility
  field_type infeasibility_;
  //! The current energy
  field_type energy_;
  bool ipoptOnly_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "filtermultigridsolver.cc"

#endif
