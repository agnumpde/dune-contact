// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <memory>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/bvector.hh>
#include <dune/common/fvector.hh>

#include <dune/matrix-vector/transpose.hh>

namespace Dune {
namespace Contact {

// Sets obstacles on the finest level
template <class GridType, class VectorType>
void TwoBodyAssembler<GridType, VectorType>::assembleObstacle()
{

    // ///////////////////////////////////////////////////////////////
    // Combine the bitfields for each grid to one long bitfield
    // ///////////////////////////////////////////////////////////////

    this->totalHasObstacle_.resize(this->numDofs());
    const auto& nonmortarBoundary = this->contactCoupling_[0]->nonmortarBoundary();

    for (int k=0; k<this->grids_[0]->size(dim); k++)
        if (nonmortarBoundary.containsVertex(k))
            this->totalHasObstacle_[k] = true;

    ///////////////////////////////////////
    //  Set the obstacle values
    ///////////////////////////////////////

    this->totalObstacles_.resize(this->numDofs());

    const auto& obs = this->contactCoupling_[0]->weakObstacle_;

    std::vector<int> globalToLocal;
    nonmortarBoundary.makeGlobalToLocal(globalToLocal);

    const auto& gridView = this->grids_[0]->leafGridView();
    const auto& indexSet = gridView.indexSet();

    for (const auto& vIt : vertices(gridView)) {

        int vIdx = indexSet.index(vIt);
        if (globalToLocal[vIdx]<0)
            continue;

        // Set the obstacle
        switch (this->coupling_[0].type_) {

            case CouplingPairBase::CONTACT:
                this->totalObstacles_[vIdx].upper(0) = obs[globalToLocal[vIdx]];
                break;

            case CouplingPairBase::STICK_SLIP:
                this->totalObstacles_[vIdx].lower(0) = 0;
                this->totalObstacles_[vIdx].upper(0) = 0;
                break;

            case CouplingPairBase::GLUE:
                for (int j=0; j<dim; j++) {
                    this->totalObstacles_[vIdx].lower(j) = 0;
                    this->totalObstacles_[vIdx].upper(j) = 0;
                }
                break;

            case CouplingPairBase::NONE:
                break;
            default:
                DUNE_THROW(Dune::NotImplemented, "Coupling type not handled yet!");
        }
    }
}

template <class GridType, class VectorType>
void TwoBodyAssembler<GridType, VectorType>::
setup(const GridType& grid0, const GridType& grid1,
               std::shared_ptr<const LevelBoundaryPatch> nonmortarPatch,
               std::shared_ptr<const LevelBoundaryPatch> mortarPatch,
               field_type obsDistance,
               typename CouplingPairBase::CouplingType couplingType,
               std::shared_ptr<Projection> projection,
               std::shared_ptr<GridGlueBackend> backend)
{
    this->grids_[0] = &grid0;
    this->grids_[1] = &grid1;
    this->coupling_[0].set(0,1, nonmortarPatch, mortarPatch, obsDistance, couplingType, projection, backend);

    setup();
}

template <class GridType, class VectorType>
void TwoBodyAssembler<GridType, VectorType>::assembleTransferOperator()
{
    if (!this->grids_[0])
        DUNE_THROW(Dune::Exception, "You have not provided a first grid!");

    if (!this->grids_[1])
        DUNE_THROW(Dune::Exception, "You have not provided a second grid!");

    if (!this->coupling_[0].obsPatch_)
        DUNE_THROW(Dune::Exception, "You have not provided a nonmortar boundary");

    if (!this->coupling_[0].mortarPatch_)
        DUNE_THROW(Dune::Exception, "You have not provided a mortar boundary");

    // ////////////////////////////////////////////////////
    //   Set up Mortar element transfer operators
    // ///////////////////////////////////////////////////

    this->contactCoupling_[0]->grid0_        = this->grids_[0];
    this->contactCoupling_[0]->grid1_        = this->grids_[1];
    this->contactCoupling_[0]->setupContactPatch(*this->coupling_[0].obsPatch_,*this->coupling_[0].mortarPatch_);
    this->contactCoupling_[0]->gridGlueBackend_ = this->coupling_[0].backend();
    this->contactCoupling_[0]->setCoveredArea(this->coveredArea_);
    this->contactCoupling_[0]->setup();

    // ///////////////////////////////////////////////////////////////
    // Setup the local coordinate systems
    // ///////////////////////////////////////////////////////////////

    field_type dist = this->coupling_[0].obsDistance_;

    const auto& coupling = *this->contactCoupling_[0];
    std::vector<Dune::FieldVector<field_type,dim> > directions(coupling.nonmortarBoundary().numVertices());

    this->coupling_[0].projection()->project(coupling.nonmortarBoundary(), coupling.mortarBoundary(),dist);

    this->coupling_[0].projection()->getObstacleDirections(directions);

    this->computeLocalCoordinateSystems(coupling.nonmortarBoundary(), this->localCoordSystems_, directions);

    computeTransformationMatrix();
}

template <class GridType, class VectorType>
void TwoBodyAssembler<GridType, VectorType>::
computeTransformationMatrix()
{

    // ////////////////////////////////////////////////////
    //   Combine local stiffness matrices into one
    // ////////////////////////////////////////////////////

    int nRows = this->grids_[0]->size(dim) + this->grids_[1]->size(dim);
    int nCols = nRows;

    const auto& nonmortarBoundary = this->contactCoupling_[0]->nonmortarBoundary();

    typedef typename MatrixType::row_type RowType;
    typedef typename RowType::ConstIterator ConstColumnIterator;

    std::vector<int> nonmortarToGlobal(nonmortarBoundary.numVertices());
    int idx = 0;
    for (int i=0; i<this->grids_[0]->size(dim); i++)
        if (nonmortarBoundary.containsVertex(i))
            nonmortarToGlobal[idx++] = i;

    // first create the index structure of the transformation matrix
    Dune::MatrixIndexSet indicesBT(nRows, nCols);

    // BT_ is the identity plus some off-diagonal elements
    for (int i=0; i<nRows; i++)
        indicesBT.add(i,i);

    // the off-diagonal part
    const MatrixType& M = this->contactCoupling_[0]->mortarLagrangeMatrix();

    for (size_t i=0; i<M.N(); i++) {

        const RowType& row = M[i];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for (; cIt!=cEndIt; ++cIt) {
            indicesBT.add(nonmortarToGlobal[i], cIt.index() + this->grids_[0]->size(dim));
        }

    }

    indicesBT.exportIdx(this->BT_);
    this->BT_ = 0;

    // Enter identity part
    for (int i=0; i<nRows; i++)
        for (int j=0; j<dim; j++)
            this->BT_[i][i][j][j] = 1;

    for (size_t i=0; i<M.N(); i++) {

        const RowType& row = M[i];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for (; cIt!=cEndIt; ++cIt) {
            this->BT_[nonmortarToGlobal[i]][cIt.index() + this->grids_[0]->size(dim)] = *cIt;
        }

    }

    // modify non-mortar dofs and rotate them in normal and tangential coordinates
    for (std::size_t k=0; k<this->localCoordSystems_.size(); k++)
      if(nonmortarBoundary.containsVertex(k))
        this->BT_[k][k] = this->localCoordSystems_[k];

}

template <class GridType, class VectorType>
void TwoBodyAssembler<GridType, VectorType>::
assembleJacobian(const std::array<const MatrixType*, 2>& submat,
         MatrixType& totalMatrix)
{

    std::size_t nRows = submat[0]->N()+submat[1]->N();
    std::size_t nCols = submat[0]->M()+submat[1]->M();

    // Create the index set of B \hat{A} B^T
    Dune::MatrixIndexSet indicesBABT(nRows, nCols);

    typedef typename MatrixType::row_type RowType;
    typedef typename RowType::ConstIterator ConstColumnIterator;

    // zero offset for the first grid
    std::array<size_t, 2> offsets = {{0, submat[0]->N()}};

    for (std::size_t q=0; q<2; q++) {
        for (std::size_t i=0; i<submat[q]->N(); i++) {

            const RowType& row = (*submat[q])[i];

            // Loop over all columns of the stiffness matrix
            ConstColumnIterator j    = row.begin();
            ConstColumnIterator jEnd = row.end();

            for (; j!=jEnd; ++j) {

                ConstColumnIterator k    = this->BT_[offsets[q] + i].begin();
                ConstColumnIterator kEnd = this->BT_[offsets[q] + i].end();

                for (; k!=kEnd; ++k) {

                    ConstColumnIterator l    = this->BT_[offsets[q] + j.index()].begin();
                    ConstColumnIterator lEnd = this->BT_[offsets[q] + j.index()].end();

                    for (; l!=lEnd; ++l)
                        indicesBABT.add(k.index(), l.index());

                }
            }
        }
    }

    // Multiply B \hat{A} B^T
    indicesBABT.exportIdx(totalMatrix);
    totalMatrix = 0;

    for (size_t q=0; q<2; q++) {
        for (std::size_t i=0; i<submat[q]->N(); i++) {

            const RowType& row = (*submat[q])[i];

            // Loop over all columns of the stiffness matrix
            ConstColumnIterator j    = row.begin();
            ConstColumnIterator jEnd = row.end();

            for (; j!=jEnd; ++j) {

                ConstColumnIterator k    = this->BT_[offsets[q] + i].begin();
                ConstColumnIterator kEnd = this->BT_[offsets[q] + i].end();
                for (; k!=kEnd; ++k) {

                    ConstColumnIterator l    = this->BT_[offsets[q] + j.index()].begin();
                    ConstColumnIterator lEnd = this->BT_[offsets[q] + j.index()].end();
                    for (; l!=lEnd; ++l) {

                        //BABT[k][l] += BT[i][k] * mat_[i][j] * BT[j][l];
                        MatrixBlock m_ij = *j;

                        MatrixBlock BT_ik_trans;
                        Dune::MatrixVector::transpose(*k, BT_ik_trans);

                        m_ij.leftmultiply(BT_ik_trans);
                        m_ij.rightmultiply(*l);

                        totalMatrix[k.index()][l.index()] += m_ij;
                    }
                }
            }
        }
    }
}

} /* namespace Contact */
} /* namespace Dune */
