// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_PROJECTIONS_CONTACT_PROJECTION_HH
#define DUNE_CONTACT_PROJECTIONS_CONTACT_PROJECTION_HH

#include <dune/common/fvector.hh>

namespace Dune {
namespace Contact {

/** \brief Abstract base class for projections that map the vertices of the nonmortar side to the master side.
 *
 *  This class is used to compute the obstacle search directions and values for the nonmortar boundary vertices.
 *
 *  \tparam BoundaryPatchType0 The type of the boundary patch for the nonmortar contact boundary
 *  \tparam BoundaryPatchType1 The type of the boundary patch for the mortar contact boundary
 */
template <class BoundaryPatchType0, class BoundaryPatchType1=BoundaryPatchType0>
class ContactProjection
{
public:
    typedef typename BoundaryPatchType0::GridView GridView;

    enum {dim = GridView::dimension};
    enum {dimworld = GridView::dimensionworld};

    typedef typename GridView::ctype ctype;
    typedef Dune::FieldVector<ctype,dimworld> CoordinateType;

    /** \brief Struct representing a boundary segment. */
    struct BoundarySegment {

        std::array<CoordinateType, 4> vertices;

        int nVertices;

        CoordinateType unitOuterNormal;

    };

    ContactProjection(ctype overlap=1e-2) :
        overlap_(overlap)
    {}


    /** \brief Return the directions in which the obstacles were found. */
    void getObstacleDirections(std::vector<CoordinateType>& obsDirections) const
    {
        obsDirections = obsDirections_;
    }

    /** \brief Return the found obstacle values. */
    template <class Vector>
    void getObstacles(Vector& obstacles) const
    {
        obstacles.resize(obstacles_.size());
        for (size_t i = 0; i < obstacles_.size(); i++)
          obstacles[i] = obstacles_[i];
    }

    /** \brief Project the vertices of nonmortar boundary onto points of the mortar boundary
     *         and compute the distance and the direction of the projection.
     */
    virtual void project(const BoundaryPatchType0& nonmortar, const BoundaryPatchType1& mortar,
                         const ctype couplingDist = std::numeric_limits<ctype>::max()) = 0;


    /** \brief Construct vector of boundary segments representing the faces of the mortar contact boundary.*/
    static void computeMortarSide(std::vector<BoundarySegment>& mortarSegments,
                  const BoundaryPatchType1& mortarPatch)
    {

        // init return values
        mortarSegments.resize(mortarPatch.numFaces());

        // loop over all mortar faces

        int i=0;
        Dune::FieldVector<ctype,dim-1> zero(0);
        for (const auto& it : mortarPatch) {

            BoundarySegment& newSegment = mortarSegments[i++];
            const auto& geometry = it.geometry();
            newSegment.nVertices = geometry.corners();

            for (int k=0; k<newSegment.nVertices; k++)
                newSegment.vertices[k] = geometry.corner(k);

            // assume flat faces, use outer normal at 0
            newSegment.unitOuterNormal = it.unitOuterNormal(zero);
        }
    }

protected:
    //! Allow some overlap
    ctype overlap_;
    //! The computed directions in which the obstacles were found.
    std::vector<CoordinateType> obsDirections_;
    //! The found obstacle values
    std::vector<ctype> obstacles_;

};

} /* namespace Contact */
} /* namespace Dune */

#endif
