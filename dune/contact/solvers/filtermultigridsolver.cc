// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#include <limits>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/solvers/quadraticipopt.hh>

#include <dune/contact/solvers/nsnewtoncontacttransfer.hh>

namespace Dune {
namespace Contact {

template<class ProblemType>
void FilterMultigridContactSolver<ProblemType>::solve()
{
  // clear the filter
  filter_.clear();

  // initialise trust--region
  field_type trustRegion = initTR_;

  auto& iterates = *iterates_;

  size_t totalSize(0);
  for (size_t i=0; i<iterates.size(); i++)
    totalSize += iterates[i].size();

  // compute energy infeasibility of initial iterate
  energy_ = problem_->energy(iterates);

  // take into account non-homogeneous Dirichlet conditions,
  infeasibility_ = std::max(problem_->infeasibility(iterates), dirichletValues_.infinity_norm());

  if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
    std::cout<< " Initial energy: " << energy_
             << " and infeasibility: " << infeasibility_ << std::endl;

  // //////////////////////////////
  //    Create the tnnmmg step
  // ////////////////////////////

  // transfer operator need to be setup in every trust region iteration
  const auto& grids = problem_->contactAssembler()->getGrids();

  // Get the overall top level
  int toplevel = 0;
  for (size_t i=0;i<grids.size();i++)
    toplevel = std::max(toplevel, grids[i]->maxLevel());

  std::vector<std::shared_ptr<NonSmoothNewtonContactTransfer<VectorType> > > mgTransfers(toplevel);
  for (size_t i=0; i<mgTransfers.size(); i++)
      mgTransfers[i] = std::make_shared<NonSmoothNewtonContactTransfer<VectorType> >();
  tnnmmgStep_.setTransferOperators(mgTransfers);

  ::LoopSolver<VectorType> solver(tnnmmgStep_,
                                  (toplevel==0) ? 1 : localIt_,
                                  localTol_, *this->errorNorm_, Solver::FULL);

  Dune::Solvers::QuadraticIPOptSolver<MatrixType,VectorType> ipopt(1e-5,
                                                            600,
                                                            Solver::REDUCED,
                                                             "ma97");

  // the trust--region constraints
  std::vector<BoxConstraint<field_type,dim> > trObs(correction_.size());
  tnnmmgStep_.setObstacles(trObs);
  ipopt.setObstacles(trObs);
  ipopt.setIgnore(dirichletNodes_);

  field_type error = std::numeric_limits<field_type>::max();
  field_type crit = std::numeric_limits<field_type>::max();

  // count number of iterations and repeated steps
  int repeat(0);
  int it(0);

  // Loop until desired tolerance or maximum number of iterations is reached
  for (; it <this->maxIterations_ and (error > this->tolerance_ or std::isnan(error)) and
       (crit > critTol_) ; ++it) {

    Dune::Timer iterationTimer;

    correction_ = dirichletValues_;

    if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
      std::cout << "************************\n"
                << "*  Filter iteration " << it << "\n"
                << "************************\n";

    if (this->verbosity_ == Solver::FULL)
      std::cout << "Trust--region radius: " << trustRegion << "\n";

    // Measure norm of the old iterate
    field_type oldNorm{0};
    for (size_t i=0; i<iterates.size(); i++)
      oldNorm += (*errorNorms_[i])(iterates[i]);

    Dune::Timer timeAss;
    problem_->assembleQP(iterates);

    if (this->verbosity_ == Solver::FULL)
      std::cout <<" Took " << timeAss.stop() << " to assemble local problem \n";

    // compute criticality measure for the last iterate
    crit = criticality();

    if (this->verbosity_ == Solver::FULL)
      std::cout << "Criticality " << crit << std::endl;

    field_type newEnergy, newInfeasibility;
    std::vector<VectorType> newIterates;

    // decrease so we can increase again
    repeat--;

    // setup the linearised problem
    tnnmmgStep_.setProblem(problem_->A(), correction_, problem_->f());
    ipopt.setProblem(problem_->A(), correction_, problem_->f());
    if (trScaling_)
      tnnmmgStep_.setTrScaling(problem_->contactAssembler()->getColNorms());

    // setup transferoperator
    NonSmoothNewtonContactTransfer<VectorType>::template setupHierarchy<typename ProblemType::DeformedGridType>(
          mgTransfers, problem_->contactAssembler());


    const auto& constraints = problem_->constraints();

    std::vector<VectorType> corrections(iterates.size());
    do {
      repeat++;

      // setup trust-region constraints
      for (size_t j=0; j<trObs.size(); j++) {

        if (!trScaling_) {
          trObs[j].upper(0) = std::min(trustRegion, constraints[j].upper(0));
          trObs[j].lower(0) = -trustRegion;

        for (int k=1; k<dim; k++)
          trObs[j][k] = {-trustRegion, trustRegion};
        } else {

          // Scaling for the trust-region norm to take into account the distortion of
          // the decoupling basis transformation
          const auto& trScaling = problem_->contactAssembler()->getColNorms();

          trObs[j].upper(0) = std::min(trustRegion*trScaling[j][0], constraints[j].upper(0));
          trObs[j].lower(0) = -trustRegion*trScaling[j][0];

          for (int k=1; k<dim; k++)
            trObs[j][k] = {-trustRegion*trScaling[j][k], trustRegion*trScaling[j][k]};
        }

        // scale Dirichlet values to trust-region constraints
        for (int k=0; k<dim; k++)
          correction_[j][k] = trObs[j][k].projectIn(correction_[j][k]);

        trObs[j] -= correction_[j];
      }

      tnnmmgStep_.setTrustRegion(trustRegion);

      // Shift Non-homogeneous Dirichlet condition into the right-hand side
      // so we always solve a problem with homogeneous ones.
      auto f = problem_->f();
      problem_->A().mmv(correction_, f);

      auto oldDir = correction_;
      correction_ = 0;

      tnnmmgStep_.setProblem(problem_->A(), correction_, f);
      ipopt.setProblem(problem_->A(), correction_, f);

      Dune::Timer time;
      if (ipoptOnly_)
         ipopt.solve();
      else {
        solver.preprocess();
        solver.solve();
      }

      if (this->verbosity_ == Solver::FULL)
        std::cout << "Solved local problem in " << time.stop() << std::endl;

      // post process
      correction_ += oldDir;
      problem_->contactAssembler()->postprocess(correction_, corrections);

      // new iterate
      newIterates = iterates;
      for (size_t i=0; i<newIterates.size(); i++)
        newIterates[i] += corrections[i];

      // compute new energy and infeasibility
      newEnergy = problem_->energy(newIterates);
      newInfeasibility = problem_->infeasibility(newIterates);

      // take into account infeasibility due to dirichlet values
      if (dirichletValues_.infinity_norm() > newInfeasibility)
        for (size_t i=0; i<correction_.size(); i++)
          for (int j=0; j<dim; j++)
            if (dirichletNodes_[i][j])
              newInfeasibility = std::max(newInfeasibility, std::fabs(dirichletValues_[i][j] - correction_[i][j]));

      if (this->verbosity_ == Solver::FULL)
        std::cout<<"New energy of potential iterate "<< newEnergy <<
                   " and infeasibility "<<newInfeasibility << std::endl;

      field_type infNorm = this->correction_.infinity_norm();

      // if we want to properly decrease the trust-region according to the inf-norm,
      // then we have to take into account the possible scaling
      field_type scaledInfNorm = infNorm;
      if (trScaling_) { // Changed from not trScaling!
        const auto& trScaling = problem_->contactAssembler()->getColNorms();
        scaledInfNorm = 0;
        for (size_t j = 0 ; j<this->correction_.size();j++)
          for (int k=0; k < dim; k++)
            scaledInfNorm = std::max(scaledInfNorm, std::fabs(this->correction_[j][k]/trScaling[j][k]));
      }

      if (!std::isfinite(newEnergy)) {
        trustRegion  = 0.1*std::min(trustRegion, scaledInfNorm);
        correction_ = dirichletValues_;
        std::cout<<"Energy is not finite, reduce trust--region "<<trustRegion<<std::endl;
        continue;
      }

      if(this->verbosity_==Solver::FULL)
        std::cout << "Correction infinite norm: " << infNorm << std::endl;

      // compute reduction of the model
      VectorType tmp(correction_.size());
      problem_->A().mv(correction_, tmp);
      field_type modelDecrease = (problem_->f()*correction_) - 0.5 * (correction_*tmp);
      field_type relativeModelDecrease = modelDecrease / std::fabs(newEnergy);
      field_type relativeEnergyDecrease = (energy_ - newEnergy)/std::fabs(newEnergy);

      // this condition ensures that only infeasible points are added to the filter
      // if this conditions fails, it means that the infeasibility of the last iterate
      // is larger than the energy decrease. In this case we except the new iterate and
      // make a step towards feasibility. The constant 1e-4 is chosen as in
      // Conn,Gould,Toint - Trust-Region methods
      bool suffDecrease  = (modelDecrease >= 1e-4*std::pow(infeasibility_,2));

      if (this->verbosity_ == Solver::FULL) {
        std::cout << "Absolute model decrease: " << modelDecrease
                  << ",  functional decrease: " << energy_ - newEnergy << std::endl;
        std::cout << "Relative model decrease: " << relativeModelDecrease
                  << ",  functional decrease: " << relativeEnergyDecrease << std::endl;
      }

      // accept step when the we probably run into rounding errors

      field_type eps = 1e-16;

      if (std::abs(modelDecrease) <eps or std::abs(energy_-newEnergy)<eps or
          std::abs(relativeModelDecrease)<eps or std::abs(relativeEnergyDecrease)<eps) {
     //     or std::abs(newInfeasibility) <eps) {
        if (this->verbosity_ == Solver::FULL)
          std::cout << "Computing trust--region ratio might lead to cancellation...exiting." << std::endl;
        // if the trust-region norm is very small then increase it a bit to allow full steps
        if (std::abs(scaledInfNorm-trustRegion)<eps)
          trustRegion =  std::min(initTR_, 2*scaledInfNorm);
        break;
      }

      // if iterate is not acceptable to the filter
      // then reject it and reduce the trust-region
      bool acceptable{true};
      if (it == 0)
        acceptable = (it == 0) ? filter_.acceptable(newInfeasibility, newEnergy)
                               : filter_.acceptable({infeasibility_, energy_}, newInfeasibility, newEnergy);
      if (not acceptable) {

        // some heuristic to reduce the trust--region
        trustRegion  = 0.25*std::min(trustRegion, scaledInfNorm);

        correction_ = dirichletValues_;
        if (this->verbosity_== Solver::FULL)
          std::cout << "Potential iterate is not acceptable to the filter, reducing trust-region: " << trustRegion << "\n";
        continue;
      }

      // check if trust-region mechanism accepts iterate
      field_type ratio = (energy_-newEnergy)/modelDecrease;
      if (this->verbosity_== Solver::FULL)
        std::cout<<"Approximation ratio "<<ratio<<std::endl;

      // Model is not a good approximation while the infeasibility is small
      if (suffDecrease && (ratio < 0.1)) {

        // some heuristic to reduce the trust--region
        trustRegion  = 0.25*std::min(trustRegion, scaledInfNorm);
        correction_ = dirichletValues_;

        if (this->verbosity_ == Solver::FULL)
          std::cout << " Unsuccessful step, shrinking trust-region: "<<trustRegion<<std::endl;
        continue;
      }

      // At this point the iterate is accepted

      // If the sufficient decrease condition fails, we add the old iterate to the filter
      if (!suffDecrease) {
        if (this->verbosity_ == Solver::FULL)
          std::cout << "Sufficient decrease failed, make a step towards feasibility!\n";
        filter_.add(infeasibility_,energy_);
        break;
      }

      // Model is a good approximation
      if (ratio > 0.9) {

        if (this->verbosity_ == Solver::FULL)
          std::cout << "Very successful step!\n";

        // some heuristic to enlage the trust-region
        trustRegion = std::min(maxTR_, std::fmax(2.5*scaledInfNorm, trustRegion));
        break;

        // Approximation is acceptable
      } else if (ratio > 0.1) {

        if (this->verbosity_ == Solver::FULL)
          std::cout<<"Successful step!\n";
        break;
      }

    } while (true);

    // compute error estimate
    field_type absError = 0;
    for (size_t i=0;i<corrections.size();i++)
      absError += (*errorNorms_[i])(corrections[i]);

    error = absError;
    if (std::isfinite(error/oldNorm))
      error /= oldNorm;

    if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
      std::cout << "In filter iteration "<< it
                << ", absolut norm of correction : " << absError
                << " relative error " << error
                << "\nEnergy " << newEnergy
                << " and infeasibility " << newInfeasibility << std::endl;

    // update iterates, energy and infeasibility
    iterates = newIterates;
    energy_ = newEnergy;
    infeasibility_ = newInfeasibility;

    // computed defect Dirichlet conditions
    for (size_t i=0; i<correction_.size(); i++)
      for (int j=0; j<dim; j++)
        if (dirichletNodes_[i][j])
          dirichletValues_[i][j] -= correction_[i][j];

    if (this->verbosity_ == Solver::FULL)
      std::cout << "Iteration took " << iterationTimer.stop() << std::endl;

  } // end of filter loop

  if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL) {
    problem_->assembleQP(iterates);
    auto crit = criticality();
    std::cout << "Solved problem in " << it << " iterations with final energy " << this->energy_
              << " and infeasibility " << infeasibility_
              << "final criticality " << crit
              << " repeating " << repeat << " steps "<< std::endl;
  }
}

} /* namespace Contact */
} /* namespace Dune */
