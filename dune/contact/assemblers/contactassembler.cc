// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:

#include <dune/matrix-vector/addtodiagonal.hh>

namespace Dune {
namespace Contact {

template <int dim, class field_type>
void ContactAssembler<dim,field_type>::householderReflection(const CoordinateType& v0,
                                                             const CoordinateType& v1,
                                                             MatrixBlock& mat)
{
  CoordinateType v = (v1 - v0);
  v *= 0.5;

  // The code from here to the end of the routine sets Orth to
  // the Householder reflexion matrix of v, that is
  // $Orth = I - 2 \frac{v v^T}{v^T v}$
  mat = 0;
  Dune::MatrixVector::addToDiagonal(mat,1.0);

  field_type norm = v.two_norm2();

  if(fabs(norm) <= 1e-8)
      return;

  for(int i=0;i<dim;i++)
    for(int j=0;j<dim;j++)
        mat[i][j] -= 2.0 *v[i]*v[j] / norm;

}

template <int dim, class field_type>
template <class BoundaryPatchType>
void ContactAssembler<dim,field_type>::computeLocalCoordinateSystems(const BoundaryPatchType& contactBoundary,
                                                    std::vector<MatrixBlock>& coordSystems,
                                                    const std::vector<CoordinateType>& obsDirections)
{
    // /////////////////////////////////////////////////////////
    // Compute a full orthonormal coordinate system for each
    // boundary segment carrying an obstacle.  We do this by computing
    // the Householder reflection of the first canonical basis
    // vector into the surface normal.
    // /////////////////////////////////////////////////////////

    auto globalToLocal = contactBoundary.makeGlobalToLocal();

    // Make first canonical basis vector
    CoordinateType e0(0);
    e0[0] = 1;

    coordSystems.resize(globalToLocal.size());
    Dune::ScaledIdentityMatrix<field_type,dim> id(1.0);

    for (size_t i=0; i<globalToLocal.size(); i++) {

        if (globalToLocal[i]>=0) {
            // There is an obstacle at this vertex --> the coordinate system
            // will consist of the surface normal and tangential vectors

            householderReflection(e0, obsDirections[globalToLocal[i]], coordSystems[i]);
        } else {
            // There is no obstacle.  The coordinate system will be the canonical one
            coordSystems[i] = id;
        }
    }
}

template <int dim, class field_type>
void ContactAssembler<dim,field_type>::computeLocalCoordinateSystems(const Dune::BitSetVector<1>& hasObs,
                                                    std::vector<MatrixBlock>& coordSystems,
                                                    const std::vector<CoordinateType>& obsDirections)
{

    // /////////////////////////////////////////////////////////
    // Compute a full orthonormal coordinate system for each
    // boundary segment carrying an obstacle.  We do this by computing
    // the Householder reflection of the first canonical basis
    // vector into the surface normal.
    // /////////////////////////////////////////////////////////

    // Make first canonical basis vector
    CoordinateType e0(0);
    e0[0] = 1;

    coordSystems.resize(hasObs.size());
    Dune::ScaledIdentityMatrix<field_type,dim> id(1.0);

    for (size_t i=0; i<obsDirections.size(); i++) {

        if (hasObs[i][0]) {
            // There is an obstacle at this vertex --> the coordinate system
            // will consist of the surface normal and tangential vectors

            householderReflection(e0, obsDirections[i], coordSystems[i]);
        } else {
            // There is no obstacle.  The coordinate system will be the canonical one
            coordSystems[i] = id;
        }
    }
}

template <int dim,class field_type>
template <class MatrixType>
void ContactAssembler<dim,field_type>::
transformMatrix(MatrixType& mat) const
{
    for(size_t rowIdx=0; rowIdx<mat.N(); rowIdx++) {

        auto& row = mat[rowIdx];

        auto cEndIt = row.end();
        for(auto cIt = row.begin(); cIt!=cEndIt; ++cIt) {

            // allow the local coordinate systems to be of smaller size
            // this way sometimes multiplications with the identity can
            // be avoided
            if (rowIdx < localCoordSystems_.size())
                cIt->leftmultiply(localCoordSystems_[rowIdx]);

            if (cIt.index() < localCoordSystems_.size())
                cIt->rightmultiply(localCoordSystems_[cIt.index()]);

        }
    }
}

template <int dim,class field_type>
template <class VectorType>
void ContactAssembler<dim,field_type>::
transformVector(VectorType& vector) const
{

    // Transform to local coordinate systems
    for (size_t i=0; i < localCoordSystems_.size(); i++) {
        typename VectorType::block_type tmp(0.0);
        localCoordSystems_[i].umv(vector[i], tmp);
        vector[i] = tmp;
    }
}

} /* namespace Contact */
} /* namespace Dune */
