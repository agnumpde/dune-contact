// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_SOLVERS_PRIMAL_DUAL_SEMI_SMOOTH_NEWTON_SOLVER_HH
#define DUNE_CONTACT_SOLVERS_PRIMAL_DUAL_SEMI_SMOOTH_NEWTON_SOLVER_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/solvers/iterativesolver.hh>

namespace Dune {
namespace Contact {

/** \brief Primal-Dual Semi-Smooth Newton method
 *
 *  See Popp, Wall et al. 2011
*/
template <class Problem>
class PrimalDualSemiSmoothNewtonSolver
    : public Dune::Solvers::IterativeSolver<typename Problem::BlockVector> {

  using BlockVector = typename Problem::BlockVector;
  using field_type = typename Dune::FieldTraits<BlockVector>::field_type;
  using ScalarVector = typename Problem::ScalarVector;
  using ScalarMatrix = typename Problem::ScalarMatrix;
  using Matrix = typename Problem::MatrixType;
  using JacobianType = typename Problem::JacobianType;

  static constexpr int dim = BlockVector::block_type::dimension;
  using Base = Dune::Solvers::IterativeSolver<BlockVector>;

public:
  PrimalDualSemiSmoothNewtonSolver(
      const Dune::ParameterTree& config, std::vector<BlockVector>& x,
      Problem& problem,
      const std::vector<std::shared_ptr<Norm<BlockVector> > >& errorNorms)
      : Base(config.get<field_type>("tolerance"),
             config.get<int>("maxIterations"),
             config.get("verbosity", NumProc::FULL)) {
    setupParameter(config);
    setProblem(x, problem);
    setErrorNorm(errorNorms);

    size_t totalSize(0);
    for (size_t i = 0; i < x.size(); i++)
      totalSize += x[i].size();

    dirichletNodes_.resize(totalSize*dim, false);
  }

  //! Setup the trust region parameter from a config file
  void setupParameter(const Dune::ParameterTree& config) {
    localTol_ = config.get<field_type>("localTolerance");
    localMaxIterations_ = config.get<int>("localMaxIterations");
    linearSolverType_ = config.get("linearSolverType", "");
  }

  /** \brief Set the Dirichlet values.
     *
     * The Dirichlet conditions are enforced during the solution
     * of the first linearised problem rather than modifying the
     * initial iterate of the filter scheme. This avoids possible problems
     *in the energy evaluation due to inverted elements.
     * Hence Dirichlet values for the correction must be handed over!
     */
  void setDirichletValues(
      const std::vector<BlockVector>& dirichletValues,
      const std::vector<Dune::BitSetVector<dim> >& dirichletNodes) {
    // only adjust correction Dirichlet conditions are prescribed
    correction_.resize(dirichletNodes_.size());
    correction_ = 0;
    int counter(0);
    for (size_t i = 0; i < dirichletNodes.size(); i++)
      for (size_t j = 0; j < dirichletNodes[i].size(); counter++, j++)
        if (dirichletNodes[i][j].any())
          for (int k = 0; k < dim; k++) {
            dirichletNodes_[dim*counter + k] = dirichletNodes[i][j][k];
            correction_[dim*counter + k][0] = dirichletValues[i][j][k];
          }

    dN_ = dirichletNodes;
  }

  //! Set initial iterate and the problem class
  void setProblem(std::vector<BlockVector>& x, Problem& problem) {
    iterates_ = Dune::stackobject_to_shared_ptr<std::vector<BlockVector> >(x);
    problem_ = Dune::stackobject_to_shared_ptr<Problem>(problem);
  }

  //! Set the error norms to be used within the filter method
  void
  setErrorNorm(const std::vector<std::shared_ptr<Norm<BlockVector> > >& errorNorms) {
    errorNorms_ = errorNorms;
  }

  //! Solve the problem
  void solve();

  //! Return solution object
  const std::vector<BlockVector>& getSol() const { return *iterates_; }

  std::string debugPath_;
  std::vector<Dune::BitSetVector<dim> > dN_;

private:
  ScalarVector combineVector(const ScalarVector& primal, const ScalarVector& dual) const {
    size_t index(0);
    ScalarVector combinedVector(primal.size() + dual.size());
    for (size_t i = 0; i < primal.size(); i++)
      combinedVector[index++] = primal[i];

    for (size_t i = 0; i < dual.size(); i++)
      combinedVector[index++] = dual[i];
    return combinedVector;
  }

  std::tuple<ScalarVector, ScalarVector> decomposeVector(const ScalarVector& primalDual) const {
    size_t index(0);
    ScalarVector primal(correction_.size());
    for (size_t i = 0; i < primal.size(); i++)
        primal[i] = primalDual[index++];

    ScalarVector dual(lagrangeMultiplier_.size());
    for (size_t i = 0; i < dual.size(); i++)
      dual[i] = primalDual[index++];
    return std::tie(primal, dual);
  }

  //! The iterates of the global scheme
  std::shared_ptr<std::vector<BlockVector> > iterates_;

  ScalarVector lagrangeMultiplier_;

  //! Dirichlet degrees of freedom
  Dune::BitSetVector<1> dirichletNodes_;

  //! The iterate for the local problems, containing possible Dirichlet values
  ScalarVector correction_;

  //! The problem type, providing methods to assemble the nonlinear and
  //! linearised
  //! energy and constraints
  std::shared_ptr<Problem> problem_;

  //! The norms to estimate the errors
  std::vector<std::shared_ptr<Norm<BlockVector> > > errorNorms_;

  //! Tolerance for the solution of the local problem
  field_type localTol_;
  //! Max iterations for the solution of the local problem
  int localMaxIterations_;

  //! Store the old energy
  field_type energy_;
  //! Store the infeasibility of the old iterate
  field_type infeasibility_;

  //! The linear solver to be used within IpOpt
  std::string linearSolverType_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "primaldualsemismoothnewtonsolver.cc"

#endif
