// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#include <config.h>

#include <dune/fufem/utilities/adolcnamespaceinjections.hh>
#include <sys/stat.h>
#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/io.hh>
#include <dune/istl/basearray.hh>

#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/localassemblers/massassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>
#include <dune/fufem/utilities/gridconstruction.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>
#include <dune/fufem/utilities/dirichletbcassembler.hh>
#include <dune/fufem/improvegrid.hh>

#include <dune/functions/functionspacebases/pqknodalbasis.hh>

#include <dune/solvers/norms/energynorm.hh>

// from the 'dune-contact module
#include <dune/contact/solvers/primaldualsemismoothnewtonsolver.hh>
#include <dune/contact/common/primaldualcontactproblem.hh>
#include <dune/contact/common/contactassemblerfactory.hh>
#include <dune/contact/common/entitylog.hh>

#define LAURSEN
#include <dune/elasticity/materials/neohookeanmaterial.hh>

#if !HAVE_SUITESPARSE_UMFPACK
#error You need Umfpack for the inversion of the non-mortar matrix
#endif

#include <fstream>
// The grid dimension
const int dim = 3;

using field_type = double;

using namespace Dune;
using namespace Dune::Contact;
using namespace Dune::GridGlue;

using std::string;
using std::vector;

// Some types that I need
using FVector = FieldVector<field_type,dim>;
using MatrixType =  BCRSMatrix<FieldMatrix<field_type, dim, dim> >;
using VectorType = BlockVector<FVector>;

int main (int argc, char *argv[]) try
{
  Dune::MPIHelper::instance(argc, argv);

  // parse data file
  ParameterTree config;
  if (argc==2)
    ParameterTreeParser::readINITree(argv[1], config);
  else
    ParameterTreeParser::readINITree("resources/pridualcontact.parset", config);

  // the path where grid related files are located, empty if everything is created on the fly
  string path               = config.get("path","");

  // result path
  string resultPath         = config.get<string>("resultPath");

  // Get names of the grids
  const static size_t nGrids = 2;
  std::array<string,2> gridName;
  gridName[0] = config.get<string>("gridName0");
  gridName[1] = config.get<string>("gridName1");

  // ////////////////////////////////////////////
  //    Create the various bone grids
  // ////////////////////////////////////////////

  using GridType = UGGrid<dim>;
  std::map<string,std::unique_ptr<GridType>> grids;

  for (size_t i=0; i<nGrids; i++) {

    const ParameterTree& gridConfig = config.sub(gridName[i]);
    if (gridConfig.hasKey("amira_parFile")) {
#if HAVE_AMIRAMESH
      grids[gridName[i]] = std::unique_ptr<GridType>(AmiraMeshReader<GridType>::read(path + gridConfig.get<string>("gridFile"),
                                                           PSurfaceBoundary<dim-1>::read(path + gridConfig.get<string>("parFile"))));
#endif
    } else if (gridConfig.hasKey(("amira_gridFile"))) {
#if HAVE_AMIRAMESH
      grids[gridName[i]] = std::unique_ptr<GridType>(AmiraMeshReader<GridType>::read(path + gridConfig.get<string>("gridFile")));
#endif
    } else if (gridConfig.hasKey("createGrid") and gridConfig.get<bool>("createGrid")==true)
      grids[gridName[i]] = GridConstruction<GridType,dim>::createGrid(gridConfig);
    else
      DUNE_THROW(Exception, "You either have to provide a gridFile or set the Parameter createGrid to use a factory");
  }

  // ////////////////////////////////////////////////////////////////////
  //   Initial uniform refinement, if requested
  // ////////////////////////////////////////////////////////////////////

  const int minLevel         = config.get<int>("minLevel");
  for (size_t j=0; j<nGrids; j++) {

    grids[gridName[j]]->setRefinementType(GridType::COPY);

    const ParameterTree& gridConfig = config.sub(gridName[j]);
    // allow different refinement levels for each grid
    int nRefine = gridConfig.get("nRefines",minLevel);

    for (int i=0; i<nRefine; i++) {
      grids[gridName[j]]->globalRefine(1);
      printAspectRatios(grids[gridName[j]]->leafGridView());
    }

    std::cout<<"Dofs "<<grids[gridName[j]]->size(dim)<<std::endl;

    LeafAmiraMeshWriter<GridType> amiramesh;
    amiramesh.addLeafGrid(*grids[gridName[j]],true);
    amiramesh.write(resultPath + gridName[j]+".init",1);
  }

  // create the deformed grids
  DeformedContinuaComplex<GridType,VectorType> deformedGrids;
  deformedGrids.addGrid(*grids[gridName[0]],gridName[0]);
  deformedGrids.addGrid(*grids[gridName[1]],gridName[1]);

  //////////////////////////////////////////////////////////////////////////
  //   Set up coarse Dirichlet boundary conditions
  //////////////////////////////////////////////////////////////////////////

  // displacements
  vector<VectorType> u(nGrids);

  for (size_t i=0; i<nGrids; i++) {
    u[i].resize(grids[gridName[i]]->size(dim));
    u[i] = 0;
  }

  //   Create contact assembler
  auto contactAssembler = ContactAssemblerFactory::createLargeDeformationAssembler(deformedGrids, config);

  // Get the overall top level
  int toplevel = 0;
  for (size_t i=0;i<nGrids;i++)
    toplevel = std::max(toplevel, grids[gridName[i]]->maxLevel());

  // total size
  vector<VectorType> extForces(nGrids);
  for (size_t i=0; i<nGrids; i++) {
    extForces[i].resize(u[i].size());
    extForces[i] = 0;
  }

  // ///////////////////////////////////////////////////
  //   Do a homotopy of the Dirichlet boundary data
  // ///////////////////////////////////////////////////

  int counter  =1;
  std::string logPath = resultPath + "/logs/";
  if (config.get<bool>("restart")) {

    logPath = resultPath + "/logsRestart/";
    counter = config.get<int>("loadingStep");

    for (int i=0; i<2; i++) {
      std::ostringstream filename;
      filename<< resultPath << "/" << gridName[i] << "loading." << std::setw(2) << std::setfill('0') << counter -1;
      AmiraMeshReader<GridType>::readFunction(u[i],filename.str());
    }
  }

  mkdir(logPath.c_str(),0744);

  vector<VectorType> dirichletValues(nGrids);
  vector<BitSetVector<dim> > dirichletNodes(nGrids);

  for (size_t i=0; i<nGrids; i++)
    DirichletBCAssembler<GridType>::assembleDirichletBC(*grids[gridName[i]],
        config.sub(gridName[i]).sub("dirichlet"), dirichletNodes[i], dirichletValues[i]);

  // the Dirichlet values for the two stages
  field_type loadingStepsVertical = config.get<field_type>("loadingStepsVertical");
  field_type loadingStepsSlide = config.get<field_type>("loadingStepsSlide");
  std::vector<VectorType> localDirichlet(nGrids);
  for (size_t i=0; i<nGrids; i++) {
    localDirichlet[i].resize(2);

    const auto& gridConfig = config.sub(gridName[i]);
    localDirichlet[i][0] = gridConfig.get<FieldVector<field_type,dim> >("localDirichletVertical");
    localDirichlet[i][0] *= 1.0/field_type(loadingStepsVertical);
    localDirichlet[i][1] = gridConfig.get<FieldVector<field_type,dim> >("localDirichletSlide");
    localDirichlet[i][1] *= 1.0/field_type(loadingStepsSlide);

  }

  // Create the materials
  using P1Basis =  DuneFunctionsBasis<typename Dune::Functions::PQkNodalBasis<typename GridType::LeafGridView, 1> >;
  vector<std::shared_ptr<P1Basis> > p1Basis(nGrids);

  // using MaterialType = GeomExactStVenantMaterial<P1Basis>;
  using MaterialType =  NeoHookeanMaterial<P1Basis>;
  //using MaterialType =  AdolcNeoHookeanMaterial<P1Basis>;
  vector<MaterialType> materials(nGrids);
  for (size_t i=0; i<nGrids;i++) {

    p1Basis[i] = std::make_shared<P1Basis>(grids[gridName[i]]->leafGridView());
    const ParameterTree& gridConfig = config.sub(gridName[i]);
    materials[i].setup(*p1Basis[i], gridConfig.get<field_type>("E"),gridConfig.get<field_type>("nu"));

  }

  //////////////////////////////////////////////////////////////
  //  Create the static contact problem
  /////////////////////////////////////////////////////////////

  // use the linearised elasticity stiffness matrix as norm for the errors
  vector<MatrixType> elast(nGrids);
  vector<std::shared_ptr<Norm<VectorType> > > errorNorms(nGrids);
  MatrixType bigElastMatrix;
  for (size_t i=0; i<nGrids; i++) {

    OperatorAssembler<P1Basis,P1Basis> globalAssembler(*p1Basis[i],*p1Basis[i]);
    const ParameterTree& gridConfig = config.sub(gridName[i]);
    StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(gridConfig.get<field_type>("E"),gridConfig.get<field_type>("nu"));

    globalAssembler.assemble(localAssembler, elast[i]);
    MassAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement, MatrixType::block_type> localMass;

    MatrixType mass;
    globalAssembler.assemble(localMass, mass);
    elast[i] = mass;

    errorNorms[i] = std::make_shared<EnergyNorm<MatrixType, VectorType> >(elast[i]);

  }

  Dune::MatrixVector::BlockMatrixView<MatrixType>::setupBlockMatrix(elast,bigElastMatrix);

  std::map<std::string,field_type> log;
  log["infeasibility"] = 0.0; log["criticality"] = 0.0;
  log["avMgIt"] =  0;
  log["repeat"] =  0;
  log["time"] =  0;
  log["filterIt"] = 0;
  log["relError"] = 0;
  log["avMgIt"] =  0;
  log["realTime"] =  0;

  EntityLog<double> logger(logPath);
  for (auto& it : log)
    logger.addLogEntity(it.first,it.second);

  for (; counter <= loadingStepsVertical + loadingStepsSlide; counter++) {

    const auto& priDualConfig = config.sub("primaldual");

    // Make static contact problem
    using ProblemType = PrimalDualContactProblem<VectorType, MaterialType>;
    ProblemType contactProblem(contactAssembler, materials, deformedGrids, extForces,
                               priDualConfig.get<field_type>("priDualConstant"),
                               priDualConfig.get<field_type>("priDualFDeps"),
                               priDualConfig.get<bool>("full_linearisation"),
                               priDualConfig.get<field_type>("initActiveSetBound"));

    // Create trust-region step
    PrimalDualSemiSmoothNewtonSolver<ProblemType> contactSolver(priDualConfig, u, contactProblem, errorNorms);


    std::ostringstream localLog;
    localLog<< logPath << "/" << counter <<"/";
    contactSolver.debugPath_ = localLog.str();

    mkdir(localLog.str().c_str(),0744);

    std::cout << "************************\n"
              << "*  Loading step " << counter << "\n"
              << "************************\n";

    //setup incremental Dirichlet
    for (size_t i=0; i<grids.size(); i++) {
      if (counter>loadingStepsVertical)
        std::cout << "Switch to sliding!\n";

      FieldVector<field_type,dim> dirichletStep;
      if (counter>loadingStepsVertical)
        dirichletStep = localDirichlet[i][1];
      else
        dirichletStep  = localDirichlet[i][0];

      for (size_t j=0; j<dirichletValues[i].size(); j++)
        if (dirichletNodes[i][j].any())
          dirichletValues[i][j]=dirichletStep;
      std::cout<<"Maximal Dirichlet Values " << dirichletValues[i].infinity_norm() << std::endl;
    }

    // loading steps on the coarsest refinement level
    contactSolver.setDirichletValues(dirichletValues, dirichletNodes);

    std::cout<<"Solving contact problem!\n";
    Dune::Timer time;
    contactSolver.solve();
 /* log["time"] =  time.stop();
  log["infeasibility"] = contactSolver.infeasibility_;
  log["criticality"] = contactSolver.crit_;
  log["avMgIt"] =  contactSolver.avMgIt_;
  log["repeat"] =  contactSolver.repeat;
  log["filterIt"] = contactSolver.it_;
  log["relError"] = contactSolver.error_;
  log["realTime"] =  contactSolver.time_;
        logger.log();

    std::cout<<"Took "<<log["time"]<<" to solve! \n";
*/
    // Output result
    for (size_t j=0; j<nGrids; j++) {

      std::ostringstream name;
      name<< resultPath << "/" << gridName[j] << "loading." << std::setw(2) << std::setfill('0') << counter;

      if (config.get<string>("writer")=="amira") {
#if HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType> amiramesh;
        amiramesh.addLeafGrid(*grids[gridName[j]],true);
        amiramesh.addVertexData(u[j], grids[gridName[j]]->leafGridView());
        amiramesh.write(name.str(),1);
#endif
      } else if (config.get<string>("writer") == "vtk") {

        VTKWriter<GridType::LeafGridView> vtkWriter(grids[gridName[j]]->leafGridView());
        auto displacement = std::make_shared<VTKBasisGridFunction<P1Basis,VectorType> >(*p1Basis[j], u[j], "Displacement");
        vtkWriter.addVertexData(displacement);
        vtkWriter.write(name.str());
      }
    }
  }

} catch (Exception e) {

  std::cout << e << std::endl;

}




