// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_ASSEMBLERS_LARGE_DEFORMATION_CONTACT_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_LARGE_DEFORMATION_CONTACT_ASSEMBLER_HH

#include <dune/contact/assemblers/largedeformationcoupling.hh>
#include <dune/contact/assemblers/nbodyassembler.hh>

namespace Dune {
namespace Contact {

/**  \brief Assembler for large deformation mortar discretised contact problems.
 *
 *  \tparam GridType Type of the corresponding grids.
 *  \tparam VectorType The vector type for the displacements.
 */
template <class GridType, class VectorType>
class LargeDeformationContactAssembler : public NBodyAssembler<GridType, VectorType>
{
  enum {dim = GridType::dimension};

  using Base = NBodyAssembler<GridType, VectorType>;
  using MatrixBlock = typename Base::MatrixBlock;
public:
  using field_type = typename Base::field_type;
  using LargeDefCoupling = LargeDeformationCoupling<field_type, GridType>;
  using ProjectionType = typename LargeDefCoupling::ProjectionType;
  using JacobianType = typename LargeDefCoupling::JacobianType;

  using MatrixType = typename Base::MatrixType;
  using LeafBoundaryPatch = typename Base::LeafBoundaryPatch;

private:
  using Base::grids_;
  using Base::coupling_;
  using Base::contactCoupling_;
  using Base::BT_;

public:
  using Base::nGrids;
  using Base::nCouplings;


  /** \brief Construct assembler from number of couplings and grids.
     *
     *  \param nGrids The number of involved bodies.
     *  \param nCouplings The number of couplings.
     */
  LargeDeformationContactAssembler(int nGrids, int nCouplings,
                                   bool exactPostprocess = true, field_type coveredArea = 0.99,
                                   field_type overlap = 1e-2,
                                   ProjectionType projectionType = ProjectionType::CLOSEST_POINT) :
    Base(nGrids, nCouplings, coveredArea),
    exactPostprocess_(exactPostprocess)
  {
    for (size_t i=0; i<contactCoupling_.size(); i++)
      contactCoupling_[i] = std::make_shared<LargeDefCoupling>(overlap, coveredArea, projectionType);
  }

  /** \brief Compute the transposed mortar transformation matrix. */
  void computeExactTransformationMatrix();

  /** \brief Compute the inexact sparse mortar transformation matrix. */
  void computeLumppedTransformationMatrix();

  /** \brief Compute the inverse of the transformation matrix. */
  void computeInverseTransformationMatrix();

  /** \brief Compute stiffness matrices in mortar transformed coordinates.
     *  \param submat Stiffness matrices w.r.t the nodal finite element basis.
     *  \param totalMatrix Reference to mortar transformed stiffness matrix for all bodies.
     */
  void assembleExactJacobian(const std::vector<const MatrixType*>& submat,
                                MatrixType& totalMatrix) const;

  /** \brief Transform and combine the right hand side into decoupling coordinates by solving a small system.
    *  \param rhs  The right hand side of each grid in Euclidean coordinates.
    *  \param totalMatrix Reference to transformed right hand side vector.
    */
  template <class VectorTypeContainer>
  void assembleExactRightHandSide(const VectorTypeContainer& rhs,
                          VectorType& totalRhs) const;

  /** \brief Transform solution back to Euclidean coordinates by solving a small system.*/
  template <class VectorTypeContainer>
  void postprocess(const VectorType& totalX, VectorTypeContainer& x) const;

  /** \brief Store old reduced non-mortar patches to possibly restore correct local indices for post-processing.*/
  void storePatches() {

    storedNmPatches_.resize(contactCoupling_.size());

    for (size_t i=0; i<contactCoupling_.size(); i++)
      storedNmPatches_[i] = contactCoupling_[i]->nonmortarBoundary();
  }

  /** \brief Setup the contact linearisations. */
  void assembleLinearisations() {

    for (size_t i=0; i<contactCoupling_.size(); i++)
      contactCoupling_[i]->setup();
  }

  /** \brief Assemble the exact problem directly.*/
  void assembleProblem();

  /** \brief Assemble the local coordinate systems corresponding to the closest point projection. */
  void assembleClpReflections();

  /** \brief Prepare the contact couplings. */
  void constructCouplings() {
    // assemble the linearisations
    for (size_t i=0; i<contactCoupling_.size(); i++) {
      contactCoupling_[i]->setGrids(*grids_[coupling_[i].gridIdx_[0]], *grids_[coupling_[i].gridIdx_[1]]);
      contactCoupling_[i]->setupContactPatch(*coupling_[i].patch0(),*coupling_[i].patch1());
      contactCoupling_[i]->gridGlueBackend_ = coupling_[i].backend();
    }
  }

  /** \brief Get the norms of the transformation matrix columns. */
  const VectorType getColNorms() const {return colNorms_; }

  const LeafBoundaryPatch& storedNmPatch(size_t index) const
  {
      return storedNmPatches_[index];
  }
  /** \brief Get the inverse transformation matrix. */
  const MatrixType& getInverseTransformation() const
  {
      return inverse_;
  }

private:
  /** \brief The norms of each column of the inverse transformation matrix
   * this is used to scale the trust-region in the decoupling coordinates to
   * catch the scaling which it is introducing to the basis vectors
  */
  VectorType colNorms_;

  //! The inverse of the decoupling transformation
  MatrixType inverse_;

  /** \brief The reduced non-mortar patche of the previous iteration
   * This is needed to get the correct local indices whenever a step is repeated.
   */
  std::vector<LeafBoundaryPatch> storedNmPatches_;


  bool exactPostprocess_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "largedeformationcontactassembler.cc"

#endif
