// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#include <config.h>

#include <sys/stat.h>

#include <dune/fufem/utilities/adolcnamespaceinjections.hh>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/unused.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/io.hh>

#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>
#include <dune/fufem/utilities/gridconstruction.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>
#include <dune/fufem/utilities/dirichletbcassembler.hh>
#include <dune/fufem/improvegrid.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/solvers/norms/energynorm.hh>

// from the 'dune-contact module
#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/projections/closestpointprojection.hh>
#include <dune/contact/solvers/filtermultigridsolver.hh>
#include <dune/contact/common/staticmgcontactproblem.hh>
#include <dune/contact/common/deformedcontinuacomplex.hh>
#include <dune/contact/assemblers/largedeformationcontactassembler.hh>

#define LAURSEN
#include <dune/elasticity/materials/neohookeanmaterial.hh>

// The grid dimension
const int dim = 3;

using field_type = double;

using namespace Dune;
using namespace Dune::Contact;
using namespace Dune::GridGlue;

using std::string;
using std::vector;

// Some types that I need
using FVector = FieldVector<field_type,dim>;
using MatrixType =  BCRSMatrix<FieldMatrix<field_type, dim, dim> >;
using VectorType = BlockVector<FVector>;

int main (int argc, char *argv[]) try
{

  Dune::MPIHelper::instance(argc, argv);

  // parse data file
  ParameterTree config;
  if (argc==2)
    ParameterTreeParser::readINITree(argv[1], config);
  else
    ParameterTreeParser::readINITree("resources/2bnonlincontact.parset", config);

  // the path where grid related files are located, empty if everything is created on the fly
  string path               = config.get("path","");

  // result path
  string resultPath         = config.get<string>("resultPath");

  // Get names of the grids
  const static size_t nGrids = 2;
  std::array<string,2> gridName;
  gridName[0] = config.get<string>("gridName0");
  gridName[1] = config.get<string>("gridName1");

  // ////////////////////////////////////////////
  //    Create the various bone grids
  // ////////////////////////////////////////////

  using GridType = UGGrid<dim>;
  std::map<string, std::shared_ptr<GridType> > grids;

  for (size_t i=0; i<nGrids; i++) {

    const ParameterTree& gridConfig = config.sub(gridName[i]);
    if (gridConfig.hasKey("amira_parFile")) {
#if HAVE_AMIRAMESH
      grids[gridName[i]] = AmiraMeshReader<GridType>::read(path + gridConfig.get<string>("gridFile"),
                                                           PSurfaceBoundary<dim-1>::read(path + gridConfig.get<string>("parFile")));
#endif
    } else if (gridConfig.hasKey(("amira_gridFile"))) {
#if HAVE_AMIRAMESH
      grids[gridName[i]] = AmiraMeshReader<GridType>::read(path + gridConfig.get<string>("gridFile"));
#endif
    } else if (gridConfig.hasKey("createGrid") and gridConfig.get<bool>("createGrid")==true)
      grids[gridName[i]] = GridConstruction<GridType,dim>::createGrid(gridConfig);
    else
      DUNE_THROW(Exception, "You either have to provide a gridFile or set the Parameter createGrid to use a factory");
  }

  // ////////////////////////////////////////////////////////////////////
  //   Initial uniform refinement, if requested
  // ////////////////////////////////////////////////////////////////////

  const int minLevel = config.get<int>("minLevel");
  for (size_t j=0; j<nGrids; j++) {

    grids[gridName[j]]->setRefinementType(GridType::COPY);

    const ParameterTree& gridConfig = config.sub(gridName[j]);
    // allow different refinement levels for each grid
    int nRefine = gridConfig.get("nRefines",minLevel);

    for (int i=0; i<nRefine; i++)
      grids[gridName[j]]->globalRefine(1);
    printAspectRatios(grids[gridName[j]]->leafGridView());
  }

  // create the deformed grids
  using DeformedGridComplex =  DeformedContinuaComplex<GridType,VectorType>;
  using DeformedGridType = DeformedGridComplex::DeformedGridType;
  DeformedContinuaComplex<GridType,VectorType> deformedGrids(grids);

  //////////////////////////////////////////////////////////////////////////
  //   Set up coarse Dirichlet boundary conditions
  //////////////////////////////////////////////////////////////////////////

  // displacements
  vector<VectorType> u(nGrids);

  for (size_t i=0; i<nGrids; i++) {
    u[i].resize(grids[gridName[i]]->size(dim));
    u[i] = 0;
  }

  //   Create contact assembler
  LargeDeformationContactAssembler<DeformedGridType, VectorType> contactAssembler(2,1);
  contactAssembler.setGrids(deformedGrids.gridVector());

  const ParameterTree& coupleConfig = config.sub("coupling");

  // the involved grids
  string nonMortarGrid = coupleConfig.get<string>("nonmortarGrid");
  string mortarGrid = coupleConfig.get<string>("mortarGrid");

  int nmIdx = (nonMortarGrid==gridName[0]) ? 0 : 1;
  int mIdx  = (mortarGrid==gridName[0]) ? 0 : 1;

  // Set mortar couplings.  Currently not more than one for each grid
  using DefLeafBoundaryPatch = BoundaryPatch<DeformedGridType::LeafGridView>;
  using DefLevelBoundaryPatch = BoundaryPatch<DeformedGridType::LevelGridView>;
  std::array<std::shared_ptr<DefLevelBoundaryPatch>, nGrids> contactPatches;

  // read field describing the nonmortar boundary, if nothing is found in the parameter file, take all
  contactPatches[0] = std::make_shared<BoundaryPatch<DeformedGridType::LevelGridView> >(deformedGrids.grid(nonMortarGrid).levelGridView(0));
  if (!BoundaryPatchFactory<DeformedGridType::LevelGridView>::setupBoundaryPatch(coupleConfig.sub(nonMortarGrid), *contactPatches[0]))
    contactPatches[0]->setup(deformedGrids.grid(nonMortarGrid).levelGridView(0), true);

  // read field describing the mortar boundary, if nothing is found in the parameter file, take all
  contactPatches[1] = std::make_shared<BoundaryPatch<DeformedGridType::LevelGridView> >(deformedGrids.grid(mortarGrid).levelGridView(0));
  if (!BoundaryPatchFactory<DeformedGridType::LevelGridView>::setupBoundaryPatch(coupleConfig.sub(mortarGrid), *contactPatches[1]))
    contactPatches[1]->setup(deformedGrids.grid(mortarGrid).levelGridView(0), true);

  // Set the coupling
  field_type overlap = coupleConfig.get<field_type>("overlap",0.3);

  using Projection = ClosestPointProjection<DefLeafBoundaryPatch>;
  auto projection = std::make_shared<Projection>(50, overlap);

  using ContactMerger = Dune::GridGlue::ContactMerge<dim,typename GridType::ctype>;
  auto merger = std::make_shared<ContactMerger>(overlap,ContactMerger::CLOSEST_POINT);
  CouplingPair<DeformedGridType, DeformedGridType, field_type> coupling;
  coupling.set(nmIdx, mIdx, contactPatches[0],contactPatches[1],
               coupleConfig.get<field_type>("couplingDistance"),
               CouplingPairBase::type(coupleConfig.get<string>("couplingType")),
               projection, merger);
  contactAssembler.setCoupling(coupling,0);

  // setup contact couplings
  contactAssembler.constructCouplings();

  // ///////////////////////////////////////////////////
  //   Do a homotopy of the Dirichlet boundary data
  // ///////////////////////////////////////////////////

  field_type loadingSteps = config.get<field_type>("loadingSteps");

  // fraction of elements that should be refined
  field_type refinementFraction DUNE_UNUSED;
  refinementFraction = config.get<field_type>("refinementFraction");

  const int adaptRefines = config.get<int>("adaptRefines");
  for (int level=0; level<=adaptRefines; level++) {

    // Get the overall top level
    int toplevel = 0;
    for (size_t i=0;i<nGrids;i++)
      toplevel = std::max(toplevel, grids[gridName[i]]->maxLevel());

    // total size
    vector<VectorType> extForces(nGrids);
    for (size_t i=0; i<nGrids; i++) {
      extForces[i].resize(u[i].size());
      extForces[i] = 0;
    }

    vector<VectorType> dirichletValues(nGrids);
    vector<BitSetVector<dim> > dirichletNodes(nGrids);

    for (size_t i=0; i<nGrids; i++) {
      DirichletBCAssembler<GridType>::assembleDirichletBC(*grids[gridName[i]],
          config.sub(gridName[i]).sub("dirichlet"), dirichletNodes[i], dirichletValues[i]);

      if (level==0) {
        // scale Dirichlet values if desired
        if (config.sub(gridName[i]).hasKey("dvScaling"))
          dirichletValues[i] *= config.sub(gridName[i]).get<field_type>("dvScaling");

        // scale with the number of loading steps
        for (size_t j=0; j<dirichletValues[i].size(); j++)
          dirichletValues[i][j] /= loadingSteps;
        // the loading steps are only applied on the coarsest level,
        // after the first refinement, the deformation already conatins
        // the Dirichlet values
      } else
        dirichletValues[i] = 0;
    }

    // Create the materials
    using P1Basis =  DuneFunctionsBasis<typename Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1> >;
    vector<std::shared_ptr<P1Basis> > p1Basis(nGrids);

    // using MaterialType = GeomExactStVenantMaterial<P1Basis>;
    using MaterialType =  NeoHookeanMaterial<P1Basis>;
    vector<MaterialType> materials(nGrids);
    for (size_t i=0; i < nGrids; ++i) {

      p1Basis[i] = std::make_shared<P1Basis>(grids[gridName[i]]->leafGridView());
      const ParameterTree& gridConfig = config.sub(gridName[i]);
      materials[i].setup(*p1Basis[i], gridConfig.get<field_type>("E"),gridConfig.get<field_type>("nu"));

    }

    //////////////////////////////////////////////////////////////
    //  Create the static contact problem
    /////////////////////////////////////////////////////////////

    // use the linearised elasticity stiffness matrix as norm for the errors
    vector<MatrixType> elast(nGrids);
    vector<std::shared_ptr<Norm<VectorType> > > errorNorms(nGrids);
    MatrixType bigElastMatrix;
    for (size_t i=0; i<nGrids; i++) {

      OperatorAssembler<P1Basis,P1Basis> globalAssembler(*p1Basis[i],*p1Basis[i]);
      const ParameterTree& gridConfig = config.sub(gridName[i]);
      StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(gridConfig.get<field_type>("E"),gridConfig.get<field_type>("nu"));

      globalAssembler.assemble(localAssembler, elast[i]);
      errorNorms[i] = std::make_shared<EnergyNorm<MatrixType, VectorType> >(elast[i]);
     }

    Dune::MatrixVector::BlockMatrixView<MatrixType>::setupBlockMatrix(elast,bigElastMatrix);
    EnergyNorm<MatrixType, VectorType> elastNorm(bigElastMatrix);

    // Make static contact problem
    using ProblemType = StaticMgContactProblem<VectorType, MaterialType>;
    ProblemType contactProblem(contactAssembler, materials, deformedGrids, extForces);

    // Create trust-region step
    FilterMultigridContactSolver<ProblemType> contactSolver(config.sub("filter"), u, contactProblem, errorNorms, elastNorm);

    ///////////////////////////////////////////////////////////////////////
    //    Solve contact problems with loading steps on the coarsest level
    ///////////////////////////////////////////////////////////////////////

    int counter = (level==0) ? loadingSteps : 1;
    for (int i=0; i<counter; i++) {

      if (level==0) {
        std::cout << "************************\n"
                  << "*  Loading step " << i << "\n"
                  << "************************\n";
      } else {
        std::cout << "************************\n"
                  << "*  Refinement step" << level << "\n"
                  << "************************\n";
      }
      // loading steps on the coarsest refinement level
      contactSolver.setDirichletValues(dirichletValues, dirichletNodes);

      std::cout<<"Solving contact problem!\n";
      Dune::Timer time;
      contactSolver.solve();
      std::cout<<"Took "<<time.elapsed()<<" to solve! \n";

      // Output result
      for (size_t j=0; j<nGrids; j++) {

        std::ostringstream name;
        name<< resultPath << "/" << gridName[j];
        if (level==0)
          name << "loading." << std::setw(2) << std::setfill('0') << i;
        else
          name << "ref." << std::setw(2) << std::setfill('0') <<level;

        if (config.get<string>("writer")=="amira") {
#if HAVE_AMIRAMESH
          LeafAmiraMeshWriter<GridType> amiramesh;
          amiramesh.addLeafGrid(*grids[gridName[j]],true);
          amiramesh.addVertexData(u[j], grids[gridName[j]]->leafGridView());
          amiramesh.write(name.str(),1);
#endif
        } else if (config.get<string>("writer") == "vtk") {

          VTKWriter<GridType::LeafGridView> vtkWriter(grids[gridName[j]]->leafGridView());
          auto displacement = std::make_shared<VTKBasisGridFunction<P1Basis,VectorType> >(*p1Basis[j], u[j], "Displacement");
          vtkWriter.addVertexData(displacement);
          vtkWriter.write(name.str());
        }
      }

    }

    // adaptive refinement is not properly working yet
    break;
#if 0
    // /////////////////////////////////////////////////////////////////////
    //   Refinement Loop
    // /////////////////////////////////////////////////////////////////////

    if (level==adaptRefines)
      break;

    std::cout<<"Warning: Adaptive refinement does not yet work satisfactory!\n";

    // ////////////////////////////////////////////////////////////////////////////
    //    Refine locally and transfer the current solution to the new leaf level
    // ////////////////////////////////////////////////////////////////////////////

    std::cout<<"Estimating error on  refinement level "<<level<<std::endl;
    // make contact problem for P2 functions

    // Create the materials
    //using P2Basis =  DuneFunctionsBasis<typename Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 2> >;
    using P2Basis =  P2NodalBasis<typename GridType::LeafGridView,field_type>;
    vector<std::shared_ptr<P2Basis> > p2Bases(nGrids);
    for (size_t j=0; j<nGrids; j++)
      p2Bases[j] = std::make_shared<P2Basis>(grids[gridName[j]]->leafGridView());

    using MaterialTypeP2 = NeoHookeanMaterial<P2Basis>;
    //using MaterialTypeP2 = GeomExactStVenantMaterial<P2Basis>;
    vector<MaterialTypeP2> p2materials(nGrids);
    for (int i=0; i<nGrids;i++) {
      const ParameterTree& gridConfig = config.sub(gridName[i]);
      p2materials[i].setup(gridConfig.get<field_type>("E"), gridConfig.get<field_type>("nu"), *p2Bases[i]);
    }
    // Make static contact problem
    using ContactProblemP2 = StaticContactProblem<VectorType,MatrixType, MaterialTypeP2>;
    auto p2contactProblem = std::make_shared<ContactProblemP2>(contactAssembler, p2materials, extForces,mortarConstraints);

    vector<std::shared_ptr<RefinementIndicator<GridType> > > refinementIndicator(nGrids);
    for (int i=0; i<nGrids; i++)
      refinementIndicator[i] = std::make_shared<RefinementIndicator<GridType> >(*grids[gridName[i]]);

    vector<std::shared_ptr<BasisGridFunction<P1Basis, VectorType> > > volumeTerm(nGrids),neumannTerm(nGrids);

    vector<std::shared_ptr<BoundaryPatch<GridType::LeafGridView> > > leafDirichletBoundary(nGrids), neumannBoundary(nGrids);
    for (int i=0; i<nGrids; i++)
      leafDirichletBoundary[i] = std::make_shared<BoundaryPatch<GridType::LeafGridView> >
          (grids[gridName[i]]->leafGridView(),dirichletNodes[i]);

    vector<GridType*> adaptiveGridVector(nGrids);
    for (int i=0; i<nGrids; i++)
      adaptiveGridVector[i] = grids[gridName[i]];

    auto uFunc0 = ::Functions::makeFunction(*p1Basis[0],u[0]);
    auto uFunc1 = ::Functions::makeFunction(*p1Basis[1],u[1]);
    std::vector<std::shared_ptr<BasisGridFunction<P1Basis,VectorType> > > uFunc =
    {stackobject_to_shared_ptr(uFunc0), stackobject_to_shared_ptr(uFunc1)};

    // setup undeformed patches
    using LevelBoundaryPatch = BoundaryPatch<GridType::LevelGridView>;
    auto nmPatch = std::make_shared<LevelBoundaryPatch>(grids[nonMortarGrid]->levelGridView(0),
                                                        *contactPatches[0]->getVertices());

    auto mPatch = std::make_shared<LevelBoundaryPatch>(grids[mortarGrid]->levelGridView(0),
                                                       *contactPatches[1]->getVertices());

    // setup estimator
    HierarchicContactEstimator<P1Basis, P2Basis> estimator(adaptiveGridVector,1);
    estimator.setupContactCoupling(0,nmIdx, mIdx,
                                   nmPatch,mPatch,
                                   coupleConfig.get<typename GridType::ctype>("couplingDistance"),
                                   CouplingPairBase::type(coupleConfig.get<string>("couplingType")));

    estimator.template estimate<ContactProblemP2>(uFunc, volumeTerm, neumannTerm, leafDirichletBoundary, neumannBoundary,
                                                  refinementIndicator, p2Bases, p2contactProblem);

    // ////////////////////////////////////////////////////
    //   Refine grids
    // ////////////////////////////////////////////////////

    std::cout<<"Mark elements"<<std::endl;
    FractionalMarkingStrategy<GridType>::mark(refinementIndicator, adaptiveGridVector, refinementFraction);

    for (int i=0; i<nGrids; i++) {

      GridFunctionAdaptor<P1Basis> adaptor(*p1Basis[i],true,true);

      grids[gridName[i]]->preAdapt();
      grids[gridName[i]]->adapt();
      grids[gridName[i]]->postAdapt();

      p1Basis[i]->update();
      adaptor.adapt(u[i]);

      if (config.get<string>("writer")=="Amira") {
#if HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType> amiramesh;
        amiramesh.addLeafGrid(*grids[gridName[i]],true);
        amiramesh.addVertexData(u[i], grids[gridName[i]]->leafGridView());
        amiramesh.write(resultPath + gridName[i]+".afterRefine.grid",1);
#endif
      }

      deformedGrids.deformation(i)->setGridView(grids[gridName[i]]->leafGridView());
      deformedGrids.grid(gridName[i]).update();
      deformedGrids.setDeformation(u[i],gridName[i]);

      toplevel = std::max(toplevel, grids[gridName[i]]->maxLevel());
    }

    std::cout << "########################################################" << std::endl;
    std::cout << "  Grids refined" << std::endl;
    for (int i=0; i<nGrids; i++) {
      std::cout << "  Grid: "<<gridName[i]<<" max level: " << grids[gridName[i]]->maxLevel()
                << "   vertices: " << grids[gridName[i]]->size(grids[gridName[i]]->maxLevel(), dim)
          << "   elements: " << grids[gridName[i]]->size(grids[gridName[i]]->maxLevel(), 0)<<std::endl;
    }
    std::cout << "      Solving on toplevel: " << toplevel << std::endl;
    std::cout << "####################################################" << std::endl;
#endif
  }

} catch (Exception e) {

  std::cout << e << std::endl;

}




