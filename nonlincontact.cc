#include <config.h>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/geometrygrid/grid.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>

#include <dune/istl/io.hh>

#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functions/deformationfunction.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>

#include <dune/solvers/iterationsteps/mmgstep.hh>

#include <dune/elasticity/assemblers/ogdenassembler.hh>
//#include <../dune-elasticity/dune-elasticity/neohookeassembler.hh">

#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif
#include <dune/solvers/iterationsteps/trustregiongsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/energynorm.hh>

#include <dune/contact/assemblers/twobodyassembler.hh>
#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/solvers/contactobsrestrict.hh>
#include <dune/contact/solvers/contacttransfer.hh>

// The grid dimension
const int dim = 3;

typedef double field_type;

const double E  = 1e3;
const double nu = 0.3;
const double d  = 0;

using namespace Dune;
using namespace Dune::Contact;
using std::string;

void setTrustRegionObstacles(double trustRegionRadius,
                             std::vector<BoxConstraint<field_type,dim> >& trustRegionObstacles,
                             const std::vector<BoxConstraint<field_type,dim> >& trueObstacles,
                             const BitSetVector<dim>& totalDirichletNodes)
{
    for (int j=0; j<trustRegionObstacles.size(); j++) {

        for (int k=0; k<dim; k++) {

            if (totalDirichletNodes[j][k])
                continue;

            if (j >= trueObstacles.size()) {

                trustRegionObstacles[j].lower(k) = -trustRegionRadius;
                trustRegionObstacles[j].upper(k) =  trustRegionRadius;

            } else {

                trustRegionObstacles[j].lower(k) = std::max(-trustRegionRadius, trueObstacles[j].lower(k));
                trustRegionObstacles[j].upper(k) = std::min( trustRegionRadius, trueObstacles[j].upper(k));

            }

            // These cases may happen when we start from an infeasable configuration
            if (trustRegionObstacles[j].upper(k) < 0
                && trustRegionObstacles[j].upper(k) < trustRegionObstacles[j].lower(k))
                trustRegionObstacles[j].lower(k) = trustRegionObstacles[j].upper(k);

            if (trustRegionObstacles[j].lower(k) > 0
                && trustRegionObstacles[j].upper(k) < trustRegionObstacles[j].lower(k))
                trustRegionObstacles[j].upper(k) = trustRegionObstacles[j].lower(k);

        }

    }

    //std::cout << trustRegionObstacles << std::endl;
//     exit(0);
}

template <class GridType, class MatrixType, class VectorType,class TrialBasis, class AnsatzBasis>
void solveTrustRegionProblem(array<OgdenAssembler<TrialBasis,AnsatzBasis>*,2>& ogdenAssembler,
                             TwoBodyAssembler<GridType, VectorType>& contactAssembler,
                             array<VectorType,2>& x,
                             std::vector<VectorType>& rhs,
                             int level,
                             double trustRegionRadius,
                             std::vector<std::vector<BoxConstraint<field_type,dim> > >& trustRegionObstacles,
                             ::LoopSolver<VectorType>& solver,
                             MonotoneMGStep<MatrixType, VectorType>& multigridStep,
                             int maxTrustRegionSteps)
{

    for (int i=0; i<maxTrustRegionSteps; i++) {

        double oldEnergy = ogdenAssembler[0]->computeEnergy(x[0])
            + ogdenAssembler[1]->computeEnergy(x[1]);


        std::cout << "------------------------------------------------------" << std::endl;
        std::cout << "      Trust-Region Step Number: " << i << ",   Energy: " << oldEnergy << std::endl;
        std::cout << "------------------------------------------------------" << std::endl;

        rhs[0] = 0;
        rhs[1] = 0;
        std::vector<const MatrixType*> globalMatrix(2);
        for (int j=0; j<2; j++) {
            globalMatrix[j] = new MatrixType;
            ogdenAssembler[j]->assembleProblem(*const_cast<MatrixType*>(globalMatrix[j]), x[j], rhs[j]);
        }

        // Debug
#if 0
        array<VectorType,2> gradients = rhs;
        gradients[0] *= -1;
        gradients[1] *= -1;
        gradientFDCheck(x[0], gradients[0], ogdenAssembler[0]);
        gradientFDCheck(x[1], gradients[1], ogdenAssembler[1]);

        hessianFDCheck(x[0], *const_cast<MatrixType*>(ogdenAssembler[0].getMatrix()), ogdenAssembler[0]);
        hessianFDCheck(x[1], *const_cast<MatrixType*>(ogdenAssembler[1].getMatrix()), ogdenAssembler[1]);
#endif
        MatrixType bilinearForm;
        contactAssembler.assembleJacobian(globalMatrix, bilinearForm);

        //printSparseMatrix(std::cout, contactAssembler.getTransformationMatrix(), "BT", "--");

        VectorType totalRhs(rhs[0].size() + rhs[1].size());
        contactAssembler.assembleRightHandSide(rhs,totalRhs);

        //std::cout << "totalRhs" << std::endl << totalRhs << std::endl;

        // The correction for both grids together in local coordinates
        VectorType totalCorr(x[0].size() + x[1].size());

        // The correction in canonical coordinates and for each grid separately
        array<VectorType, 2> corr;

        // ///////////////////////////////////////////////////////////////////////////////////////
        //   Loop and decrease the trust-region radius until we get objective function reduction
        // ///////////////////////////////////////////////////////////////////////////////////////
        do {

            std::cout << "### Trust-Region Radius: " << trustRegionRadius << " ###" << std::endl;

            totalCorr = 0;

            // Create trust-region obstacle on grid0.maxLevel()
            setTrustRegionObstacles(trustRegionRadius,
                                    trustRegionObstacles[level],
                                    contactAssembler.obstacles_[level],
                                    *multigridStep.ignoreNodes_);

            if (dynamic_cast<MultigridStep<MatrixType,VectorType>*>(solver.iterationStep_))
                dynamic_cast<MultigridStep<MatrixType,VectorType>*>(solver.iterationStep_)->setProblem(bilinearForm, totalCorr, totalRhs, level+1);
            else {
                DUNE_THROW(NotImplemented, "You need a multigrid step!");
                //solver.iterationStep_->setProblem(*contactAssembler.mat_, totalCorr, totalRhs);
            }

            solver.preprocess();

            // /////////////////////////////
            //    Solve !
            // /////////////////////////////
            solver.solve();

            totalCorr = multigridStep.getSol();

            //std::cout << "totalCorr: \n" << totalCorr << std::endl;

            contactAssembler.postprocess(totalCorr, corr);

//             std::cout << "corr[0]: \n" << corr[0] << std::endl;
//             std::cout << "corr[1]: \n" << corr[1] << std::endl;

            // ////////////////////////////////////////////////////
            //   Check whether trust-region step can be accepted
            // ////////////////////////////////////////////////////

            printf("infinity norm of the correction: %g %g\n",
                   corr[0].infinity_norm(), corr[1].infinity_norm());

            if (corr[0].infinity_norm() < 1e-4 && corr[1].infinity_norm() < 1e-4)
                return;

            /** \todo Faster with expression templates */
            VectorType newIterate0 = x[0];  newIterate0 += corr[0];
            VectorType newIterate1 = x[1];  newIterate1 += corr[1];

            double energy = ogdenAssembler[0]->computeEnergy(newIterate0)
                + ogdenAssembler[1]->computeEnergy(newIterate1);

            // //////////////////////////////////////////////////////
            //   Compute the model decrease
            //   It is $ m(x) - m(x+s) = -<g,s> - 0.5 <s, Hs>
            //   Note that rhs = -g
            // //////////////////////////////////////////////////////
            double modelDecrease = 0;
            for (size_t j=0; j<2; j++) {
                VectorType tmp(corr[j].size());
                tmp = 0;
                globalMatrix[j]->umv(corr[j], tmp);
                modelDecrease += (rhs[j]*corr[j]) - 0.5 * (corr[j]*tmp);
            }

            std::cout << "Model decrease: " << modelDecrease
                      << ",  functional decrease: " << oldEnergy - energy << std::endl;

            if (modelDecrease < 0)
                DUNE_THROW(SolverError, "Model energy is not decreased!");

            if (energy >= oldEnergy)
                DUNE_THROW(SolverError, "Richtung ist keine Abstiegsrichtung!");

            // //////////////////////////////////////////////
            //   Check for acceptance of the step
            // //////////////////////////////////////////////
            if ( (oldEnergy-energy) / modelDecrease > 0.9) {
                // very successful iteration

                //x = newIterate;
                trustRegionRadius *= 2;
                break;

            } else if ( (oldEnergy-energy) / modelDecrease > 0.01
                        || std::abs(oldEnergy-energy) < 1e-12) {
                // successful iteration
                //x = newIterate;
                break;

            } else {
                // unsuccessful iteration
                trustRegionRadius /= 2;
                std::cout << "Unsuccessful iteration!" << std::endl;
            }

            //  Write current energy
            std::cout << "--- Current energy: " << energy << " ---" << std::endl;

        } while (true);

        //  Add correction to the current solution
        x[0] += corr[0];
        x[1] += corr[1];

        // Subtract correction from the current obstacle
#warning Obstacle should be reassembled at each iteration!
        for (int k=0; k<corr[0].size(); k++)
            contactAssembler.obstacles_[level][k] -= totalCorr[k];

    }

}

void contactDirection(const double* pos, double* dir) {
    dir[0] = 0;
    dir[1] = -1;
    //dir[2] = 0;
}

#ifdef THREEDIM
bool refineCondition(const FieldVector<double,dim>& pos) {
    return pos[2] > -2 && pos[2] < -0.5;
}
#else
bool refineCondition(const FieldVector<double,dim>& pos) {
    return pos[1] > -25 && pos[1] < 25;
}
#endif

bool refineAll(const FieldVector<double,dim>& pos) {
    return true;
}


int main (int argc, char *argv[]) try
{

    // Some types that I need
    typedef BCRSMatrix<FieldMatrix<double, dim, dim> > MatrixType;
    typedef BlockVector<FieldVector<double, dim> >     VectorType;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("nonlincontact.parset", parameterSet);

    // read solver settings
    const int minLevel         = parameterSet.get("minLevel", int(0));
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int maxNewtonStepsI  = parameterSet.get("maxNewtonStepsI", int(0));
    const int maxNewtonStepsII = parameterSet.get("maxNewtonStepsII", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const int nu1              = parameterSet.get("nu1", int(0));
    const int nu2              = parameterSet.get("nu2", int(0));
    const int mu               = parameterSet.get("mu", int(0));
    const int baseIt           = parameterSet.get("baseIt", int(0));
    const double tolerance     = parameterSet.get("tolerance", double(0));
    const double baseTolerance = parameterSet.get("baseTolerance", double(0));
    const double refinementFraction = parameterSet.get("refinementFraction", double(1));

    // read problem settings
    string path        = parameterSet.get("path", "xyz");
    string objectName0 = parameterSet.get("gridFile0", "xyz");
    string objectName1 = parameterSet.get("gridFile1", "xyz");
    string dnFile0     = parameterSet.get("dnFile0", "xyz");
    string dnFile1     = parameterSet.get("dnFile1", "xyz");
    string dvFile0     = parameterSet.get("dvFile0", "xyz");
    string dvFile1     = parameterSet.get("dvFile1", "xyz");
    string obsFilename = parameterSet.get("obsFilename", "xyz");
    double obsDistance = parameterSet.get("obsDistance", double(0));
    double scaling     = parameterSet.get("scaling", double(1));
    double trustRegionRadius = parameterSet.get("trustRegionRadius", double(1));

    string interSol0   = parameterSet.get("intersol0", "notFound");
    string interSol1   = parameterSet.get("intersol1", "notFound");

    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    typedef UGGrid<dim> GridType;
    typedef BoundaryPatch<GridType::LevelGridView> LevelBoundaryPatch;
    typedef BoundaryPatch<GridType::LeafGridView> LeafBoundaryPatch;

    typedef DeformationFunction<GridType::LeafGridView,VectorType> Deformation;
    typedef GeometryGrid<GridType,Deformation> DefGridType;
    typedef P1NodalBasis<GridType::LeafGridView, field_type> P1Basis;

    typedef BoundaryPatch<DefGridType::LevelGridView> DefLevelBoundaryPatch;

    array<GridType,2> grid;

    grid[0].setRefinementType(GridType::COPY);
    grid[1].setRefinementType(GridType::COPY);

    AmiraMeshReader<GridType>::read(grid[0], path + objectName0);
    AmiraMeshReader<GridType>::read(grid[1], path + objectName1);

    array<std::vector<VectorType>, 2> dirichletValues;
    dirichletValues[0].resize(1);
    dirichletValues[1].resize(1);
    dirichletValues[0][0].resize(grid[0].size(0, dim));
    dirichletValues[1][0].resize(grid[1].size(0, dim));
    AmiraMeshReader<GridType>::readFunction(dirichletValues[0][0], path + dvFile0);
    AmiraMeshReader<GridType>::readFunction(dirichletValues[1][0], path + dvFile1);

    // Scale Dirichlet values
    dirichletValues[0][0] *= scaling;
    dirichletValues[1][0] *= scaling;

    array<VectorType,2> coarseDirichletValues;
    coarseDirichletValues[0] = dirichletValues[0][0];
    coarseDirichletValues[1] = dirichletValues[1][0];

    array<LevelBoundaryPatch, 2> coarseDirichletBoundary;
    coarseDirichletBoundary[0].setup(grid[0].levelGridView(0));
    coarseDirichletBoundary[1].setup(grid[1].levelGridView(0));
    readBoundaryPatch<GridType>(coarseDirichletBoundary[0], path + dnFile0);
    readBoundaryPatch<GridType>(coarseDirichletBoundary[1], path + dnFile1);

    array<std::vector<LevelBoundaryPatch>, 2> dirichletBoundary;
    dirichletBoundary[0].resize(maxLevel+1);
    dirichletBoundary[1].resize(maxLevel+1);
    dirichletBoundary[0][0] = coarseDirichletBoundary[0];
    dirichletBoundary[1][0] = coarseDirichletBoundary[1];

    // //////////////////////////////////////////////////////////
    //   Prolong Dirichlet information to all grid levels
    // //////////////////////////////////////////////////////////
    array<std::vector<BitSetVector<dim> >, 2> dirichletNodes;
    dirichletNodes[0].resize(maxLevel+1);
    dirichletNodes[1].resize(maxLevel+1);


    int fSSize0 = grid[0].size(0,dim);
    dirichletNodes[0][0].resize(fSSize0);
    for (int j=0; j<fSSize0; j++)
        dirichletNodes[0][0][j] = dirichletBoundary[0][0].containsVertex(j);

    int fSSize1 = grid[1].size(0,dim);
    dirichletNodes[1][0].resize(fSSize1);
    for (int j=0; j<fSSize1; j++)
        dirichletNodes[1][0][j] = dirichletBoundary[1][0].containsVertex(j);

    // ////////////////////////////////////////////////////////////
    //    Create solution and rhs vectors
    // ////////////////////////////////////////////////////////////

    std::vector<VectorType> rhs(2);
    array<VectorType, 2> x;

    rhs[0].resize(grid[0].size(grid[0].maxLevel(), dim));
    rhs[1].resize(grid[1].size(grid[1].maxLevel(), dim));
    x[0].resize(grid[0].size(grid[0].maxLevel(), dim));
    x[1].resize(grid[1].size(grid[1].maxLevel(), dim));

    // Initial solution
    if (interSol0=="notFound")
        x[0] = 0;
    else
        AmiraMeshReader<GridType>::readFunction(x[0], interSol0);

    if (interSol1=="notFound")
        x[1] = 0;
    else
        AmiraMeshReader<GridType>::readFunction(x[1], interSol1);

    // create deformed grids
    array<Deformation*,2> deformation;
    array<DefGridType*,2> defGrid;
    for (int i=0;i<2;i++) {
        deformation[i]=new Deformation(grid[i].leafGridView(),x[i]);
        defGrid[i]= new DefGridType(&grid[i],deformation[i]);
    }

    // read field describing the obstacles and the mortar boundary
    BitSetVector<1> obsField;
    readBitField(obsField, grid[0].size(0, dim), path + obsFilename);
    LevelBoundaryPatch untransfObsPatch(grid[0].levelGridView(0), obsField);
    DefLevelBoundaryPatch obsPatch(defGrid[0]->levelGridView(0), obsField);
    printf("obsPatch contains %d faces\n", obsPatch.numFaces());

    DefLevelBoundaryPatch mortarPatch(defGrid[1]->levelGridView(0),true);
    if (parameterSet.hasKey("mortarPatch"))
        readBoundaryPatch<DefGridType>(mortarPatch, path + parameterSet.get<string>("mortarPatch"));

    // make dirichlet bitfields containing dirichlet information for both grids
    int size = grid[0].size(dim)+grid[1].size(dim);
    BitSetVector<dim> totalDirichletNodes(size);

    int idx = 0;
    for (int j=0; j<dirichletNodes[0][0].size(); j++)
        totalDirichletNodes[idx++] = dirichletNodes[0][0][j];

    for (int j=0; j<dirichletNodes[1][0].size(); j++)
        totalDirichletNodes[idx++]  = dirichletNodes[1][0][j];

    // Assemble contact problem
    TwoBodyAssembler<DefGridType, VectorType> contactAssembler(*defGrid[0], *defGrid[1],
                                                                obsPatch, mortarPatch,
                                                                obsDistance, CouplingPairBase::CONTACT);

    // //////////////////////////////////////////////////////
    //    Lengthen obstacle vector, to formally account for both
    //    bodies, even though the second one doesn't actually have obstacles
    // //////////////////////////////////////////////////////

    std::vector<BitSetVector<1> > hasObstacle(1);
    hasObstacle[0].resize(size, false);

    std::vector<std::vector<BoxConstraint<field_type,dim> > > trustRegionObstacles(1);
    trustRegionObstacles[0].resize(size);

    // //////////////////////////////////////
    //   Create a solver
    // //////////////////////////////////////

    // First create a base solver
#ifdef HAVE_IPOPT
    QuadraticIPOptSolver<MatrixType,VectorType> baseSolver;
#endif
    baseSolver.verbosity_ = Solver::QUIET;
    baseSolver.tolerance_ = baseTolerance;

    // Make pre and postsmoothers
    TrustRegionGSStep<MatrixType, VectorType> presmoother, postsmoother;


    MonotoneMGStep<MatrixType, VectorType> multigridStep(1);

    multigridStep.setMGType(mu, nu1, nu2);
    multigridStep.ignoreNodes_       = &totalDirichletNodes;
    multigridStep.basesolver_        = &baseSolver;
    multigridStep.setSmoother(&presmoother, &postsmoother);
    multigridStep.hasObstacle_       = &hasObstacle;
    multigridStep.obstacles_         = &trustRegionObstacles;
    multigridStep.obstacleRestrictor_ = new ContactObsRestriction<VectorType>;

    // Create the transfer operators
    multigridStep.mgTransfer_.resize(grid[0].maxLevel());
    for (int i=0; i<multigridStep.mgTransfer_.size(); i++)
        multigridStep.mgTransfer_[i] = NULL;

    EnergyNorm<MatrixType, VectorType> energyNorm;

    ::LoopSolver<VectorType> solver(&multigridStep,
                                                   1,   // exact coarse grid solver
                                                   tolerance,
                                                   &energyNorm,
                                                   Solver::FULL);

    // ///////////////////////////////////////////////////
    //   Do a homotopy of the Dirichlet boundary data
    // ///////////////////////////////////////////////////
    typedef OgdenAssembler<P1Basis,P1Basis> Assembler;
    //typedef NeoHookeAssembler<P1Basis,P1Basis, 3> Assembler;

    array<Assembler*,2> ogdenAssembler;

    P1Basis p1BasisO(grid[0].leafGridView());
    ogdenAssembler[0]=new Assembler(p1BasisO,p1BasisO);

    P1Basis p1BasisU(grid[1].leafGridView());
    ogdenAssembler[1]=new Assembler(p1BasisU,p1BasisU);

    for (int i=0; i<2; i++) {
          ogdenAssembler[i]->setEandNu(E, nu);
          ogdenAssembler[i]->d_ = d;
    }

    double loadFactor    = 0;
    double loadIncrement = 1;
#if 1
    do {

        if (loadFactor < 1) {

            // ////////////////////////////////////////////////////
            //   Compute new feasible load increment
            // ////////////////////////////////////////////////////
            loadIncrement *= 2;  // double it once, cause we're going to half it at least once, too!
            do {

                loadIncrement /= 2;

                // Set new Dirichlet values in solution
                for (int j=0; j<2; j++)
                    for (int k=0; k<x[j].size(); k++)
                        for (int l=0; l<dim; l++)
                            if (dirichletNodes[j][grid[0].maxLevel()][k][l])
                                x[j][k][l] = dirichletValues[j][grid[0].maxLevel()][k][l] * (loadFactor + loadIncrement);

            } while (isnan(ogdenAssembler[0]->computeEnergy(x[0])) ||
                     isnan(ogdenAssembler[1]->computeEnergy(x[1])));

            loadFactor += loadIncrement;

            std::cout << "####################################################" << std::endl;
            std::cout << "New load factor: " << loadFactor
                      << "    new load increment: " << loadIncrement << std::endl;
            std::cout << "####################################################" << std::endl;

        }

        // /////////////////////////////////////////////////////
        //   Assemble the obstacle and the transfer operator
        // /////////////////////////////////////////////////////

        //update the deformed grids!
        for (int j=0;j<2;j++)
            deformation[j]->setDeformation(x[j]);

        contactAssembler.assembleObstacle();
        contactAssembler.assembleTransferOperator();

        /** \todo We don't really need this, because we have only one level anyways */

        // //////////////////////////////////////////////////////////////////////
        //   The MG transfer operators involve the mortar coupling
        //   coupling operator and have to be reassembled at each loading step
        // //////////////////////////////////////////////////////////////////////
        for (int k=0; k<multigridStep.mgTransfer_.size(); k++) {

            ContactMGTransfer<VectorType>* newTransferOp = new ContactMGTransfer<VectorType>;
            newTransferOp->setup(grid[0], grid[1], k, k+1,
                                 contactAssembler.contactCoupling_[0].mortarLagrangeMatrix(k),
                                 contactAssembler.localCoordSystems_[k],
                                 contactAssembler.localCoordSystems_[k+1],
                                 hasObstacle[k],  // BUG!!
                                 hasObstacle[k+1]);

            if (multigridStep.mgTransfer_[k])
                delete(multigridStep.mgTransfer_[k]);
            multigridStep.mgTransfer_[k] = newTransferOp;

        }

        // //////////////////////////////////////////////////////////
        //   Assemble linear elasticity matrix for the energy norm
        //   used by the MMG termination criterion
        // //////////////////////////////////////////////////////////
        // Assemble separate linear elasticity problems
        P1Basis p1Basis0(grid[0].leafGridView());
        P1Basis p1Basis1(grid[1].leafGridView());

        OperatorAssembler<P1Basis,P1Basis> globalAssembler0(p1Basis0,p1Basis0);
        OperatorAssembler<P1Basis,P1Basis> globalAssembler1(p1Basis1,p1Basis1);

        StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(2.5e5, 0.3);

        MatrixType stiffnessMatrix0, stiffnessMatrix1;
        globalAssembler0.assemble(localAssembler, stiffnessMatrix0);
        globalAssembler1.assemble(localAssembler, stiffnessMatrix1);

        array<const MatrixType*, 2> submat;
        submat[0] = &stiffnessMatrix0;
        submat[1] = &stiffnessMatrix1;

        MatrixType transformedLinearMatrix;
        contactAssembler.assemble(submat, transformedLinearMatrix);

        energyNorm.setMatrix(&transformedLinearMatrix);

        // /////////////////////////////////////////////////////
        //   Trust-Region Solver
        // /////////////////////////////////////////////////////
        solveTrustRegionProblem<DefGridType, MatrixType, VectorType, P1Basis, P1Basis>(ogdenAssembler,
                                                                  contactAssembler,
                                                                  x, rhs, grid[0].maxLevel(),
                                                                  trustRegionRadius,
                                                                  trustRegionObstacles,
                                                                  solver,
                                                                  multigridStep,
                                                                  maxNewtonStepsI);

        // Output result
        LeafAmiraMeshWriter<GridType> amiramesh0;
        amiramesh0.addLeafGrid(grid[0],true);
        amiramesh0.addVertexData(x[0], grid[0].leafGridView());
        amiramesh0.write("0resultGrid");

        LeafAmiraMeshWriter<GridType> amiramesh1;
        amiramesh1.addLeafGrid(grid[1],true);
        amiramesh1.addVertexData(x[1], grid[1].leafGridView());
        amiramesh1.write("1resultGrid");

    } while (loadFactor < 1);
#endif

    // /////////////////////////////////////////////////////////////////////
    //   Refinement Loop
    // /////////////////////////////////////////////////////////////////////

    solver.maxIterations_ = numIt;

    for (int toplevel=1; toplevel<=maxLevel; toplevel++) {

        // ////////////////////////////////////////////////////////////////////////////
        //    Refine locally and transfer the current solution to the new leaf level
        // ////////////////////////////////////////////////////////////////////////////

//         GeometricEstimator<GridType> estimator;

//         estimator.estimate(grid[0], (toplevel<=minLevel) ? refineAll : refineCondition, x[0]);
//         estimator.estimate(grid[1], (toplevel<=minLevel) ? refineAll : refineCondition, x[1]);

        HierarchicContactEstimator<GridType> estimator(grid[0], grid[1]);

        estimator.couplings_[0].obsPatch_    = &untransfObsPatch;
        estimator.couplings_[0].obsDistance_ = obsDistance;
        estimator.couplings_[0].type_        = CouplingPairBase::CONTACT;

        array<LeafBoundaryPatch, 2> leafDirichletBoundary;
        for (int j=0; j<grid.size(); j++)
            BoundaryPatchProlongator<GridType>::prolong(coarseDirichletBoundary[j], leafDirichletBoundary[j]);

        OgdenMaterialLocalStiffness<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localOgdenStiffness(E, nu, d);

        std::vector<RefinementIndicator<GridType>*> refinementIndicator(2);
        refinementIndicator[0] = new RefinementIndicator<GridType>(grid[0]);
        refinementIndicator[1] = new RefinementIndicator<GridType>(grid[1]);

        estimator.estimate(x[0], x[1],
                           &leafDirichletBoundary[0],
                           &leafDirichletBoundary[1],
                           refinementIndicator,
                           &localOgdenStiffness, &localOgdenStiffness);

        // ////////////////////////////////////////////////////
        //   Refine grids
        // ////////////////////////////////////////////////////

        std::vector<GridType*> adaptiveGridVector(2);
        adaptiveGridVector[0] = &grid[0];
        adaptiveGridVector[1] = &grid[1];
        FractionalMarkingStrategy<GridType>::mark(refinementIndicator, adaptiveGridVector, refinementFraction);

        for (int i=0; i<2; i++) {

            P1Basis p1Basis(grid[i].leafGridView());
        	GridFunctionAdaptor<P1Basis> adaptor(p1Basis,true,true);

        	grid[i].preAdapt();
        	grid[i].adapt();
        	grid[i].postAdapt();

        	p1Basis.update();
        	adaptor.adapt(x[i]);

            deformation[i]->setDeformation(x[i]);
            deformation[i]->setGridView(grid[i].leafGridView());
        	defGrid[i]->update();
        }

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        std::cout << "  Grid: 0   Level: " << grid[0].maxLevel()
                  << "   vertices: " << grid[0].size(grid[0].maxLevel(), dim)
                  << "   elements: " << grid[0].size(grid[0].maxLevel(), 0) << std::endl;
        std::cout << "  Grid: 1   Level: " << grid[1].maxLevel()
                  << "   vertices: " << grid[1].size(grid[1].maxLevel(), dim)
                  << "   elements: " << grid[1].size(grid[1].maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

        std::cout << "####################################################" << std::endl;
        std::cout << "      Solving on level: " << toplevel << std::endl;
        std::cout << "####################################################" << std::endl;

        // does this keep the level 0 entries?
        dirichletBoundary[0].resize(toplevel+1);
        dirichletBoundary[1].resize(toplevel+1);
        dirichletBoundary[0][0] = coarseDirichletBoundary[0];
        dirichletBoundary[1][0] = coarseDirichletBoundary[1];


        dirichletNodes[0].resize(toplevel+1);
        dirichletNodes[1].resize(toplevel+1);

        BoundaryPatchProlongator<GridType>::prolong(dirichletBoundary[0]);
        BoundaryPatchProlongator<GridType>::prolong(dirichletBoundary[1]);

        for (int i=0; i<=toplevel; i++) {

            int fSSize0 = grid[0].size(i,dim);
            dirichletNodes[0][i].resize(fSSize0*dim);
            for (int j=0; j<fSSize0; j++)
                for (int k=0; k<dim; k++)
                    dirichletNodes[0][i][j*dim+k] = dirichletBoundary[0][i].containsVertex(j);

            int fSSize1 = grid[1].size(i,dim);
            dirichletNodes[1][i].resize(fSSize1 * dim);
            for (int j=0; j<fSSize1; j++)
                for (int k=0; k<dim; k++)
                    dirichletNodes[1][i][j*dim+k] = dirichletBoundary[1][i].containsVertex(j);

        }

        dirichletValues[0].resize(toplevel+1);
        dirichletValues[1].resize(toplevel+1);
        dirichletValues[0][0] = coarseDirichletValues[0];
        dirichletValues[1][0] = coarseDirichletValues[1];

        sampleOnBitField(grid[0], dirichletValues[0], dirichletNodes[0]);
        sampleOnBitField(grid[1], dirichletValues[1], dirichletNodes[1]);

        // ///////////////////////////////////////////////
        //   Create new rhs vectors
        // ///////////////////////////////////////////////

        rhs[0].resize(grid[0].size(toplevel,dim));
        rhs[1].resize(grid[1].size(toplevel,dim));

        // Set right hand side vectors
        for (int i=0; i<2; i++) {

            rhs[i] = 0;

            for (int j=0; j<rhs[i].size(); j++)
                for (int k=0; k<dim; k++) {
                    if (dirichletNodes[i][toplevel][j][k])
                        x[i][j][k] = dirichletValues[i][toplevel][j][k];

                }

        }

        // /////////////////////////////////////////////////////////////////////////////
        //   Make dirichlet bitfields containing dirichlet information for both grids
        // /////////////////////////////////////////////////////////////////////////////
        totalDirichletNodes.resize(toplevel+1);

        for (int i=0; i<=toplevel; i++) {

            int offset = dirichletValues[0][i].size();

            totalDirichletNodes.resize(dirichletNodes[0][i].size() + dirichletNodes[1][i].size());

            for (int j=0; j<dirichletNodes[0][i].size(); j++)
                totalDirichletNodes[j] = dirichletNodes[0][i][j];

            for (int j=0; j<dirichletNodes[1][i].size(); j++)
                totalDirichletNodes[offset + j]  = dirichletNodes[1][i][j];


        }

        trustRegionObstacles.resize(toplevel+1);
        for (int i=0; i<=toplevel; i++)
            trustRegionObstacles[i].resize(grid[0].size(i, dim) + grid[1].size(i, dim));


        // /////////////////////////////////////////////////////
        //   Assemble the obstacle and the transfer operator
        // /////////////////////////////////////////////////////

        //update the deformed grids!
        for (int j=0;j<2;j++)
            deformation[j]->setDeformation(x[j]);

        contactAssembler.assembleObstacle();
        contactAssembler.assembleTransferOperator();

         // //////////////////////////////////////////////////////
        //    Lengthen obstacle vector, to formally account for both
        //    bodies, even though the second one doesn't actually have obstacles
        // //////////////////////////////////////////////////////

        hasObstacle.resize(toplevel+1);
        for (int j=0; j<=toplevel; j++)
            hasObstacle[j].resize(grid[0].size(j, dim) + grid[1].size(j,dim), true);

        // //////////////////////////////////////////////////////////////////////
        //   The MG transfer operators involve the mortar coupling
        //   coupling operator and have to be reassembled at each loading step
        // //////////////////////////////////////////////////////////////////////
        for (int k=0; k<multigridStep.mgTransfer_.size(); k++)
            delete(multigridStep.mgTransfer_[k]);

        multigridStep.mgTransfer_.resize(toplevel);

        for (int k=0; k<multigridStep.mgTransfer_.size(); k++) {

            ContactMGTransfer<VectorType>* newTransferOp = new ContactMGTransfer<VectorType>;
            newTransferOp->setup(grid[0], grid[1], k, k+1,
                                 contactAssembler.contactCoupling_[0].mortarLagrangeMatrix(k),
                                 contactAssembler.localCoordSystems_[k],
                                 contactAssembler.localCoordSystems_[k+1],
                                 *contactAssembler.nonmortarBoundary_[0][k].getVertices(),
                                 *contactAssembler.nonmortarBoundary_[0][k+1].getVertices());

            multigridStep.mgTransfer_[k] = newTransferOp;


        }

        // //////////////////////////////////////////////////////////
        //   Assemble linear elasticity matrix for the energy norm
        //   used by the MMG termination criterion
        // //////////////////////////////////////////////////////////
        // Assemble separate linear elasticity problems
        P1Basis p1Basis0(grid[0].leafGridView());
        P1Basis p1Basis1(grid[1].leafGridView());

        OperatorAssembler<P1Basis,P1Basis> globalAssembler0(p1Basis0,p1Basis0);
        OperatorAssembler<P1Basis,P1Basis> globalAssembler1(p1Basis1,p1Basis1);

        StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(2.5e5, 0.3);

        MatrixType stiffnessMatrix0, stiffnessMatrix1;
        globalAssembler0.assemble(localAssembler, stiffnessMatrix0);
        globalAssembler1.assemble(localAssembler, stiffnessMatrix1);

        array<const MatrixType*, 2> submat;
        submat[0] = &stiffnessMatrix0;
        submat[1] = &stiffnessMatrix1;

        MatrixType transformedLinearMatrix;
        contactAssembler.assemble(submat, transformedLinearMatrix);

        energyNorm.setMatrix(&transformedLinearMatrix);

        // /////////////////////////////////////////////////////
        //   Trust-Region Solver
        // /////////////////////////////////////////////////////

        solveTrustRegionProblem<DefGridType, MatrixType, VectorType, P1Basis, P1Basis>(ogdenAssembler,
                                                                  contactAssembler,
                                                                  x, rhs, grid[0].maxLevel(),
                                                                  trustRegionRadius,
                                                                  trustRegionObstacles,
                                                                  solver,
                                                                  multigridStep,
                                                                  maxNewtonStepsII);


        // ///////////////////////
        //   Output result
        // ///////////////////////
        LeafAmiraMeshWriter<GridType> amiramesh0;
        amiramesh0.addLeafGrid(grid[0],true);
        amiramesh0.addVertexData(x[0], grid[0].leafGridView());
        amiramesh0.write("0resultGrid");

        LeafAmiraMeshWriter<GridType> amiramesh1;
        amiramesh1.addLeafGrid(grid[1],true);
        amiramesh1.addVertexData(x[1], grid[1].leafGridView());
        amiramesh1.write("1resultGrid");

    }

 } catch (Exception e) {

    std::cout << e << std::endl;

 }
