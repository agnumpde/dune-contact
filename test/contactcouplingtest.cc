// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=4 sw=4 et sts=4:
#include <config.h>

#include <iostream>

#include <dune/common/parametertree.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/fufem/utilities/boundarypatchfactory.hh>
#include <dune/fufem/utilities/gridconstruction.hh>
#include <dune/contact/assemblers/dualmortarcouplinghierarchy.hh>

using namespace Dune;
using namespace Dune::Contact;

class ContactCouplingTest
{
public:

    template <class GridType>
    bool testForRowSumOne(std::shared_ptr<Dune::Contact::DualMortarCoupling<double,GridType> > coupling);

    template <class GridType>
    bool rowSumOneTest(const std::string& problemName,
                       const std::vector<std::shared_ptr<GridType> >& grids,
                       const std::vector<BoundaryPatch<typename GridType::LevelGridView> >& contactPatches,
                       bool fullHierarchyCoupling);

    bool rowSumOneForHexaCubesTest();
    bool rowSumOneForIroningTest();
    // TODO Hierarchy coupling test

};


template <class GridType>
bool ContactCouplingTest::testForRowSumOne(std::shared_ptr<DualMortarCoupling<double, GridType> > coupling)
{

  const double eps = 1e-5;
  const static int dim = GridType::dimension;
  FieldMatrix<double,dim, dim> id(0);
  id[0][0] = id[1][1] = id[2][2] = 1;

  auto* hierarchyCoupling = dynamic_cast<DualMortarCouplingHierarchy<double,GridType>*>(coupling.get());
  // check the full mortar hierarchy
  if (hierarchyCoupling) {
    for (size_t i=0; i<hierarchyCoupling->numLevels(); i++) {
        
      const auto& M = hierarchyCoupling->mortarLagrangeMatrix(i);

      for (size_t j=0; j<M.N(); j++) {

        FieldMatrix<double, dim, dim> sum(0);
        double absSum = 0;

        for (auto cIt : M[j]) {

          sum += cIt;
          absSum += cIt.infinity_norm();
        }

        bool isIdentity = (sum -= id).infinity_norm() < eps;

        if (!isIdentity && (absSum>eps))
          return false;
      }
    }
    // only check the leaf level
  } else {

    const auto& M = coupling->mortarLagrangeMatrix();

    for (size_t j=0; j<M.N(); j++) {

        FieldMatrix<double, dim, dim> sum(0);
        double absSum = 0;
        for (auto cIt : M[j]) {

            sum += cIt;
            absSum += cIt.infinity_norm();
        }

        bool isIdentity = (sum -= id).infinity_norm() < eps;

        if (!isIdentity && (absSum>eps))
            return false;
    }
  }

  return true;
}

template <class GridType>
bool ContactCouplingTest::rowSumOneTest(const std::string& problemName,
                                        const std::vector<std::shared_ptr<GridType> >& grids,
                                        const std::vector<BoundaryPatch<typename GridType::LevelGridView > > &contactPatches,
                                        bool fullHierarchyCoupling)
{
    std::cout << "#######################################################################" << std::endl;
    std::cout << "   Objects:  " << problemName                                                  << std::endl;
    std::cout << "   Testing whether rows of coupling matrix sum to one"                     << std::endl;
    std::cout << "#######################################################################" << std::endl;

    // Directly assemble the coupling operator
    using CouplingHierarchy = DualMortarCouplingHierarchy<double,GridType>;
    std::shared_ptr<DualMortarCoupling<double,GridType> > contactCoupling;
    if (fullHierarchyCoupling)
        contactCoupling = std::shared_ptr<CouplingHierarchy>(new CouplingHierarchy(*grids[0],*grids[1]));
    else
        contactCoupling = std::make_shared<DualMortarCoupling<double,GridType> >(*grids[0],*grids[1]);

    contactCoupling->setupContactPatch(contactPatches[0],contactPatches[1]);
    contactCoupling->setup();

    return testForRowSumOne(contactCoupling);
}


bool ContactCouplingTest::rowSumOneForHexaCubesTest()
{

    using GridType = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3> >;
    std::vector<std::shared_ptr<GridType> > grids(2);

    std::array<int, 3> elements;
    elements.fill(4);
    FieldVector<double,3> lower(0);
    FieldVector<double,3> upper(1);

    grids[0] = std::make_shared<GridType>(lower, upper, elements);

    elements.fill(2);
    lower[0] += 1;
    upper[0] += 1;

    grids[1] = std::make_shared<GridType>(lower, upper, elements);

    using LevelBoundaryPatch = BoundaryPatch<typename GridType::LevelGridView>;
    std::vector<LevelBoundaryPatch> contactPatches(2);
    contactPatches[0].setup(grids[0]->levelGridView(0),true);
    contactPatches[1].setup(grids[1]->levelGridView(0),true);

    try {

        return rowSumOneTest("Ironing test", grids, contactPatches, false);

    } catch (Dune::Exception& e) {
        std::cerr << e << std::endl;
        return false;
    }

}

bool ContactCouplingTest::rowSumOneForIroningTest()
{
    using GridType = UGGrid<3>;

    std::vector<std::shared_ptr<GridType> > grids(2);

    ParameterTree configBox;
    configBox["lowerCorner"] = "0 0 0";
    configBox["upperCorner"] = "4 9 3";
    configBox["elements"] = "4 8 2";
    configBox["tetrahedral"] = "1";
    grids[0] = std::move(GridConstruction<GridType,3>::createCuboid(configBox));

    ParameterTree configDie;
    configDie["center"] = "-0.7 4.5 6.2";
    configDie["thickness"] = "0.2";
    configDie["length"]= "5.2";
    configDie["innerRadius"]= "3";
    configDie["fromAngle"] = "3.1415";
    configDie["toAngle"] = "6.283";
    configDie["nElementRing"] = "25";
    configDie["nElementLength"] = "10";
    configDie["closeTube"] = "0";
    configDie["axis"] = "0";
    configDie["tetrahedra"] = "1";
    configDie["parameterizedBoundary"] = "0";
    grids[1] = std::move(GridConstruction<GridType,3>::createTubeSegment(configDie));

    using LevelBoundaryPatch = BoundaryPatch<typename GridType::LevelGridView>;
    std::vector<LevelBoundaryPatch> contactPatches(2);
    contactPatches[0].setup(grids[0]->levelGridView(0));
    contactPatches[1].setup(grids[1]->levelGridView(0));

    configBox["nCriterions"] = "1";
    auto& c0 = configBox.sub("criterion0");
    c0["type"] = "UpperCentre";
    c0["component"] = "2";
    c0["bound"] = "2.9999";
    BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(configBox, contactPatches[0]);

    configDie["nCriterions"] = "2";
    auto& c1 = configDie.sub("criterion0");
    c1["type"] = "LowerCentre";
    c1["component"] = "2";
    c1["bound"] = "4.5"; //5

    auto& c2 = configDie.sub("criterion1");
    c2["type"] = "LowerNormal";
    c2["component"] = "2";
    c2["bound"] = "-1e-1";
    BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(configDie, contactPatches[1]);

    try {
      return rowSumOneTest("Ironing test", grids, contactPatches, false);

    } catch (Dune::Exception& e) {

      std::cerr << e << std::endl;
      return false;
    }
}

int main (int argc, char *argv[])
{

  Dune::MPIHelper::instance(argc, argv);
  ContactCouplingTest test;

  bool passed = true;
  passed = passed and test.rowSumOneForHexaCubesTest();
  std::cout<< "HexaCube test passed "<< (passed ? "successfully\n" : "unsuccessfully\n");

  passed = passed and test.rowSumOneForIroningTest();
  std::cout<< "Ironing test passed "<< (passed ? "successfully\n" : "unsuccessfully\n");

  return passed  ? 0 : 1;

}
