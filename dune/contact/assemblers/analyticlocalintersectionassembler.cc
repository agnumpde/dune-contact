// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=2 sw=2 sts=2:

#include <dune/common/fvector.hh>
#include <dune/geometry/type.hh>
#include <dune/matrix-vector/crossproduct.hh>
#include <dune/matrix-vector/addtodiagonal.hh>
#include <dune/matrix-vector/algorithm.hh>


namespace Dune {
namespace Contact {

template <class GlueType, class MatrixType>
void AnalyticLocalIntersectionAssembler<GlueType, MatrixType>::computeRemoteVertexTypes(const std::vector<int>& idx0,
                                const std::vector<CoordinateType>& corners0,
                                const std::vector<CoordinateType>& normals0,
                                const GeometryType& type0,
                                const std::vector<int>& idx1,
                                const std::vector<CoordinateType>& corners1,
                                const std::vector<CoordinateType>& normals1,
                                const GeometryType& type1,
                                std::vector<std::array< std::array<int,5>, dim> >& rVertexTypes)
{
  using std::get;

  // store types in a temporary first so we can remove doubles and order them
  std::vector<std::array<int,5> >tempTypes;

  // store local coordinates so we can remove doubles and sort them
  using LocalCoordinateType = FieldVector<ctype,dim-1>;
  std::vector<LocalCoordinateType> localCorners;

  // The difference between the closest point projection and the normal projection is just the ordering
  // of the involved surfaces. The closest point projection is done along the outer normal field of grid2
  // (due to being a best approximation) and the outer normal projection is using the outer normal field
  // of grid1 instead.
  std::array<decltype(std::cref(corners0)),2> cornersRef ={corners0, corners1};
  std::array<decltype(std::cref(normals0)),2> directionsRef ={normals0, normals1};
  std::array<decltype(std::cref(idx0)),2> idxRef ={idx0, idx1};

  // use this to compute local coordinates of the forward projection and the edge indices

  const auto& ref0 = ReferenceElements<ctype, dim-1>::general(type0);
  const auto& ref1 = ReferenceElements<ctype, dim-1>::general(type1);

  // Determine which is the grid we use for outer normal projection
  size_t domGrid = (isNormalProjection_) ? 0 : 1;
  size_t tarGrid = (isNormalProjection_) ? 1 : 0;

  /////////////////////////////////////////////////////
  //  Compute all corners of the intersection polytope
  /////////////////////////////////////////////////////

  const auto corners = std::tie(cornersRef[domGrid].get(),cornersRef[tarGrid].get());
  const auto normals = std::tie(directionsRef[domGrid].get(), directionsRef[tarGrid].get());

  Dune::GridGlue::Projection<CoordinateType> p(this->overlap_,
                                               this->maxNormalProduct_);

  p.project(corners, normals);

  /* projection */
  {
    const auto& success = get<0>(p.success());
    const auto& images = get<0>(p.images());
    for (unsigned i = 0; i < dim; ++i)
      if (success[i]) {
        localCorners.emplace_back();
        if (isNormalProjection_)
          localCorners.back() = ref0.position(i, dim-1);
        else
          for (unsigned j = 0; j < dim-1; ++j)
            localCorners.back()[j] = images[i][j];
        tempTypes.push_back({{0, idxRef[domGrid].get()[i], 0, 0, 0}});
      }
  }

  /* inverse projection */
  {
    const auto& success = get<1>(p.success());
    const auto& preimages = get<1>(p.images());
    for (unsigned i = 0; i < dim; ++i) {
      if (success[i]) {
        localCorners.emplace_back();
        if (isNormalProjection_) {
          for (unsigned j = 0; j < dim; ++j)
            localCorners.back()[j] = preimages[i][j];
        } else
          localCorners.back() = ref0.position(i,dim-1);
        tempTypes.push_back({{1, idxRef[tarGrid].get()[i], 0, 0, 0}});
      }
    }
  }

  /* edge intersections */
  {
    size_t grid0Idx{not isNormalProjection_};
    for (unsigned i = 0; i < p.numberOfEdgeIntersections(); ++i) {
      const auto& is = p.edgeIntersections()[i];

      localCorners.emplace_back();
      for (unsigned j = 0; j < dim-1; ++j)
        localCorners.back()[j] = is.local[grid0Idx][j];

      tempTypes.push_back({{2,
                            idx0[ref0.subEntity(is.edge[grid0Idx],1,0,dim-1)],
                            idx0[ref0.subEntity(is.edge[grid0Idx],1,1,dim-1)],
                            idx1[ref1.subEntity(is.edge[(grid0Idx+1)%2],1,0,dim-1)],
                            idx1[ref1.subEntity(is.edge[(grid0Idx+1)%2],1,1,dim-1)]}});

      if (tempTypes.back()[1]>tempTypes.back()[2])
        std::swap(tempTypes.back()[1],tempTypes.back()[2]);

      if (tempTypes.back()[3]>tempTypes.back()[4])
        std::swap(tempTypes.back()[3],tempTypes.back()[4]);
    }
  }

  // remove possible doubles
  size_t counter(1);
  for (size_t i=1; i < localCorners.size(); i++) {
    bool contained = false;
    for (size_t j=0; j < counter; j++)
      if ( (localCorners[j]-localCorners[i]).two_norm()<1e-10) {
        contained = true;
        break;
      }

    if (!contained) {
      if (counter < i) {
        localCorners[counter] = localCorners[i];
        tempTypes[counter] = tempTypes[i];
      }
      counter++;
    }
  }

  localCorners.resize(counter);
  tempTypes.resize(counter);

  int nCorners = localCorners.size();

  // sort remaining points cyclic
  std::vector<int> ordering(nCorners);

  for (size_t k=0; k<ordering.size(); k++)
    ordering[k] = k;

  // make ordering such that 0 1 2 is chosen
  if (nCorners == dim) {
    for (int k= dim-1; k > 0; k--)
      std::swap(ordering[k], ordering[k-1]);
  } else {

    LocalCoordinateType center(0);
    for (int i=0; i<nCorners; i++)
      center.axpy(1.0/nCorners, localCorners[i]);

    // compute angles inside the polygon plane w.r.t to this axis
    auto edge0 = localCorners[1] - localCorners[0];

    // Compute a vector that is perpendicular to the edge but lies in the polytope plane
    // So we have a unique ordering
    auto  edge1 = localCorners[2] - localCorners[0];
    auto normal0 = edge1;
    normal0.axpy(-(edge0*edge1),edge0);

    std::vector<ctype> angles(nCorners);

    for (size_t i=0; i < localCorners.size(); i++) {

      auto  edge = localCorners[i] - center;

      ctype x(edge*edge0);
      ctype y(edge*normal0);

      angles[i] = std::atan2(y, x);
      if (angles[i]<0)
        angles[i] += 2*M_PI;
    }

    // bubblesort
    for (int i=nCorners; i > 1; i--) {
      bool swapped = false;

      for (int j=0; j < i-1; j++) {

        if (angles[j] > angles[j+1]) {
          swapped = true;
          std::swap(angles[j], angles[j+1]);
          std::swap(ordering[j], ordering[j+1]);
        }
      }
      if (!swapped)
        break;
    }
  }
  // add corner types according to the intersection creation in ContactMerge
  for (size_t i=1; i < localCorners.size()-1+(dim==2); i++) {
    std::array<std::array<int, 5>, dim> isTypes;

    for (int j=0;j<dim-1; j++)
      isTypes[j] = tempTypes[ordering[i+j]];

    // last node is always the first one
    isTypes[dim-1] = tempTypes[ordering[0]];
    rVertexTypes.push_back(isTypes);
  }

}

template <class GlueType, class MatrixType>
void AnalyticLocalIntersectionAssembler<GlueType, MatrixType>::assemble(
    const std::vector<int>& face0, const std::vector<CoordinateType>& corners0,
    const std::vector<CoordinateType>& normals0, const GeometryType& type0,
    const std::vector<int>& face1, const std::vector<CoordinateType>& corners1,
    const std::vector<CoordinateType>& normals1, const GeometryType& type1,
    int remoteIdx, MatrixType& linRemoteCorners)
{
  namespace MV = Dune::MatrixVector;
  using field_type = typename PromotionTraits<ctype,typename MatrixType::field_type>::PromotedType;
  int size0 = this->unreducedPatch0().gridView().size(dim);
  int size1 = this->unreducedPatch1().gridView().size(dim);

  // compute vertex corner types
  std::vector<std::array< std::array<int,5>, dim> > rVertexTypes;
  computeRemoteVertexTypes(face0, corners0, normals0, type0,
                           face1, corners1, normals1, type1, rVertexTypes);

  size_t offsetDom = (isNormalProjection_) ? 0 : size0;
  size_t offsetTar = (isNormalProjection_) ? size0 : 0;

  const auto& domNormals = (isNormalProjection_) ? normals0 : normals1;
  const auto& domCorners = (isNormalProjection_) ? corners0 : corners1;
  const auto& tarCorners = (isNormalProjection_) ? corners1 : corners0;

  const auto& domGlobalToLocal = (isNormalProjection_) ? this->redGlobalToLocal0_
                                                       : this->redGlobalToLocal1_;
  // compute edges of target grid
  std::vector<CoordinateType> edges(tarCorners.size());
  for (size_t i=0; i < edges.size(); i++)
    edges[i] = tarCorners[(i+2)%dim] - tarCorners[(i+1)%dim];

  // and the face normal
  CoordinateType tarFaceNormal(0);
  if (dim == 2)
    tarFaceNormal = {{(tarCorners[1][1] - tarCorners[0][1]), (tarCorners[0][0] - tarCorners[1][0])}};
  else if (dim == 3)
    tarFaceNormal = MV::crossProduct(edges[1], edges[2]);

  ///////////////////////////////////////////////////////////////////
  //         Linearistaion of the remote intersection corners
  ///////////////////////////////////////////////////////////////////

  std::array<MatrixType, dim> linProjDomVertex;

  // loop over number of intersections
  for(const auto& rVertexTypeList : rVertexTypes) {
    // loop over intersection vertices
    for (size_t i=0; i<dim; i++, remoteIdx++) {

      // get corner in consistency with dune-grid-glue
      int vIdx = remoteIdx;
      const auto& rVertex = rVertexTypeList[i];

      switch (rVertex[0]) {
      // inverse projection of grid vertex
      case (1) : {
        if (not isNormalProjection_) {
          MV::addToDiagonal(linRemoteCorners[vIdx][rVertex[1]], 1.0);
          break;
        }

        // setup all missing linearised projected points
        for (size_t k=0; k < dim; k++) {
          if (linProjDomVertex[k].N()==0) {
            CoordinateType projVertex;
            linearisedForwardProjection(linProjDomVertex[k], projVertex,
                                        offsetDom, offsetTar, size0 + size1,
                                        face0[k],
                                        corners0[k], normals0[k],
                                        linAverageNormals_[domGlobalToLocal[face0[k]]],
                face1, tarCorners[0], edges, tarFaceNormal);
          }
        }

        // get the local index of the target vertex
        size_t localIdx1(0);
        for (; localIdx1 < face1.size(); localIdx1++)
          if (face1[localIdx1] == rVertex[1])
            break;

        if (dim == 3)
          linearised3DInverseProjectedVertex(face0, face1, corners0, corners1,
                                             normals0, tarFaceNormal, edges, localIdx1,
                                             linProjDomVertex, linRemoteCorners[vIdx]);
        else
          linearised2DInverseProjectedVertex(face0, face1, corners0, corners1,
                                             normals0, tarFaceNormal, localIdx1,
                                             linProjDomVertex, linRemoteCorners[vIdx]);
        break;
      }
      // forward projection of grid vertex
      case (0) : {

        if (isNormalProjection_) {
          MV::addToDiagonal(linRemoteCorners[vIdx][rVertex[1]], 1.0);
          break;
        }

        // get the local index grid1 is the domain grid
        size_t localIdx1(0);
        for (; localIdx1 < face1.size(); localIdx1++)
          if (face1[localIdx1] == rVertex[1])
            break;

        // forward projections of grid1 vertices are linearised according linProjVertex
        if (linProjDomVertex[localIdx1].N()==0) {
          CoordinateType projVertex;
          linearisedForwardProjection(linProjDomVertex[localIdx1], projVertex,
                                      offsetDom, offsetTar, size0 + size1,
                                      rVertex[1], corners1[localIdx1], normals1[localIdx1],
              linAverageNormals_[this->redGlobalToLocal1_[rVertex[1]]],
              face0, corners0[0], edges, tarFaceNormal);
        }

        const auto cEnd = linProjDomVertex[localIdx1][0].end();
        for (auto cI = linProjDomVertex[localIdx1][0].begin(); cI != cEnd; cI++) {
            if (not linRemoteCorners.exists(vIdx,cI.index()))
                DUNE_THROW(Dune::Exception, "NOt ex");
          linRemoteCorners[vIdx][cI.index()] += *cI;}
        break;
      }
      case (2): {

        // intersection vertex is edge

        // in this case the remote vertex is a convex combination of two grid0 vertices
        // the parameter depends also on the grid1 vertices and normals

        // find grid1 vertex indices of the edge
        std::array<size_t,2> localIdx1;
        for (size_t k=0; k<2; k++) {
          for (size_t j=0; j<face1.size(); j++)
            if (face1[j] == rVertex[k+3]) {
              localIdx1[k]=j;
              break;
            }
        }

        // find grid0 vertex indices
        std::array<size_t,2> localIdx0;
        for (size_t k=0; k<2; k++) {
          for (size_t j=0; j<face0.size(); j++)
            if (face0[j] == rVertex[k+1]) {
              localIdx0[k]=j;
              break;
            }
        }

        /////////////////////////////////////////////////////////////////////////
        ///  The linearisation of an edge intersection X can be computed as follows:
        ///
        ///  Let (A,B) and (C,D) be two edges (domain and target resp.) intersecting,
        ///  then the intersection in non-mortar coordinates is given as
        ///
        ///     X = A +\mu (B-A) (outer normal)
        ///     X = A' +\mu (B' -A') (closest point)
        ///
        ///  For the linearisation we need to linearise \mu by solving the edge intersection
        ///  problem of the projected domain grid corners (A',B') and
        ///  the target grid corners (C,D) we get
        ///
        ///   \mu = -\frac{(A'-C)\times(D-C)\cdot n} {(B'-A')\times (D-C)\cdot n},
        ///
        ///  where n is a normal of the target triangle.
        ///////////////////////////////////

        /// linearise \mu
        const auto& domLocalIdx = (isNormalProjection_) ? localIdx0 : localIdx1;
        const auto& tarLocalIdx = (isNormalProjection_) ? localIdx1 : localIdx0;

        // compute projected domain vertices
        std::array<CoordinateType,2> projDom;
        for (size_t j=0; j<2; j++) {
          auto dDomC = domCorners[domLocalIdx[j]] - tarCorners[0];
          field_type alpha = -(tarFaceNormal*dDomC)/(tarFaceNormal*domNormals[domLocalIdx[j]]);
          projDom[j] = domCorners[domLocalIdx[j]];
          projDom[j].axpy(alpha, domNormals[domLocalIdx[j]]);
        }

        // for closest point considered equation in projected coordinates,
        // else in domain coords
        CoordinateType dBA = (isNormalProjection_) ? domCorners[domLocalIdx[1]] - domCorners[domLocalIdx[0]]
                                                   : projDom[1] -projDom[0];

        auto dDC = tarCorners[tarLocalIdx[1]] - tarCorners[tarLocalIdx[0]];
        auto dprojBprojA = projDom[1] - projDom[0];
        auto dprojBprojAxdDC = MV::crossProduct(dprojBprojA, dDC);
        field_type denom = 1.0/(dprojBprojAxdDC*tarFaceNormal);

        auto dProjAC = projDom[0] - tarCorners[tarLocalIdx[0]];
        auto dProjACxdDC = MV::crossProduct(dProjAC, dDC);
        field_type numerator = (dProjACxdDC*tarFaceNormal)*denom*denom;

        // linearisation of A'
        for (size_t l=0; l<2; l++)
          if (linProjDomVertex[domLocalIdx[l]].N()==0) {
            CoordinateType projVertex;
            int globalDomIdx = rVertex[l+1 +2*(!isNormalProjection_)];
            linearisedForwardProjection(linProjDomVertex[domLocalIdx[l]], projVertex,
                offsetDom, offsetTar, size0 + size1,
                globalDomIdx,
                domCorners[domLocalIdx[l]], domNormals[domLocalIdx[l]],
                linAverageNormals_[domGlobalToLocal[globalDomIdx]],
                ((isNormalProjection_) ? face1 : face0),
                tarCorners[0], edges, tarFaceNormal);
          }

        auto dDCxNormal = MV::crossProduct(dDC, tarFaceNormal);
        MV::sparseRangeFor(linProjDomVertex[domLocalIdx[0]][0], [&](auto&& col, auto&& idx) {
          CoordinateType dummy(0);
          col.usmtv(-denom-numerator, dDCxNormal, dummy);
          linRemoteCorners[vIdx][idx] += This::dyadic(dBA,dummy);});

        // linearisation of D
        CoordinateType dummyDC(0);
        dummyDC.axpy(-denom, dProjAC);
        dummyDC.axpy(numerator, dprojBprojA);
        auto dummyDC2 = dyadic(dBA, MV::crossProduct(tarFaceNormal, dummyDC));
        int globalTarIdx = offsetTar + rVertex[2 + 2*(isNormalProjection_)];
        linRemoteCorners[vIdx][globalTarIdx] += dummyDC2;

        // linearisation of -C
        dummyDC2.axpy(-denom, dyadic(dBA, dDCxNormal));
        globalTarIdx = offsetTar + rVertex[1 + 2*(isNormalProjection_)];
        linRemoteCorners[vIdx][globalTarIdx] -= dummyDC2;

        // linearisation of B'
        MV::sparseRangeFor(linProjDomVertex[domLocalIdx[1]][0],[&](auto&& col, auto&& idx) {
          CoordinateType dummy;
          col.mtv(dDCxNormal, dummy);
          linRemoteCorners[vIdx][idx].axpy(numerator, This::dyadic(dBA, dummy));});

        // linearisation of mNormal
        CoordinateType dummyN(0);
        dummyN.axpy(numerator, dprojBprojAxdDC);
        dummyN.axpy(-denom, dProjACxdDC);
        for (int k=0; k<dim; k++)
          linRemoteCorners[vIdx][face0[k]] += (
                dyadic(dBA, MV::crossProduct(dummyN, edges[k])));

        /// linearise mu independent parts
        field_type mu = -numerator/denom;
        if (isNormalProjection_) {
          MV::addToDiagonal(linRemoteCorners[vIdx][rVertex[1]], 1 - mu);
          MV::addToDiagonal(linRemoteCorners[vIdx][rVertex[2]], mu);
        } else {
          MV::sparseRangeFor(linProjDomVertex[localIdx1[0]][0],
            [&](auto&& col, auto&& idx){
              linRemoteCorners[vIdx][idx].axpy(1-mu, col);});

          MV::sparseRangeFor(linProjDomVertex[localIdx1[1]][0],
            [&](auto&& col, auto&& idx){
              linRemoteCorners[vIdx][idx].axpy(mu, col);});
        }
        break;
      }
      }
    }
  }
}

template <class GlueType, class MatrixType>
void AnalyticLocalIntersectionAssembler<GlueType, MatrixType>::linearisedForwardProjection(
    MatrixType& linProj, CoordinateType& projVertex,
    size_t offsetDom, size_t offsetTar, size_t totalSize,
    int vertexIdx, const CoordinateType& vertex,
    const CoordinateType& avNormal,
    const typename MatrixType::row_type& linNormal,
    const std::vector<int> face, const CoordinateType& faceCorner,
    const std::vector<CoordinateType>& faceEdges,
    const CoordinateType& faceNormal)
{

  //////////////////////////////////////////
  /// The projection of a grid0 vertex A onto the grid1 triangle plane is given by
  ///
  ///    A' = A + \alpha n_A,
  /// where
  ///    n_A   - averaged nodal
  ///    n     - the opposing face normal
  ///    C     - first opposing face corner
  ///  \alpha = -\frac{(A-C)\cdot n}{n_A\cdot n}
  ///////////////////////////////////

  namespace MV = Dune::MatrixVector;

  // make index set
  MatrixIndexSet matIs(1, totalSize);

  // the parts involved in the normal linearisation
  MV::sparseRangeFor(linNormal, [&](auto&&, auto&& idx) {
    matIs.add(0, offsetDom + idx);});

  // the corners of the target triangle
  for (const auto& idx : face)
    matIs.add(0, offsetTar + idx);

  matIs.exportIdx(linProj);
  linProj=0;

  // now the entries
  CoordinateType AC = vertex - faceCorner;

  auto denomAlpha = 1.0/(avNormal*faceNormal);
  auto alpha = -(faceNormal*AC) * denomAlpha;

  projVertex = vertex;
  projVertex.axpy(alpha, avNormal);

  // \delta A
  MV::addToDiagonal(linProj[0][offsetDom + vertexIdx], 1.0);

  // \delta n_A
  MV::sparseRangeFor(linNormal, [&](auto&& col, auto&& idx) {
    linProj[0][offsetDom + idx].axpy(alpha, col);});

  /// \delta\alpha

  // derive numerator of alpha
  // A
  auto linAlphaFirst = dyadic(avNormal, faceNormal);
  linProj[0][offsetDom + vertexIdx].axpy(-denomAlpha, linAlphaFirst);

  // -C
  linProj[0][offsetTar + face[0]].axpy(denomAlpha, linAlphaFirst);

  // face normal
  if (dim==3) {
    for (size_t k=0; k < face.size(); k++)
      linProj[0][offsetTar + face[k]].axpy(-denomAlpha, dyadic(avNormal,MV::crossProduct(AC, faceEdges[k])));
  } else if (dim==2) {
    for (size_t k=0; k < face.size(); k++)
      linProj[0][offsetTar + face[k]].axpy(-denomAlpha,
                                           dyadic(avNormal, CoordinateType({std::pow(-1,k)*AC[1], -std::pow(-1,k)*AC[0]})));
  }

  //  derive denominator of alpha
  auto scal = (AC*faceNormal)*denomAlpha*denomAlpha;

  // average normal
  MV::sparseRangeFor(linNormal, [&](auto&& col, auto&& idx) {
    CoordinateType dummy;
    col.mtv(faceNormal, dummy);
    linProj[0][offsetDom + idx].axpy(scal, This::dyadic(avNormal, dummy));});

  // faceNormal
  if (dim==3) {
    for (size_t k=0; k<face.size(); k++)
      linProj[0][offsetTar + face[k]].axpy(scal, dyadic(avNormal,MV::crossProduct(avNormal, faceEdges[k])));
  } else if (dim==2) {
    for (size_t k=0; k<face.size(); k++)
      linProj[0][offsetTar + face[k]].axpy(scal,
                                           dyadic(avNormal, CoordinateType({std::pow(-1,k)*avNormal[1], -std::pow(-1,k)*avNormal[0]})));
  }
}

template <class GlueType, class MatrixType>
void AnalyticLocalIntersectionAssembler<GlueType, MatrixType>::linearised2DInverseProjectedVertex(
          const std::vector<int>& domainFace,
          const std::vector<int>& targetFace,
          const std::vector<CoordinateType>& domainVertices,
          const std::vector<CoordinateType>& targetVertices,
          const std::vector<CoordinateType>& domainNormals,
          const CoordinateType& targetFaceNormal,
          size_t targetLocalIndex,
          const std::array<MatrixType,dim>& linProjDomVertex,
          typename MatrixType::row_type& linRemoteCorner) const
{
  namespace  MV = Dune::MatrixVector;

  /////////////////////////////////////////////////////////////////////////
  ///  The linearisation of a target intersection vertex M in 2D, for the outer normal
  ///  projection can be computed as follows:
  ///
  ///  Let (A,B) a domain triangle and M the target vertex, then
  ///
  ///     M_inverse = A +\mu (B-A)
  ///
  ///  By projecting the domain corners (A',B') onto the target face plane we get
  ///
  ///   \mu = \frac{(M-A')\cdot(B'-A')} {(B'-A')\cdot (B'-A')},
  ////////////////////////////////////////////////////////////////////////////////

  // projected vertices
  auto projNm = domainVertices;
  for (size_t j=0; j < dim; ++j) {
    auto dNmC = targetVertices[0] - domainVertices[j];
    auto alpha = (targetFaceNormal*dNmC)/(targetFaceNormal*domainNormals[j]);
    projNm[j].axpy(alpha, domainNormals[j]);
  }

  auto dMProjA = targetVertices[targetLocalIndex] - projNm[0];
  auto dProjBProjA = projNm[1] - projNm[0];

  // explicit assume that we consider the outer normal projection!
  auto offsetTarget = this->unreducedPatch0().gridView().size(dim);

  auto dBA = domainVertices[1] - domainVertices[0];

  ///    first derive the numerator
  auto denomMu = 1.0/(dProjBProjA.two_norm2());

  /// the first part \delta (M-A')\times (C'-A') \cdot n

  // M
  auto linM = dyadic(dBA, dProjBProjA);
  linRemoteCorner[offsetTarget + targetFace[targetLocalIndex]].axpy(denomMu, linM);

  // -A'
  /// sacling for the denominator linearisation
  auto scaling = -2*(dMProjA*dProjBProjA)*denomMu; // one denomMu is added later
  MV::sparseRangeFor(linProjDomVertex[0][0], [&](auto&& col, auto&& idx){
    CoordinateType dummy;
    col.mtv(dProjBProjA, dummy);
    dummy *= (scaling + 1); // denominator and nomin part
    col.umtv(dMProjA, dummy);
    dummy *= denomMu; // nominator part
    linRemoteCorner[idx] -= This::dyadic(dBA, dummy);});

  // B'
  MV::sparseRangeFor(linProjDomVertex[1][0], [&](auto&& col, auto&& idx){
    CoordinateType dummy;
    col.mtv(dProjBProjA, dummy);
    dummy *= scaling; // denominator
    col.umtv(dMProjA, dummy);
    dummy *= denomMu; // nominator
    linRemoteCorner[idx] += This::dyadic(dBA, dummy);});

  auto mu=(dMProjA*dProjBProjA)*denomMu;

  /// linearse parts don't depending on mu or eta
  //     X = A +\mu (B-A)
  MV::addToDiagonal(linRemoteCorner[domainFace[0]], 1-mu);
  MV::addToDiagonal(linRemoteCorner[domainFace[1]], mu);
}

template <class GlueType, class MatrixType>
void AnalyticLocalIntersectionAssembler<GlueType, MatrixType>::linearised3DInverseProjectedVertex(
          const std::vector<int>& domainFace,
          const std::vector<int>& targetFace,
          const std::vector<CoordinateType>& domainVertices,
          const std::vector<CoordinateType>& targetVertices,
          const std::vector<CoordinateType>& domainNormals,
          const CoordinateType& targetFaceNormal,
          const std::vector<CoordinateType>& targetEdges,
          size_t targetLocalIndex,
          const std::array<MatrixType, dim>& linProjDomVertex,
          typename MatrixType::row_type& linRemoteCorner) const
{
  namespace MV = Dune::MatrixVector;

  /////////////////////////////////////////////////////////////////////////
  ///  The linearisation of a target intersection vertex M in 3D, for the outer normal
  ///  projection can be computed as follows:
  ///
  ///  Let (A,B,C) a grid0 (domain) triangle and M the grid1 (target) vertex, then
  ///
  ///     M_inverse = A +\mu (B-A) + \eta (C-A)
  ///
  ///  For the linearisation of this equation we need to linearise \mu and \eta
  ///  According to the grid-glue implementation these are computing in the target plane
  ///
  ///  By projecting the grid0 corners (A',B',C') onto the grid1 triangle plane we get
  ///
  ///   \mu = \frac{(M-A')\times(C'-A')\cdot n} {(B'-A')\times (C'-A')\cdot n},
  ///   \eta = \frac{(M-A')\times(B'-A')\cdot n} {(C'-A')\times (B'-A')\cdot n},
  ///
  ///  where n is a normal of the grid1 triangle.
  ////////////////////////////////////////////////////////////////////////////////

  // TODO adjust to non-triangles
  // projected vertices and projected Vertices minus A'
  auto projNm = domainVertices;
  std::vector<CoordinateType> dProjNmA(dim);

  for (size_t j=0; j < dim; ++j) {
    auto dNmC = targetVertices[0] - domainVertices[j];
    auto alpha = (targetFaceNormal*dNmC)/(targetFaceNormal*domainNormals[j]);
    projNm[j].axpy(alpha, domainNormals[j]);
    dProjNmA[j] = projNm[j] - projNm[0];
  }

  // dProjNmA[0] will contain M-A'
  dProjNmA[0] = targetVertices[targetLocalIndex]-projNm[0];

  // compute everything for mu and eta
  std::array<unsigned, 2> idx = {{1, 2}};
  std::array<field_type, 2> mueta;

  // explicit assume that we consider the outer normal projection!
  size_t offsetTarget = this->unreducedPatch0().gridView().size(dim);

  for (size_t l=0; l < 2; l++) {
    // eta and mu only differ in the order of B' and C'
    std::swap(idx[0], idx[1]);
    auto dBA = domainVertices[idx[1]] - domainVertices[0];

    ////////////////////////////////////////
    ///    first derive the numerator
    /////////////////////////////////////////

    auto denomMu = 1.0/(MV::crossProduct(dProjNmA[idx[1]], dProjNmA[idx[0]])*targetFaceNormal);

    /// the first part \delta (M-A')\times (C'-A') \cdot n

    // M
    auto dCAxnormal1 = MV::crossProduct(dProjNmA[idx[0]], targetFaceNormal);
    auto linFirst = dyadic(dBA, dCAxnormal1);
    linRemoteCorner[offsetTarget + targetFace[targetLocalIndex]].axpy(denomMu, linFirst);

    auto normal1xdMA = MV::crossProduct(targetFaceNormal, dProjNmA[0]);
    auto normal1xdBA = MV::crossProduct(targetFaceNormal, dProjNmA[idx[1]]);
    auto dMAxdCA = MV::crossProduct(dProjNmA[0], dProjNmA[idx[0]]);
    auto numerator = -(dMAxdCA*targetFaceNormal)*denomMu*denomMu;

    /// the second part (M-A')\times \delta(C'-A') \cdot n
    // C'

    CoordinateType dummyC(0);
    dummyC.axpy(denomMu, normal1xdMA);
    dummyC.axpy(numerator, normal1xdBA);
    MV::sparseRangeFor(linProjDomVertex[idx[0]][0], [&](auto&& col, auto&& idx) {
      CoordinateType dummy;
      col.mtv(dummyC, dummy);
      linRemoteCorner[idx] += This::dyadic(dBA, dummy);});

    /// linearisation of mNormal, the third part (M-A')\times(C'-A')\cdot\delta n

    for (int k=0; k<dim; k++)
      linRemoteCorner[offsetTarget + targetFace[k]].axpy(denomMu,
                                         dyadic(dBA, MV::crossProduct(dMAxdCA, targetEdges[k])));

    // linearisation of B'
    MV::sparseRangeFor(linProjDomVertex[idx[1]][0], [&](auto&& col, auto&& idx) {
      CoordinateType dummy;
      col.mtv(dCAxnormal1, dummy);
      linRemoteCorner[idx].axpy(numerator, This::dyadic(dBA, dummy));});

    // linearisation of -A'
    CoordinateType dummyA(0);
    dummyA -= dummyC;
    dummyA.axpy(-denomMu-numerator, dCAxnormal1);
    MV::sparseRangeFor(linProjDomVertex[0][0], [&](auto&& col, auto&& idx) {
      CoordinateType dummy;
      col.mtv(dummyA,dummy);
      linRemoteCorner[idx] += This::dyadic(dBA, dummy);});

    /// linearisation of mNormal, the third part (B'-A')\times(D-C)\cdot\delta n
    auto dBAxdCA = MV::crossProduct(dProjNmA[idx[1]], dProjNmA[idx[0]]);

    for (int k=0; k < dim; k++)
      linRemoteCorner[offsetTarget + targetFace[k]].axpy(numerator,
                                                    dyadic(dBA, MV::crossProduct(dBAxdCA, targetEdges[k])));

    mueta[l]=(dMAxdCA*targetFaceNormal)/(dBAxdCA*targetFaceNormal);
  }

  /// linearse parts don't depending on mu or eta
  //     X = A +\mu (B-A) + \eta (C-A)
  MV::addToDiagonal(linRemoteCorner[domainFace[0]], 1-mueta[0]-mueta[1]);
  MV::addToDiagonal(linRemoteCorner[domainFace[1]], mueta[0]);
  MV::addToDiagonal(linRemoteCorner[domainFace[2]], mueta[1]);
}

} /* namespace Contact */
} /* namespace Dune */
