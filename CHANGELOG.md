Changes in dune-contact v2.6.0
==============================

* There is now a subdirectory `src`, and some of the compilable source code
  files have moved there.  This makes `dune-contact` more consistent with
  other Dune modules.

Changes in dune-contact v2.5.0
==============================

* The interface for the onebodyassembler changed to be more flexible
  when discrete obstacles are used. The methods to compute the actual
  obstacle values now have to be handed over to the assembler

* A filter trust--region solver for static large deformation contact
  problems has been added. An example how to use it can be found in
  2bnonlincontact.cc

Incompatible changes in dune-grid-glue v2.5.0
---------------------------------------------

* All interfaces have been moved into the `Dune::Contact` namespace.
