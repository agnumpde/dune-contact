#include <config.h>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>

#include <dune/istl/solvers.hh>
#include <dune/istl/io.hh>

#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/utilities/dirichletbcassembler.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/readbitfield.hh>
#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif

#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/energynorm.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/differencenormsquared.hh>

#include <dune/fufem/estimators/fractionalmarking.hh>

#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/estimators/zoukhsignoriniestimator.hh>
#include <dune/contact/solvers/nsnewtonmgstep.hh>
#include <dune/contact/solvers/contacttransfer.hh>
#include <dune/contact/solvers/nsnewtoncontacttransfer.hh>
#include <dune/contact/assemblers/onebodyassembler.hh>

#include <dune/fufem/makehalfcircle.hh>
#include <dune/fufem/makesphere.hh>

using namespace Dune;
using namespace Dune::Contact;

// The grid dimension
const int dim = 2;

// Some types that I need
typedef BCRSMatrix<FieldMatrix<double, dim, dim> > MatrixType;
typedef FieldVector<double,dim> FVector;
typedef BlockVector<FVector> VectorType;
typedef UGGrid<dim> GridType;
typedef BoundaryPatch<GridType::LevelGridView> LevelBoundary;
typedef BoundaryPatch<GridType::LeafGridView> LeafBoundary;


const double E  = 17e6;
const double nu = 0.3;

// Analytic obstacle given by the x-axis
std::function<double(const FVector&, const FVector&)> obstacle = [] (const FVector& x, const FVector& dir)
{
    return (std::fabs(dir[1])>1e-10) ? (-x[1]/dir[1]) : std::numeric_limits<double>::max();
};

void compute (const GridType& grid,
              VectorType& x,
              const VectorType& coarseDirichletValues,
              const BitSetVector<1>& coarseDirichletNodes,
              const LevelBoundary& obsPatch,
              double tolerance,
              int numIt)
{


    // Initial solution
    x.resize(grid.size(dim));
    x=0;

    BitSetVector<1> scalarDirichletNodes;
    // Assemble leaf Dirichlet conditions and write them directly into the initial solution
    DirichletBCAssembler<GridType>::assembleDirichletBC(grid,coarseDirichletNodes,coarseDirichletValues,
                                                        scalarDirichletNodes, x);
    // BitSetVector<1> -> BitSetVector<dim>
    BitSetVector<dim> dirichletNodes;
    DirichletBCAssembler<GridType>::inflateBitField(scalarDirichletNodes,dirichletNodes);

    //    Create rhs vectors
    VectorType rhs(grid.size(dim));
    rhs = 0;

    int toplevel = grid.maxLevel();

    // ////////////////////////////////////////////////////////////
    //   Assemble stiffness matrix
    // ////////////////////////////////////////////////////////////

    typedef P1NodalBasis<GridType::LeafGridView> FEBasis;
    FEBasis basis(grid.leafGridView());
    OperatorAssembler<FEBasis,FEBasis> assembler(basis, basis);

    StVenantKirchhoffAssembler<GridType, FEBasis::LocalFiniteElement, FEBasis::LocalFiniteElement> localAssembler(E, nu);
    MatrixType* stiffnessMatrix = new MatrixType;

    assembler.assemble(localAssembler, *stiffnessMatrix);

    // ////////////////////////////////////////////////////////////
    //   Assemble contact problem
    // ////////////////////////////////////////////////////////////

    OneBodyAssembler<GridType> contactAssembler(grid, obstacle, obsPatch);

    contactAssembler.assembleObstacles();
    contactAssembler.transformMatrix(*stiffnessMatrix);

    contactAssembler.transformVector(x);

    // ////////////////////////////////////////////////////////////
    //   Prolong obstacle patch to all levels
    // ////////////////////////////////////////////////////////////

    const LeafBoundary& leafObsPatch = contactAssembler.obsPatch_;
    BitSetVector<dim> obsNodes(grid.size(dim));

    for (size_t i=0; i<obsNodes.size();i++)
        obsNodes[i][0]= leafObsPatch.containsVertex(i);

    // ////////////////////////////////////////////////
    //   Create a solver
    // ////////////////////////////////////////////////

    // First create a base solver
#ifdef HAVE_IPOPT
    QuadraticIPOptSolver<MatrixType,VectorType> baseSolver(1e-8, 100, NumProc::QUIET);
#endif

    // Make pre and postsmoothers
    ProjectedBlockGSStep<MatrixType, VectorType> presmoother, postsmoother;

    NonSmoothNewtonMGStep<MatrixType, VectorType> multigridStep(*stiffnessMatrix, x, rhs);

    multigridStep.setMGType(1, 3, 3);  // A V(3,3) cycle
    multigridStep.setIgnore(dirichletNodes);
    multigridStep.setBaseSolver(baseSolver);
    multigridStep.setSmoother(presmoother, postsmoother);
    multigridStep.setHasObstacle(obsNodes);
    multigridStep.setObstacles(contactAssembler.getObstacles());
    multigridStep.setVerbosity(NumProc::QUIET);

    // //////////////////////////////////////////////
    //   Create the transfer operators
    // //////////////////////////////////////////////
    std::vector<NonSmoothNewtonContactTransfer<VectorType> > mgTransfers(toplevel);
    for (size_t i=0; i<mgTransfers.size(); i++)
        mgTransfers[toplevel-i-1].setup(grid, i, contactAssembler.localCoordSystems_);
    multigridStep.setTransferOperators(mgTransfers);

    EnergyNorm<MatrixType, VectorType> energyNorm(multigridStep);

    ::LoopSolver<VectorType> solver(multigridStep,
                                                   // IPOpt doesn't like to be started at the solution, so make sure we really
                                                   // call it only once when it is the only solver
                                                   (toplevel==0) ? 1 : numIt,
                                                   tolerance,
                                                   energyNorm,
                                                   Solver::FULL);

    // /////////////////////////////
    //    Solve !
    // /////////////////////////////
    solver.preprocess();
    solver.solve();

    x = multigridStep.getSol();

    contactAssembler.transformVector(x);

}

int main(int argc, char *argv[]) try
{
    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("1badaptmeasure.parset", parameterSet);

    // read solver settings
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const double tolerance     = parameterSet.get("tolerance", double(0));
    const double refinementFraction = parameterSet.get("refinementFraction", double(1));
    const bool paramBoundaries = parameterSet.get("paramBoundaries", int(0));

    // read problem settings
    std::string path                 = parameterSet.get("path", "xyz");
    std::string objectName           = parameterSet.get("gridFile", "xyz");
    std::string parFile              = parameterSet.get("parFile", "xyz");
    std::string dirichletFile        = parameterSet.get("dirichletFile", "xyz");
    std::string dirichletValuesFile  = parameterSet.get("dirichletValuesFile", "xyz");
    std::string obsFilename          = parameterSet.get("obsFilename", "xyz");
    double scaling                   = parameterSet.get("scaling", double(1));

    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    std::unique_ptr<GridType> gridPtr;

    if (paramBoundaries) {
        gridPtr = AmiraMeshReader<GridType>::read(path + objectName, PSurfaceBoundary<dim-1>::read(path + parFile));
    } else {
        //AmiraMeshReader<GridType>::read(uniformGrid[0], path + object0Name);
        //makeHalfCircleQuad(uniformGrid);
        //makeHalfCircle(uniformGrid);
        gridPtr = std::make_unique<GridType>();
        makeHalfCircleThreeTris(*gridPtr);
        //makeSphere(uniformGrid[0], FieldVector<double,3>(0), 1);
    }
    GridType& uniformGrid = *gridPtr;
    uniformGrid.setRefinementType(GridType::COPY);

    // /////////////////////////////////////////////////////
    //   Read coarse grid Dirichlet information
    // /////////////////////////////////////////////////////
    VectorType dirichletValues;
    dirichletValues.resize(uniformGrid.size(0, dim));
    AmiraMeshReader<GridType>::readFunction(dirichletValues, path + dirichletValuesFile);

    // Scale Dirichlet values
    dirichletValues *= scaling;

    BitSetVector<1> coarseDirichletNodes;
    readBitField(coarseDirichletNodes, uniformGrid.size(0, dim), path + dirichletFile);

    // /////////////////////////////////////////////////////
    // read field describing the obstacles
    // /////////////////////////////////////////////////////

    LevelBoundary obsPatch(uniformGrid.levelGridView(0));
    readBoundaryPatch<GridType>(obsPatch, path + obsFilename);

    // /////////////////////////////////////////////////////
    // refine uniformly until minlevel
    // /////////////////////////////////////////////////////
    for (int i=0; i<maxLevel; i++) {
        uniformGrid.globalRefine(1);

        if (paramBoundaries)
            improveGrid(uniformGrid, 10);

    }

    // /////////////////////////////////////////////////////////
    //   Solve the problem on the uniformly refined grid
    // /////////////////////////////////////////////////////////
    VectorType uniformX;
    compute(uniformGrid, uniformX, dirichletValues, coarseDirichletNodes,
            obsPatch, tolerance, numIt);

    // /////////////////////////////////////////////////////////
    //   Load coarse grids again
    // /////////////////////////////////////////////////////////

    std::unique_ptr<GridType> adaptiveGridPtr;

    if (paramBoundaries) {
        adaptiveGridPtr = AmiraMeshReader<GridType>::read(path + objectName, PSurfaceBoundary<dim-1>::read(path + parFile));
    } else {
        adaptiveGridPtr = std::make_unique<GridType>();
        //AmiraMeshReader<GridType>::read(adaptiveGrid[0], path + object0Name);
        //makeHalfCircleQuad(adaptiveGrid);
        //makeHalfCircle(adaptiveGrid);
        makeHalfCircleThreeTris(*adaptiveGridPtr);
        //makeSphere(adaptiveGrid[0], FieldVector<double,3>(0), 1);
    }

    GridType& adaptiveGrid = *adaptiveGridPtr;
    adaptiveGrid.setRefinementType(GridType::COPY);
    // /////////////////////////////////////////////////////
    // read field describing the obstacles again, so this time
    // it is attached to the adaptive grid.
    // /////////////////////////////////////////////////////

    obsPatch.setup(adaptiveGrid.levelGridView(0));
    readBoundaryPatch<GridType>(obsPatch, path + obsFilename);


    // /////////////////////////////////////////////////////////
    //   Refinement loop
    // /////////////////////////////////////////////////////////

    std::ofstream logfile("1badaptmeasure.statistics");

    VectorType adaptiveX;

    for (int i=0; i<maxLevel+1; i++) {

        // /////////////////////////////////////////////////////////////
        //   Solve problem on locally refined grid
        // /////////////////////////////////////////////////////////////
        compute(adaptiveGrid, adaptiveX, dirichletValues, coarseDirichletNodes,
                obsPatch, tolerance, numIt);

        // /////////////////////////////////////////////////////////////
        //   Compute 'actual' error by comparing the the solution
        //   on the uniformly refined grid
        // /////////////////////////////////////////////////////////////

        StVenantKirchhoffAssembler<GridType,P1NodalBasis<GridType::LeafGridView>::LocalFiniteElement, P1NodalBasis<GridType::LeafGridView>::LocalFiniteElement > p1LocalStiffness(E, nu);

        double actualError = std::sqrt(DifferenceNormSquared<GridType>::compute(uniformGrid, uniformX,
                                                                                adaptiveGrid, adaptiveX,
                                                                                &p1LocalStiffness));

        // /////////////////////////////////////////////////////////////
        //   Estimate the error with an a posteriori estimator
        // /////////////////////////////////////////////////////////////

        // ////////////////////////////////////////////////////
        //    Estimate error per element
        // ////////////////////////////////////////////////////
#define ZOU_KH_ESTIMATOR
#ifndef ZOU_KH_ESTIMATOR
        HierarchicContactEstimator<GridType> estimator(adaptiveGrid);

        estimator.couplings_[0].obsPatch_    = &obsPatch;
        estimator.couplings_[0].obsDistance_ = obsDistance;
        estimator.couplings_[0].type_        = CouplingPairBase::RIGID;
        estimator.couplings_[0].obsFunction_ = obstacle;
#else
        LeafBoundary leafObsPatch(adaptiveGrid.leafGridView());
        BoundaryPatchProlongator<GridType>::prolong(obsPatch, leafObsPatch);

        ZouKornhuberSignoriniEstimator<GridType> estimator(adaptiveGrid,
                                                           leafObsPatch,
                                                           obstacle);
#endif

        LevelBoundary dirichletBoundary(adaptiveGrid.levelGridView(0), coarseDirichletNodes);

        LeafBoundary leafDirichletBoundary(adaptiveGrid.leafGridView());
        BoundaryPatchProlongator<GridType>::prolong(dirichletBoundary, leafDirichletBoundary);

        RefinementIndicator<GridType> refinementIndicator(adaptiveGrid);

#ifdef ZOU_KH_ESTIMATOR
        StVenantKirchhoffAssembler<GridType,ZouKornhuberBasis<GridType::LeafGridView>::LocalFiniteElement, ZouKornhuberBasis<GridType::LeafGridView>::LocalFiniteElement > estimatorLocalStiffness(E, nu);
#else
        StVenantKirchhoffAssembler<GridType,P2NodalBasis<GridType::LeafGridView>::LocalFiniteElement, P2NodalBasis<GridType::LeafGridView>::LocalFiniteElement > estimatorLocalStiffness(E, nu);

#endif

        estimator.estimate(adaptiveX,
                           &leafDirichletBoundary,
                           refinementIndicator,
                           &estimatorLocalStiffness);

       // ////////////////////////////////////////////////////////////
       //   Estimate estimator efficiency
       // ////////////////////////////////////////////////////////////

       double estimatedError = std::sqrt(estimator.getErrorSquared(0, &estimatorLocalStiffness));

       std::cout << "Actual error: " << actualError << "     "
                 << "estimated error: " << estimatedError << "     "
                 << "estimator efficiency: " << estimatedError/actualError << "   "
                 << "vertices: " << adaptiveGrid.size(dim)
                 << std::endl;

       logfile << actualError << "     " << estimatedError
               << "     " << estimatedError/actualError
               << "     " << adaptiveGrid.size(dim) << std::endl;

       if (i==maxLevel)
           break;

       // ////////////////////////////////////////////////////////
       //   Refine the grids and transfer the current solution
       // ////////////////////////////////////////////////////////

       FractionalMarkingStrategy<GridType>::mark(refinementIndicator, adaptiveGrid, refinementFraction);

       P1NodalBasis<GridType::LeafGridView,double> p1Basis(adaptiveGrid.leafGridView());
       GridFunctionAdaptor<P1NodalBasis<GridType::LeafGridView,double> > adaptor(p1Basis,true,true);

        adaptiveGrid.preAdapt();
        adaptiveGrid.adapt();
        adaptiveGrid.postAdapt();

        p1Basis.update();
        adaptor.adapt(adaptiveX);

        if (paramBoundaries)
            improveGrid(adaptiveGrid,10);

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        std::cout << "  Grid: 0   Level: " << adaptiveGrid.maxLevel()
                  << "   vertices: " << adaptiveGrid.size(adaptiveGrid.maxLevel(), dim)
                  << "   elements: " << adaptiveGrid.size(adaptiveGrid.maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

    }

    // Output result
    LeafAmiraMeshWriter<GridType> amiramesh;
    amiramesh.addLeafGrid(adaptiveGrid);
    amiramesh.addVertexData(adaptiveX, adaptiveGrid.leafGridView());
    amiramesh.write("1badaptmeasure.result");

} catch (Exception e) {

    std::cout << e << std::endl;

 }
