// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:

#include <dune/fufem/boundarypatchprolongator.hh>

namespace Dune {
namespace Contact {

template <class GridType, class field_type>
void OneBodyAssembler<GridType,field_type>::assembleObstacles()
{

    BoundaryPatchProlongator<GridType>::prolong(*coarseObsPatch_, obsPatch_);
    std::cout << "--- " << obsPatch_.numVertices() << " obstacles found on leaf level! ---" << std::endl;

    auto normals = obsPatch_.getNormals();

    const auto& leafView = grid_->leafGridView();
    const auto& indexSet = leafView.indexSet();

    // Compute local coordinate systems for the dofs with obstacles
    this->computeLocalCoordinateSystems(obsPatch_, this->localCoordSystems_, normals);
    obstacles_.resize(grid_->size(dim));

    for (const auto& v : vertices(leafView)) {

        int idx = indexSet.index(v);
        if (!obsPatch_.containsVertex(idx))
            continue;

        // the first row of the householder reflection contains the search direction
        const auto& obsDir = this->localCoordSystems_[idx][0];
        obstacles_[indexSet.index(v)].upper(0) = obstacleFunction_(v.geometry().corner(0),obsDir);
        if (obstacles_[indexSet.index(v)].upper(0)<-overlap_)
            obstacles_[indexSet.index(v)].upper(0) = std::numeric_limits<field_type>::max();
    }

}

} /* namespace Contact */
} /* namespace Dune */
