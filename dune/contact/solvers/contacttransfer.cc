// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <dune/common/fvector.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/matrix-vector/blockmatrixview.hh>
#include <dune/fufem/staticmatrixtools.hh>

#include <dune/solvers/transferoperators/genericmultigridtransfer.hh>

namespace Dune {
namespace Contact {

template<class VectorType>
template<class GridType>
void ContactMGTransfer<VectorType>::setup(const GridType& grid, int cL, int fL,
                                                   const CoordSystemVector& coarseLocalCoordSystems,
                                                   const CoordSystemVector& fineLocalCoordSystems)
{

    // Create standard prolongation matrix
    TruncatedDenseMGTransfer<VectorType>::setup(grid, cL, fL);

    // ///////////////////////////////////////////////////////////////
    // Transform matrix to account for the local coordinate systems
    // ///////////////////////////////////////////////////////////////

    typedef typename OperatorType::row_type RowType;
    typedef typename RowType::Iterator ColumnIterator;

    for(size_t rowIdx=0; rowIdx<this->matrix_.N(); rowIdx++) {

        RowType& row = this->matrix_[rowIdx];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt) {

            // The first multiplication should really be transposed, but we know that
            // the matrix is symmetric anyways
            cIt->leftmultiply(fineLocalCoordSystems[rowIdx]);
            cIt->rightmultiply(coarseLocalCoordSystems[cIt.index()]);

        }

    }

}


/** \todo This method is not very memory efficient! */
template<class VectorType>
template<class GridType0, class GridType1>
void ContactMGTransfer<VectorType>::setup(const GridType0& grid0, const GridType1& grid1,
                                            int cL, int fL,
                                            const MatrixType& transferOperator,
                                            const CoordSystemVector& coarseLocalCoordSystems,
                                            const CoordSystemVector& fineLocalCoordSystems,
                                            const BitSetVector<1>& coarseHasObstacle,
                                            const BitSetVector<1>& fineHasObstacle)
{
    // if the max levels are not equal we make a virtual copy of the grid
    // if they are not equal, then the dualmortarhierarchy already took care of the mortar matrices
    int cL0 = std::min(grid0.maxLevel(),cL);
    int fL0 = std::min(grid0.maxLevel(),fL);

    // make identity matrix
    MatrixType subMat0;
    if (cL0==fL0) {
        MatrixIndexSet indexSet(grid0.size(fL0,blocksize),grid0.size(fL0,blocksize));
        indexSet.exportIdx(subMat0);
        subMat0=0;

        for (int i=0; i<grid0.size(fL0,blocksize); i++)
            StaticMatrix::addToDiagonal(subMat0[i][i],1.0);

    } else
        // Create standard prolongation matrix
        GenericMultigridTransfer::setup<MatrixType,GridType0,typename VectorType::field_type>(subMat0, grid0, cL, fL);

    // ///////////////////////////////////////////////////////////////
    // Transform matrix to account for the local coordinate systems
    // ///////////////////////////////////////////////////////////////

    typedef typename OperatorType::row_type RowType;
    typedef typename RowType::Iterator ColumnIterator;
    typedef typename RowType::ConstIterator ConstIterator;

    for(size_t rowIdx=0; rowIdx<subMat0.N(); rowIdx++) {

        RowType& row = subMat0[rowIdx];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt) {

            cIt->leftmultiply(fineLocalCoordSystems[rowIdx]);
            cIt->rightmultiply(coarseLocalCoordSystems[cIt.index()]);

        }

    }

    // if the max levels are not equal we make a virtual copy of the grid
    // if they are not equal, then the dualmortarhierarchy already took care of the mortar matrices
    int cL1 = std::min(grid1.maxLevel(),cL);
    int fL1 = std::min(grid1.maxLevel(),fL);

    // Set up second sub matrix
    // As only the first grid carries obstacles the second one doesn't
    // need local coordinate transformations.

    // make identity matrix
    MatrixType subMat1;
    if (cL1==fL1) {
        MatrixIndexSet indexSet(grid1.size(fL1,blocksize),grid0.size(fL1,blocksize));
        indexSet.exportIdx(subMat1);
        subMat1=0;

        for (int i=0; i<grid1.size(fL1,blocksize); i++)
            StaticMatrix::addToDiagonal(subMat1[i][i],1.0);

    } else
        // Create standard prolongation matrix
        GenericMultigridTransfer::setup<MatrixType,GridType1,typename VectorType::field_type>(subMat1, grid1, cL, fL);

    // ////////////////////////////////////////////////////////
    //   Compute the modification \f$(I_l^{l+1}_{is} M_l \f$
    // ////////////////////////////////////////////////////////

    //  Global means:  counting all the degrees of freedom
    //  Local means:  counting only those dofs on the nonmortar boundary
    std::vector<int> coarseGlobalToLocal(coarseHasObstacle.size());
    int idx = 0;
    for (unsigned int i=0; i<coarseHasObstacle.size(); i++) {

        coarseGlobalToLocal[i] = (coarseHasObstacle[i][0]) ? idx : -1;

        if (coarseHasObstacle[i][0])
            idx++;

    }

    MatrixIndexSet imIndexSet(subMat0.N(), subMat1.M());

    for (size_t i=0; i<imIndexSet.rows(); i++) {

        if (fineHasObstacle[i][0])
            continue;

        const RowType& subMatRow = subMat0[i];

        ConstIterator sMIt    = subMatRow.begin();
        ConstIterator sMEndIt = subMatRow.end();

        for (; sMIt!=sMEndIt; ++sMIt) {

            if (!coarseHasObstacle[sMIt.index()][0])
                continue;

            const RowType& row = transferOperator[coarseGlobalToLocal[sMIt.index()]];

            ConstIterator cIt    = row.begin();
            ConstIterator cEndIt = row.end();

            for (; cIt!=cEndIt; ++cIt)
                imIndexSet.add(i, cIt.index());

        }

    }

    MatrixType IM(subMat0.N(), subMat1.M(), BCRSMatrix<MatrixBlock>::random);
    imIndexSet.exportIdx(IM);
    IM = 0;

    for (size_t i=0; i<IM.N(); i++) {

         if (fineHasObstacle[i][0])
                continue;

        const RowType& subMatRow = subMat0[i];

        ConstIterator sMIt    = subMatRow.begin();
        ConstIterator sMEndIt = subMatRow.end();

        for (; sMIt!=sMEndIt; ++sMIt) {

            if (!coarseHasObstacle[sMIt.index()][0])
                continue;

            const RowType& row = transferOperator[coarseGlobalToLocal[sMIt.index()]];

            ConstIterator cIt    = row.begin();
            ConstIterator cEndIt = row.end();

            for (; cIt!=cEndIt; ++cIt) {

                MatrixBlock prod = *cIt;
                // The prolongation matrix entry in sMIt maps onto a dim x dim space spanned by a
                // *local* coordinate system.  However, the transferoperator is set up with respect to
                // canonical coordinates everywhere.  Therefore we have to put the transformation
                // from local to canonical coordinates into the product.  This explains the following line
                // we only have to multiply with the coarse local system because the the row has no fine obs
                // thus fineLocalCoord at this dof is identity
                prod.leftmultiply(coarseLocalCoordSystems[sMIt.index()]);

                prod.leftmultiply(*sMIt);
                IM[i][cIt.index()] += prod;
            }

        }

    }

    // ///////////////////////////////////
    //   Combine the two submatrices
    // ///////////////////////////////////
    int nRows = subMat0.N() + subMat1.N();
    int nCols = subMat0.M() + subMat1.M();

    MatrixIndexSet totalIndexSet(nRows, nCols);
    totalIndexSet.import(subMat0);
    totalIndexSet.import(subMat1, subMat0.N(), subMat0.M());

    // Add the indices of the modification
    /** \todo Use the existing index set here! */
    for (size_t i=0; i<IM.N(); i++) {

            const RowType& row = IM[i];

            ConstIterator cIt    = row.begin();
            ConstIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt)
                totalIndexSet.add(i, cIt.index()+subMat0.M());

    }

    MatrixType totalMat(nRows, nCols, MatrixType::random);
    totalIndexSet.exportIdx(totalMat);
    totalMat = 0;

    // copy first matrix
    for(size_t rowIdx=0; rowIdx<subMat0.N(); rowIdx++) {

        RowType& row = subMat0[rowIdx];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            totalMat[rowIdx][cIt.index()] = *cIt;

    }

    // copy second matrix
    for(size_t rowIdx=0; rowIdx<this->matrix_.N(); rowIdx++) {

        RowType& row = subMat1[rowIdx];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            totalMat[rowIdx+subMat0.N()][cIt.index()+subMat0.M()] = *cIt;

    }

    // Add the values of the modification
    for (size_t i=0; i<IM.N(); i++) {

            const RowType& row = IM[i];

            ConstIterator cIt    = row.begin();
            ConstIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt)
                totalMat[i][cIt.index()+subMat0.M()] += *cIt;

    }

    this->matrix_ = totalMat;

}


/** \todo This method is not very memory efficient! */
template<class VectorType>
template<class GridType>
void ContactMGTransfer<VectorType>::setup(const std::vector<const GridType*>& grids, int cL, int fL,
               const std::vector<const CoordSystemVector*>& coarseLocalCoordSystems,
               const std::vector<const CoordSystemVector*>& fineLocalCoordSystems,
               const std::vector<const BitSetVector<1>*>& coarseHasObstacle,
               const std::vector<const BitSetVector<1>*>& fineHasObstacle,
               const std::vector<const MatrixType*>& mortarTransferOps,
               const std::vector<std::array<int,2> >& gridIdx)
{
    const int nGrids = grids.size();
    const int nCouplings = mortarTransferOps.size();

    // ////////////////////////////////////////////////////////////
    //   Create the standard prolongation matrices for each grid
    // ////////////////////////////////////////////////////////////

    std::vector<TruncatedDenseMGTransfer<VectorType>* > gridTransfer(nGrids);
    std::vector<const MatrixType*> submat(nGrids);

    for (int i=0; i<nGrids; i++) {

        gridTransfer[i] = new TruncatedDenseMGTransfer<VectorType>;

        if (fL <= grids[i]->maxLevel()) {
            gridTransfer[i]->setup(*grids[i], cL, fL);
            submat[i] =&gridTransfer[i]->getMatrix();
        } else {
            // If the transfer operator corresponds to a level that is larger than the max level
            // then we make a virtual copy of that level by using the identity
            MatrixType* mat = new MatrixType;
            MatrixIndexSet indexSet(grids[i]->size(blocksize),grids[i]->size(blocksize));
            for (int j=0; j<grids[i]->size(blocksize); j++)
                indexSet.add(j,j);

            indexSet.exportIdx(*mat);

            for (int j=0; j<grids[i]->size(blocksize); j++)
                StaticMatrix::addToDiagonal((*mat)[j][j],1.0);

            submat[i] = mat;
        }
    }

    // ///////////////////////////////////////////////////////////////
    // Transform matrix to account for the local coordinate systems
    // ///////////////////////////////////////////////////////////////

    // TODO: Also transform possible identity matrices?
    for (int j=0; j<nCouplings; j++) {

            //for (int i=0; i<nGrids;i++)
            for(size_t rowIdx=0; rowIdx<submat[gridIdx[j][0]]->N(); rowIdx++) {

                MatrixType& mat = *const_cast<MatrixType*>(submat[gridIdx[j][0]]);
                RowType& row =  mat[rowIdx];

                ColumnIterator cIt    = row.begin();
                ColumnIterator cEndIt = row.end();

                for(; cIt!=cEndIt; ++cIt) {

                    if ((*fineHasObstacle[j])[rowIdx][0])
                        cIt->leftmultiply((*fineLocalCoordSystems[j])[rowIdx]);
                    //cIt->leftmultiply(fineLocalCoordSystems[blockView.row(i,rowIdx)]);
                    if ((*coarseHasObstacle[j])[cIt.index()][0])
                        cIt->rightmultiply((*coarseLocalCoordSystems[j])[cIt.index()]);
                    //cIt->rightmultiply(coarseLocalCoordSystems[blockView.col(i,cIt.index())]);
                }

            }
    }


    // ////////////////////////////////////////
    //   Create the index set
    // ////////////////////////////////////////

    Dune::MatrixVector::BlockMatrixView<MatrixType> blockView(submat);

    int nRows = blockView.nRows();
    int nCols = blockView.nCols();

    MatrixIndexSet indices(nRows, nCols);

    for (int i=0; i<nGrids; i++)
        indices.import(*submat[i], blockView.row(i,0), blockView.col(i,0));

    // ////////////////////////////////////////////////////////
    //   Compute the modifications \f$(I_l^{l+1}_{is} M_l \f$
    // ////////////////////////////////////////////////////////

    for (int i=0; i<nCouplings; i++) {

        //  Global means:  counting all the degrees of freedom
        //  Local means:  counting only those dofs on the nonmortar boundary
        std::vector<int> coarseGlobalToLocal(coarseHasObstacle[i]->size());
        int idx = 0;
        for (size_t j=0; j<coarseHasObstacle[i]->size(); j++) {

            coarseGlobalToLocal[j] = ((*coarseHasObstacle[i])[j][0]) ? idx : -1;

            if ((*coarseHasObstacle[i])[j][0])
                idx++;

        }

        for (size_t j=0; j<submat[gridIdx[i][0]]->N(); j++) {

            if ((*fineHasObstacle[i])[j][0])
                continue;

            const RowType& subMatRow = (*submat[gridIdx[i][0]])[j];

            ConstIterator sMIt    = subMatRow.begin();
            ConstIterator sMEndIt = subMatRow.end();

            for (; sMIt!=sMEndIt; ++sMIt) {

                if (!(*coarseHasObstacle[i])[sMIt.index()][0])
                    continue;

                const RowType& row = (*mortarTransferOps[i])[coarseGlobalToLocal[sMIt.index()]];

                ConstIterator cIt    = row.begin();
                ConstIterator cEndIt = row.end();

                for (; cIt!=cEndIt; ++cIt)
                    indices.add(blockView.row(gridIdx[i][0],j), blockView.col(gridIdx[i][1], cIt.index()));

            }

        }

    }

    // /////////////////////////////////////////////////////////////////////
    //   Actually assemble the matrix
    // /////////////////////////////////////////////////////////////////////

    indices.exportIdx(this->matrix_);
    this->matrix_ = 0;

    // The individual matrices
    for (int i=0; i<nGrids; i++) {

        for(size_t rowIdx=0; rowIdx<submat[i]->N(); rowIdx++) {

            const RowType& row = (*submat[i])[rowIdx];

            ConstIterator cIt    = row.begin();
            ConstIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt)
                this->matrix_[blockView.row(i,rowIdx)][blockView.col(i, cIt.index())] = *cIt;

        }

    }


    // The couplings
    for (int i=0; i<nCouplings; i++) {

        //  Global means:  counting all the degrees of freedom
        //  Local means:  counting only those dofs on the nonmortar boundary
        /** \todo Don't compute this twice! */
        std::vector<int> coarseGlobalToLocal(coarseHasObstacle[i]->size());
        int idx = 0;
        for (size_t j=0; j<coarseHasObstacle[i]->size(); j++) {

            coarseGlobalToLocal[j] = ((*coarseHasObstacle[i])[j][0]) ? idx : -1;

            if ((*coarseHasObstacle[i])[j][0])
                idx++;

        }

        for (size_t j=0; j<submat[gridIdx[i][0]]->N(); j++) {

            if ((*fineHasObstacle[i])[j][0])
                continue;

            const RowType& subMatRow = (*submat[gridIdx[i][0]])[j];

            ConstIterator sMIt    = subMatRow.begin();
            ConstIterator sMEndIt = subMatRow.end();

            for (; sMIt!=sMEndIt; ++sMIt) {

                if (!(*coarseHasObstacle[i])[sMIt.index()][0])
                    continue;

                const RowType& row = (*mortarTransferOps[i])[coarseGlobalToLocal[sMIt.index()]];

                ConstIterator cIt    = row.begin();
                ConstIterator cEndIt = row.end();

                for (; cIt!=cEndIt; ++cIt) {

                    MatrixBlock prod = *cIt;
                    // The prolongation matrix entry in sMIt maps onto a dim x dim space spanned by a
                    // *local* coordinate system.  However, the transferoperator is set up with respect to
                    // canonical coordinates everywhere.  Therefore we have to put the transformation
                    // from local to canonical coordinates into the product.  This explains the following line
                    prod.leftmultiply((*coarseLocalCoordSystems[i])[sMIt.index()]);

                    prod.leftmultiply(*sMIt);
                    this->matrix_[blockView.row(gridIdx[i][0],j)][blockView.col(gridIdx[i][1], cIt.index())] += prod;
                }

            }

        }

    }

    // Delete separate transfer objects
    for (int i=0; i<nGrids; i++) {
        delete(gridTransfer[i]);
        if (fL > grids[i]->maxLevel())
            delete(submat[i]);
    }
}

} /* namespace Contact */
} /* namespace Dune */
