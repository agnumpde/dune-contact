// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_COMMON_FILTER_HH
#define DUNE_CONTACT_COMMON_FILTER_HH

#include <set>
#include <utility>

namespace Dune {
namespace Contact {

/** \brief A filter denotes a set of tuples representing the energy and infeasibility of points.
 *         It can be used to compare iterates in nonlinear constrained optimization and is used
 *         to extend Trust-Region methods to constrained optimization.
 */
template <class field_type=double>
class Filter {

  using FilterPair = std::pair<field_type, field_type>;
public:
  Filter(field_type filterTol = 1e-4) :
    Filter(10.0, -std::numeric_limits<field_type>::max(),filterTol)
  {}

  Filter(field_type infeasibility, field_type energy, field_type filterTol = 1e-4) :
    filterTol_(filterTol)
  {
    filter_.insert({infeasibility, energy});
  }

  Filter& operator=(const Filter& other) = default;
  Filter& operator=(Filter&& other) = default;

  //! Check if pair is acceptable to the filter
  bool acceptable(field_type infeasibility, field_type energy) const {

    FilterPair testPair{infeasibility, energy};
    for (const auto& it : filter_)
      if (firstDominates(it, testPair))
        return false;
    return true;
  }

  //! Check if pair is acceptable to the filter, plus one extra pair
  bool acceptable(const FilterPair& additional, field_type infeasibility, field_type energy) const {

    if (firstDominates(additional, {infeasibility, energy}))
      return false;
    return acceptable(infeasibility, energy);
  }

  //! Print the filter
  void print() const {
    std::cout << "Filter pairs (Infeasibility, Energy) : \n";
    for (const auto& it : filter_)
      std::cout << "(" << it.first <<", " << it.second<<")\n";
  }

  //! Add pair to the filter and remove all dominated ones
  void add(field_type infeasibility, field_type energy) {

    FilterPair newPair{infeasibility, energy};
    for (auto&& it : filter_)
      if (firstDominates(newPair, it))
        filter_.erase(it);
    filter_.insert(newPair);
  }

  //! Empty the filter
  void clear() { filter_.clear(); }

private:
  //! Test if the first pair dominates the second one
  bool firstDominates(const FilterPair& p1, const FilterPair& p2) const {
    return (p1.first * (1-filterTol_) <= p2.first) and
        (p1.second <= p2.second + filterTol_*p2.first);
  }

  //! The set of pairs that are contained in the filter
  std::set<FilterPair> filter_;
  //! All acceptable pairs within a small envelope are also dismissed - this is critical for global convergence
  field_type filterTol_;
};

} /* namespace Contact */
} /* namespace Dune */

#endif
