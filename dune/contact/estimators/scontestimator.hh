#ifndef SIMPLE_CONTACT_ESTIMATOR
#define SIMPLE_CONTACT_ESTIMATOR

#include <dune/common/bitsetvector.hh>
#include <dune/istl/bcrsmatrix.hh>

namespace Dune {
namespace Contact {

template <class GridType>
class SimpleContactEstimator {

public:

    /** \brief Mark all elements for refinement which are involved in a contact coupling

    \param depth Also mark elements which are 'depth' neighbors away from a contact element
     */
    template <class MatrixBlock>
    void estimate(GridType& grid0, GridType& grid1,
                  const Dune::BitSetVector<1>& nonmortarContact,
                  const Dune::BCRSMatrix<MatrixBlock>& transferOperator,
                  int depth);

};

template <class GridType>
template <class MatrixBlock>
void SimpleContactEstimator<GridType>::estimate(GridType& grid0, GridType& grid1,
                                                const Dune::BitSetVector<1>& nonmortarContact,
                                                const Dune::BCRSMatrix<MatrixBlock>& transferOperator,
                                                int depth)
{
    const int maxlevel = grid0.maxLevel();
    const int dim = GridType::dimension;

    typedef typename GridType::template Codim<0>::Entity EntityType;
    typedef typename EntityType::LevelIntersectionIterator NeighborIterator;

    const typename GridType::Traits::LevelIndexSet& indexSet0 = grid0.levelIndexSet(maxlevel);
    const typename GridType::Traits::LevelIndexSet& indexSet1 = grid1.levelIndexSet(maxlevel);

    // Create list of all mortar contact vertices
    Dune::BitSetVector<1> mortarContact(grid1.size(maxlevel, dim), false);

    Dune::BitSetVector<1> markedElements0(indexSet0.size(0),false);
    Dune::BitSetVector<1> markedElements1(indexSet1.size(0),false);

    typedef typename Dune::BCRSMatrix<MatrixBlock>::row_type RowType;
    typedef typename RowType::ConstIterator ColumnIterator;

    for (int i=0; i<transferOperator.N(); i++) {

        const RowType& row = transferOperator[i];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            mortarContact[cIt.index()] = true;

    }

    // Mark all elements that contain at least one contact node
    typedef typename GridType::template Codim<0>::LevelIterator ElementIterator;

    ElementIterator eIt    = grid0.template lbegin<0>(maxlevel);
    ElementIterator eEndIt = grid0.template lend<0>(maxlevel);

    for (; eIt!=eEndIt; ++eIt) {

        for (int i=0; i<eIt->template count<dim>(); i++)
            if (nonmortarContact[indexSet0.template subIndex<dim>(*eIt,i)]) {
                //grid0.mark(1,eIt);
                markedElements0[indexSet0.index(*eIt)] = true;
                break;
            }
    }

    eIt    = grid1.template lbegin<0>(maxlevel);
    eEndIt = grid1.template lend<0>(maxlevel);

    for (; eIt!=eEndIt; ++eIt) {

        for (int i=0; i<eIt->template count<dim>(); i++)
            if (mortarContact[indexSet1.template subIndex<dim>(*eIt,i)]) {
                //grid1.mark(1,eIt);
                markedElements1[indexSet1.index(*eIt)] = true;
                break;
            }
    }

    // ///////////////////////////////////////////////////////
    //   Mark the neighbors of marked elements
    // ///////////////////////////////////////////////////////

    for (int i=0; i<depth; i++) {

        Dune::BitSetVector<1> oldMarkedElements0 = markedElements0;
        Dune::BitSetVector<1> oldMarkedElements1 = markedElements1;

        // first for grid0
        eIt    = grid0.template lbegin<0>(maxlevel);
        eEndIt = grid0.template lend<0>(maxlevel);

        for (; eIt!=eEndIt; ++eIt) {

            NeighborIterator nIt    = eIt->ilevelbegin();
            NeighborIterator nEndIt = eIt->ilevelend();

            // Loop over all neighbors
            for (; nIt!=nEndIt; ++nIt)
                if (nIt.neighbor()
                    && oldMarkedElements0[indexSet0.index(*nIt.outside())])
                    markedElements0[indexSet0.index(*eIt)] = true;

        }

        // then for grid1
        eIt    = grid1.template lbegin<0>(maxlevel);
        eEndIt = grid1.template lend<0>(maxlevel);

        for (; eIt!=eEndIt; ++eIt) {

            NeighborIterator nIt    = eIt->ilevelbegin();
            NeighborIterator nEndIt = eIt->ilevelend();

            // Loop over all neighbors
            for (; nIt!=nEndIt; ++nIt)
                if (nIt.neighbor()
                    && oldMarkedElements1[indexSet1.index(*nIt.outside())])
                    markedElements1[indexSet1.index(*eIt)] = true;

        }

    }

    // ////////////////////////////////////////////////////////
    //   Actually refine the elements
    // ////////////////////////////////////////////////////////

    // first for grid0
    eIt    = grid0.template lbegin<0>(maxlevel);
    eEndIt = grid0.template lend<0>(maxlevel);

    for (; eIt!=eEndIt; ++eIt)
        if (markedElements0[indexSet0.index(*eIt)])
            grid0.mark(1,eIt);

    // then for grid1
    eIt    = grid1.template lbegin<0>(maxlevel);
    eEndIt = grid1.template lend<0>(maxlevel);

    for (; eIt!=eEndIt; ++eIt)
        if (markedElements1[indexSet1.index(*eIt)])
            grid1.mark(1,eIt);


    printf("%d resp. %d elements were marked on grids 0 and 1\n",
           markedElements0.count(), markedElements1.count());

}

} /* namespace Contact */
} /* namespace Dune */

#endif
