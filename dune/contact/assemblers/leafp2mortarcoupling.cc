#include <dune/common/exceptions.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>

#include <dune/istl/matrixindexset.hh>

#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/prolongboundarypatch.hh>

#include <dune/grid-glue/merging/contactmerge.hh>
#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

namespace Dune {
namespace Contact {

template <class field_type, class GridType0, class GridType1>
template <class Basis0, class Basis1>
void LeafP2MortarCoupling<field_type, GridType0, GridType1>::setup(const BasisGridFunction<Basis0, VectorType0>& sol0,
                                                                  const BasisGridFunction<Basis1, VectorType1>& sol1)
{
  using namespace Dune;

  // ///////////////////////////////////////////////////////
  // Determine the contact boundarys on the leaf level
  // ///////////////////////////////////////////////////////

  typedef typename GridType0::LeafGridView GridView0;
  typedef typename GridType1::LeafGridView GridView1;

  const GridView0& gridView0 = nonmortarBoundary_.gridView();
  const GridView1& gridView1 = mortarBoundary_.gridView();

  typedef Dune::GridGlue::Codim1Extractor<GridView0> Extractor0;
  typedef Dune::GridGlue::Codim1Extractor<GridView1> Extractor1;

  typedef Dune::GridGlue::GridGlue<Extractor0,Extractor1> GlueType;

  using Element0 = typename GridView0::template Codim<0>::Entity;
  using Element1 = typename GridView1::template Codim<0>::Entity;

  auto desc0 = [&] (const Element0& e, unsigned int face) -> bool {
    return nonmortarBoundary_.contains(e,face);
  };

  auto desc1 = [&] (const Element1& e, unsigned int face) -> bool {
    return mortarBoundary_.contains(e,face);
  };

  auto extract0 = std::make_shared<Extractor0>(gridView0,desc0);
  auto extract1 = std::make_shared<Extractor1>(gridView1,desc1);

  if (!this->gridGlueBackend_)
    this->gridGlueBackend_ = std::make_shared< Dune::GridGlue::ContactMerge<dimworld, ctype> >(0.4);
  GlueType glue(extract0, extract1, this->gridGlueBackend_);
  glue.build();

  // Get all fine grid boundary segments that are totally covered by the grid-glue segments
  using Pair = std::pair<int,int>;
  std::map<Pair,ctype> coveredArea, fullArea;

  // initialize with area of boundary faces
  const auto& indexSet0 = gridView0.indexSet();
  for (const auto& bIt : nonmortarBoundary_) {
    const Pair p(indexSet0.index(bIt.inside()),bIt.indexInInside());
    fullArea[p] = bIt.geometry().volume();
    coveredArea[p] = 0;
  }

  // sum up the remote intersection areas to find out which are totally covered
  for (const auto& rIs : intersections(glue))
    coveredArea[Pair(indexSet0.index(rIs.inside()),rIs.indexInInside())] += rIs.geometry().volume();

  // add all fine grid faces that are totally covered by the contact mapping
  nonmortarBoundary_.setup(gridView0, false);
  for (const auto& bIt : nonmortarBoundary_) {
    const auto& inside = bIt.inside();
    if(coveredArea[Pair(indexSet0.index(inside),bIt.indexInInside())]/
       fullArea[Pair(indexSet0.index(inside),bIt.indexInInside())] >= this->coveredArea_)
      nonmortarBoundary_.addFace(inside, bIt.indexInInside());
  }

  mortarBoundary_.setup(gridView1, false);
  for (const auto& rIs : intersections(glue))
    if (nonmortarBoundary_.contains(rIs.inside(),rIs.indexInInside()))
      mortarBoundary_.addFace(rIs.outside(),rIs.indexInOutside());

  // get the bases
  const auto& basis0 = sol0.basis();
  const auto& basis1 = sol1.basis();

  // caches for storing which dofs are belonging to which faces
  using DofCache = std::map<Dune::GeometryType, std::vector<std::vector<int> > >;
  DofCache dofsPerFaceCache0, dofsPerFaceCache1;

  // lambda that either returns the dof indices from the cache or computes them
  auto dofs0 = [&] (const typename Basis0::LocalFiniteElement& lfe, int faceIdx) {
    if (dofsPerFaceCache0.find(lfe.type())!=dofsPerFaceCache0.end())
      return dofsPerFaceCache0.find(lfe.type())->second[faceIdx];

    const auto& refElement = Dune::ReferenceElements<ctype, dim>::general(lfe.type());

    std::vector<std::vector<int> > newVector(refElement.size(1));
    auto it = dofsPerFaceCache0.insert(std::make_pair(lfe.type(), newVector));

    const auto& coeff = lfe.localCoefficients();
    for (size_t j=0; j<coeff.size(); j++) {

      // Loop over all faces
      for (int k=0; k<refElement.size(1); k++)
        if (isOnFace(lfe.type(), coeff.localKey(j).subEntity(), coeff.localKey(j).codim(),k))
          it.first->second[k].push_back(j);
    }
    return it.first->second[faceIdx];
  };

  auto dofs1 = [&] (const typename Basis1::LocalFiniteElement& lfe, int faceIdx) {
    if (dofsPerFaceCache1.find(lfe.type())!=dofsPerFaceCache1.end())
      return dofsPerFaceCache1.find(lfe.type())->second[faceIdx];

    const auto& refElement = Dune::ReferenceElements<ctype, dim>::general(lfe.type());

    std::vector<std::vector<int> > newVector(refElement.size(1));
    auto it = dofsPerFaceCache1.insert(std::make_pair(lfe.type(), newVector));

    const auto& coeff = lfe.localCoefficients();
    for (size_t j=0; j<coeff.size(); j++) {

      // Loop over all faces
      for (int k=0; k<refElement.size(1); k++)
        if (isOnFace(lfe.type(), coeff.localKey(j).subEntity(), coeff.localKey(j).codim(),k))
          it.first->second[k].push_back(j);
    }
    return it.first->second[faceIdx];
  };

  // ///////////////////////////////////////////////////////////////////
  //   Set up the two output matrices
  // ///////////////////////////////////////////////////////////////////

  Dune::MatrixIndexSet nonmortarIndices(basis0.size(), basis0.size());
  Dune::MatrixIndexSet mortarIndices(basis0.size(), basis1.size());

  // loop over all intersections and compute the matrix entries
  for (const auto& is : intersections(glue)) {

    if (!nonmortarBoundary_.contains(is.inside(), is.indexInInside()))
      continue;

    const auto& inside = is.inside();
    const auto& outside = is.outside();

    const auto& lagrangeDofs = dofs0(basis0.getLocalFiniteElement(inside),is.indexInInside());
    const auto& mortarDofs = dofs1(basis1.getLocalFiniteElement(outside),is.indexInOutside());


    for (size_t j=0; j<lagrangeDofs.size(); j++) {

      int rowIdx = basis0.index(inside, lagrangeDofs[j]);
      bool rowIsConstrained = basis0.isConstrained(rowIdx);
      const auto& rowConstraints = basis0.constraints(rowIdx);

      for (size_t k=0; k<lagrangeDofs.size(); k++) {

        int colIdx = basis0.index(inside, lagrangeDofs[k]);
        bool colIsConstrained = basis0.isConstrained(colIdx);
        const auto& colConstraints = basis0.constraints(colIdx);

        // setup nonmortar index pattern
        if (rowIsConstrained and colIsConstrained) {

          for (size_t rw=0; rw<rowConstraints.size(); ++rw)
            for (size_t cw=0; cw<colConstraints.size(); ++cw)
              nonmortarIndices.add(rowConstraints[rw].index, colConstraints[cw].index);

        } else if (rowIsConstrained) {

          for (size_t rw=0; rw<rowConstraints.size(); ++rw)
            nonmortarIndices.add(rowConstraints[rw].index, colIdx);

        } else if (colIsConstrained) {
          for (size_t cw=0; cw<colConstraints.size(); ++cw)
            nonmortarIndices.add(rowIdx, colConstraints[cw].index);

        } else
          nonmortarIndices.add(rowIdx, colIdx);
      }

      // mortar index pattern
      for (size_t k=0; k<mortarDofs.size(); k++) {

        int colIdx = basis1.index(outside, mortarDofs[k]);
        bool colIsConstrained = basis1.isConstrained(colIdx);
        const auto& colConstraints = basis1.constraints(colIdx);

        if (rowIsConstrained and colIsConstrained) {

          for (size_t rw=0; rw<rowConstraints.size(); ++rw)
            for (size_t cw=0; cw<colConstraints.size(); ++cw)
              mortarIndices.add(rowConstraints[rw].index, colConstraints[cw].index);

        } else if (rowIsConstrained) {

          for (size_t rw=0; rw<rowConstraints.size(); ++rw)
            mortarIndices.add(rowConstraints[rw].index, colIdx);

        } else if (colIsConstrained) {
          for (size_t cw=0; cw<colConstraints.size(); ++cw)
            mortarIndices.add(rowIdx, colConstraints[cw].index);

        } else
          mortarIndices.add(rowIdx, colIdx);
      }
    }
  }

  //   Enter all indices into the empty matrices
  nonmortarIndices.exportIdx(nonmortarLagrangeMatrix_);
  mortarIndices.exportIdx(mortarLagrangeMatrix_);

  mortarLagrangeMatrix_    = 0;
  nonmortarLagrangeMatrix_ = 0;

  this->weakObstacle_.resize(basis0.size());
  this->weakObstacle_            = 0;

  weakNormal_.resize(basis0.size());
  weakNormal_            = 0;

  // create quadrature rule key for the remote intersection and adjust order later
  QuadratureRuleKey remoteQuadKey(Dune::GeometryTypes::simplex(dim-1),1);

  // loop over all remote intersections and compute the matrix entries
  for (const auto& is : intersections(glue)) {

    if (!nonmortarBoundary_.contains(is.inside(), is.indexInInside()))
      continue;

    // compute integration element of remote intersection
    /** \todo Use the correct quadrature point */
    ctype integrationElement = is.geometry().integrationElement(Dune::FieldVector<ctype,dim-1>(0));

    const auto& inside = is.inside();
    const auto& outside = is.outside();

    // types of the elements supporting the boundary segments in question
    GeometryType nonmortarEType = inside.type();
    GeometryType mortarEType    = outside.type();

    const auto& lagrangeDofs = dofsPerFaceCache0.find(nonmortarEType)->second[is.indexInInside()];
    const auto& mortarDofs   = dofsPerFaceCache1.find(mortarEType)->second[is.indexInOutside()];

    // Shape functions on the supporting element.  We're only
    // going to use their traces on the boundary
    const auto& mortarLfe = basis1.getLocalFiniteElement(outside);
    const auto& nonmortarLfe = basis0.getLocalFiniteElement(inside);

    // Select a quadrature rule
    QuadratureRuleKey mQuad(mortarLfe);
    QuadratureRuleKey nmQuad(nonmortarLfe);
    remoteQuadKey.setOrder(mQuad.order()*nmQuad.order());
    const auto& quad = QuadratureRuleCache<ctype, dim-1>::rule(remoteQuadKey);

    const auto& geomInInside = is.geometryInInside();
    const auto& geomInOutside = is.geometryInOutside();
    const auto& geometry = is.geometry();
    const auto& geomOutside = is.geometryOutside();

    for (unsigned int l=0; l<quad.size(); l++) {

      // Local coordinates of the quadrature points with respect to the supporting element
      FieldVector<ctype,dim> nonmortarQuadPos = geomInInside.global(quad[l].position());
      FieldVector<ctype,dim> mortarQuadPos    = geomInOutside.global(quad[l].position());

      // ///////////////////////////////////////////////////////////////////
      //   Compute unit normal by using the contact mapping
      //   (which is a normal mapping)
      // ///////////////////////////////////////////////////////////////////

      // The current quadrature point in world coordinates
      FieldVector<ctype,dim> nonmortarQpWorld = geometry.global(quad[l].position());
      FieldVector<ctype,dim> mortarQpWorld    = geomOutside.global(quad[l].position());;

      FieldVector<ctype,dim> unitNormal = mortarQpWorld - nonmortarQpWorld;
      ctype distance = unitNormal.two_norm();
      unitNormal /= distance;

      std::vector<Dune::FieldVector<field_type,1> > nonmortarValues, mortarValues;
      nonmortarLfe.localBasis().evaluateFunction(nonmortarQuadPos, nonmortarValues);
      mortarLfe.localBasis().evaluateFunction(mortarQuadPos, mortarValues);

      // evaluate displacement
      Dune::FieldVector<field_type,dim> nonmortarQpSol, mortarQpSol;
      sol0.evaluateLocal(inside, nonmortarQuadPos, nonmortarQpSol);
      sol1.evaluateLocal(outside, mortarQuadPos,    mortarQpSol);

      Dune::FieldVector<ctype,dim> defect = mortarQpWorld + mortarQpSol;
      defect -= nonmortarQpWorld + nonmortarQpSol;
      field_type defectDistance = defect.two_norm();

      // loop over all Lagrange multiplier shape functions
      for (size_t j=0; j<lagrangeDofs.size(); j++) {

        int rowIdx = basis0.index(inside,lagrangeDofs[j]);
        bool rowIsConstrained = basis0.isConstrained(rowIdx);
        const auto& rowConstraints = basis0.constraints(rowIdx);

        // ////////////////////////////////////////////////////////////////////////////////////
        //   Part I: assemble the coupling of the nonmortar side
        // ////////////////////////////////////////////////////////////////////////////////////

        // loop over all shape functions on the mortar side

        for (size_t k=0; k<lagrangeDofs.size(); k++) {

          int colIdx = basis0.index(inside, lagrangeDofs[k]);
          bool colIsConstrained = basis0.isConstrained(colIdx);
          const auto& colConstraints = basis0.constraints(colIdx);

          field_type update = integrationElement * quad[l].weight()
              * nonmortarValues[lagrangeDofs[j]] * nonmortarValues[lagrangeDofs[k]];

          if (rowIsConstrained and colIsConstrained) {

            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
              for (size_t cw=0; cw<colConstraints.size(); ++cw)
                Dune::MatrixVector::addToDiagonal(nonmortarLagrangeMatrix_[rowConstraints[rw].index][colConstraints[cw].index],
                    rowConstraints[rw].factor * colConstraints[cw].factor*update);
          } else if (rowIsConstrained) {

            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
              Dune::MatrixVector::addToDiagonal(nonmortarLagrangeMatrix_[rowConstraints[rw].index][colIdx],
                  rowConstraints[rw].factor*update);
          } else if (colIsConstrained) {

            for (size_t cw=0; cw<colConstraints.size(); ++cw)
              Dune::MatrixVector::addToDiagonal(nonmortarLagrangeMatrix_[rowIdx][colConstraints[cw].index],
                  colConstraints[cw].factor*update);
          } else
            Dune::MatrixVector::addToDiagonal(nonmortarLagrangeMatrix_[rowIdx][colIdx],update);
        }

        // ////////////////////////////////////////////////////////////////////////////////////
        //   Part II: assemble the coupling between the mortar side and the mortar space
        // ////////////////////////////////////////////////////////////////////////////////////

        // loop over all shape functions on the mortar side
        for (size_t k=0; k<mortarDofs.size(); k++) {

          int colIdx = basis1.index(outside, mortarDofs[k]);
          bool colIsConstrained = basis1.isConstrained(colIdx);
          const auto& colConstraints = basis1.constraints(colIdx);

          field_type update = integrationElement * quad[l].weight()
              * nonmortarValues[lagrangeDofs[j]]
              * mortarValues[mortarDofs[k]];

          if (rowIsConstrained and colIsConstrained) {

            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
              for (size_t cw=0; cw<colConstraints.size(); ++cw)
                Dune::MatrixVector::addToDiagonal(mortarLagrangeMatrix_[rowConstraints[rw].index][colConstraints[cw].index],
                    rowConstraints[rw].factor * colConstraints[cw].factor*update);
          } else if (rowIsConstrained) {

            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
              Dune::MatrixVector::addToDiagonal(mortarLagrangeMatrix_[rowConstraints[rw].index][colIdx],
                  rowConstraints[rw].factor*update);
          } else if (colIsConstrained) {

            for (size_t cw=0; cw<colConstraints.size(); ++cw)
              Dune::MatrixVector::addToDiagonal(mortarLagrangeMatrix_[rowIdx][colConstraints[cw].index],
                  colConstraints[cw].factor*update);
          } else
            Dune::MatrixVector::addToDiagonal(mortarLagrangeMatrix_[rowIdx][colIdx],update);
        }

        // /////////////////////////////////////////////////////////////////////////
        //   Part III:  Compute the weak defect obstacle and the weak normal vector
        // /////////////////////////////////////////////////////////////////////////

        field_type obs = integrationElement * quad[l].weight() * nonmortarValues[lagrangeDofs[j]] * defectDistance;
        field_type scal = integrationElement * quad[l].weight()*nonmortarValues[lagrangeDofs[j]];

        if (rowIsConstrained) {
          for (size_t w=0; w<rowConstraints.size(); ++w) {
            this->weakObstacle_[rowConstraints[w].index][0] += rowConstraints[w].factor*obs;

            weakNormal_[rowConstraints[w].index].axpy(scal*rowConstraints[w].factor,unitNormal);
          }
        } else {
          this->weakObstacle_[rowIdx][0] += obs;
          weakNormal_[rowIdx].axpy(scal, unitNormal);
        }

      }

    }

  }

  // ////////////////////////////////////////////////////////////
  //   Normalize weak normal
  // ////////////////////////////////////////////////////////////
  for (size_t i=0; i<weakNormal_.size(); i++) {
    ctype norm = weakNormal_[i].two_norm();
    if (norm != 0)
      weakNormal_[i] /= norm;
  }
}

} /* namespace Contact */
} /* namespace Dune */
