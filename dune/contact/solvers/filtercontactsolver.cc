// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#include <limits>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>
#include <dune/fufem/boundarywriter.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/solvers/quadraticipopt.hh>

namespace Dune {
namespace Contact {

template<class ProblemType>
void FilterContactSolver<ProblemType>::solve()
{

  // clear the filter
  filter_.clear();

  // initialise trust--region
  field_type trustRegion = initTR_;

  auto& iterates = *iterates_;

  size_t totalSize(0);
  for (size_t i=0; i<iterates.size(); i++)
    totalSize += iterates[i].size();

  // If no Dirichlet conditions are prescribed we have to setup the initial correction
  if (correction_.size() != totalSize) {
    correction_.resize(totalSize);
    correction_ = 0;
  }

  // compute energy infeasibility of initial iterate and build the contact mapping
  energy_ = problem_->energy(iterates);
  infeasibility_ = problem_->infeasibility(iterates);

  // if there are non-homogeneous Dirichlet conditions,
  // then we have to adjust the initial infeasibility
  infeasibility_ = std::max(infeasibility_, correction_.infinity_norm());

  if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
    std::cout<< " Initial energy: " << energy_
             << " and infeasibility: " << infeasibility_ << std::endl;

  // initialise the iteration counter and error
  int it(0);
  field_type error = std::numeric_limits<field_type>::max();

  // create IpOpt solver
  QuadraticIPOptSolver<Matrix, Vector,JacobianType> ipoptSolver(localTol_, localMaxIterations_,
                                                                       Solver::QUIET, linearSolverType_);
  ipoptSolver.setIgnore(dirichletNodes_);

  // the trust--region constraints
  std::vector<BoxConstraint<field_type,dim> > trObs(correction_.size());

  // count the rejected steps
  int repeat(0);

  // Loop until desired tolerance or maximum number of iterations is reached
  for (; it<this->maxIterations_ && (error>this->tolerance_ or std::isnan(error)); it++)
  {

    if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL) {
      std::cout << "************************\n"
                << "*  Filter iteration " << it << "\n"
                << "************************\n";

    }

    if (this->verbosity_ == Solver::FULL)
      std::cout << "Trust--region radius: " << trustRegion << "\n";

    // Measure norm of the old iterate
    field_type oldNorm(0);
    for (size_t i=0; i<iterates_->size(); i++)
      oldNorm += (*errorNorms_[i])(iterates[i]);

    // assemble the local quadratic problem
    problem_->assembleQP(iterates);

    field_type newEnergy;
    field_type newInfeasibility;
    std::vector<Vector> newIterates;

    // decrease so we can increase again
    repeat--;
    // setup the linearised problem
    ipoptSolver.setProblem(problem_->A(), correction_, problem_->f());
    ipoptSolver.setLinearConstraints(problem_->constraintJacobian(),
                                     problem_->constraints());
    do {
      repeat++;

      // setup trust-region constraints
      for (size_t j=0; j<trObs.size(); j++) {
        for (int k=0; k<dim; k++) {
          trObs[j].lower(k) = -trustRegion;
          trObs[j].upper(k) =  trustRegion;
        }
      }
      ipoptSolver.setObstacles(trObs);

      Dune::Timer time;
      ipoptSolver.solve();

      if (this->verbosity_ == Solver::FULL)
        std::cout << "Solved local problem in " << time.stop() << " secs\n";

      // new iterate
      int counter(0);
      newIterates = iterates;
      for (size_t i=0; i<newIterates.size(); i++)
        for (size_t j=0; j<newIterates[i].size(); j++)
          newIterates[i][j] += correction_[counter++];

      // compute new energy and infeasibility
      newEnergy = problem_->energy(newIterates);
      newInfeasibility = problem_->infeasibility(newIterates);

      if (this->verbosity_ == Solver::FULL)
        std::cout<<"New energy of potential iterate "<<newEnergy<<
                   " and infeasibility "<<newInfeasibility<<std::endl;

      // if the correction is very small stop the loop
      field_type infNorm = correction_.infinity_norm();
      if(this->verbosity_==Solver::FULL)
        std::cout << "Correction infinite norm: " << infNorm << std::endl;

      if(infNorm <1e-11) {
        if (this->verbosity_ == Solver::FULL)
          std::cout<<"Correction is very small..expected rounding problems.\n";
        break;
      }

      // compute reduction of the model
      Vector tmp(correction_.size());
      problem_->A().mv(correction_, tmp);
      field_type modelDecrease = (problem_->f()*correction_) - 0.5 * (correction_*tmp);

      field_type relativeModelDecrease = modelDecrease / std::fabs(newEnergy);
      field_type relativeEnergyDecrease = (energy_ - newEnergy)/std::fabs(newEnergy);

      // this condition ensures that only infeasible points are added to the filter
      // if this conditions fails, it means that the infeasibility of the last iterate
      // is larger than the energy decrease. In this case we except the new iterate and
      // make a step towards feasibility. The constant 1e-4 is chosen as in
      // Conn,Gould,Toint - Trust-Region methods
      bool suffDecrease  = (modelDecrease >= 1e-4*infeasibility_*infeasibility_);


      if (this->verbosity_ == Solver::FULL) {
        std::cout << "Absolute model decrease: " << modelDecrease
                  << ",  functional decrease: " << energy_ - newEnergy << std::endl;
        std::cout << "Relative model decrease: " << relativeModelDecrease
                  << ",  functional decrease: " << relativeEnergyDecrease << std::endl;

      }

      //if (std::abs(modelDecrease) <1e-15 or std::abs(energy_-newEnergy)<1e-15) {
      // accept step when the we probably run into rounding errors
      if (std::abs(modelDecrease) <1e-11 or std::abs(energy_-newEnergy)<1e-11 or
           std::abs(relativeModelDecrease)<1e-11 or std::abs(relativeEnergyDecrease)<1e-11) {
        if (this->verbosity_ == Solver::FULL)
          std::cout << "Computing trust--region ratio might lead to cancellation...exiting." << std::endl;
        break;
      }

      // if iterate is not acceptable to the filter
      // then reject it and reduce the trust-region

      auto oldPair = std::make_pair<field_type,field_type>(field_type(infeasibility_),field_type(energy_));
      if (!filter_.acceptable(oldPair, newInfeasibility, newEnergy)) {

        // some heuristic to reduce the trust--region
        trustRegion  = 0.5*std::min(trustRegion,infNorm);

        if (this->verbosity_== Solver::FULL)
          std::cout << "Potential iterate is not acceptable to the filter, reducing trust-region: " << trustRegion << "\n";

        continue;
      }

      // check if trust-region mechanism accepts iterate
      field_type ratio = (energy_-newEnergy)/modelDecrease;

      // Model is not a good approximation while the infeasibility is small
      if (suffDecrease && (ratio < 0.01)) {

        // some heuristic to reduce the trust--region
        trustRegion  = 0.5*std::min(trustRegion,infNorm);

        if (this->verbosity_ == Solver::FULL)
          std::cout << " Unsuccessful step, shrinking trust-region: "<<trustRegion<<std::endl;
        continue;
      }

      // At this point the iterate is accepted

      // If the sufficient decrease condition fails, we add the old iterate to the filter
      if (!suffDecrease) {
        if (this->verbosity_ == Solver::FULL)
          std::cout << "Sufficient decrease failed, make a step towards feasibility!\n";

        filter_.add(infeasibility_,energy_);
        break;
      }

      // Model is a good approximation
      if (ratio > 0.9) {

        if (this->verbosity_ == Solver::FULL)
          std::cout << "Very successful step!\n";

        // some heuristic to enlage the trust-region
        trustRegion = std::min(maxTR_, 2*std::max(infNorm,trustRegion));
        break;
        // Approximation is acceptable
      } else if (ratio > 0.01) {// || std::abs(energy_-newEnergy) < 1e-12) {

        if (this->verbosity_ == Solver::FULL)
          std::cout<<"Successful step!\n";
        break;
      }

    } while (true);

    // compute error estimate

    std::vector<Vector> corrections(iterates.size());
    int counter(0);
    for (size_t i=0; i<iterates.size();i++) {
      corrections[i].resize(iterates[i].size());
      for (size_t j=0; j<corrections[i].size(); j++)
        corrections[i][j] = correction_[counter++];
    }

    field_type absError = 0;
    for (size_t i=0;i<corrections.size();i++)
      absError += (*errorNorms_[i])(corrections[i]);

    error = absError;
    if (std::isfinite(error/oldNorm))
      error /=oldNorm;

    if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
      std::cout << "In filter iteration "<< it
                << ", absolut norm of correction : " << absError
                << " relative error " << error
                << "\nEnergy " << newEnergy
                << " and infeasibility " << newInfeasibility << std::endl;

    // update iterates, energy and infeasibility
    iterates = newIterates;
    energy_ = newEnergy;
    infeasibility_ = newInfeasibility;

    // set correction to zero to remove any Dirichlet values
    correction_  = 0;

  } // end of filter loop

  if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
    std::cout << "Solved problem in " << it << " iterations with final energy " << energy_
              << " and infeasibility " << infeasibility_
              << " repeating " << repeat << " steps "<< std::endl;
}

} /* namespace Contact */
} /* namespace Dune */
