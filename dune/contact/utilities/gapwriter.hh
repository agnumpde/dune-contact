// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_CONTACT_UTILITIES_GAP_WRITER_HH
#define DUNE_CONTACT_UTILITIES_GAP_WRITER_HH

#include <string>
#include <algorithm>

#include <dune/common/parametertree.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/grid/io/file/amirameshwriter.hh>

#include <dune/contact/projections/normalprojection.hh>
#include <dune/contact/projections/closestpointprojection.hh>

namespace Dune {
namespace Contact {

/** \brief Class that provides methods to write the gap between two bodies to file.
 *
 *  Flags that have to be provided in the parameter file are:
 *
 *  double overlap - The acceptable amount of overlap the boundaries might have
 *  string projectionType - The projection type, by now only 'normal' or 'closestpoint' are supported
 *  int numberCLPIteration - In case of a closest point projection, this is the number of
 *                           Gauss-Seidel iterations performed
 *  double maxDistance - Only search this far
 *  string format - Determines in which format to write, by now only Amira (the default) is supported
 *
 * In case you want to write a time series, then provide sub-trees for the given names of the grids
 * These sub-trees should contain a
 *
 *  string pathWithPrefix - The path where displacement fields are located with a prefix
 *                          The files looked for are pathWithPrefix + timestep
 * The first tree should have
 *
 *  string gapFilename - The name of the result files for the first body, written is in the format
 *                       pathWithPrefix + gapFilename + timestep
 *
 *  The parameter file should have
 *
 *  int numberTimeSteps - The number of timesteps
*/
struct GapWriter
{

  template <int dim, class field_type = double>
  using Vector = Dune::BlockVector<Dune::FieldVector<field_type, dim> >;

  GapWriter() = default;

  /** \brief Write the vertex distance field of two boundary patches to file. */
  template <class Patch0, class Patch1, class field_type = double>
  static void writeGapToFile(const Dune::ParameterTree& config,
                             const Patch0& patch0,
                             const Patch1& patch1);

  /** \brief Write the vertex distance field of a series of deformed grids to file. */
  template <class DeformedGridComplex, class Patch, class field_type = double>
  static void writeTimeSeriesToFile(const Dune::ParameterTree& config,
                                    DeformedGridComplex& deformedGrids,
                                    const std::array<std::string, 2>& names,
                                    const Patch& patch0,
                                    const Patch& patch1);

  /** \brief Compute the vertex distance field of two boundary patches. */
  template <class Patch0, class Patch1, class field_type = double>
  static Vector<1, field_type> getDistanceField(const Dune::ParameterTree& config,
                             const Patch0& patch0,
                             const Patch1& patch1);

};

template <class Patch0, class Patch1, class field_type = double>
GapWriter::Vector<1, field_type> GapWriter::getDistanceField(const Dune::ParameterTree& config,
                                       const Patch0& patch0,
                                       const Patch1& patch1)
{
  std::unique_ptr<Dune::Contact::ContactProjection<Patch0, Patch1> > projection;

  field_type overlap = config.get<field_type>("overlap");

  std::string projType = config.get<std::string>("projectionType");
  std::transform(std::begin(projType), std::end(projType), std::begin(projType), ::tolower);

  if (projType == "normal")
    projection = std::make_unique<Dune::Contact::NormalProjection<Patch0, Patch1> >(overlap);
  else if (projType == "closestpoint")
    projection = std::make_unique<Dune::Contact::ClosestPointProjection<Patch0> >(
                        config.get<int>("numberCLPIteration", 50), overlap);
  else
    DUNE_THROW(Dune::NotImplemented, "GapWriter for projection " + projType + " not implemented!");

  field_type maxDistance = config.get<field_type>("maxDistance");
  projection->project(patch0, patch1, maxDistance);
  Vector<1, field_type> distance;
  projection->getObstacles(distance);

  auto globalToLocal = patch0.makeGlobalToLocal();
  Vector<1,field_type> globalDistance(globalToLocal.size());
  globalDistance = maxDistance;
  for (size_t j = 0; j < globalDistance.size(); j++)
    if (globalToLocal[j] != -1 and distance[globalToLocal[j]] < maxDistance)
      globalDistance[j] = std::max(field_type(0), distance[globalToLocal[j]][0]);

  return globalDistance;
}

template <class Patch0, class Patch1, class field_type>
void GapWriter::writeGapToFile(const Dune::ParameterTree& config,
                               const Patch0& patch0,
                               const Patch1& patch1)
{
  auto distance = getDistanceField(config, patch0, patch1);

  std::string format = config.get<std::string>("format", "amira");
  std::transform(std::begin(format), std::end(format), std::begin(format), ::tolower);

  if (format == "amira") {
#if HAVE_AMIRAMESH
      using Grid = typename Patch0::GridView::Grid;
      Dune::LeafAmiraMeshWriter<Grid>::writeBlockVector(patch0.getGridView().grid(),
                                            distance, config.get<std::string>("resultFile"));
#endif
  } else
      DUNE_THROW(Dune::NotImplemented, "Format " + format + " is not yet implemented");
}

template <class DeformedGridComplex, class Patch, class field_type>
void GapWriter::writeTimeSeriesToFile(const Dune::ParameterTree& config,
                               DeformedGridComplex& deformedGrids,
                               const std::array<std::string, 2>& names,
                               const Patch& patch0,
                               const Patch& patch1)
{

  std::array<std::string, 2> pathWithPrefix;
  pathWithPrefix= {config.sub(names[0]).template get<std::string>("pathWithPrefix"),
                   config.sub(names[1]).template get<std::string>("pathWithPrefix") };
  std::string gapFileName = config.sub(names[0]).template get<std::string>("gapFileName");

  size_t numberTimeSteps = config.get<size_t>("numberTimeSteps");
  using Grid = typename DeformedGridComplex::DeformedGridType;
  for (size_t i = 0; i < numberTimeSteps; i++) {
    std::stringstream step;
    step << std::setw(2) << std::setfill('0') << i;

#if HAVE_AMIRAMESH
    for (size_t j=0; j<2; j++) {
      typename DeformedGridComplex::Vector displace;
      Dune::AmiraMeshReader<Grid>::readFunction(displace,
                                           pathWithPrefix[j] + step.str());
      deformedGrids.setDeformation(displace, names[j]);
    }

    auto distance = getDistanceField(config, patch0, patch1);
    Dune::LeafAmiraMeshWriter<Grid>::writeBlockVector(patch0.gridView().grid(), distance,
                                                      pathWithPrefix[0] + gapFileName + step.str());
#endif
  }
}

} // end namespace Contact
} // end namespace Dune

#endif
