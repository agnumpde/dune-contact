#ifndef DUNE_CONTACT_COMMON_DUALBASISADAPTER_HH
#define DUNE_CONTACT_COMMON_DUALBASISADAPTER_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/localfunctions/dualmortarbasis/dualpq1factory.hh>

namespace Dune {
namespace Contact {

template<typename GV, typename F = double>
class DualBasisAdapter
{
public:
  using Field = F;
  using GridView = GV;
  using Element = typename GridView::template Codim<0>::Entity;
  using LocalCoordinate = typename GridView::template Codim<0>::Geometry::LocalCoordinate;
  using Value = typename std::vector< Dune::FieldVector<Field, 1> >;

  virtual ~DualBasisAdapter() = default;

  virtual void bind(const Element& element, int facet) = 0;
  virtual void evaluateFunction(const LocalCoordinate& x, Value& y) const = 0;
};

template<typename GV, typename F = double>
class DualBasisAdapterGlobal
  : public DualBasisAdapter<GV, F>
{
  using Base = DualBasisAdapter<GV, F>;

public:
  using typename Base::Field;
  using typename Base::GridView;
  using typename Base::Element;
  using typename Base::LocalCoordinate;
  using typename Base::Value;

  void bind(const Element& element, int) override
    {
      fe = &cache.get(element.type());
    }

  void evaluateFunction(const LocalCoordinate& x, Value& y) const override
    {
      fe->localBasis().evaluateFunction(x, y);
    }

private:
  using Cache = Dune::DualPQ1LocalFiniteElementCache<typename GridView::ctype, Field, GridView::dimension, true>;
  using FiniteElement = typename Cache::FiniteElementType;

  Cache cache;
  const FiniteElement* fe;
};

} /* namespace Contact */
} /* namespace Dune */

#endif
