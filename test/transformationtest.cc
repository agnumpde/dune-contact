// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#include <config.h>

#include <iostream>
#include <memory>

#include <dune/common/parametertreeparser.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/io.hh>
#include <dune/istl/matrixmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/grid/uggrid.hh>

#include <dune/fufem/utilities/gridconstruction.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>

#include <dune/contact/assemblers/largedeformationcontactassembler.hh>
#include <dune/contact/common/deformedcontinuacomplex.hh>
#include <dune/contact/projections/closestpointprojection.hh>

using namespace Dune;
using namespace Dune::Contact;

using field_type = double;

template <class DeformedGrid>
bool testTransformation(const std::vector<const DeformedGrid*>& defGrids,
                        const ParameterTree& config,
                        std::shared_ptr<BoundaryPatch<typename DeformedGrid::LevelGridView> > nmPatch,
                        std::shared_ptr<BoundaryPatch<typename DeformedGrid::LevelGridView> > mPatch)
{

  constexpr size_t dim = DeformedGrid::dimension;

  //   Create contact assembler
  using Vector = BlockVector<FieldVector<field_type, dim> >;
  LargeDeformationContactAssembler<DeformedGrid, Vector> contactAssembler(2,1);
  contactAssembler.setGrids(defGrids);

  using DefLeafBoundaryPatch =  BoundaryPatch<typename DeformedGrid::LeafGridView>;
  using Projection = ClosestPointProjection<DefLeafBoundaryPatch>;
  auto projection = std::make_shared<Projection>(50, config.get<field_type>("overlap"));

  using ContactMerger = Dune::GridGlue::ContactMerge<dim, field_type>;
  auto merger = std::make_shared<ContactMerger>(config.get<field_type>("overlap"), ContactMerger::CLOSEST_POINT);
  merger->minNormalAngle(config.get<field_type>("minAngle"));

  CouplingPair<DeformedGrid, DeformedGrid, field_type> coupling;
  coupling.set(0, 1, nmPatch, mPatch,5,
               CouplingPairBase::CONTACT, projection, merger);
  contactAssembler.setCoupling(coupling,0);

  // setup contact couplings
  contactAssembler.assembleProblem();

  // check if transformation decouples the obstacles
  using LargeDefCoupling = LargeDeformationCoupling<field_type, DeformedGrid>;
  auto ccoupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactAssembler.getContactCouplings()[0]);

  const auto& nmMat = ccoupling->exactNonmortarMatrix();
  const auto& mMat = ccoupling->exactMortarMatrix();

  // copy in a big matrix
  MatrixIndexSet is(nmMat.N(), nmMat.M() + mMat.M());
  is.import(nmMat);
  is.import(mMat, 0, nmMat.M());

  BCRSMatrix<FieldMatrix<field_type,1 ,dim> > linNonPet;
  is.exportIdx(linNonPet);
  linNonPet = 0;

  for (size_t i=0; i<nmMat.N(); i++) {
    for (auto col = nmMat[i].begin(); col != nmMat[i].end(); col++)
      linNonPet[i][col.index()] = *col;

    for (auto col = mMat[i].begin(); col != mMat[i].end(); col++)
      linNonPet[i][nmMat.M() + col.index()] = *col;
  }

  // multiply transformation from the right to the matrix (nmMat mMat 0)
  // and check if only the diagonal on the non-mortar part is not vanishing
  const auto& transformation = contactAssembler.getTransformationMatrix();

  BCRSMatrix<FieldMatrix<field_type,1 ,dim> > res;
  matMultMat(res,linNonPet, transformation);

  // mapping from boundary indices to grid indices
  const auto& nonmortarBoundary = ccoupling->nonmortarBoundary();
  std::vector<size_t> nonmortarToGlobal(nonmortarBoundary.numVertices());
  int idx = 0;
  for (int j=0; j<defGrids[0]->size(dim); j++)
    if (nonmortarBoundary.containsVertex(j))
      nonmortarToGlobal[idx++] = j;

  bool passed(true);
  for (size_t i=0; i<res.N(); i++)
    for (auto col = res[i].begin(); col != res[i].end(); col++) {
      const auto & entry = ((*col)[0]);
      for (size_t k=0; k<dim; k++)
        if (k==0 and col.index() == nonmortarToGlobal[i]) {
          if (std::fabs(entry[0]+1)>1e-15) {
            std::cout << "Diagonal nonmortar entry not minus one! "<< entry[0] << std::endl;
            passed = false;
          }
        } else if (std::fabs(entry[k])>1e-15) {
          passed = false;
          if ((int) col.index()<defGrids[0]->size(dim) and nonmortarBoundary.containsVertex(col.index())){
            std::cout << "Non-mortar non-diagonal entry not zero! "<< entry[k] << std::endl;
           std::cout<<k<< " th local and col "<<col.index()<<" glob "<<nonmortarToGlobal[i]<<std::endl;
          }
          else
            std::cout << "Mortar non-diagonal entry not zero! "<< entry[k] << std::endl;
        }
    }
  return passed;
}

template <class DeformedGrid>
bool testInverseTransformation(const std::vector<const DeformedGrid*>& defGrids,
                               const ParameterTree& config,
                               std::shared_ptr<BoundaryPatch<typename DeformedGrid::LevelGridView> > nmPatch,
                               std::shared_ptr<BoundaryPatch<typename DeformedGrid::LevelGridView> > mPatch)
{

  constexpr size_t dim = DeformedGrid::dimension;

  //   Create contact assembler
  using Vector = BlockVector<FieldVector<field_type, dim> >;
  LargeDeformationContactAssembler<DeformedGrid, Vector> contactAssembler(2,1);
  contactAssembler.setGrids(defGrids);

  using DefLeafBoundaryPatch =  BoundaryPatch<typename DeformedGrid::LeafGridView>;
  using Projection = ClosestPointProjection<DefLeafBoundaryPatch>;
  auto projection = std::make_shared<Projection>(50, config.get<field_type>("overlap"));

  using ContactMerger = Dune::GridGlue::ContactMerge<dim, field_type>;
  auto merger = std::make_shared<ContactMerger>(config.get<field_type>("overlap"), ContactMerger::CLOSEST_POINT);
  merger->minNormalAngle(config.get<field_type>("minAngle"));

  CouplingPair<DeformedGrid, DeformedGrid, field_type> coupling;
  coupling.set(0, 1, nmPatch, mPatch, 5, CouplingPairBase::CONTACT, projection, merger);
  contactAssembler.setCoupling(coupling,0);

  // setup contact couplings
  contactAssembler.assembleProblem();
  contactAssembler.computeInverseTransformationMatrix();

  const auto& transformation = contactAssembler.getTransformationMatrix();
  const auto& inverse        = contactAssembler.getInverseTransformation();

  BCRSMatrix<FieldMatrix<field_type,dim ,dim> > res;
  matMultMat(res, inverse, transformation);

  // check if transformation decouples the obstacles
  using LargeDefCoupling = LargeDeformationCoupling<field_type, DeformedGrid>;
  auto ccoupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactAssembler.getContactCouplings()[0]);

  // mapping from boundary indices to grid indices
  const auto& nonmortarBoundary = ccoupling->nonmortarBoundary();
  const auto& mortarBoundary = ccoupling->mortarBoundary();

  std::vector<size_t> nonmortarToGlobal(nonmortarBoundary.numVertices());
  int idx = 0;
  for (int j=0; j<defGrids[0]->size(dim); j++)
    if (nonmortarBoundary.containsVertex(j))
      nonmortarToGlobal[idx++] = j;

  bool passed(true);
  for (size_t i=0; i<res.N(); i++)
    for (auto col = res[i].begin(); col != res[i].end(); col++) {
      for (size_t k=0; k<dim; k++) {
        for (size_t l=0; l<dim; l++) {
          const auto & entry = (*col)[k][l];
          if (col.index() == i and k==l) {
            if (std::fabs(entry-1)>1e-14) {
              passed = false;
              if (col.index() == nonmortarToGlobal[i])
                std::cout << "(" << i << "," << l <<") diagonal nonmortar entry not one! "<< entry << std::endl;
              else if (col.index()<(size_t) defGrids[0]->size(dim) and !nonmortarBoundary.containsVertex(col.index()))
                std::cout << "(" << i << "," << l <<") diagonal unrednonmortar entry not one! "<< entry << std::endl;
              else if (col.index()>(size_t) defGrids[0]->size(dim)
                       and mortarBoundary.containsVertex(col.index()-defGrids[0]->size(dim)))
                std::cout << "(" << i << "," << l <<") diagonal mortar entry not one! "<< entry << std::endl;
              else
                std::cout << "(" << i << "," << l <<") other diagonal entry not one! "<< entry << std::endl;
            }
          } else if (std::fabs(entry)>1e-14) {
            passed = false;
            std::cout << "(" << i << "," << col.index() <<"), block (" << k << "," << l <<")";
            if (col.index()<(size_t) defGrids[0]->size(dim))
              if (nonmortarBoundary.containsVertex(col.index()))
                std::cout << "nonmortar entry not zero! "<< entry << std::endl;
              else
                std::cout << "unrednonmortar entry not zero! "<< entry << std::endl;
            else if (col.index()>(size_t) defGrids[0]->size(dim)
                     and mortarBoundary.containsVertex(col.index()-defGrids[0]->size(dim)))
              std::cout << " mortar entry not zero! "<< entry << std::endl;
            else
              std::cout << "other entry not one! "<< entry << std::endl;
          }
        }
      }
    }
  return passed;
}

int main (int argc, char *argv[]) try
{

  Dune::MPIHelper::instance(argc, argv);

  ParameterTree config;
  ParameterTreeParser::readINITree("../resources/test_grids.parset", config);

  // Set the coupling
  std::string nonmortar = "box";
  std::string mortar = "die";
  int boxIdx = (nonmortar == "box") ? 0 : 1;
  int dieIdx = (nonmortar == "die") ? 0 : 1;

  bool passed(true);
  {
    using Grid2D = UGGrid<2>;
    std::vector<std::unique_ptr<Grid2D> > grids(2);

    auto configBox = config.sub("box2D");
    auto configDie = config.sub("die2D");
    grids[boxIdx] = GridConstruction<Grid2D,2>::createRectangle(configBox);
    grids[dieIdx] = GridConstruction<Grid2D,2>::createCircle(configDie);

    std::vector<size_t> refines = {configBox.get<size_t>("nRefines"),
                                   configDie.get<size_t>("nRefines")};

    for (int i=0;i<2;i++) {
      grids[i]->setRefinementType(Grid2D::COPY);

      for (size_t j=0; j<refines[i]; j++)
        grids[i]->globalRefine(1);
    }

    using Vector = BlockVector<FieldVector<field_type, 2> >;
    using DeformedComplex =  DeformedContinuaComplex<Grid2D,Vector>;
    DeformedComplex defGrids;
    defGrids.addGrid(*grids[0], nonmortar);
    defGrids.addGrid(*grids[1], mortar);

    // Set mortar couplings.  Currently not more than one for each grid
    using DeformedGrid = typename DeformedComplex::DeformedGridType;
    using DefLevelBoundaryPatch =  BoundaryPatch<DeformedGrid::LevelGridView>;

    std::map<std::string,std::shared_ptr<DefLevelBoundaryPatch> > contactPatches;

    // read field describing the nonmortar boundary
    contactPatches["box"] = std::make_shared<DefLevelBoundaryPatch>(defGrids.grid("box").levelGridView(0));
    contactPatches["die"] = std::make_shared<DefLevelBoundaryPatch>(defGrids.grid("die").levelGridView(0));
    BoundaryPatchFactory<DeformedGrid::LevelGridView>::setupBoundaryPatch(configBox, *contactPatches["box"]);
    BoundaryPatchFactory<DeformedGrid::LevelGridView>::setupBoundaryPatch(configDie, *contactPatches["die"]);

    passed = passed and testTransformation(defGrids.gridVector(), config,
                                   contactPatches[nonmortar], contactPatches[mortar]);
    std::cout << "2D Transformation test "<< (passed ? "passed" : "failed") << "!\n";

    bool passedInv = testInverseTransformation(defGrids.gridVector(), config,
                           contactPatches[nonmortar], contactPatches[mortar]);
    std::cout << "2D Inverse Transformation test "<< (passedInv ? "passed" : "failed") << "!\n";

    passed = passed and passedInv;
  }

  {

    using Grid3D = UGGrid<3>;
    std::vector<std::unique_ptr<Grid3D> > grids(2);

    auto configBox = config.sub("box3D");
    auto configDie = config.sub("die3D");
    grids[boxIdx] = GridConstruction<Grid3D,3>::createCuboid(configBox);
    grids[dieIdx] = GridConstruction<Grid3D,3>::createTubeSegment(configDie);
    std::vector<size_t> refines = {configBox.get<size_t>("nRefines"),
                                   configDie.get<size_t>("nRefines")};

    for (int i=0;i<2;i++) {
      grids[i]->setRefinementType(Grid3D::COPY);

      for (size_t j=0; j<refines[i]; j++)
        grids[i]->globalRefine(1);
    }

    using Vector = BlockVector<FieldVector<field_type, 3> >;
    using DeformedComplex =  DeformedContinuaComplex<Grid3D,Vector>;
    DeformedComplex defGrids;
    defGrids.addGrid(*grids[0], nonmortar);
    defGrids.addGrid(*grids[1], mortar);

    // Set mortar couplings.  Currently not more than one for each grid
    using DeformedGrid = typename DeformedComplex::DeformedGridType;
    using DefLevelBoundaryPatch =  BoundaryPatch<DeformedGrid::LevelGridView>;

    std::map<std::string,std::shared_ptr<DefLevelBoundaryPatch> > contactPatches;

    // read field describing the nonmortar boundary
    contactPatches["box"] = std::make_shared<DefLevelBoundaryPatch>(defGrids.grid("box").levelGridView(0));
    contactPatches["die"] = std::make_shared<DefLevelBoundaryPatch>(defGrids.grid("die").levelGridView(0));
    BoundaryPatchFactory<DeformedGrid::LevelGridView>::setupBoundaryPatch(configBox, *contactPatches["box"]);
    BoundaryPatchFactory<DeformedGrid::LevelGridView>::setupBoundaryPatch(configDie, *contactPatches["die"]);

    bool passed3D =  testTransformation(defGrids.gridVector(), config,
                                   contactPatches[nonmortar], contactPatches[mortar]);
    std::cout << "3D Transformation test " << (passed3D ? "passed" : "failed") << "!\n";

    passed = passed and passed3D;

    bool passedInv = testInverseTransformation(defGrids.gridVector(), config,
                           contactPatches[nonmortar], contactPatches[mortar]);
    std::cout << "3D Inverse Transformation test "<< (passedInv ? "passed" : "failed") << "!\n";

    passed = passed and passedInv;

  }

  return passed ? 0 : 1;

} catch (Exception e) {

  std::cout << e << std::endl;

}
