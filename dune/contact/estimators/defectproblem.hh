// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ESTIMATORS_DEFECT_PROBLEM_HH
#define DUNE_CONTACT_ESTIMATORS_DEFECT_PROBLEM_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/istl/bdmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#if HAVE_IPOPT
#include "IpTNLP.hpp"
#else
#error You need IpOpt to use the DefectProblem
#endif

namespace Dune {
namespace Contact {

/** \brief Class representing the contact defect problem.
 *
 *         This class implements all methods needed for IpOpt to solve it.
 */
template <int dim, class field_type=double>
class DefectProblem : public Ipopt::TNLP
{

    using BlockType =  Dune::FieldMatrix<field_type, dim,dim>;
    using DiagonalMatrixType =  Dune::BDMatrix<BlockType>;
    using MatrixType = Dune::BCRSMatrix<BlockType>;
    using VectorType =  Dune::BlockVector<Dune::FieldVector<field_type,dim> >;

public:
    /** default constructor */
    DefectProblem(int numGrids, int numCouplings, field_type jacobianCutoff = 1e-8)
        : jacobianCutoff_(jacobianCutoff),
          nonmortarLagrangeMatrix_(numCouplings),
          mortarLagrangeMatrix_(numCouplings),
          obstacles_(numCouplings),
          couplings_(numCouplings)
    {}

    /** default destructor */
    virtual ~DefectProblem() {}

    /**@name Overloaded from TNLP */
    //@{
    /** Method to return some info about the nlp */
    virtual bool get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
                              Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style);

    /** Method to return the bounds for my problem */
    virtual bool get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
                                 Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u);

    /** Method to return the starting point for the algorithm */
    virtual bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
                                    bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
                                    Ipopt::Index m, bool init_lambda,
                                    Ipopt::Number* lambda);

    /** Method to return the objective value */
    virtual bool eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value);

    /** Method to return the gradient of the objective */
    virtual bool eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f);

    /** Method to return the constraint residuals */
    virtual bool eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g);

    /** Method to return:
         *   1) The structure of the jacobian (if "values" is NULL)
         *   2) The values of the jacobian (if "values" is not NULL)
         */
    virtual bool eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                            Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow, Ipopt::Index *jCol,
                            Ipopt::Number* values);

    /** Method to return:
         *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
         *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
         */
    virtual bool eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                        Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
                        bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
                        Ipopt::Index* jCol, Ipopt::Number* values);

    //@}

    /** @name Solution Methods */
    //@{
    /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
    virtual void finalize_solution(Ipopt::SolverReturn status,
                                   Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
                                   Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
                                   Ipopt::Number obj_value,
                                   const Ipopt::IpoptData* ip_data,
                                   Ipopt::IpoptCalculatedQuantities* ip_cq);
    //@}

    /** \brief The number of bodies in our n-body contact problem */
    int numBlocks() const {
        return hessians_->size();
    }

    /** \brief Return the number of pairwise couplings */
    int numCouplings() const {
        return nonmortarLagrangeMatrix_.size();
    }

    bool isRigidObstacle(int i) const {
        assert(i>=0 && i <(int) couplings_.size());
        return couplings_[i][0] == couplings_[i][1];
    }

    virtual int getNumHessianNonZeros() const;

    virtual int getNumVariables() const;

    virtual void getEqualityConstraints(int& constraints, int& numJacobianNonzeros);

    /** \brief The number of obstacles in the i-th coupling */
    int numObstacles(int i) const {
        int n = 0;

        if (isRigidObstacle(i)) {

            /** \todo I suspect we could simply return zero here */
            for (size_t j=0; j<obstacles_[i].size(); j++) {

                bool actualObstacle = false;

                for (int k=0; k<dim; k++)
                    if ( std::abs(obstacles_[i][j].lower(k)) > -1e10
                         || std::abs(obstacles_[i][j].upper(k)) < 1e10) {
                        actualObstacle = true;
                        break;
                    }

                if (actualObstacle)
                    n++;

            }

        } else {

            for (size_t j=0; j<nonmortarLagrangeMatrix_[i]->N(); j++)
                if ((*nonmortarLagrangeMatrix_[i])[j].begin() != (*nonmortarLagrangeMatrix_[i])[j].end())
                    n++;
        }

        return n;
    }

    /** \brief The total number of obstacles */
    int numObstacles() const {
        int n = 0;
        for (int i=0; i<numCouplings(); i++)
            n += numObstacles(i);

        return n;
    }


    // /////////////////////////////////
    //   Data
    // /////////////////////////////////

    /** \brief All entries in the constraint Jacobian smaller than the value
            here are removed.  This increases stability.
        */
    const field_type jacobianCutoff_;

    //! The quadratic terms are assumed to be diagonal
    const std::vector<DiagonalMatrixType>* hessians_;

    //! The initial and solution vector
    std::vector<VectorType>* x_;

    //! The residual
    const std::vector<VectorType>* rhs_;

    //! The Dirichlet bitfield
    const std::vector<Dune::BitSetVector<dim> >* dirichletNodes_;

    //! The P2 Lagrange nonmortar matrices
    std::vector<const MatrixType*> nonmortarLagrangeMatrix_;

    //! The P2 Lagrange mortar matrices
    std::vector<const MatrixType*> mortarLagrangeMatrix_;

    //! The weak mortar obstacles
    std::vector<std::vector<BoxConstraint<field_type,dim> > > obstacles_;

    /** \brief Pairs of grids that interact
            If the two numbers are equal then it is a rigid obstacle.
        */
    std::vector<std::array<int,2> > couplings_;


private:
    /**@name Methods to block default compiler methods.
         */
    //@{
    DefectProblem(const DefectProblem&);
    DefectProblem& operator=(const DefectProblem&);
    //@}
};

} /* namespace Contact */
} /* namespace Dune */

#include "defectproblem.cc"

#endif
