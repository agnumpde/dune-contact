// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=2 sw=2 sts=2:
#ifndef DUNE_CONTACT_ASSEMBLERS_ANALYTIC_LOCAL_INTERSECTION_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_ANALYTIC_LOCAL_INTERSECTION_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/geometry/type.hh>
#include <dune/matrix-vector/crossproduct.hh>
#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/contact/assemblers/localintersectionassembler.hh>

namespace Dune {
namespace Contact {


/** \brief Class to assemble the analytic linearisation of the remote intersection corners.
 *
 *  This class computes for each involved grid0 and grid1 face its intersection
 *  and then linearises the resulting remote intersection corners w.r.t
 *  the grid0/grid1 deformation using analytic formulas
 *  With the current implementation of the projection in dune-grid-glue the global
 *  continuity of the mapping was traded in for an efficient implementation, hence
 *  the linearisation has to be computed on each intersection seperately.
 */
template <class GlueType, class MatrixType>
class AnalyticLocalIntersectionAssembler : public LocalIntersectionAssembler<GlueType>
{
  using Base = LocalIntersectionAssembler<GlueType>;
  using This = AnalyticLocalIntersectionAssembler<GlueType, MatrixType>;
  using GridView0 = typename Base::GridView0;
  using GridView1 = typename Base::GridView1;
  using ctype = typename Base::ctype;
  using field_type = typename PromotionTraits<ctype,typename MatrixType::field_type>::PromotedType;

  const static int dim = GridView0::dimension;
  using CoordinateType = FieldVector<ctype,dim>;

public:

  AnalyticLocalIntersectionAssembler(const BoundaryPatch<GridView0>& reducedPatch0,
                                     const BoundaryPatch<GridView0>& unreducedPatch0,
                                     const BoundaryPatch<GridView1>& reducedPatch1,
                                     const BoundaryPatch<GridView1>& unreducedPatch1,
                                     ctype overlap, ctype maxNormalProduct,
                                     const MatrixType& linAverageNormals,
                                     bool isNormalProjection = false) :
      Base(reducedPatch0, unreducedPatch0, reducedPatch1, unreducedPatch1, overlap, maxNormalProduct),
      linAverageNormals_(linAverageNormals), isNormalProjection_(isNormalProjection)
  {}

  /** \brief Compute and add the linearisation of remote intersection corners.
   *
   *  This method computes for a grid0 and grid1 face its intersection
   *  and then linearises the resulting remote intersection corners w.r.t
   *  the grid0/grid1 deformation using the analytic formula.
   *  This is about three times faster than automatic differentiation
   *  With the current implementation of the projection in dune-grid-glue
   *
   *  \param face0 The grid0 face given by global indices of its corners
   *  \param corners0 The grid0 corner coordinates of the face
   *  \param normals0 The grid0 nodally averaged normals
   *  \param type0 The geometry type of the grid0 face
   *  \param face1 The grid1 face given by global indices of its corners
   *  \param corners1 The grid1 corner coordinates of the face
   *  \param normals1 The grid1 nodally averaged normals
   *  \param type1 The geometry type of the grid1 face
   *  \param remoteIdx The offset for the row index of the matrix
   *  \param linRemoteCorners The matrix to be filled with the linearised projection
   */
  void assemble(const std::vector<int>& face0, const std::vector<CoordinateType>& corners0,
                const std::vector<CoordinateType>& normals0, const GeometryType& type0,
                const std::vector<int>& face1, const std::vector<CoordinateType>& corners1,
                const std::vector<CoordinateType>& normals1, const GeometryType& type1,
                int remoteIdx, MatrixType& linRemoteCorners);

  /** \brief Compute the linearisation of a forward projected vertex
   *
   *  \param linProj The linearised projection to be filled
   *  \param projectedVertex The linearised projection to be filled
   *  \param offsetDom The linearisation contains columns for both, the projection and the opposite
   *                 face. This offset regulates the non-mortar and mortar values go to the
   *                 correct global columns. Non-mortar first, mortar second, i.e. for outer normal projection
   *                 this offset should be zero, else size(grid0)
   *  \param offsetTar  The linearisation contains columns for both, the projection and the opposite
   *                 face. Either 0 or size(grid0)
   *  \param totalSize grid0 + grid1 vertices
   *  \param vertexIdx Global index of the projected vertex
   *  \param vertex Coordinates of the projected vertex
   *  \param avNormal The averaged normal along which is projected
   *  \param linNormal The linearisation of the averaged normal
   *  \param face Global indices of the opposing face
   *  \param faceCorner Coordinates of the first face corner
   *  \param faceEdges Coordinates of the edges of the face
   *  \param faceNormal Face normal
   */
  static void linearisedForwardProjection(MatrixType& linProj,
          CoordinateType& projectedVertex,
          size_t offsetDom, size_t offsetTar, size_t totalSize, int vertexIdx,
          const CoordinateType& vertex, const CoordinateType& avNormal,
          const typename MatrixType::row_type& linNormal,
          const std::vector<int> face, const CoordinateType& faceCorner,
          const std::vector<CoordinateType>& faceEdges, const CoordinateType& faceNormal);

private:

  /** \brief Compute the intersection types for the remote intersection corners.
   *
   * Each remote intersection corner originates from either:
   *
   * - forward projection of grid0 vertex
   * - inverse projection of grid1 vertex
   * - intersection of grid0 and grid1 edge
   *
   * The array rVertexTypes contains the following data:
   *
   * 0 - the type of the vertex (0 -grid0, 1 -grid1, 2 -edge intersect)
   * 1 - for type 0 and 1, the corresponding grid0/grid1 global index
   * for type 2:
   *  1,2 the global indices of the grid0 edge
   *  3,4 the global indices of the grid1 edge
   */
  void computeRemoteVertexTypes(const std::vector<int>& face0, const std::vector<CoordinateType>& corners0,
                                const std::vector<CoordinateType>& normals0, const GeometryType& type0,
                                const std::vector<int>& face1, const std::vector<CoordinateType>& corners1,
                                const std::vector<CoordinateType>& normals1, const GeometryType& type1,
                                std::vector<std::array< std::array<int,5>, dim> >& rVertexTypes);


  void linearised2DInverseProjectedVertex(
          const std::vector<int>& domainFace,
          const std::vector<int>& targetFace,
          const std::vector<CoordinateType>& domainVertices,
          const std::vector<CoordinateType>& targetVertices,
          const std::vector<CoordinateType>& domainNormals,
          const CoordinateType& targetFaceNormal,
          size_t targetLocalIndex,
          const std::array<MatrixType, dim>& linProjDomVertex,
          typename MatrixType::row_type& linRemoteCorner) const;

  void linearised3DInverseProjectedVertex(
          const std::vector<int>& domainFace,
          const std::vector<int>& targetFace,
          const std::vector<CoordinateType>& domainVertices,
          const std::vector<CoordinateType>& targetVertices,
          const std::vector<CoordinateType>& domainNormals,
          const CoordinateType& targetFaceNormal,
          const std::vector<CoordinateType>& targetEdges,
          size_t targetLocalIndex,
          const std::array<MatrixType, dim>& linProjDomVertex,
          typename MatrixType::row_type& linRemoteCorner) const;

  //! Dyadic product
  static auto dyadic(const CoordinateType& a, const CoordinateType& b) {

    typename MatrixType::block_type c(0);

    for (int i=0; i < CoordinateType::dimension; i++)
      c[i].axpy(a[i], b);
    return c;
  }

  //! The linearised nodally averaged normal field
  const MatrixType& linAverageNormals_;

  //! Flag to determine whether or not the outer normal projection is used
  bool isNormalProjection_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "analyticlocalintersectionassembler.cc"

#endif
