// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_SOLVERS_CONTACT_MG_TRANSFER_HH
#define DUNE_CONTACT_SOLVERS_CONTACT_MG_TRANSFER_HH

/** \file
    \brief Multigrid transfer operators for the monotone multigrid method for mortar discretised contact problems.
*/

#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>

namespace Dune {
namespace Contact {

/** \brief Multigrid restriction and prolongation operator for contact problems
 *
 * Currently only works for first-order Lagrangian elements!
 */
template<class VectorType>
class ContactMGTransfer : public TruncatedDenseMGTransfer<VectorType> {

    enum {blocksize = VectorType::block_type::dimension};

    typedef typename VectorType::field_type field_type;

    typedef FieldMatrix<field_type, blocksize, blocksize> MatrixBlock;

    typedef BCRSMatrix<MatrixBlock> MatrixType;

    typedef std::vector<MatrixBlock > CoordSystemVector;

    typedef typename MultigridTransfer<VectorType>::OperatorType OperatorType;
    typedef typename OperatorType::row_type RowType;
    typedef typename RowType::Iterator ColumnIterator;
    typedef typename RowType::ConstIterator ConstIterator;

public:

    /** \brief Set up multigrid transfer operator for a one-body (Signorini) problem
     */
    template <class GridType>
    void setup(const GridType& grid, int cL, int fL,
               const CoordSystemVector& coarseLocalCoordSystems,
               const CoordSystemVector& fineLocalCoordSystems);

    /** \brief Set up multigrid transfer operator for a two-body problem

    This method sets up the multigrid transfer operator for a two-body contact
    problem.  It first constructs the standard prolongation matrices for the
    two grids and then combines them into a single matrix.  Then, this matrix
    is modified as described on page 12--14 of Wohlmuth, Krause: "Monotone
    Methods on Nonmatching Grids for Nonlinear Contact Problems".  That is,
    the standard prolongation matrix
    \f[ xyz \f]
    is replaced by
    \f[ \begin{pmatrix}
         (I_l^{l+1})_{ii} & (I_l^{l+1})_{im} +  (I_l^{l+1})_{is} M_l & (I_l^{l+1})_{is} \\
         0                & (I_l^{l+1})_{mm}                         & 0                \\
         0                & 0                                        & (I_l^{l+1})_{ss}
        \end{pmatrix} \f].
    \param coarseFSpace0 The function space on the coarse grid of the first object
    \param fineFSpace0   The function space on the fine grid of the first object
    \param coarseFSpace1 The function space on the coarse grid of the second object
    \param fineFSpace1   The function space on the fine grid of the second object
    \param coarseLocalCoordSystems
    \param fineLocalCoordSystems
    \param hasObstacle   Contains a set bit for each nonmortar object dof carrying an obstacle
    */
    template <class GridType0, class GridType1>
    void setup(const GridType0& grid0, const GridType1& grid1,
               int cL, int fL,
               const MatrixType& transferOperator,
               const CoordSystemVector& coarseLocalCoordSystems,
               const CoordSystemVector& fineLocalCoordSystems,
               const Dune::BitSetVector<1>& coarseHasObstacle,
               const Dune::BitSetVector<1>& fineHasObstacle);

    /** \brief Set up multigrid transfer operator for an n-body problem

    */
    template <class GridType>
    void setup(const std::vector<const GridType*>& grids, int cL, int fL,
               const std::vector<const CoordSystemVector*>& coarseLocalCoordSystems,
               const std::vector<const CoordSystemVector*>& fineLocalCoordSystems,
               const std::vector<const Dune::BitSetVector<1>*>& coarseHasObstacle,
               const std::vector<const Dune::BitSetVector<1>*>& fineHasObstacle,
               const std::vector<const MatrixType*>& mortarTransferOps,
               const std::vector<std::array<int,2> >& gridIdx);
};

} /* namespace Contact */
} /* namespace Dune */

#include "contacttransfer.cc"

#endif
