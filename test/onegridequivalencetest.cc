// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=4 sw=4 et sts=4:
#include <config.h>

#include <iostream>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>

#include <dune/contact/assemblers/twobodyassembler.hh>
#include <dune/contact/projections/normalprojection.hh>

using namespace Dune;
using namespace Dune::Contact;

template <class T, int n>
struct CompareVectors {
    bool operator()(const FieldVector<T,n>& s1, const FieldVector<T,n>& s2) const
    {
        for (int i=0; i<n; i++) {
            if (s1[i]<s2[i])
                return true;
            else if (s1[i]>s2[i])
                return false;
        }
            
        return false;
    }
};

template <class GridType>
bool testOneGridEquivalence(std::vector<std::shared_ptr<GridType> > grids,
                            const std::vector<std::shared_ptr<BoundaryPatch<typename GridType::LevelGridView> > >& contactPatches,
                            double obsDistance,
                            std::shared_ptr<GridType> compoundGrid)
{
    const int dim = GridType::dimension;

    int maxLevel = 2;

    grids[0]->globalRefine(maxLevel);
    grids[1]->globalRefine(maxLevel);
    compoundGrid->globalRefine(maxLevel);
    
    // /////////////////////////////////////////////////////
    //  Assemble two-body contact problem
    // /////////////////////////////////////////////////////


    using DuneP1Basis = Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1>;
    using P1Basis =  DuneFunctionsBasis<DuneP1Basis>;
    using Lfe = typename P1Basis::LocalFiniteElement;
    StVenantKirchhoffAssembler<GridType, Lfe, Lfe> localStiffness(2.5e5, 0.3);

    using MatrixType = BCRSMatrix<FieldMatrix<double,dim,dim> >;
    std::array<MatrixType, 2> matrix;
    std::array<const MatrixType*,2> submat;
    for (int i=0; i<2; i++)
    {
        P1Basis p1Basis(grids[i]->leafGridView());
        OperatorAssembler<P1Basis,P1Basis> assembler(p1Basis,p1Basis);
        assembler.assemble(localStiffness, matrix[i]);
        submat[i] = &matrix[i];
    }

    using VectorType = BlockVector<FieldVector<double,dim> >;
    TwoBodyAssembler<GridType, VectorType> contactAssembler;

    using LeafBoundaryPatch = BoundaryPatch<typename GridType::LeafGridView>;
    auto projection = std::make_shared<NormalProjection<LeafBoundaryPatch> >(0.1);

    contactAssembler.setup(*grids[0], *grids[1],
                           contactPatches[0], contactPatches[1],
                           obsDistance,
                           CouplingPairBase::CONTACT,
                           projection);

    MatrixType contactMatrix;
    contactAssembler.assembleJacobian(submat, contactMatrix);

    // undo rotation into normal and tangential coordinates
    {
        const auto& coordSystems = contactAssembler.getLocalCoordSystems();
        assert((int) coordSystems.size() == grids[0]->size(dim));
        for (size_t i=0; i<contactMatrix.N(); i++)
            for (auto&& cIt = contactMatrix[i].begin(); cIt != contactMatrix[i].end(); cIt++) {
                if (i <coordSystems.size())
                    cIt->leftmultiply(coordSystems[i]);
                if (cIt.index() <coordSystems.size())
                    cIt->rightmultiply(coordSystems[cIt.index()]);
            }
    }

    // /////////////////////////////////////////////////////
    //  Assemble linear problem on the compound grid
    // /////////////////////////////////////////////////////

    MatrixType compoundMatrix;
    {
        P1Basis p1Basis(compoundGrid->leafGridView());
        OperatorAssembler<P1Basis,P1Basis> assembler(p1Basis,p1Basis);
        assembler.assemble(localStiffness, compoundMatrix);
    }

    // ///////////////////////////////////////////////////////////////////////
    //   Compare the two matrices
    // Since we have a pointwise coupling, the entries in the contactMatrix
    // which do not regard nonmortar dofs have to be identical to the
    // corresponding ones in the compoundMatrix.
    // ///////////////////////////////////////////////////////////////////////

    using ValueType = std::pair<FieldVector<typename GridType::ctype,dim>, int>;
    using VertexMap = std::map<FieldVector<typename GridType::ctype,dim>, int, CompareVectors<double,dim> >;

    // contact grids
    VertexMap contactVertexMap;
    
    {
        const auto& indexSet = grids[0]->leafIndexSet();
        for (const auto& v : vertices(grids[0]->leafGridView())) {
            int idx = indexSet.index(v);
            if (!contactAssembler.contactCoupling_[0]->nonmortarBoundary().containsVertex(idx))
                contactVertexMap.insert(ValueType(v.geometry().corner(0), idx));
        }
    }


    {
        const auto& indexSet = grids[1]->leafIndexSet();
        for (const auto& v : vertices(grids[1]->leafGridView()))
            contactVertexMap.insert(ValueType(v.geometry().corner(0),
                                          grids[0]->size(dim) + indexSet.index(v)));
    }

    // compoundGrid
    VertexMap compoundVertexMap;

    {
        const auto& indexSet = compoundGrid->leafIndexSet();
        for (const auto& v : vertices(compoundGrid->leafGridView()))
            compoundVertexMap.insert(ValueType(v.geometry().corner(0),indexSet.index(v)));
    }

    assert(contactVertexMap.size() == compoundVertexMap.size());

    std::vector<int> equivDof(compoundGrid->size(dim));

    auto contactIterator  = contactVertexMap.begin();
    auto compoundIterator = compoundVertexMap.begin();

    // Map indices of the single grids to the corr. compound grid indices
    for (; contactIterator != contactVertexMap.end(); ++contactIterator, ++compoundIterator)
        equivDof[compoundIterator->second] = contactIterator->second;

    // /////////////////////////////////////////////////////////////////////
    //   Actually compare the two matrices
    // /////////////////////////////////////////////////////////////////////

    for (std::size_t i=0; i<compoundMatrix.N(); i++) {

        auto cIt    = compoundMatrix[i].begin();
        auto cEndIt = compoundMatrix[i].end();

        for (; cIt!=cEndIt; ++cIt) {

            auto tmp = *cIt;
            tmp -= contactMatrix[equivDof[i]][equivDof[cIt.index()]];

            if (tmp.infinity_norm()/cIt->infinity_norm() > 1e-8) {
                std::cerr << "++++ Matrix entries differ! ++++" << std::endl;
                std::cerr << "Contact matrix entry (" << equivDof[i] << ", " << equivDof[cIt.index()] << ")\n";
                std::cerr << contactMatrix[equivDof[i]][equivDof[cIt.index()]] << std::endl;
                std::cerr << "Compound matrix entry (" << i << ", " << cIt.index() << ")\n";
                std::cerr << *cIt << std::endl;
                return false;
            }

        }

    }
    return true;
}

int main (int argc, char *argv[]) try
{

    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);
    // /////////////////////////////////////////////
    //   Test two cubes made of hexahedra
    // /////////////////////////////////////////////
    {

        std::cout<<"***************************************************\n";
        std::cout<<"*           Testing two hexahedral cubes          *\n";
        std::cout<<"***************************************************\n";

        using GridType = YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3> >;
        std::vector<std::shared_ptr<GridType> > grids(2);
        std::shared_ptr<GridType> compoundGrid;

        std::array<int, 3> elements;
        elements.fill(2);
        FieldVector<double,3> lower(0);
        FieldVector<double,3> upper(1);

        grids[0] = std::make_shared<GridType>(lower, upper, elements);

        lower[2] += 1.005;
        upper[2] += 1.005;

        grids[1] = std::make_shared<GridType>(lower, upper, elements);

        elements[2] = 4;
        lower[2] = 0;
        upper[2] = 2;

        compoundGrid = std::make_shared<GridType>(lower, upper, elements);

        using LevelBoundaryPatch = BoundaryPatch<GridType::LevelGridView>;
        std::vector<std::shared_ptr<LevelBoundaryPatch> > contactPatches(2);

        ParameterTree config0;
        config0["nCriterions"] = "1";
        auto& c0 = config0.sub("criterion0");
        c0["type"] = "UpperCentre";
        c0["component"] = "2";
        c0["bound"] = "1.0";

        contactPatches[0] = std::make_shared<LevelBoundaryPatch>(grids[0]->levelGridView(0));
        BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(config0, *contactPatches[0]);

        ParameterTree config1;
        config1["nCriterions"] = "1";
        auto& c1 = config1.sub("criterion0");
        c1["type"] = "LowerCentre";
        c1["component"] = "2";
        c1["bound"] = "1.005";
        contactPatches[1] = std::make_shared<LevelBoundaryPatch>(grids[1]->levelGridView(0));
        BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(config1, *contactPatches[1]);

        passed = passed and testOneGridEquivalence(grids, contactPatches, 0.01, compoundGrid);
    }

    // /////////////////////////////////////////////
    //   Test two quadrilaterals made of quadrilaterals
    // /////////////////////////////////////////////
    {

        std::cout<<"***************************************************\n";
        std::cout<<"*     Testing hexahedra quadrilaterals            *\n";
        std::cout<<"***************************************************\n";

        using GridType = UGGrid<2>;
        std::vector<std::shared_ptr<GridType> > grids(2);
        std::shared_ptr<GridType> compoundGrid;

        using FV = FieldVector<double,2>;
        using V = std::vector<unsigned int>;
        auto gt = Dune::GeometryTypes::cube(2);
        {
            GridFactory<GridType> factory;

            factory.insertVertex(FV({0, 0}));
            factory.insertVertex(FV({1, 0}));
            factory.insertVertex(FV({0, 1}));
            factory.insertVertex(FV({1, 1}));

            factory.insertElement(gt, V({0, 1, 2, 3}));
            grids[0]  = std::shared_ptr<GridType>(factory.createGrid());
        }

        {
            GridFactory<GridType> factory;

            factory.insertVertex(FV({0, 1.005}));
            factory.insertVertex(FV({1, 1.005}));
            factory.insertVertex(FV({0, 2.005}));
            factory.insertVertex(FV({1, 2.005}));

            factory.insertElement(gt, V({0, 1, 2, 3}));
            grids[1]  = std::shared_ptr<GridType>(factory.createGrid());
        }


        {
            GridFactory<GridType> factory;
            factory.insertVertex(FV({0, 0}));
            factory.insertVertex(FV({1, 0}));
            factory.insertVertex(FV({0, 1}));
            factory.insertVertex(FV({1, 1}));
            factory.insertVertex(FV({0, 2}));
            factory.insertVertex(FV({1, 2}));

            factory.insertElement(gt, V({0, 1, 2, 3}));
            factory.insertElement(gt, V({2, 3, 4, 5}));
            compoundGrid = std::shared_ptr<GridType>(factory.createGrid());
        }

        using LevelBoundaryPatch = BoundaryPatch<GridType::LevelGridView>;
        std::vector<std::shared_ptr<LevelBoundaryPatch> > contactPatches(2);


        ParameterTree config0;
        config0["nCriterions"] = "1";
        auto& c0 = config0.sub("criterion0");
        c0["type"] = "UpperCentre";
        c0["component"] = "1";
        c0["bound"] = "1";
        contactPatches[0] = std::make_shared<LevelBoundaryPatch>(grids[0]->levelGridView(0));
        BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(config0, *contactPatches[0]);

        ParameterTree config1;
        config1["nCriterions"] = "1";
        auto& c1 = config1.sub("criterion0");
        c1["type"] = "LowerCentre";
        c1["component"] = "1";
        c1["bound"] = "1.005";

        contactPatches[1] = std::make_shared<LevelBoundaryPatch>(grids[1]->levelGridView(0));
        BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(config1, *contactPatches[1]);

        passed = passed and testOneGridEquivalence(grids, contactPatches, 0.01, compoundGrid);
    }

    // /////////////////////////////////////////////
    //   Test two quadrilaterals made of triangles
    // /////////////////////////////////////////////
    {

        std::cout<<"***************************************************\n";
        std::cout<<"*     Testing triangulated quadrilaterals         *\n";
        std::cout<<"***************************************************\n";

        using GridType = UGGrid<2>;
        std::vector<std::shared_ptr<GridType> > grids(2);
        std::shared_ptr<GridType> compoundGrid;
        
        using FV = FieldVector<double,2>;
        using V = std::vector<unsigned int>;
        auto gt = Dune::GeometryTypes::simplex(2);
        {
            GridFactory<GridType> factory;

            factory.insertVertex(FV({0, 0}));
            factory.insertVertex(FV({1, 0}));
            factory.insertVertex(FV({0, 1}));
            factory.insertVertex(FV({1, 1}));

            factory.insertElement(gt, V({0, 1, 2}));
            factory.insertElement(gt, V({2, 1, 3}));

            grids[0]  = std::shared_ptr<GridType>(factory.createGrid());
        }

        {
            GridFactory<GridType> factory;

            factory.insertVertex(FV({0, 1.005}));
            factory.insertVertex(FV({1, 1.005}));
            factory.insertVertex(FV({0, 2.005}));
            factory.insertVertex(FV({1, 2.005}));

            factory.insertElement(gt, V({0, 1, 2}));
            factory.insertElement(gt, V({2, 1, 3}));

            grids[1]  = std::shared_ptr<GridType>(factory.createGrid());
        }


        {
            GridFactory<GridType> factory;
            factory.insertVertex(FV({0, 0}));
            factory.insertVertex(FV({1, 0}));
            factory.insertVertex(FV({0, 1}));
            factory.insertVertex(FV({1, 1}));
            factory.insertVertex(FV({0, 2}));
            factory.insertVertex(FV({1, 2}));

            factory.insertElement(gt, V({0, 1, 2}));
            factory.insertElement(gt, V({2, 1, 3}));
            factory.insertElement(gt, V({2, 3, 4}));
            factory.insertElement(gt, V({4, 3, 5}));

            compoundGrid = std::shared_ptr<GridType> (factory.createGrid());
        }

        using LevelBoundaryPatch = BoundaryPatch<GridType::LevelGridView>;
        std::vector<std::shared_ptr<LevelBoundaryPatch> > contactPatches(2);

        ParameterTree config0;
        config0["nCriterions"] = "1";
        auto& c0 = config0.sub("criterion0");
        c0["type"] = "UpperCentre";
        c0["component"] = "1";
        c0["bound"] = "1";
        contactPatches[0] = std::make_shared<LevelBoundaryPatch>(grids[0]->levelGridView(0));
        BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(config0, *contactPatches[0]);

        ParameterTree config1;
        config1["nCriterions"] = "1";
        auto& c1 = config1.sub("criterion0");
        c1["type"] = "LowerCentre";
        c1["component"] = "1";
        c1["bound"] = "1.005";

        contactPatches[1] = std::make_shared<LevelBoundaryPatch>(grids[1]->levelGridView(0));
        BoundaryPatchFactory<GridType::LevelGridView>::setupBoundaryPatch(config1, *contactPatches[1]);

        passed = passed and testOneGridEquivalence(grids, contactPatches, 0.01, compoundGrid);
    }

    return passed ? 0 : 1;

} catch (Exception e) {

    std::cout << e << std::endl;

}
