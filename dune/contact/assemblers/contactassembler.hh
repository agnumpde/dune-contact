// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_CONTACTASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_CONTACTASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

namespace Dune {
namespace Contact {

template <int dim, class field_type=double>
class ContactAssembler
{
    typedef Dune::FieldMatrix<field_type, dim, dim> MatrixBlock;
    typedef Dune::FieldVector<field_type, dim> CoordinateType;

public:

    ContactAssembler() {}

    /** \brief Computes the Householder reflection matrix that maps v0 into v1 */
    static void householderReflection(const CoordinateType& v0,
                               const CoordinateType& v1,
                               MatrixBlock& mat);

    /** \brief Compute local coordinates for a subset of the grid vertices.
     *         The basis vectors are given by the obstacle directions and the tangential vectors
     *         for all vertices contained in the boundary patch and else the euclidean basis.
     *
     * \param contactBoundary Compute coordinate systems on all vertices of the boundary patch
     * \param obsDirections The directions which the first euclidean vector shall be rotated to,
     *                      given in local boundary indices
     * \param contactSystems Save the local coordinate systems in this vector.
     */
    template <class BoundaryPatchType>
    static void computeLocalCoordinateSystems(const BoundaryPatchType& contactBoundary,
                                        std::vector<MatrixBlock>& coordSystems,
                                        const std::vector<CoordinateType>& obsDirections);

    static void computeLocalCoordinateSystems(const Dune::BitSetVector<1>& hasObs,
                                        std::vector<MatrixBlock>& coordSystems,
                                        const std::vector<CoordinateType>& obsDirections);

    /** \brief Transform a matrix to local coordinates.
     *         Note that the localCoordsSystems_ have to be assembled first.
     *
     *  \param mat Reference to the matrix to be transformed
     */
    template <class MatrixType>
    void transformMatrix(MatrixType& mat) const;

    /** \brief Transform a vector to local coordinates.
     *         Note that the localCoordsSystems_ have to be assembled first.
     *
     *  \param vector Reference to the vector to be transformed
     */
    template <class VectorType>
    void transformVector(VectorType& vector) const;

    /** \brief Getter for localCoordSystems_. */
    const std::vector<MatrixBlock>& getLocalCoordSystems() const {
        return localCoordSystems_;
    }

    /** \brief Vector containing the basis transformation matrices.*/
    std::vector<MatrixBlock> localCoordSystems_;

};

} /* namespace Contact */
} /* namespace Dune */

#include "contactassembler.cc"

#endif
