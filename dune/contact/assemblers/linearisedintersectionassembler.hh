// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=2 sw=2 sts=2:
#ifndef DUNE_CONTACT_ASSEMBLERS_LINEARISED_INTERSECTION_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_LINEARISED_INTERSECTION_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/common/promotiontraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/istl/matrixindexset.hh>

#include <dune/fufem/arithmetic.hh>
#include <dune/fufem/boundarypatch.hh>

#include <dune/contact/assemblers/analyticlocalintersectionassembler.hh>

#include <dune/grid-glue/merging/contactmerge.hh>
#include <dune/grid-glue/gridglue.hh>

namespace Dune {
namespace Contact {

template <class GlueType>
class LinearisedIntersectionAssembler
{

  using GridView0 = typename GlueType::Grid0View;
  using GridView1 = typename GlueType::Grid1View;
  using ctype = typename PromotionTraits<typename GridView0::ctype, typename GridView1::ctype>::PromotedType;
  using Patch0 = BoundaryPatch<GridView0>;
  using Patch1 = BoundaryPatch<GridView1>;

public:

  LinearisedIntersectionAssembler(std::shared_ptr<GlueType> glue, bool isNormalProjection = false) :
   glue_(glue), isNormalProjection_(isNormalProjection)
  {}

  template <class MatrixType>
  void assemble(const Patch0& reducedPatch0,
                const Patch0& patch0,
                const Patch1& reducedPatch1,
                const Patch1& patch1,
                ctype overlap, ctype maxNormalAngle,
                MatrixType& linRemoteCorners,
                const MatrixType& linAverageNormals)
  {
    AnalyticLocalIntersectionAssembler<GlueType, MatrixType> localAssembler(reducedPatch0, patch0,
                                                                reducedPatch1, patch1,
                                                                overlap, maxNormalAngle,
                                                                linAverageNormals, isNormalProjection_);
    assemble(localAssembler, linAverageNormals, linRemoteCorners);
  }

  template <class LocalAssembler, class MatrixType>
  void assemble(LocalAssembler& localAssembler,
                const MatrixType& linAverageNormals,
                MatrixType& linRemoteCorners);

  /** \brief Compute the nodally averaged normal field.
   *
   *  We cannot use the 'normals' method of the patch, because we have to take exactly the same normals
   *  as the ContactMerge projection in the dune-grid-glue module.
   */
  template <class GridView>
  auto setupAveragedNormals(const BoundaryPatch<GridView>& patch) const;

private:
  //! Setup index pattern
  template <class LocalAssembler, class MatrixType>
  void setupIndexPattern(LocalAssembler& localAssembler,
                const MatrixType& linAverageNormals,
                MatrixType& linRemoteCorners);

    //! Compute normal the way the Codim1Extractor is doing it
    template <class CornerVector>
    auto computeNormal(const CornerVector& corners) const {
      std::decay_t<decltype(corners[0])> normal(0);
      if (GridView0::dimension == 2) {
        normal[0] =  corners[1][1] - corners[0][1];
        normal[1] =  corners[0][0] - corners[1][0];
      } else if (GridView0::dimension == 3) {
        auto edge0 = corners[1] - corners[0];
        auto edge1 = corners[2] - corners[0];
        normal = Dune::MatrixVector::crossProduct(edge0, edge1);
      }
      normal /= normal.two_norm();
      return normal;
    }

    //! the grid-glue object
    std::shared_ptr<GlueType> glue_;

    //! flag that is set if the outer normal projection is used
    bool isNormalProjection_;
};

template <class GlueType>
template <class GridView>
auto LinearisedIntersectionAssembler<GlueType>::setupAveragedNormals(const BoundaryPatch<GridView>& patch) const
{
  constexpr static int dim = GridView::dimension;
  using CoordinateType = FieldVector<ctype, dim>;

  const auto& indexSet = patch.gridView().indexSet();

  std::vector<CoordinateType> normals(indexSet.size(dim));

  for (size_t i=0; i<normals.size(); i++)
    normals[i] = 0;

  //compute nodally averaged normals
  for (const auto& e : elements(patch.gridView())) {
    if (not e.hasBoundaryIntersections())
      continue;

    const auto& ref = Dune::ReferenceElements<ctype, dim>::general(e.type());

    for (const auto& is : intersections(patch.gridView(), e)) {
      if (!patch.contains(is))
        continue;

      int nCorners = ref.size(is.indexInInside(), 1, dim);

      std::vector<CoordinateType> corners(nCorners);
      const auto& geometry = e.geometry();
      for (int i=0; i<nCorners; i++)
        corners[i] = geometry.corner(ref.subEntity(is.indexInInside(), 1, i, dim));

      auto faceNormal = computeNormal(corners);

      // in the Codim1Extractor these are swapped sometimes, so we also have to do this
      Dune::FieldVector<ctype, dim-1> zero(0);
      if (is.unitOuterNormal(zero) * faceNormal < 0.0) {
        std::swap(corners[0], corners[1]);
        faceNormal = computeNormal(corners);
      }

      for (int j=0; j < nCorners;j++)
        normals[indexSet.subIndex(e,ref.subEntity(is.indexInInside(), 1, j, dim), dim)] += faceNormal;
    }
  }
  for (auto& normal : normals)
    normal /= normal.two_norm();

  return normals;
}

template <class GlueType>
template <class LocalAssembler, class MatrixType>
void LinearisedIntersectionAssembler<GlueType>::assemble(LocalAssembler& localAssembler,
                                                         const MatrixType& linAverageNormals,
                                                         MatrixType& linRemoteCorners)
{
  // Setup index pattern
  setupIndexPattern(localAssembler, linAverageNormals, linRemoteCorners);
  linRemoteCorners = 0;

  // the grid0 dimension
  const static int dim = GlueType::Grid0View::dimension;
  using field_type = typename PromotionTraits<typename MatrixType::field_type, ctype>::PromotedType;
  using CoordinateType = FieldVector<field_type,dim>;

  const auto& gridView0 = glue_->template patch<0>().gridView();
  const auto& gridView1 = glue_->template patch<1>().gridView();

  const auto& indexSet0 = gridView0.indexSet();
  const auto& indexSet1 = gridView1.indexSet();

  const auto& rIsIndexSet = glue_->indexSet();
  const auto& reducedPatch0 = localAssembler.reducedPatch0();

  auto&& avNormals0 = setupAveragedNormals(localAssembler.unreducedPatch0());
  auto&& avNormals1 = setupAveragedNormals(localAssembler.unreducedPatch1());

  // each intersection pair of grid0 and grid1 only have to be treated once
  std::pair<int,unsigned> face0{-1, 0}, face1{-1, 0};

  for (const auto& rIs : intersections(*glue_)) {

    const auto& inside = rIs.inside();
    if (!reducedPatch0.contains(inside, rIs.indexInInside()))
      continue;

    const auto& outside = rIs.outside();

    // check if intersection is already handled
    if (face0 == std::make_pair<int,unsigned>(indexSet0.index(inside), rIs.indexInInside())
        and face1 == std::make_pair<int,unsigned>(indexSet1.index(outside), rIs.indexInOutside()))
      continue;

    // update involved faces
    face0 = {indexSet0.index(inside), rIs.indexInInside()};
    face1 = {indexSet1.index(outside), rIs.indexInOutside()};

    // create a mapping to check which element basis functions correspond to the face
    const auto& ref0 = ReferenceElements<ctype,dim>::general(inside.type());

    std::vector<int> idx0(ref0.size(rIs.indexInInside(), 1, dim));
    std::vector<int> globalIdx0(idx0.size());

    for (size_t i=0; i<idx0.size(); i++) {
      idx0[i] = ref0.subEntity(rIs.indexInInside(), 1, i, dim);
      globalIdx0[i] = indexSet0.subIndex(inside, idx0[i], dim);
    }

    const auto& ref1 = ReferenceElements<ctype,dim>::general(outside.type());

    std::vector<int> idx1(ref1.size(rIs.indexInOutside(), 1, dim));
    std::vector<int> globalIdx1(idx1.size());

    for (size_t i=0; i<idx1.size(); i++) {
      idx1[i] = ref1.subEntity(rIs.indexInOutside(), 1, i, dim);
      globalIdx1[i] = indexSet1.subIndex(outside, idx1[i], dim);
    }

    const auto& geometry0  = inside.geometry();
    const auto& geometry1  = outside.geometry();

    std::vector<CoordinateType> corners0(idx0.size());
    for (size_t i=0; i<corners0.size(); i++)
      corners0[i] = geometry0.corner(idx0[i]);

    std::vector<CoordinateType> corners1(idx1.size());
    for (size_t i=0; i<corners1.size(); i++)
      corners1[i] = geometry1.corner(idx1[i]);

    auto reconstructedNormal = computeNormal(corners0);

    auto findIntersection = [](const auto& view, const auto& element, auto idx) {
      auto is = view.ibegin(element);
      for (; is != view.iend(element); is++)
        if (is->indexInInside() == idx)
          break;
      return is;
    };

    auto is0 = findIntersection(gridView0, inside, rIs.indexInInside());
    Dune::FieldVector<ctype,dim-1> zero(0);
    if (is0->unitOuterNormal(zero) * reconstructedNormal < 0.0) {
      std::swap(corners0[0], corners0[1]);
      std::swap(globalIdx0[0], globalIdx0[1]);
    }

    // compute again to take swapping into account
    reconstructedNormal = computeNormal(corners1);

    auto is1 = findIntersection(gridView1, outside, rIs.indexInOutside());
    if (is1->unitOuterNormal(zero) * reconstructedNormal < 0.0) {
      std::swap(corners1[0], corners1[1]);
      std::swap(globalIdx1[0], globalIdx1[1]);
    }

    // extract corresponding normals
    std::vector<CoordinateType> normals0(idx0.size());
    for (size_t i=0; i<normals0.size(); i++)
      normals0[i] = avNormals0[globalIdx0[i]];

    std::vector<CoordinateType> normals1(idx1.size());
    for (size_t i=0; i<normals1.size(); i++)
      normals1[i] = avNormals1[globalIdx1[i]];

    // the offset in the global matrix
    int remoteIdx = rIsIndexSet.index(rIs)*dim;
    localAssembler.assemble(globalIdx0, corners0, normals0, is0->type(),
                            globalIdx1, corners1, normals1, is1->type(),
                            remoteIdx, linRemoteCorners);
  }
}

template <class GlueType>
template <class LocalAssembler, class MatrixType>
void LinearisedIntersectionAssembler<GlueType>::setupIndexPattern(LocalAssembler& localAssembler,
                                                         const MatrixType& linAverageNormals,
                                                         MatrixType& linRemoteCorners)
{
  const static int dim = GlueType::Grid0View::dimension;
  using field_type = typename PromotionTraits<typename MatrixType::field_type, ctype>::PromotedType;

  const auto& indexSet0 = glue_->template patch<0>().gridView().indexSet();
  const auto& indexSet1 = glue_->template patch<1>().gridView().indexSet();

  auto size0 = indexSet0.size(dim);
  size_t offsetDomain = (isNormalProjection_) ? 0 : size0;

  // get a global indexset for the remote intersections
  const auto& rIsIndexSet = glue_->indexSet();

  // create global vertex indices
  MatrixIndexSet matIndexSet(rIsIndexSet.size()*dim, size0 + indexSet1.size(dim));

  const auto& reducedPatch0 = localAssembler.reducedPatch0();
  const auto& reducedPatch1 = localAssembler.reducedPatch1();

  auto globalToLocal = (isNormalProjection_) ?  reducedPatch0.makeGlobalToLocal()
                                             : reducedPatch1.makeGlobalToLocal();

  // loop over all intersections
  for (const auto& rIs : intersections(*glue_)) {

    int rIdx = rIsIndexSet.index(rIs);

    const auto& inside = rIs.inside();
    const auto& outside = rIs.outside();

    // if intersection is not contained then skip it
    if (!reducedPatch0.contains(inside, rIs.indexInInside()))
      continue;

    const auto& ref0 = ReferenceElements<field_type, dim>::general(inside.type());
    const auto& ref1 = ReferenceElements<field_type, dim>::general(outside.type());

    for (int i=0; i<dim; i++) {

      int vIdx = rIdx*dim+i;

      // coupling with grid0 dofs
      for (int j=0; j < ref0.size(rIs.indexInInside(),1,dim); ++j) {
        int colIdx = indexSet0.subIndex(inside,
                          ref0.subEntity(rIs.indexInInside(),1,j,dim), dim);

        matIndexSet.add(vIdx, colIdx);
      }

      // coupling with grid1 dofs
      for (int k=0; k<ref1.size(rIs.indexInOutside(),1,dim); k++) {

        int colIdx = indexSet1.subIndex(outside,
                          ref1.subEntity(rIs.indexInOutside(),1,k,dim), dim);

        matIndexSet.add(vIdx,size0 + colIdx);
      }

      auto domSize = (isNormalProjection_) ? ref0.size(rIs.indexInInside(),1,dim)
                                           : ref1.size(rIs.indexInOutside(),1,dim);

      for (int k=0; k < domSize; ++k) {

        int idx = (isNormalProjection_) ? indexSet0.subIndex(inside,
                          ref0.subEntity(rIs.indexInInside(), 1, k, dim), dim)
                                        : indexSet1.subIndex(outside,
                          ref1.subEntity(rIs.indexInOutside(), 1, k, dim), dim);

        // also all nodes involved in the average normal
        const auto& row = linAverageNormals[globalToLocal[idx]];
        const auto& cEnd = row.end();
        for (auto cI = row.begin(); cI != cEnd; cI++)
          matIndexSet.add(vIdx, offsetDomain + cI.index());
      }
    }
  }

  matIndexSet.exportIdx(linRemoteCorners);
}

} /* namespace Contact */
} /* namespace Dune */

#endif
