#include <config.h>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/geometrygrid.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>

#include <dune/istl/solvers.hh>
#include <dune/istl/io.hh>

#include <dune/elasticity/assemblers/ogdenassembler.hh>

#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functions/deformationfunction.hh>

#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif

#include <dune/solvers/iterationsteps/trustregiongsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/energynorm.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/differencenormsquared.hh>
#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>

#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/assemblers/twobodyassembler.hh>
#include <dune/contact/solvers/nsnewtonmgstep.hh>
#include <dune/contact/solvers/nsnewtoncontacttransfer.hh>

using namespace Dune;
using namespace Dune::Contact;

// The grid dimension
const int dim = 2;

typedef double field_type;

// Some types that I need
typedef BCRSMatrix<FieldMatrix<double, dim, dim> > MatrixType;
typedef BlockVector<FieldVector<double, dim> >     VectorType;
typedef UGGrid<dim> GridType;
typedef DeformationFunction<GridType::LeafGridView,VectorType> Deformation;
typedef GeometryGrid<GridType,Deformation> DeformedGridType;
typedef BoundaryPatch<GridType::LevelGridView> LevelBoundary;
typedef BoundaryPatch<GridType::LeafGridView> LeafBoundary;
typedef BoundaryPatch<DeformedGridType::LevelGridView> DefLevelBoundary;

const double E[2]  = {17e6,1e6};
const double nu[2] = {0.3, 0.3};
const double d[2]  = {0,0};

class UnfeasableInitialIterateException : public Exception {};

void setTrustRegionObstacles(double trustRegionRadius,
                             std::vector<BoxConstraint<field_type,dim> >& trustRegionObstacles,
                             const std::vector<BoxConstraint<field_type,dim> >& trueObstacles,
                             const BitSetVector<dim>& totalDirichletNodes)
{
    for (int j=0; j<trustRegionObstacles.size(); j++) {
        
        for (int k=0; k<dim; k++) {
            
            if (totalDirichletNodes[j][k])
                continue;
            
            if (j >= trueObstacles.size()) {
                
                trustRegionObstacles[j].lower(k) = -trustRegionRadius;
                trustRegionObstacles[j].upper(k) =  trustRegionRadius;
                
            } else {

                trustRegionObstacles[j].lower(k) = std::max(-trustRegionRadius, trueObstacles[j].lower(k));
                trustRegionObstacles[j].upper(k) = std::min( trustRegionRadius, trueObstacles[j].upper(k));
           
            }

            // These cases may happen when we start from an infeasable configuration
            if (trustRegionObstacles[j].upper(k) < 0 
                && trustRegionObstacles[j].upper(k) < trustRegionObstacles[j].lower(k))
                trustRegionObstacles[j].lower(k) = trustRegionObstacles[j].upper(k);

            if (trustRegionObstacles[j].lower(k) > 0 
                && trustRegionObstacles[j].upper(k) < trustRegionObstacles[j].lower(k))
                trustRegionObstacles[j].upper(k) = trustRegionObstacles[j].lower(k);

        }
        
    }

    //std::cout << trustRegionObstacles << std::endl;
//     exit(0);
}


template <int numGrids>
void compute (const array<DeformedGridType*, numGrids>& grid,
              array<Deformation*,numGrids>& deformation,
              array<VectorType,numGrids>& x,
              const array<VectorType,numGrids>& coarseDirichletValues, 
              const array<BitSetVector<1>,numGrids>& coarseDirichletNodes,
              const array<LevelBoundary, numGrids>& coarseDirichletBoundary,
              const DefLevelBoundary& obsPatch,
              double obsDistance,
              double trustRegionRadius,
              int maxTrustRegionSteps,
              double mgTolerance,
              int mgIterations)
{

    array<std::vector<VectorType>, numGrids> dirichletValues;
    array<std::vector<BitSetVector<dim> >, 2> dirichletNodes;
    array<std::vector<LevelBoundary>, 2> dirichletBoundary;
        
    std::vector<VectorType> rhs(numGrids);
    
    for (int i=0; i<numGrids; i++) {
        
        dirichletValues[i].resize(grid[i]->maxLevel()+1);
        dirichletValues[i][0] = coarseDirichletValues[i];
        
        /** \todo We don't need the whole hierarchy, do we? */
        dirichletNodes[i].resize(grid[i]->maxLevel()+1);
        
        dirichletBoundary[i].resize(grid[i]->maxLevel()+1);
        dirichletBoundary[i][0].setup(grid[i]->hostGrid().levelGridView(0), coarseDirichletNodes[i]);
        
        // Initial solution
        assert(x[i].size() == grid[i]->size(dim));
        
        BoundaryPatchProlongator<GridType>::prolong(dirichletBoundary[i]);
        
        for (int j=0; j<=grid[i]->maxLevel(); j++) {
            
            int fSSize = grid[i]->size(j,dim);
            dirichletNodes[i][j].resize(fSSize);
            for (int k=0; k<fSSize; k++)
                dirichletNodes[i][j][k] = dirichletBoundary[i][j].containsVertex(k);
            
        }
        
        sampleOnBitField(*grid[i], dirichletValues[i], dirichletNodes[i]);
        
        //    Create rhs vectors
        
        rhs[i].resize(grid[i]->size(grid[i]->maxLevel(),dim));
        rhs[i] = 0;
        
        // Copy Dirichlet information into the solution vector
        for (int j=0; j<rhs[i].size(); j++)
            for (int k=0; k<dim; k++) {
                if (dirichletNodes[i].back()[j][k])
                    x[i][j][k] = dirichletValues[i].back()[j][k];
                
            }
        
    }

    int toplevel = std::max(grid[0]->maxLevel(), grid[1]->maxLevel());
    
    
    // make dirichlet bitfields containing dirichlet information for both grids
    std::vector<BitSetVector<dim> > totalDirichletNodes(toplevel+1);
    
    for (int colevel=0; colevel<=toplevel; colevel++) {
        
        int levelGrid0 = std::max(0, grid[0]->maxLevel()-colevel);
        int levelGrid1 = std::max(0, grid[1]->maxLevel()-colevel);
        
        int offset = dirichletValues[0][levelGrid0].size();
        
        totalDirichletNodes[toplevel-colevel].resize(dirichletNodes[0][levelGrid0].size() + dirichletNodes[1][levelGrid1].size());
        
        for (int j=0; j<dirichletNodes[0][levelGrid0].size(); j++)
            totalDirichletNodes[toplevel-colevel][j] = dirichletNodes[0][levelGrid0][j];
        
        for (int j=0; j<dirichletNodes[1][levelGrid1].size(); j++)
            totalDirichletNodes[toplevel-colevel][offset + j]  = dirichletNodes[1][levelGrid1][j];
        
    }


    // Assemble contact problem
    DefLevelBoundary mortarPatch(grid[1]->levelGridView(0),true);
    TwoBodyAssembler<DeformedGridType, VectorType, false> contactAssembler(*grid[0], *grid[1],
                                                                   obsPatch, mortarPatch, obsDistance,
                                                                   CouplingPairBase::CONTACT);

    BitSetVector<1> hasObstacle(grid[0]->size(dim) + grid[1]->size(dim), false);
    std::vector<BoxConstraint<field_type,dim> > trustRegionObstacles(grid[0]->size(dim) + grid[1]->size(dim));


    // //////////////////////////////////////
    //   Create a solver
    // //////////////////////////////////////

    // First create a base solver
#ifdef HAVE_IPOPT
    QuadraticIPOptSolver<MatrixType,VectorType> baseSolver;
#endif
    baseSolver.verbosity_ = Solver::QUIET;
    baseSolver.tolerance_ = 1e-8;   // base grid solver tolerance

    // Make pre and postsmoothers
    TrustRegionGSStep<MatrixType, VectorType> presmoother, postsmoother;
    

    NonSmoothNewtonMGStep<MatrixType, VectorType> multigridStep(1);

    multigridStep.setMGType(1, 3, 3);   // A V(3,3) cycle
    multigridStep.ignoreNodes_       = &totalDirichletNodes[toplevel];
    multigridStep.basesolver_        = &baseSolver;
    multigridStep.setSmoother(&presmoother, &postsmoother);
    multigridStep.hasObstacle_       = &hasObstacle;
    multigridStep.obstacles_         = &trustRegionObstacles;
    multigridStep.verbosity_         = Solver::QUIET;

    // Create the transfer operators
    multigridStep.mgTransfer_.resize(grid[0]->maxLevel());
    for (int i=0; i<multigridStep.mgTransfer_.size(); i++)
        multigridStep.mgTransfer_[i] = NULL;

    EnergyNorm<MatrixType, VectorType> energyNorm;

    ::LoopSolver<VectorType> solver(&multigridStep,
                                                   mgIterations,
                                                   mgTolerance,
                                                   &energyNorm,
                                                   Solver::QUIET);

    
    // //////////////////////////////////////////////////////////
    //   Assemble linear elasticity matrix for the energy norm
    //   used by the MMG termination criterion
    // //////////////////////////////////////////////////////////
    // Assemble separate linear elasticity problems
    typedef P1NodalBasis<GridType::LeafGridView> P1Basis;
    P1Basis p1Basis0(grid[0]->hostGrid().leafView());
    P1Basis p1Basis1(grid[1]->hostGrid().leafView());
    
    OperatorAssembler<P1Basis,P1Basis> globalAssembler0(p1Basis0,p1Basis0);
    OperatorAssembler<P1Basis,P1Basis> globalAssembler1(p1Basis1,p1Basis1);
    
    StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(2.5e5, 0.3);

    MatrixType stiffnessMatrix0, stiffnessMatrix1;
    globalAssembler0.assemble(localAssembler, stiffnessMatrix0);
    globalAssembler1.assemble(localAssembler, stiffnessMatrix1);
        
    array<const MatrixType*, 2> linearElasticityMatrices;
    linearElasticityMatrices[0] = &stiffnessMatrix0;
    linearElasticityMatrices[1] = &stiffnessMatrix1;

    typedef OgdenAssembler<P1Basis, P1Basis> Assembler;
    //typedef NeoHookeAssembler<P1Basis, P1Basis> Assembler;

    array<Assembler*,2> ogdenAssembler;
    ogdenAssembler[0] = new Assembler(p1Basis0,p1Basis0);
    ogdenAssembler[1] = new Assembler(p1Basis1,p1Basis1);
    
    for (int i=0; i<2; i++) {
        ogdenAssembler[i]->setEandNu(E[i], nu[i]);
        ogdenAssembler[i]->d_ = d[i];
    }
    // /////////////////////////////////////////////////////
    //   Trust-Region Solver
    // /////////////////////////////////////////////////////

    for (int i=0; i<maxTrustRegionSteps; i++) {
        
        double oldEnergy = ogdenAssembler[0]->computeEnergy(x[0])
            + ogdenAssembler[1]->computeEnergy(x[1]);
        
        if (std::isnan(oldEnergy))
            DUNE_THROW(UnfeasableInitialIterateException, "Unfeasable initial iterate!");
        
        std::cout << "------------------------------------------------------" << std::endl;
        std::cout << "      Trust-Region Step Number: " << i << ",   Energy: " << oldEnergy << std::endl;
        std::cout << "------------------------------------------------------" << std::endl;

        // Deform the grid
        deformation[0]->setDeformation(x[0]);
        deformation[1]->setDeformation(x[1]);

        // /////////////////////////////////////////////////////
        //   Assemble the obstacle and the transfer operator 
        // /////////////////////////////////////////////////////
        contactAssembler.assembleObstacle();
        contactAssembler.assembleTransferOperator();

        // /////////////////////////////////////////////////////
        //   Check if initial iterate is inadmissible
        // /////////////////////////////////////////////////////
        bool initialIterateIsAdmissible = true;
        for (int j=0; j<contactAssembler.obstacles_[grid[0]->maxLevel()].size() && initialIterateIsAdmissible; j++)
            for (int k=0; k<dim; k++)
                if (contactAssembler.obstacles_[grid[0]->maxLevel()][j].lower(k) > 0
                    || contactAssembler.obstacles_[grid[0]->maxLevel()][j].upper(k) < 0) {
                    initialIterateIsAdmissible = false;
                    break;
                }

        
        // //////////////////////////////////////////////////////
        //    Lengthen obstacle vector, to formally account for both
        //    bodies, even though the second one doesn't actually have obstacles
        // //////////////////////////////////////////////////////
        
        hasObstacle.unsetAll();
        for (int j=0; j<grid[0]->size(dim); j++)
            hasObstacle[j] = contactAssembler.nonmortarBoundary_[0][grid[0]->maxLevel()].containsVertex(j);

        // //////////////////////////////////////////////////////////////////////
        //   The MG transfer operators involve the mortar coupling operator
        //   and have to be reassembled at each trust region step.
        // //////////////////////////////////////////////////////////////////////
        for (int j=0; j<multigridStep.mgTransfer_.size(); j++) {
        
            NonSmoothNewtonContactTransfer<VectorType>* newTransferOp = new NonSmoothNewtonContactTransfer<VectorType>;
            newTransferOp->setup(grid[0]->hostGrid(), grid[1]->hostGrid(), j,
                                 contactAssembler.contactCoupling_[0].maxLevelMortarLagrangeMatrix(),
                                 contactAssembler.localCoordSystems_[grid[0]->maxLevel()-j],
                                 hasObstacle);
            
            if (multigridStep.mgTransfer_[toplevel-j-1])
                delete(multigridStep.mgTransfer_[toplevel-j-1]);
            multigridStep.mgTransfer_[toplevel-j-1] = newTransferOp;
            
        }
    
        // Since we're solving a trust-region subproblem all dofs have obstacles
        hasObstacle.setAll();


        // ///////////////////////////////////////////////////////////////////////////////
        //   Make compound linear elasticity matrix for the multigrid stopping criterion
        // ///////////////////////////////////////////////////////////////////////////////
        MatrixType transformedLinearMatrix;
        contactAssembler.assemble(linearElasticityMatrices, transformedLinearMatrix);
        
        energyNorm.setMatrix(&transformedLinearMatrix);
    

        // ///////////////////////////////////////////////////////////////////////////////
        //   Assemble quadratic problem
        // ///////////////////////////////////////////////////////////////////////////////
        rhs[0] = 0;
        rhs[1] = 0;

        std::vector<const MatrixType*> elastMatrix(numGrids);
        for (int i=0; i<numGrids; i++) {
            elastMatrix[i] = new MatrixType;
            ogdenAssembler[i]->assembleProblem(*const_cast<MatrixType*>(elastMatrix[i]), x[i], rhs[i]);
        }

        // Transform the nonlinear elasticity matrix into the mortar basis
        MatrixType transformedMatrix;
        contactAssembler.assembleJacobian(elastMatrix,transformedMatrix);
        
        VectorType totalRhs(rhs[0].size() + rhs[1].size());
        contactAssembler.assembleRightHandSide(rhs, totalRhs);
        
        // The correction for both grids together in local coordinates
        VectorType totalCorr(x[0].size() + x[1].size());
        
        // The correction in canonical coordinates and for each grid separately
        array<VectorType, 2> corr;
        
        // ///////////////////////////////////////////////////////////////////////////////////////
        //   Loop and decrease the trust-region radius until we get objective function reduction
        // ///////////////////////////////////////////////////////////////////////////////////////
        do {
            
            std::cout << "### Trust-Region Radius: " << trustRegionRadius << " ###" << std::endl;
            
            totalCorr = 0;
            
            // Create trust-region obstacle on grid0.maxLevel()
            setTrustRegionObstacles(trustRegionRadius,
                                    trustRegionObstacles,
                                    contactAssembler.obstacles_[grid[0]->maxLevel()],
                                    totalDirichletNodes[grid[0]->maxLevel()]);
            
            if (dynamic_cast<MultigridStep<MatrixType,VectorType>*>(solver.iterationStep_))
                dynamic_cast<MultigridStep<MatrixType,VectorType>*>(solver.iterationStep_)->setProblem(transformedMatrix, totalCorr, totalRhs, grid[0]->maxLevel()+1);
            else {
                DUNE_THROW(Exception, "You need a multigrid step!");
                //solver.iterationStep_->setProblem(*contactAssembler.mat_, totalCorr, totalRhs);
            }

            solver.preprocess();
            
            // /////////////////////////////
            //    Solve !
            // /////////////////////////////
            solver.solve();
            
            totalCorr = multigridStep.getSol();
            
            //std::cout << "totalCorr: \n" << totalCorr << std::endl;

            contactAssembler.postprocess(totalCorr, corr);
            
            // ////////////////////////////////////////////////////
            //   Check whether trust-region step can be accepted
            // ////////////////////////////////////////////////////
            
            printf("infinity norm of the correction: %g %g\n", 
                   corr[0].infinity_norm(), corr[1].infinity_norm());
            
            if (corr[0].infinity_norm() < 1e-4 
                && corr[1].infinity_norm() < 1e-4)
                return;
            
            /** \todo Faster with expression templates */
            VectorType newIterate0 = x[0];  newIterate0 += corr[0];
            VectorType newIterate1 = x[1];  newIterate1 += corr[1];
            
            double energy = ogdenAssembler[0]->computeEnergy(newIterate0)
                + ogdenAssembler[1]->computeEnergy(newIterate1);
            
            // //////////////////////////////////////////////////////
            //   Compute the model decrease
            //   It is $ m(x) - m(x+s) = -<g,s> - 0.5 <s, Hs>
            //   Note that rhs = -g
            // //////////////////////////////////////////////////////
            double modelDecrease = 0;
            for (size_t j=0; j<2; j++) {
                VectorType tmp(corr[j].size());
                tmp = 0;
                elastMatrix[j]->umv(corr[j], tmp);
                modelDecrease += (rhs[j]*corr[j]) - 0.5 * (corr[j]*tmp);
            }

            std::cout << "Model decrease: " << modelDecrease 
                      << ",  functional decrease: " << oldEnergy - energy << std::endl;

            if (modelDecrease < 0) {
                if (!initialIterateIsAdmissible)
                    std::cout << "Model energy is not decreased, but initial iterate was not admissible!" << std::endl;
                else
                    DUNE_THROW(SolverError, "Model energy is not decreased!");
            }

            if (energy >= oldEnergy) {
                if (!initialIterateIsAdmissible)
                    std::cout << "True energy is not decreased, but initial iterate was not admissible!" << std::endl;
                else
                    DUNE_THROW(SolverError, "Richtung ist keine Abstiegsrichtung!");
            }

            // //////////////////////////////////////////////
            //   Check for acceptance of the step
            // //////////////////////////////////////////////
            if ( (oldEnergy-energy) / modelDecrease > 0.9) {
                // very successful iteration
                
                //x = newIterate;
                trustRegionRadius *= 2;
                break;
                
            } else if ( (oldEnergy-energy) / modelDecrease > 0.01
                        || std::abs(oldEnergy-energy) < 1e-12) {
                // successful iteration
                //x = newIterate;
                break;
                
            } else {
                // unsuccessful iteration
                trustRegionRadius /= 2;
                std::cout << "Unsuccessful iteration!" << std::endl;
            }
            
            //  Write current energy
            std::cout << "--- Current energy: " << energy << " ---" << std::endl;

        } while (true);
        
        //  Add correction to the current solution
        x[0] += corr[0];
        x[1] += corr[1];

        // cleanup
        for (int i=0; i<numGrids;i++) {
            delete(elastMatrix[i]);
            delete(ogdenAssembler[i]);
        }
    }

}

int main(int argc, char *argv[]) try 
{
    // parse data file
    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("nladaptmeasure.parset", parameterSet);

    // read solver settings
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int maxTrustRegionSteps = parameterSet.get("maxTrustRegionSteps", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const int baseIt           = parameterSet.get("baseIt", int(0));
    const double tolerance     = parameterSet.get("tolerance", double(0));
    const double baseTolerance = parameterSet.get("baseTolerance", double(0));
    const double refinementFraction = parameterSet.get("refinementFraction", double(1));
    const bool paramBoundaries = parameterSet.get("paramBoundaries", int(0));
    
    // read problem settings
    std::string path                 = parameterSet.get("path", "xyz");
    std::string object0Name          = parameterSet.get("gridFile0", "xyz");
    std::string object1Name          = parameterSet.get("gridFile1", "xyz");
    std::string parFile0             = parameterSet.get("parFile0", "xyz");
    std::string parFile1             = parameterSet.get("parFile1", "xyz");
    std::string dirichletFile0       = parameterSet.get("dirichletFile0", "xyz");
    std::string dirichletFile1       = parameterSet.get("dirichletFile1", "xyz");
    std::string dirichletValuesFile0 = parameterSet.get("dirichletValuesFile0", "xyz");
    std::string dirichletValuesFile1 = parameterSet.get("dirichletValuesFile1", "xyz");
    std::string obsFilename          = parameterSet.get("obsFilename", "xyz");
    double obsDistance               = parameterSet.get("obsDistance", double(0));
    double scaling                   = parameterSet.get("scaling", double(1));
    double trustRegionRadius = parameterSet.get("trustRegionRadius", double(1));

    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    array<GridType, 2> uniformGrid;
    uniformGrid[0].setRefinementType(GridType::COPY);
    uniformGrid[1].setRefinementType(GridType::COPY);
    
    if (paramBoundaries) {
        AmiraMeshReader<GridType>::read(uniformGrid[0], path + object0Name, path + parFile0);
        AmiraMeshReader<GridType>::read(uniformGrid[1], path + object1Name, path + parFile1);
    } else {
        AmiraMeshReader<GridType>::read(uniformGrid[0], path + object0Name);
        AmiraMeshReader<GridType>::read(uniformGrid[1], path + object1Name);
    }

    // /////////////////////////////////////////////////////
    //   Read coarse grid Dirichlet information
    // /////////////////////////////////////////////////////
    array<VectorType, 2> dirichletValues;
    dirichletValues[0].resize(uniformGrid[0].size(0, dim));
    dirichletValues[1].resize(uniformGrid[1].size(0, dim));
    AmiraMeshReader<GridType>::readFunction(dirichletValues[0], path + dirichletValuesFile0);
    AmiraMeshReader<GridType>::readFunction(dirichletValues[1], path + dirichletValuesFile1);

    // Scale Dirichlet values
    dirichletValues[0] *= scaling;
    dirichletValues[1] *= scaling;

    array<BitSetVector<1>, 2> dirichletNodes;
    readBitField(dirichletNodes[0], uniformGrid[0].size(0, dim), path + dirichletFile0);
    readBitField(dirichletNodes[1], uniformGrid[1].size(0, dim), path + dirichletFile1);
    
    array<LevelBoundary, 2> coarseDirichletBoundary;
    coarseDirichletBoundary[0].setup(uniformGrid[0].levelView(0));
    coarseDirichletBoundary[1].setup(uniformGrid[1].levelView(0));
    readBoundaryPatch<GridType>(coarseDirichletBoundary[0], path + dirichletFile0);
    readBoundaryPatch<GridType>(coarseDirichletBoundary[1], path + dirichletFile1);

    // ////////////////////////////////////////////////////////////////////
    //   Solve the problem on the coarsest level (using path following,
    //   if necessary), to get a good start iterate.
    // ////////////////////////////////////////////////////////////////////
    
    array<VectorType,2> uniformX, scaledDirichletValues;
    uniformX[0].resize(uniformGrid[0].size(dim));
    uniformX[1].resize(uniformGrid[1].size(dim));
    uniformX[0] = 0;
    uniformX[1] = 0;

    // create deformed grids
    array<Deformation*,2> deformation;
    array<DeformedGridType*,2> defUniformGrid;
    for (int i=0;i<2;i++) {
        deformation[i]=new Deformation(uniformGrid[i].leafGridView(),uniformX[i]);
        defUniformGrid[i]= new DeformedGridType(&uniformGrid[i],deformation[i]);
    }

    // /////////////////////////////////////////////////////
    // read field describing the obstacles
    // /////////////////////////////////////////////////////

    DefLevelBoundary obsPatch(defUniformGrid[0]->levelView(0));
    readBoundaryPatch<DeformedGridType>(obsPatch, path + obsFilename);

    double loadFactor    = 0;
    double loadIncrement = 1;

    do {

        if (loadFactor < 1) {

            // ////////////////////////////////////////////////////
            //   Compute new feasible load increment
            // ////////////////////////////////////////////////////
            bool initialIterateIsFeasable;
            loadIncrement *= 2;  // double it once, cause we're going to half it at least once, too!
            do {
                initialIterateIsFeasable = true;
                loadIncrement /= 2;
                
                // Set new Dirichlet values in solution
                for (int j=0; j<2; j++) {

                    scaledDirichletValues[j] = dirichletValues[j];
                    scaledDirichletValues[j] *= loadFactor + loadIncrement;

                    for (int k=0; k<uniformX[j].size(); k++)
                        for (int l=0; l<dim; l++)
                            if (dirichletNodes[j][k][l])
                                uniformX[j][k][l] = dirichletValues[j][k][l] * (loadFactor + loadIncrement);
                
                }

                std::cout << "####################################################" << std::endl;
                std::cout << "New load factor: " << loadFactor + loadIncrement
                          << "    new load increment: " << loadIncrement << std::endl;
                std::cout << "####################################################" << std::endl;

                try {
                    compute<2>(defUniformGrid, deformation, uniformX, scaledDirichletValues, dirichletNodes, coarseDirichletBoundary,
                               obsPatch, obsDistance, trustRegionRadius, maxTrustRegionSteps, tolerance, numIt);
                } catch (UnfeasableInitialIterateException e) {
                    std::cout << "Reducing path following parameter\n";
                    initialIterateIsFeasable = false;
                }

            } while (initialIterateIsFeasable == false);
            
            loadFactor += loadIncrement;

        } 

    } while (loadFactor < 1);

    // Store level 0 solution for later use as a starting iterate
    array<VectorType,2> levelZeroSolution = uniformX;

    // /////////////////////////////////////////////////////
    // refine uniformly until minlevel
    // /////////////////////////////////////////////////////
    typedef P1NodalBasis<GridType::LeafGridView> P1Basis;
    for (int i=0; i<maxLevel; i++) {

        for (int j=0; j<uniformGrid.size(); j++) {

            for (GridType::Codim<0>::LeafIterator eIt = uniformGrid[j].leafbegin<0>();
                 eIt != uniformGrid[j].leafend<0>();
                 ++eIt)
                uniformGrid[j].mark(1, *eIt);
            P1Basis p1Basis(uniformGrid[j].leafView());
            GridFunctionAdaptor<P1Basis> adaptor(p1Basis,true,true);
           
            uniformGrid[j].preAdapt();
            uniformGrid[j].adapt();
            uniformGrid[j].postAdapt();

            p1Basis.update();
            adaptor.adapt(uniformX[j]);
                    	
            
            if (paramBoundaries)
                improveGrid(uniformGrid[j], 10);

            deformation[j]->setDeformation(uniformX[j]);
            deformation[j]->setGridView(uniformGrid[j].leafGridView());
            defUniformGrid[j]->update();
        }
    }

    // /////////////////////////////////////////////////////////
    //   Solver the problem on the uniformly refined grids
    // /////////////////////////////////////////////////////////

    if (maxLevel!=0)
        compute<2>(defUniformGrid, deformation, uniformX, dirichletValues, dirichletNodes,
                    coarseDirichletBoundary, obsPatch, obsDistance, trustRegionRadius,
                    maxTrustRegionSteps, tolerance, numIt);

    // Output result
    LeafAmiraMeshWriter<GridType>::writeGrid(uniformGrid[0], "0uniformGrid");
    LeafAmiraMeshWriter<GridType>::writeGrid(uniformGrid[1], "1uniformGrid");
    
    if (dim==2) {
        
        for (int i=0; i<2; i++)
            deformation[i]->setDeformation(uniformX[i]);

        LeafAmiraMeshWriter<DeformedGridType>::writeGrid(*defUniformGrid[0], "0uniformGridDef");
        LeafAmiraMeshWriter<DeformedGridType>::writeGrid(*defUniformGrid[1], "1uniformGridDef");

    }
    // /////////////////////////////////////////////////////////
    //   Load coarse grids again
    // /////////////////////////////////////////////////////////
    
    array<GridType, 2> adaptiveGrid;
    adaptiveGrid[0].setRefinementType(GridType::COPY);
    adaptiveGrid[1].setRefinementType(GridType::COPY);
    
    if (paramBoundaries) {
        AmiraMeshReader<GridType>::read(adaptiveGrid[0], path + object0Name, path + parFile0);
        AmiraMeshReader<GridType>::read(adaptiveGrid[1], path + object1Name, path + parFile1);
    } else {
        AmiraMeshReader<GridType>::read(adaptiveGrid[0], path + object0Name);
        AmiraMeshReader<GridType>::read(adaptiveGrid[1], path + object1Name);
    }

    // create the deformed grids
    array<DeformedGridType*,2> defAdaptiveGrid;
    for (int i=0;i<2;i++) {
        VectorType displace(adaptiveGrid[i].size(dim));
        displace = 0;

        deformation[i]->setDeformation(displace);
        deformation[i]->setGridView(adaptiveGrid[i].leafGridView());
        defAdaptiveGrid[i]= new DeformedGridType(&adaptiveGrid[i],deformation[i]);
    }

    // /////////////////////////////////////////////////////
    // read field describing the obstacles again, so this time
    // it is attached to the adaptive grid.
    // /////////////////////////////////////////////////////

    obsPatch.setup(defAdaptiveGrid[0]->levelGridView(0));
    readBoundaryPatch<DeformedGridType>(obsPatch, path + obsFilename);


    // /////////////////////////////////////////////////////////
    //   Refinement loop
    // /////////////////////////////////////////////////////////

    std::ofstream logfile("nladaptmeasure.results");

    array<VectorType,2> adaptiveX = levelZeroSolution;

    for (int i=0; i<maxLevel+1; i++) {

        // /////////////////////////////////////////////////////////////
        //   Solve problem on locally refined grid
        // /////////////////////////////////////////////////////////////
        if (i!=0)
            compute<2>(defAdaptiveGrid, deformation, adaptiveX, dirichletValues, dirichletNodes, coarseDirichletBoundary,
                       obsPatch, obsDistance, trustRegionRadius, maxTrustRegionSteps, tolerance, numIt);
        
        // /////////////////////////////////////////////////////////////
        //   Compute 'actual' error by comparing the the solution
        //   on the uniformly refined grid
        // /////////////////////////////////////////////////////////////
        array<StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement>*,2> linearLocalStiffness;
        linearLocalStiffness[0]=new StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> (E[0],nu[0]);
        linearLocalStiffness[1]=new StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> (E[1],nu[1]);

        double actualError = 0;
        for (size_t j=0; j<uniformGrid.size(); j++) {
            actualError += DifferenceNormSquared<GridType>::compute(uniformGrid[j], uniformX[j], 
                                                                    adaptiveGrid[j], adaptiveX[j],
                                                                    linearLocalStiffness[j]);
        }
        actualError = std::sqrt(actualError);

        // /////////////////////////////////////////////////////////////
        //   Estimate the error with an a posteriori estimator
        // /////////////////////////////////////////////////////////////

        // ////////////////////////////////////////////////////
        //    Estimate error per element 
        // ////////////////////////////////////////////////////
        HierarchicContactEstimator<GridType,field_type> estimator(adaptiveGrid[0], adaptiveGrid[1]);

        LevelBoundary undefObsPatch(adaptiveGrid[0].levelGridView(0),*obsPatch.getVertices());
        estimator.couplings_[0].obsPatch_    = &undefObsPatch;
        estimator.couplings_[0].obsDistance_ = obsDistance;
        estimator.couplings_[0].type_        = CouplingPairBase::CONTACT;

        std::vector<VectorType> hierarchicError(2);
        
        std::vector<RefinementIndicator<GridType>*> refinementIndicator(2);
        refinementIndicator[0] = new RefinementIndicator<GridType>(adaptiveGrid[0]);

        refinementIndicator[1] = new RefinementIndicator<GridType>(adaptiveGrid[1]);

        /** \todo We really need the leaf level Dirichlet boundary instead of the toplevel */
        array<std::vector<LevelBoundary>, 2> dirichletBoundary;
        array<LeafBoundary, 2> leafDirichletBoundary;
        
        for (int j=0; j<adaptiveGrid.size(); j++) {
            dirichletBoundary[j].resize(adaptiveGrid[j].maxLevel()+1);
            dirichletBoundary[j][0].setup(adaptiveGrid[j].levelView(0), dirichletNodes[j]);
            BoundaryPatchProlongator<GridType>::prolong(dirichletBoundary[j]);
        }
        BoundaryPatchProlongator<GridType>::prolong(coarseDirichletBoundary[0], leafDirichletBoundary[0]);
        BoundaryPatchProlongator<GridType>::prolong(coarseDirichletBoundary[1], leafDirichletBoundary[1]);
        typedef P2NodalBasis<GridType::LeafGridView> P2Basis;
        array<OgdenMaterialLocalStiffness<GridType,typename P2Basis::LocalFiniteElement, typename P2Basis::LocalFiniteElement>,2> ogdenLocalStiffness;
        ogdenLocalStiffness[0].setEandNu(E[0], nu[0], d[0]);
        ogdenLocalStiffness[1].setEandNu(E[1], nu[1], d[1]);

        estimator.estimate(adaptiveX[0], adaptiveX[1], 
                          &leafDirichletBoundary[0], &leafDirichletBoundary[1],
                          refinementIndicator, 
                          &ogdenLocalStiffness[0], &ogdenLocalStiffness[1]);

       // ////////////////////////////////////////////////////////////
       //   Estimate estimator efficiency
       // ////////////////////////////////////////////////////////////
       
        double estimatedError = std::sqrt(estimator.getErrorSquared(0, &ogdenLocalStiffness[0]) 
                                         + estimator.getErrorSquared(1, &ogdenLocalStiffness[1]));
       
       std::cout << "Actual error: " << actualError << "     "
                 << "estimated error: " << estimatedError << "     "
                 << "estimator efficiency: " << estimatedError/actualError << "   "
                 << "vertices: " << adaptiveGrid[0].size(dim) + adaptiveGrid[1].size(dim)
                 << std::endl;

       logfile << actualError << "     " << estimatedError 
               << "     " << estimatedError/actualError 
               << "     " << adaptiveGrid[0].size(dim) + adaptiveGrid[1].size(dim) << std::endl;

       if (i==maxLevel)
           break;

       // ////////////////////////////////////////////////////////
       //   Refine the grids and transfer the current solution
       // ////////////////////////////////////////////////////////
       std::vector<GridType*> adaptiveGridVector(2);
       adaptiveGridVector[0] = &adaptiveGrid[0];
       adaptiveGridVector[1] = &adaptiveGrid[1];
       FractionalMarkingStrategy<GridType>::mark(refinementIndicator, adaptiveGridVector, refinementFraction);

        for (int j=0; j<adaptiveGrid.size(); j++) {

            P1Basis p1Basis(adaptiveGrid[j].leafView());
            GridFunctionAdaptor<P1Basis> adaptor(p1Basis,true,true);
           
            adaptiveGrid[j].preAdapt();
            adaptiveGrid[j].adapt();
            adaptiveGrid[j].postAdapt();

            p1Basis.update();
            adaptor.adapt(adaptiveX[j]);
                    	
            if (paramBoundaries)
                improveGrid(adaptiveGrid[j],10);

            deformation[j]->setDeformation(adaptiveX[j]);
            deformation[j]->setGridView(adaptiveGrid[j].leafGridView());
            defAdaptiveGrid[j]->update();

        }

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        std::cout << "  Grid: 0   Level: " << adaptiveGrid[0].maxLevel()
                  << "   vertices: " << adaptiveGrid[0].size(adaptiveGrid[0].maxLevel(), dim) 
                  << "   elements: " << adaptiveGrid[0].size(adaptiveGrid[0].maxLevel(), 0) << std::endl;
        std::cout << "  Grid: 1   Level: " << adaptiveGrid[1].maxLevel()
                  << "   vertices: " << adaptiveGrid[1].size(adaptiveGrid[1].maxLevel(), dim) 
                  << "   elements: " << adaptiveGrid[1].size(adaptiveGrid[1].maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

        // Output result
        LeafAmiraMeshWriter<GridType>::writeGrid(adaptiveGrid[0], "0resultGrid");
        LeafAmiraMeshWriter<GridType>::writeGrid(adaptiveGrid[1], "1resultGrid");

    }

    // Output result
    LeafAmiraMeshWriter<GridType>::writeGrid(adaptiveGrid[0], "0resultGrid");
    LeafAmiraMeshWriter<GridType>::writeGrid(adaptiveGrid[1], "1resultGrid");
    
    LeafAmiraMeshWriter<GridType>::writeBlockVector(adaptiveGrid[0], adaptiveX[0], "0resultGridFunc");
    LeafAmiraMeshWriter<GridType>::writeBlockVector(adaptiveGrid[1], adaptiveX[1], "1resultGridFunc");

    if (dim==2) {
        
        for (int i=0; i<2; i++)
            deformation[i]->setDeformation(adaptiveX[i]);

        LeafAmiraMeshWriter<DeformedGridType>::writeGrid(*defAdaptiveGrid[0], "0resultGridDef");
        LeafAmiraMeshWriter<DeformedGridType>::writeGrid(*defAdaptiveGrid[1], "1resultGridDef");

    }

} catch (Exception e) {

    std::cout << e << std::endl;

 }
