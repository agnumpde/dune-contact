// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_P2_ONEBODY_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_P2_ONEBODY_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/contact/assemblers/onebodyassembler.hh>

namespace Dune {
namespace Contact {

/** \brief An assembler for one-body contact problems with p2 elements
 */
template <class GridType, class field_type=double>
class P2OneBodyAssembler : public OneBodyAssembler<GridType, field_type>
{
    enum {dim = GridType::dimension};

    using Base = OneBodyAssembler<GridType,field_type>;
    using CoordinateType = typename Base::CoordinateType;
    using Obstacle = typename Base::Obstacle;
    using LevelBoundaryPatch = typename Base::LevelBoundaryPatch;
    using LeafBoundaryPatch = typename Base::LeafBoundaryPatch;
    using MatrixBlock = Dune::FieldMatrix<field_type, dim, dim>;

public:

    P2OneBodyAssembler() = default;

    P2OneBodyAssembler(const GridType& grid, const Obstacle& obsFunction,
                       const LevelBoundaryPatch& obsPatch) :
        Base(grid,obsFunction,obsPatch)
    {}

    /** \brief Compute local coordinate system for a subset of the p2 surface dofs
     *
     * \param hasObstacle Compute coordinate systems on all vertices which have
     their bit set in this bitfield
    */
    void computeLocalCoordinateSystems(const LeafBoundaryPatch& contactBoundary,
                                       std::vector<MatrixBlock>& coordSystems);

    /** \brief Computes the obstacles values and setup the householder reflections. */
    void assembleObstacles();
};

} /* namespace Contact */
} /* namespace Dune */

#include "p2onebodyassembler.cc"

#endif
