// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_COMMON_GAPFUNCTION_HH
#define DUNE_CONTACT_COMMON_GAPFUNCTION_HH

#include <limits>
#include <array>

#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bdmatrix.hh>

#include <dune/geometry/type.hh>

#include <dune/contact/common/deformedcontinuacomplex.hh>

#include <dune/fufem/boundarypatch.hh>

#ifndef HAVE_DUNE_GRID_GLUE
#error You need the dune-grid-glue module to use this code
#endif

#include <dune/grid-glue/merging/contactmerge.hh>
#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>

namespace Dune {
namespace Contact {

template <class GridType, class field_type>
class GapFunction
{

  public:
    enum {dim = GridType::dimension};
    enum {dimworld = GridType::dimensionworld};
    typedef typename GridType::ctype ctype;

    // local types
    typedef Dune::FieldVector<field_type,1> LocalVectorType;
    typedef Dune::FieldMatrix<field_type,dim,dim> FMatrix;
    typedef Dune::FieldVector<ctype,dim> CoordinateType;
    typedef std::vector<CoordinateType> CoordinateVectorType;

    // global types
    typedef Dune::BlockVector< CoordinateType > VectorType;
    typedef Dune::BlockVector< LocalVectorType > RangeType;
    typedef Dune::BCRSMatrix<FMatrix> MatrixType;
    typedef Dune::BCRSMatrix< Dune::FieldMatrix<field_type,dim,1>  > JacobianTransposedType;
    typedef Dune::BCRSMatrix< Dune::FieldMatrix<field_type,1,dim>  > JacobianType;
    typedef Dune::BDMatrix<FMatrix> DiagonalMatrixType;

    // deformed grid stuff
    using DeformedComplex = DeformedContinuaComplex<GridType,VectorType>;
    using DeformedGridType = typename DeformedComplex::DeformedGridType;
    using Deformation = typename DeformedComplex::DeformationFunction;
    using LeafGridView = typename DeformedGridType::LeafGridView;
    using LeafBoundaryPatch = BoundaryPatch<LeafGridView>;

    // grid glue stuff
    using Extractor = Dune::GridGlue::Codim1Extractor<LeafGridView>;
    using GlueType = Dune::GridGlue::GridGlue<Extractor,Extractor>;
    using ContactMerger = Dune::GridGlue::ContactMerge<dim,ctype>;

    //! Default constructor
    GapFunction() = default;

    //! Constructor
    GapFunction(DeformedGridType& nonmortarGrid, DeformedGridType& mortarGrid,
                Deformation& nonmortarDeform, Deformation& mortarDeform,
                std::shared_ptr<const LeafBoundaryPatch> nonmortarPatch,
                std::shared_ptr<const LeafBoundaryPatch> mortarPatch,
                std::shared_ptr<ContactMerger> merger)
    {

        setup(nonmortarGrid, mortarGrid,
              nonmortarDeform, mortarDeform,
              nonmortarPatch, mortarPatch, merger);
    }

    //! Constructor
    GapFunction(std::shared_ptr<DeformedGridType> nonmortarGrid, std::shared_ptr<DeformedGridType> mortarGrid,
                std::shared_ptr<Deformation> nonmortarDeform, std::shared_ptr<Deformation> mortarDeform,
                std::shared_ptr<const LeafBoundaryPatch> nonmortarPatch,
                std::shared_ptr<const LeafBoundaryPatch> mortarPatch,
                std::shared_ptr<ContactMerger> merger)
    {

        setup(nonmortarGrid, mortarGrid,
              nonmortarDeform, mortarDeform,
              nonmortarPatch, mortarPatch, merger);
    }

    //! Setup members
    void setup (DeformedGridType& nonmortarGrid, DeformedGridType& mortarGrid,
                Deformation& nonmortarDeform, Deformation& mortarDeform,
                std::shared_ptr<const LeafBoundaryPatch> nonmortarPatch,
                std::shared_ptr<const LeafBoundaryPatch> mortarPatch,
                std::shared_ptr<ContactMerger> merger)
    {
        complex_.addDeformedGrid(nonmortarGrid,"0",nonmortarDeform);
        complex_.addDeformedGrid(mortarGrid,"1",mortarDeform);

        // we store the original contact patches in here so we can use them on each evaluation
        patches_[0] = nonmortarPatch;
        patches_[1] = mortarPatch;

        merger_ = merger;
    }

    //! Setup members
    void setup (std::shared_ptr<DeformedGridType> nonmortarGrid, std::shared_ptr<DeformedGridType> mortarGrid,
                std::shared_ptr<Deformation> nonmortarDeform, std::shared_ptr<Deformation> mortarDeform,
                std::shared_ptr<const LeafBoundaryPatch> nonmortarPatch,
                std::shared_ptr<const LeafBoundaryPatch> mortarPatch,
                std::shared_ptr<ContactMerger> merger)
    {
        complex_.addDeformedGrid(nonmortarGrid,"0",nonmortarDeform);
        complex_.addDeformedGrid(mortarGrid,"1",mortarDeform);

        // we store the original contact patches in here so we can use them on each evaluation
        patches_[0] = nonmortarPatch;
        patches_[1] = mortarPatch;

        merger_ = merger;
    }

    /** \brief Destructor */
    ~GapFunction() {}

    /**  Evaluate the mortar constraint.
         *
         *  \param nonmortarDisp    Displacement of the nonmortar grid.
         *  \param mortarDisp   Displacement of the mortar grid.
         *  \param glue Compute the merged grid?
         *  \returns The value of the constraint.
        */
    RangeType operator()(const VectorType& nonmortarDisp, const VectorType& mortarDisp, bool glue = true);

    /** Compute the derivative of the nonmortar constraint w.r.t the deformation coefficients.
         *
         *  \param nonmortarDisp    Displacement of the nonmortar grid.
         *  \param mortarDisp   Displacement of the mortar grid.
         *  \param linearMat    Matrix to store the derivative.
         *  \param glue Compute the merged grid?
        */
    void firstDerivative(const VectorType& nonmortarDisp, const VectorType& mortarDisp,
                         JacobianType& linearMat, bool glue = true);

    void computeMergedGrid(const VectorType& nonmortarDisp, const VectorType& mortarDisp) {

        complex_.setDeformation(nonmortarDisp,"0");
        complex_.setDeformation(mortarDisp,"1");
        computeMergedGrid();
    }

    //! Return the DeformedGridComplex
    DeformedComplex& getDeformedComplex() {return complex_;}

    //! Return the gridglue object
    std::shared_ptr<GlueType> getGlue() {return glue_;}

    //! Return the unreduced contact patches
    const LeafBoundaryPatch& getUnreducedBoundaryPatch(int idx) const {
        return *patches_[idx];
    }

    //! Return the reduced contact patches
    const LeafBoundaryPatch& getReducedBoundaryPatch(int idx) const {
        return (idx==0) ? reducedNonmortar_ : reducedMortar_;
    }

    //! The nonmortar boundary for which the merged grid could be built
    LeafBoundaryPatch reducedNonmortar_;
    //! The mortar boundary for which the merged grid could be built
    LeafBoundaryPatch reducedMortar_;

protected:
    void computeMergedGrid();


private:
    /** \brief Dyadic product of two vectors. */
    FMatrix dyadic(const CoordinateType& a, const CoordinateType& b) const
    {
        FMatrix c(0);

        for (int i=0; i<dim; i++)
            c[i].axpy(a[i],b);

        return c;
    }

    template <class Gradient>
    CoordinateVectorType dualCovBasis(const std::vector<Gradient>& jacobians,
                                  const CoordinateVectorType& corners) const
    {

        const int jDim = Gradient::dimension;
        FieldMatrix<field_type,dim,jDim> mass(0);
        for (size_t j=0; j<corners.size(); j++)
            for (size_t i=0; i<corners.size(); i++)
                mass[j].axpy(corners[i][j],jacobians[i]);

        FieldMatrix<field_type,jDim,dim> trans;
        Dune::MatrixVector::transpose(mass,trans);
        auto inverse = mass.leftmultiplyany(trans);

        try {
            inverse.invert();
        } catch(Dune::FMatrixError e) {
            DUNE_THROW(Dune::Exception, "Computing dual covariant basis failed, metric not invertible: " << inverse.determinant() << " \n");
        }
        auto dual = inverse.rightmultiplyany(trans);

        CoordinateVectorType dualCovBasis(dual.N());
        for (size_t i=0; i<dual.N(); i++)
            dualCovBasis[i] = dual[i];

        return dualCovBasis;
    }

    //! The deformed grids
    DeformedComplex complex_;

    //! Store pointer on the unreduced contact patches
    std::array<std::shared_ptr<const LeafBoundaryPatch>, 2> patches_;

    //! The glue object to be used
    std::shared_ptr<GlueType> glue_;
    //! The merger
    std::shared_ptr<ContactMerger> merger_;

    //! The mortar matrix of the approximate linearisation
    MatrixType mortarMatrix_;
    //! The nonmortar matrix of the approximate linearisation
    DiagonalMatrixType nonmortarMatrix_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "gapfunction.cc"

#endif
