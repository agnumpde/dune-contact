/* begin dune-contact */

/* Define to the version of dune-contact */
#define DUNE_CONTACT_VERSION "${DUNE_CONTACT_VERSION}"

/* Define to the major version of dune-contact */
#define DUNE_CONTACT_VERSION_MAJOR ${DUNE_CONTACT_VERSION_MAJOR}

/* Define to the minor version of dune-contact */
#define DUNE_CONTACT_VERSION_MINOR ${DUNE_CONTACT_VERSION_MINOR}

/* Define to the revision of dune-contact */
#define DUNE_CONTACT_VERSION_REVISION ${DUNE_CONTACT_VERSION_REVISION}

/* end dune-contact */
