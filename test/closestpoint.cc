#include <config.h>


#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>

#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/io.hh>

#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/boundarypatchprolongator.hh>

#include <dune/tnnmg/problem-classes/simplexconstrainedgsproblem.hh>
#include <dune/tnnmg/problem-classes/convexproblem.hh>
#include <dune/tnnmg/problem-classes/bisection.hh>
#include <dune/tnnmg/nonlinearities/zerononlinearity.hh>
#include <dune/tnnmg/iterationsteps/genericnonlineargs.hh>

#include <dune/contact/projections/closestpointprojection.hh>

// The grid dimension
const int dim = 3;

const double E  = 17e6;
const double nu = 0.3;

using namespace Dune;
using namespace Dune::Contact;

int main (int argc, char *argv[]) try
{
    // Some types that I need
    typedef BCRSMatrix<FieldMatrix<double, dim, dim> > OperatorType;
    typedef BlockVector<FieldVector<double,dim> >      VectorType;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("closestpoint.parset", parameterSet);

    // read solver settings
    
    const int minLevel         = parameterSet.get("minLevel", int(0));
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const int nu1              = parameterSet.get("nu1", int(0));
    const int nu2              = parameterSet.get("nu2", int(0));
    const int mu               = parameterSet.get("mu", int(0));
    const int baseIt           = parameterSet.get("baseIt", int(0));
    const double tolerance     = parameterSet.get("tolerance", double(0));
    const double baseTolerance = parameterSet.get("baseTolerance", double(0));
    const bool nestedIteration = parameterSet.get("nestedIteration", int(0));
    const double refinementFraction = parameterSet.get("refinementFraction", double(1));
    const bool paramBoundaries = parameterSet.get("paramBoundaries", int(0));
    /*
    // read problem settings
    std::string path                = parameterSet.get("path", "xyz");
    std::string resultPath                = parameterSet.get("resultPath", "xyz");
    std::string gridFile            = parameterSet.get("gridFile", "xyz");
    std::string parFile             = parameterSet.get("parFile", "xyz");
    std::string dirichletFile       = parameterSet.get("dirichletFile", "xyz");
    std::string dirichletValuesFile = parameterSet.get("dirichletValuesFile", "xyz");
    std::string obsFilename         = parameterSet.get("obsFilename", "xyz");
    double obsDistance              = parameterSet.get("obsDistance", double(0));

    // /////////////////////////////
    //   Generate the grid
    // /////////////////////////////
    typedef UGGrid<dim> GridType;
    typedef P1NodalBasis<GridType::LeafGridView, double> P1Basis;
    typedef BoundaryPatch<GridType::LevelGridView> LevelBoundaryPatch;    
    typedef BoundaryPatch<GridType::LeafGridView> LeafBoundaryPatch;    


    GridType* grid;
  
    if (paramBoundaries)
        AmiraMeshReader<GridType>::read(*grid, path + gridFile, path + parFile);
    else {
        //AmiraMeshReader<GridType>::read(*grid, path + gridFile);
    	FieldVector<double,2> center(0);
    	center[1] = 15;

        grid=createCircle<GridType>(center,15);
    }

    grid->setRefinementType(GridType::COPY);
  */  
/*
    OperatorType A;
    
    MatrixIndexSet ind(1,1);
    ind.add(0,0);

    ind.exportIdx(A);
    FieldMatrix<double,dim,dim> a(0);
    a[0][0]=1;
    a[1][1]=1;
    //a[2][2]=1;
    A[0][0] = a;

    VectorType r(1);
    r=0;r[0][0] = 1000; 
   
    // linear functional target*p_i
    VectorType l(1);
    l[0][0] = 1000;
 
    VectorType x(1); x=0;

    VectorType sol(1); sol=0;
    sol[0][0] = 0.25; sol[0][1] = 0.25; sol[0][2] = 0.5;

    typedef ZeroNonlinearity<FieldVector<double,3>, FieldMatrix<double,3,3> > ZeroNonlin;
    typedef ConvexProblem<ZeroNonlin,OperatorType,OperatorType> ConvProblem;
    typedef SimplexConstrainedGSProblem<ConvProblem> SimplexProblem;
    typedef GenericNonlinearGS<SimplexProblem> GSeidel;

    ZeroNonlin zeroNonlin;
    ConvProblem convProb(1,A,0,r,zeroNonlin,r,x);
    SimplexProblem simplProb(parameterSet,convProb);    
    GSeidel gseidel;
    gseidel.setProblem(sol,simplProb);
    BitSetVector<dim> dir(1,false);
    gseidel.ignoreNodes_ = &dir;
    for (int i=0;i<numIt;i++)
        gseidel.iterate();

    sol = gseidel.getSol();
    std::cout<<"sol "<<sol<<std::endl;
*/

    FieldVector<double,dim> target(0),zero(0);
    target = parameterSet.get("target",zero);

    std::vector<FieldVector<double,dim> > corners(3);
    for (int i=0;i<3;i++) {
        corners[i] = 0;
        if (i<2)
            corners[i][i] = 1;
    }
    corners[0][0] = -1;

    ClosestPointProjection<double,3,3> projector(numIt);
    projector.setProblem(corners,target);
    projector.project();
    std::cout<<"Corners \n";
    std::cout<<corners[0]<<"\n"<<corners[1]<<"\n"<<corners[2]<<std::endl;
    FieldVector<double,dim> sol = projector.getSol();
    std::cout<<"Solution "<<projector.getSol()<<std::endl;
    FieldVector<double,3> pro = corners[0];
    pro *= sol[0];

    pro.axpy(sol[1],corners[1]);
    pro.axpy(sol[2],corners[2]);
    std::cout<<"Projected "<<target<<" to "<<pro<<"\n";
    std::cout<<" At distance "<<projector.distance()<<std::endl;



 } catch (Exception e) {

    std::cout << e << std::endl;

 }



