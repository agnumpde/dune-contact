// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:

#include <memory>

#include <dune/istl/matrixindexset.hh>

#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/matrix-vector/blockmatrixview.hh>

namespace Dune {
namespace Contact {

template <class VectorType, class MatrixType, class MaterialType>
typename VectorType::field_type StaticContactProblem<VectorType,MatrixType,MaterialType>::infeasibility(const std::vector<VectorType>& iterates)
{
    // The infeasibility is the maximal nonlinear constraint violation
    field_type infeasibility(0);

    for (size_t i=0; i<contactConstraints_->size(); i++) {

        // the involved bodies
        int idx0 = contactAssembler_->coupling_[i].gridIdx_[0];
        int idx1 = contactAssembler_->coupling_[i].gridIdx_[1];

        // evaluate constraints
        auto& contactConstraint = (*contactConstraints_)[i];
        constraints_[i] = contactConstraint(iterates[idx0],iterates[idx1],true);

         for (size_t j=0; j<constraints_[i].size(); j++)
             infeasibility = std::max(infeasibility, -constraints_[i][j]);
    }

    return infeasibility;
}


template <class VectorType, class MatrixType, class MaterialType>
void StaticContactProblem<VectorType,MatrixType,MaterialType>::assembleQP(const std::vector<VectorType>& iterates)
{
    // assemble linear and quadratic terms
    for (size_t i=0;i<iterates.size();i++) {

        auto& material = (*materials_)[i];
        const auto& basis = material.basis();

        auto displace = std::make_shared<BasisGridFunction>(basis,iterates[i]);

        // assemble quadratic term
        OperatorAssembler<Basis,Basis> globalAssembler(basis,basis);
        if (stiffMat_[i])
            delete(stiffMat_[i]);
        MatrixType* mat = new MatrixType;
        globalAssembler.assemble(material.secondDerivative(displace),*mat);
        stiffMat_[i] = mat;

        // assemble linear term
        rhs_[i].resize(iterates[i].size());

        FunctionalAssembler<Basis> funcAssembler(basis);
        funcAssembler.assemble(material.firstDerivative(displace),rhs_[i]);
        rhs_[i] -= (*extForces_)[i];
        rhs_[i] *= -1;

    }

    // collect all data in one big vector/matrix
    ContactAssembler::concatenateVectors(rhs_, f_);
    Dune::MatrixVector::BlockMatrixView<MatrixType>::setupBlockMatrix(stiffMat_, A_);

    // setup the linearised constraints
    assembleConstraints(iterates, false);

}

template <class VectorType, class MatrixType, class MaterialType>
void StaticContactProblem<VectorType,MatrixType,MaterialType>::assembleDefectQP(
                          const std::vector<VectorType>& iterates,
                          std::vector<VectorType>& residual,
                          std::vector<DiagonalMatrixType>& hessians)
{
    // assemble linear and quadratic terms
    residual.resize(iterates.size());
    hessians.resize(iterates.size());

    for (size_t i=0;i<iterates.size();i++) {

        const Basis& basis = (*materials_)[i].basis();

        assert(iterates[i].size() == basis.size());

        auto displace = std::make_shared<BasisGridFunction>(basis,iterates[i]);

        // assemble quadratic term

        hessians[i] = DiagonalMatrixType(basis.size());
        hessians[i] = 0;

        const auto& locHessianAssembler =  (*materials_)[i].secondDerivative(displace);

        // ////////////////////////////////////////////////////////////////////////
        //   Loop over all elements and assemble diagonal entries of the hessian
        // ////////////////////////////////////////////////////////////////////////

        const auto& gridView = basis.getGridView();

        for (const auto& e: elements(gridView)) {

            // Get set of shape functions
            const auto& lfe = basis.getLocalFiniteElement(e);
            const auto& localBasis = lfe.localBasis();

            typename MaterialType::LocalHessian::LocalMatrix localMatrix(localBasis.size(),localBasis.size());
            locHessianAssembler.assemble(e,localMatrix, lfe, lfe);

            // Add to residual vector
            for (size_t j=0; j<localBasis.size(); j++) {

                int globalRow = basis.index(e, j);

                // Assemble diagonal part of second order stiffness matrix
                hessians[i][globalRow][globalRow] += localMatrix[j][j];
            }
        }

        // assemble linear term
        residual[i].resize(iterates[i].size());

        FunctionalAssembler<Basis> funcAssembler(basis);
        funcAssembler.assemble((*materials_)[i].firstDerivative(displace),residual[i]);
        residual[i] -= (*extForces_)[i];
        residual[i] *= -1;
    }

}


template <class VectorType, class MatrixType, class MaterialType>
typename VectorType::field_type StaticContactProblem<VectorType,MatrixType,MaterialType>::energy(const std::vector<VectorType>& iterates) const
{
    // The energy is given by the material energy and the external forces
    field_type energy(0);

    for (size_t i=0;i<iterates.size();i++) {

        const auto& material = (*materials_)[i];

        // potential energy
        auto it = std::make_shared<BasisGridFunction>(material.basis(),iterates[i]);
        energy += material.energy(it);

        // external terms
        energy -= ((*extForces_)[i]*iterates[i]);
    }

    return energy;
}

template <class VectorType, class MatrixType, class MaterialType>
void StaticContactProblem<VectorType,MatrixType,MaterialType>::assembleConstraints(const std::vector<VectorType>& iterates, bool glue)
{
    for (size_t i=0; i<contactConstraints_->size(); i++) {

        // the involved bodies
        int idx0 = contactAssembler_->coupling_[i].gridIdx_[0];
        int idx1 = contactAssembler_->coupling_[i].gridIdx_[1];

        auto& contactConstraint = (*contactConstraints_)[i];

        // the nonlinear mortar constraints
        constraints_[i] = contactConstraint(iterates[idx0], iterates[idx1],glue);

        // the exact constraint linearisation
        contactConstraint.firstDerivative(iterates[idx0], iterates[idx1], constraintJacobians_[i],false);

    }

    // setup total constraints
    size_t size(0);
    for (size_t i=0; i<constraints_.size();i++)
        size += constraints_[i].size();

    // compute offsets for the involved grids
    std::vector<int> offsets(stiffMat_.size());
    offsets[0] = 0;

    for (size_t k=0; k<stiffMat_.size()-1; k++)
        offsets[k+1] = offsets[k] + iterates[k].size();

    totalConstraints_.resize(size);
    Dune::MatrixIndexSet indices(size, offsets.back()+iterates.back().size());

    size_t counter(0);
    for (size_t i=0; i<constraints_.size(); i++) {

        // the grids involved
        int grid0Idx = contactAssembler_->coupling_[i].gridIdx_[0];
        int grid1Idx = contactAssembler_->coupling_[i].gridIdx_[1];

        // the size of the first grid
        size_t size0 = rhs_[grid0Idx].size();

        for (size_t j=0; j<constraints_[i].size(); j++) {

            // combine all constraint matrices in one big matrix
            const auto& row = constraintJacobians_[i][j];

            auto cIt = row.begin();
            const auto& cEnd = row.end();

            // distinguish between the nonmortar and mortar dofs
            for (; cIt != cEnd; ++cIt) {
                if (cIt.index() < size0)
                    indices.add(counter,offsets[grid0Idx]+cIt.index());
                else
                    indices.add(counter,offsets[grid1Idx]+cIt.index()-size0);
            }

            // write the nonlinear constraints in a big box-constraint-vector to be used in IpOpt
            totalConstraints_[counter].clear();
            totalConstraints_[counter++].lower(0) = -constraints_[i][j];
        }
    }

    assert(counter==size);

    indices.exportIdx(totalConstrJacobian_);

    counter = 0;
    for (size_t i=0; i<constraintJacobians_.size(); i++) {

        // the grids involved
        int grid0Idx = contactAssembler_->coupling_[i].gridIdx_[0];
        int grid1Idx = contactAssembler_->coupling_[i].gridIdx_[1];

        // the size of the first grid
        size_t size0 = rhs_[grid0Idx].size();

        for (size_t j=0; j<constraintJacobians_[i].N(); j++) {

            // combine all constraint matrices in one big matrix
            const auto& row = constraintJacobians_[i][j];

            auto cIt        = row.begin();
            const auto& cEnd = row.end();

            // distinguish between the nonmortar and mortar dofs
            for (; cIt != cEnd; ++cIt) {
                if (cIt.index() < size0)
                    totalConstrJacobian_[counter][offsets[grid0Idx]+cIt.index()] = *cIt;
                else
                    totalConstrJacobian_[counter][offsets[grid1Idx]+cIt.index()-size0] = *cIt;
            }
            counter++;
        }
    }
}

} /* namespace Contact */
} /* namespace Dune */
