// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/computeenergy.hh>

namespace Dune {
namespace Contact {

template <class MatrixType, class VectorType>
void TnnmmgContactStep<MatrixType, VectorType>::iterate()
{
  this->preprocessCalled = false;
  const double eps = 1e-12;
  using ProjGSStep = ProjectedBlockGSStep<MatrixType, VectorType>;

  int& level = this->level_;

  // Define references just for ease of notation
  auto& mat = *this->matrixHierarchy_[level];
  auto& x   = *this->xHierarchy_[level];
  auto& rhs = this->rhsHierarchy_[level];
  auto& obstacles = *this->obstacleHierarchy_[level];
  auto& hasObs = *this->hasObstacleHierarchy_[level];

  Dune::BitSetVector<blockSize> critical(x.size(), false);

  // Solve directly if we're looking at the coarse problem
  if (level == 0) {

    this->basesolver_->solve();

    // Determine critical nodes
    for (size_t i=0; i<obstacles.size(); i++)
      for (int j=0; j< blockSize; j++)
        if ((obstacles[i].lower(j) >= x[i][j] - eps) or (obstacles[i].upper(j) <= x[i][j] + eps))
          critical[i][j] = true;
  } else {

    // Presmoothing
    auto& presmoother = *dynamic_cast<ProjGSStep*>(this->presmoother_[level].get());
    presmoother.setProblem(mat, x, rhs);
    presmoother.obstacles_ = &obstacles;
    presmoother.hasObstacle_ = &hasObs;
    presmoother.ignoreNodes_ = this->ignoreNodesHierarchy_[level];

    for (int i=0; i<this->nu1_; i++)
      presmoother.iterate();
    // First, a backup of the obstacles for postsmoothing
    auto obstacleBackup = obstacles;

    // Compute defect obstacles
    for (size_t i=0; i<obstacles.size(); i++)
      obstacles[i] -= x[i];

    // ///////////////////////
    //    Truncation
    // ///////////////////////

    // Determine critical nodes of defect obstacles
    const double eps = 1e-14;
    for (size_t i=0; i<obstacles.size(); i++)
      for (int j=0; j<blockSize; j++)
        if ((obstacles[i].lower(j) >= -eps) || (obstacles[i].upper(j) <= eps))
          critical[i][j] = true;

    ///////////////////////////////////////////////////////////////////////////////
    // Compute the part of the coarse grid matrix that needs to be recomputed.
    // There are two reasons why a matrix entry may need to be recomputed:
    // 1) A corresponding fine grid vertex switched from critical to non-critical or vice versa
    // 2) A corresponding fine grid matrix entry got recomputed
    ///////////////////////////////////////////////////////////////////////////////

    Dune::BitSetVector<blockSize> changed(critical.size());
    for (size_t i=0; i<changed.size(); i++)
      for (int j=0; j<blockSize; j++)
        changed[i][j] = (critical[i][j] != this->oldCritical_[level][i][j]);

    if (level < (int) this->numLevels()-1 )
      for (size_t i=0; i<changed.size(); i++)
        for (int j=0; j<blockSize; j++)
          changed[i][j] = (changed[i][j] || this->recompute_[level][i][j]);
    this->oldCritical_[level] = critical;

    // Set bitfield of nodes that will be truncated
    auto& transfer = *std::dynamic_pointer_cast<Transfer>(this->mgTransfer_[level-1]);

    transfer.restrict(changed, this->recompute_[level-1]);

    transfer.setCriticalBitField(&critical);

    // Restrict stiffness matrix
    transfer.galerkinRestrict(mat, *(const_cast<MatrixType*>(this->matrixHierarchy_[level-1].get())));

    // Restrict obstacles on lower levels using the standard transfer operators
    if (level != (int) this->numLevels()-1) {
      this->obstacleRestrictor_->restrict(obstacles, *this->obstacleHierarchy_[level-1],
          hasObs, *this->hasObstacleHierarchy_[level-1], transfer, critical);
    } else {

      // compute coarse grid defect obstacles that take into account the scaling
      // induced by the decoupling transformation
      using DenseTransfer = TruncatedDenseMGTransfer<VectorType>;
      const auto& inverse = dynamic_cast<DenseTransfer*>(&transfer)->getMatrix();

      auto& cObs = *this->obstacleHierarchy_[level-1];
      cObs.resize(this->xHierarchy_[level-1]->size());

      // compute defect obstacles made up only of trust-region constraints
      BoxConstraint<field_type, blockSize> trObs, zeroObs;
      for (size_t i=0; i<blockSize; i++) {
        trObs[i] = {-trustRegion_, trustRegion_};
        zeroObs[i] = {0,0};
      }

      for (size_t k=0; k<cObs.size(); k++)
        cObs[k] = zeroObs;

      // non-zeros in each column
      VectorType colNonZero(cObs.size());
      colNonZero=0;
      for (size_t i=0; i<inverse.N(); i++)  {

        auto defectObs = trObs;
        // If component dependent scaling is set, then apply it
        if (trScaling_ != nullptr)
          for (int k=0; k < blockSize; k++) {
            defectObs.upper(k) *= (*trScaling_)[i][k];
            defectObs.lower(k) *= (*trScaling_)[i][k];
        }

        defectObs -= x[i];

        auto colEnd = inverse[i].end();

        // count non-zeros in each row
        std::vector<field_type> rowNonZero(blockSize, 0);
        for (auto col = inverse[i].begin(); col != colEnd; col++)
          for (int k=0; k<blockSize; k++)
            for (int j=0; j<blockSize; j++)
              if (not(*this->ignoreNodesHierarchy_[level-1])[col.index()][j]
                  and (std::fabs((*col)[k][j])>1e-16) )
                rowNonZero[k]++;

        for (auto col = inverse[i].begin(); col != colEnd; col++)
          for (int k=0; k<blockSize; k++) {
            for (int j=0; j<blockSize; j++) {
                auto& a_kj = (*col)[k][j];
              if ((*this->ignoreNodesHierarchy_[level-1])[col.index()][j]
                  or (std::fabs(a_kj)<=1e-16) )
                continue;
              // compute defect constraints which yield a feasible coarse correction
              // w.r.t the trust-region constraints in decoupling coordinates
              auto lower = defectObs.lower(k)/(rowNonZero[k]*a_kj);
              auto upper = defectObs.upper(k)/(rowNonZero[k]*a_kj);
              if (a_kj < 0)
                std::swap(upper, lower);

              // instead of using the max and min we use an average to allow
              // some more space for the coarse correction
              cObs[col.index()].lower(j) += lower;
              cObs[col.index()].upper(j) += upper;
              colNonZero[col.index()][j]++;
            }
          }
      }

      for (size_t i=0; i<cObs.size();i++)
        for (int j=0; j<blockSize; j++) {
          if (cObs[i].upper(j) < cObs[i].lower(j))
            std::swap(cObs[i].upper(j), cObs[i].lower(j));
          cObs[i].upper(j) /= colNonZero[i][j];
          cObs[i].lower(j) /= colNonZero[i][j];
        }
    }

    // compute and restrict residual
    VectorType fineResidual = rhs;
    mat.mmv(x, fineResidual);

    // restrict residual
    transfer.restrict(fineResidual, this->rhsHierarchy_[level-1]);

    // Choose all zeros as the initial correction
    *this->xHierarchy_[level-1] = 0;

    // ///////////////////////////////////////
    // Recursively solve the coarser system
    // ///////////////////////////////////////

    level--;
    for (int i=0; i<this->mu_; i++)
      iterate();
    level++;

    // ////////////////////////////////////////
    // Prolong
    // ////////////////////////////////////////

    // add correction to the presmoothed solution
    VectorType v;
    transfer.prolong(*this->xHierarchy_[level-1], v);

    // Remove pointer to the temporary critical bitfield
    // this avoids a memory problem when the same mmg step is reused
    transfer.setCriticalBitField(nullptr);

    // restore the true (non-defect) obstacles
    obstacles = obstacleBackup;

    if (level != (int) this->numLevels()-1) {

      x += v;

    } else {

      // //////////////////////////////////////////////////////////
      //   Line search in the direction of the projected coarse
      //   grid correction to ensure monotonicity.
      // //////////////////////////////////////////////////////////

      // Construct obstacles in the direction of the projected correction
      BoxConstraint<field_type,1> lineSearchObs;
      for (size_t i=0; i<v.size(); i++) {

        // compute local defect constraints
        auto defectObs = obstacles[i];
        defectObs -= x[i];

        for (int j = 0; j < blockSize; ++j) {
          // project correction into local defect constraints
          v[i][j] = defectObs[j].projectIn(v[i][j]);

          // This may be Inf and if we multiply with it, we may get NaN.
          // That is not a problem, however, since the resulting constraints
          // are passed through std::fmin/fmax and will then be ignored.
          const auto inverseCoarseCorrection = field_type(1) / v[i][j];

          // compute relative defect constraints
          defectObs.lower(j) *= inverseCoarseCorrection;
          defectObs.upper(j) *= inverseCoarseCorrection;

          // orient relative defect constraints
          if (defectObs.lower(j) > defectObs.upper(j))
            std::swap(defectObs.lower(j), defectObs.upper(j));

          // update directional constraints
          lineSearchObs.lower(0) = std::fmax(lineSearchObs.lower(0), defectObs.lower(j));
          lineSearchObs.upper(0) = std::fmin(lineSearchObs.upper(0), defectObs.upper(j));
        }
      }

      // Line search - only if correction is non-zero
      VectorType tmp(x.size());
      mat.mv(v,tmp);
      field_type diag = (tmp*v);

      field_type alpha(0);
      if (diag <= 0) {
        // concave case, this is more expensive

        // check energy at upper bound
        auto dummy = x;
        dummy.axpy(lineSearchObs.upper(0),v);
        field_type engUpper = computeEnergy(mat, dummy, rhs);

        // check energy at lower bound
        dummy.axpy(lineSearchObs.lower(0)-lineSearchObs.upper(0),v);
        field_type engLower = computeEnergy(mat, dummy, rhs);

        if (engUpper <= engLower) {
          alpha = lineSearchObs.upper(0);
        } else
          alpha = lineSearchObs.lower(0);
      } else  {
        // convex case
        alpha = (fineResidual*v) / diag;
      }

      alpha = lineSearchObs[0].projectIn(alpha);

      // add scaled correction
      x.axpy(alpha, v);
    }

    // Postsmoothing
    auto* postsmoother = dynamic_cast<ProjGSStep*>(this->postsmoother_[level].get());
    postsmoother->setProblem(mat, x, rhs);
    postsmoother->obstacles_ = &obstacles;
    postsmoother->hasObstacle_ = &hasObs;
    postsmoother->ignoreNodes_ = this->ignoreNodesHierarchy_[level];

    for (int i=0; i<this->nu2_; i++)
      postsmoother->iterate();
  }

  // ////////////////////////////////////////////////////////////////////
  //   Track the number of critical nodes found during this iteration
  // ////////////////////////////////////////////////////////////////////

  if (level== (int) this->numLevels()-1 && this->verbosity_==NumProc::FULL) {

    std::cout << critical.count() << " critical nodes found on level " << level;

    int changes = 0;
    for (unsigned int i=0; i<this->oldCritical.size(); i++)
      for (int j=0; j<blockSize; j++)
        if (this->oldCritical[i][j] !=critical[i][j])
          changes++;

    std::cout << ", and " << changes << " changes." << std::endl;
    this->oldCritical = critical;
  }

  // Debug: output energy
  if (level== (int) this->numLevels()-1 && this->verbosity_==NumProc::FULL)
    std::cout << "Total energy: " << std::setprecision(10) << computeEnergy(mat, x, rhs) << std::endl;

}

} /* namespace Contact */
} /* namespace Dune */
