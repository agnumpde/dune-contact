// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_SOLVERS_DG_MULTIGRID_TRANSFER_HH
#define DUNE_CONTACT_SOLVERS_DG_MULTIGRID_TRANSFER_HH

#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/fufem/dgindexset.hh>

namespace Dune {
namespace Contact {

/** \brief Multigrid restriction and prolongation for a first-order Discontinuous-Galerkin-Space
 *
 */
template <class GridType>
class DGMultigridTransfer
{

    enum {elementOrder = 1};
    using ctype = typename GridType::ctype;
    using MatrixType = Dune::BCRSMatrix<Dune::FieldMatrix<ctype,1,1> >;


const MatrixType* getDGProlongation(const DGIndexSet<typename GridType::LevelGridView>& coarseDGIndex,
                                    const DGIndexSet<typename GridType::LevelGridView>& fineDGIndex,
                                    const GridType& grid, int cL, int fL);

public:
    template <class GridType1>
    void restrictCouplingOperator(const GridType& grid0, const GridType1& grid1,
                                  int cL, int fL,
                                  const MatrixType& fineDGMatrix,
                                  MatrixType& coarseDGMatrix);

};

} /* namespace Contact */
} /* namespace Dune */

#include "dgtransfer.cc"

#endif
