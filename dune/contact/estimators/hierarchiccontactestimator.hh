// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ESTIMATORS_HIERARCHIC_CONTACT_ESTIMATOR_HH
#define DUNE_CONTACT_ESTIMATORS_HIERARCHIC_CONTACT_ESTIMATOR_HH

#include <dune/common/fvector.hh>
#include <dune/common/promotiontraits.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/estimators/hierarchicestimatorbase.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/boundarypatch.hh>

#include <dune/contact/common/couplingpair.hh>

namespace Dune {
namespace Contact {

template <class BasisType, class EnlargedBasisType>
class HierarchicContactEstimator
{

    using GridType = typename BasisType::GridView::Grid;
    using LeafBoundaryPatch =  BoundaryPatch<typename GridType::LeafGridView>;
    using LevelBoundaryPatch = BoundaryPatch<typename GridType::LevelGridView>;

    enum {dim = GridType::dimension};

    using field_type = typename PromotionTraits<typename BasisType::ReturnType,
                                       typename EnlargedBasisType::ReturnType>::PromotedType;
    using BlockType = FieldMatrix<field_type,dim,dim>;
    using VectorType = BlockVector<FieldVector<field_type, dim> >;

    using GridFunction = BasisGridFunction<BasisType, VectorType>;

    using EnlargedLfe = typename EnlargedBasisType::LocalFiniteElement;
    using EnlargedGridFunction = BasisGridFunction<EnlargedBasisType, VectorType>;
    using EnlargedLocalAssembler = LocalOperatorAssembler<GridType, EnlargedLfe, EnlargedLfe, BlockType>;

    using Coupling = CouplingPair<GridType, GridType, field_type>;
public:

    /** \brief Constructor for a single */
    HierarchicContactEstimator(GridType& grid)
        : couplings_(1), grids_(1)
    {
        grids_[0] = &grid;

        couplings_[0].gridIdx_[0] = 0;
    }

    /** \brief Constructor for two grids */
    HierarchicContactEstimator(GridType& grid0, GridType& grid1)
        : couplings_(1), grids_(2)
    {
        grids_[0] = &grid0;
        grids_[1] = &grid1;

        couplings_[0].gridIdx_[0] = 0;
        couplings_[0].gridIdx_[1] = 1;
    }

    /** \brief Constructor for an arbitrary number of grids */
    HierarchicContactEstimator(const std::vector<GridType*>& grids, int numCouplings)
        : couplings_(numCouplings), grids_(grids)
    {}

    /** \brief Return the number of grids involved */
    int numGrids() const {
        return grids_.size();
    }

    /** \brief The number of pairwise couplings between grids */
    int numCouplings() const {
        return couplings_.size();
    }

    /** \brief Setup i'th 1-body contact coupling
     *
     * \param idx The idx of the coupling
     * \param obsPatch The contact boundary
     * \param dist The maximal search distance
     * \param type The contact type
     * \param obsFunction The distance function to the obstacle for given point in the grid
     */
    void setupContactCoupling(std::shared_ptr<const LevelBoundaryPatch> obsPatch,
                              field_type dist, CouplingPairBase::CouplingType type,
                              typename Coupling::Obstacle obsFunction) {
        couplings_[0].set(obsPatch,dist,type,obsFunction);
    }

    /** \brief Setup i'th 1-body contact coupling
     *
     * \param idx The idx of the coupling
     * \param gridIdx0 Index of the nonmortar grid
     * \param gridIdx1 Index of the mortar grid
     * \param nonmortarPatch The nonmortar boundary
     * \param mortarPatch The mortar boundary
     * \param dist The maximal search distance
     * \param type The contact type
     * \param projection The projection to be used for the identification of the surfaces
     * \param backend A grid-glue backend, if not set then ContactMerge is used.
     */
    void setupContactCoupling(int idx, int gridIdx0, int gridIdx1,
                              std::shared_ptr<const LevelBoundaryPatch> nonmortarPatch,
                              std::shared_ptr<const LevelBoundaryPatch> mortarPatch,
                              field_type dist, CouplingPairBase::CouplingType type,
                              std::shared_ptr<typename Coupling::Projection> projection = nullptr,
                              std::shared_ptr<typename Coupling::BackEndType> backend = nullptr) {
        couplings_[idx].set(gridIdx0,gridIdx1,nonmortarPatch,mortarPatch,dist,type,projection,backend);
    }


    static void householderReflection(const FieldVector<field_type, dim>& v0,
                                      const FieldVector<field_type, dim>& v1,
                                      Dune::FieldMatrix<field_type, dim, dim>& mat)
    {
        FieldVector<field_type, dim> v = (v1 - v0);
        v *= 0.5;

        // The code from here to the end of the routine sets Orth to
        // the Householder reflexion matrix of v, that is
        // $Orth = I - 2 \frac{v v^T}{v^T v}$
        mat = 0;
        for (int i=0; i<dim; i++)
            mat[i][i] = 1;

        field_type norm = v.two_norm2();

        if(norm <= 1e-8)
            return;

        for(int i=0;i<dim;i++)
            for(int j=0;j<dim;j++)
                mat[i][j] -= 2.0 *v[i]*v[j] / norm;

    }

    /** \brief Estimate error for a one-body problem

    Convenience method for the special case of one body.
    */
    void estimate(std::shared_ptr<GridFunction> x,
                  std::shared_ptr<LeafBoundaryPatch> dirichletBoundary,
                  std::shared_ptr<RefinementIndicator<GridType> > refinementIndicator,
                  std::shared_ptr<EnlargedBasisType> enlargedBasis,
                  std::shared_ptr<EnlargedLocalAssembler> localStiffness,
                  std::shared_ptr<GridFunction> volumeForce = nullptr,
                  std::shared_ptr<GridFunction> neumannForce = nullptr)
    {
        // Copy current solution into a std::vector
        std::vector<std::shared_ptr<GridFunction> > xInStdVector(1);
        xInStdVector[0] = x;

        /** \todo Copy current rhs in a std::vector */
        std::vector<std::shared_ptr<GridFunction> > volumeTermInStdVector(1);
        volumeTermInStdVector[0] = volumeForce;

        std::vector<std::shared_ptr<GridFunction> > neumannTermInStdVector(1);
        neumannTermInStdVector[0] = neumannForce;

        // Copy dirichlet boundary into a std::vector
        std::vector<std::shared_ptr<LeafBoundaryPatch> > dirichletBoundaryInVector(1);
        dirichletBoundaryInVector[0] = dirichletBoundary;

        // Make a vector of refinement indicators
        std::vector<std::shared_ptr<RefinementIndicator<GridType> > > refinementIndicatorInVector(1);
        refinementIndicatorInVector[0] = refinementIndicator;

        // Copy enlarged basis into a std::vector
        std::vector<std::shared_ptr<EnlargedBasisType> > enlargedBasisInVector(1);
        enlargedBasisInVector[0] =enlargedBasis;

        // Copy local stiffness assemblers into a std::vector
        std::vector<std::shared_ptr<EnlargedLocalAssembler> > localStiffnesses(1);
        localStiffnesses[0] = localStiffness;

        // Call the general n-body estimation method
        estimate(xInStdVector,
                 volumeTermInStdVector, neumannTermInStdVector,
                 dirichletBoundaryInVector,
                 refinementIndicatorInVector,
                 enlargedBasisInVector,
                 localStiffnesses);

    }

    /** \brief Estimate error for the general n-body problem with no forces */
    void estimate(const std::vector<std::shared_ptr<GridFunction> >& x,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& dirichletBoundary,
                  std::vector<std::shared_ptr<RefinementIndicator<GridType> > >& refinementIndicator,
                  const std::vector<std::shared_ptr<EnlargedBasisType> >& enlargedBasis,
                  std::vector<std::shared_ptr<EnlargedLocalAssembler> >& localStiffness) {

        std::vector<std::shared_ptr<GridFunction> > force(x.size(),nullptr);
        estimate(x,force,force, dirichletBoundary, refinementIndicator,
                 enlargedBasis, localStiffness);
    }

    /** \brief Estimate error for the general n-body problem */
    void estimate(const std::vector<std::shared_ptr<GridFunction> >& x,
                  const std::vector<std::shared_ptr<GridFunction> >& volumeTerm,
                  const std::vector<std::shared_ptr<GridFunction> >& neumannTerm,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& dirichletBoundary,
                  std::vector<std::shared_ptr<RefinementIndicator<GridType> > >& refinementIndicator,
                  const std::vector<std::shared_ptr<EnlargedBasisType> >& enlargedBasis,
                  std::vector<std::shared_ptr<EnlargedLocalAssembler> >& localStiffness);

    /** \brief Estimate error for the general large deformation n-body problem */
    template <class ProblemType>
    void estimate(const std::vector<std::shared_ptr<GridFunction> >& x,
                  const std::vector<std::shared_ptr<GridFunction> >& volumeTerm,
                  const std::vector<std::shared_ptr<GridFunction> >& neumannTerm,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& dirichletBoundary,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& neumannBoundary,
                  std::vector<std::shared_ptr<RefinementIndicator<GridType> > >& refinementIndicator,
                  const std::vector<std::shared_ptr<EnlargedBasisType> >& enlargedBasis,
                  std::shared_ptr<ProblemType> contactProblem);

    /** \brief Compute the error in the energy norm implied by the block diagonal preconditioner. */
    field_type getErrorSquared(int grid,
                            std::shared_ptr<EnlargedLocalAssembler> localStiffness) const
    {
        EnlargedBasisType enlargedBasis(grids_[grid]->leafGridView());
        return HierarchicEstimatorBase<EnlargedBasisType, dim, field_type>::energyNormSquared(enlargedBasis,predXNodal_[grid], *localStiffness);
    }

    // /////////////////////////////////
    //   Data members
    // /////////////////////////////////

    std::vector<Coupling> couplings_;

    std::vector<VectorType> predXNodal_;

protected:

    std::vector<GridType*> grids_;

};

} /* namespace Contact */
} /* namespace Dune */

#include "hierarchiccontactestimator.cc"

#endif
