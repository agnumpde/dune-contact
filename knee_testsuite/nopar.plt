set xlabel 'iterations'
set ylabel 'error'
set logscale y

plot 'nopar_mmg_cont_normal/statistics' using 1:3 with lines title "Contact problem"                    , \
     'nopar_mmg_lin_normal/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \
     'nopar_mmg_can_normal/statistics'  using 1:3 with lines title "Linear problem, nodal basis"        , \
     'nopar_gmg_cont_normal/statistics' using 1:3 with lines title "Nonsmooth Newton multigrid"      

set output "nopar_normal.eps"
set terminal postscript eps color "Arial,20" linewidth 6
replot

set output "nopar_normal.svg"
set terminal svg font "Arial,20" linewidth 6
replot


plot 'nopar_mmg_cont_parallel/statistics' using 1:3 with lines title "Contact problem"                    , \
     'nopar_mmg_lin_parallel/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \
     'nopar_mmg_can_parallel/statistics'  using 1:3 with lines title "Linear problem, nodal basis"        , \
     'nopar_gmg_cont_parallel/statistics' using 1:3 with lines title "Nonsmooth Newton multigrid"      

set output "nopar_parallel.eps"
set terminal postscript eps color "Arial,20" linewidth 6
replot

set output "nopar_parallel.svg"
set terminal svg font "Arial,20" linewidth 6
replot