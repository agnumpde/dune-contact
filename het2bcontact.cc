#include <config.h>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>

#include <dune/istl/io.hh>
#include <dune/istl/solvers.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/q1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p0basis.hh>

#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/localassemblers/vonmisesstressassembler.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>

#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/energynorm.hh>

#include <dune/contact/assemblers/hettwobodyassembler.hh>
#include <dune/contact/solvers/nsnewtoncontacttransfer.hh>
#include <dune/contact/solvers/nsnewtonmgstep.hh>
#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/projections/analyticdirectionsprojection.hh>
#include <dune/contact/projections/normalprojection.hh>

#include <dune/elasticity/estimators/zienkiewiczzhuestimator.hh>

// The grid dimension
const int dim = 3;
typedef double field_type;

field_type E  = 17e6;
field_type nu = 0.3;

field_type mortarE = 2.5e5;
field_type mortarNu = 0.3;

using namespace Dune;
using namespace Dune::Contact;

//#define EXPLICIT_CONTACT_DIRECTIONS
// For explicit contact directions, use the AnalyticContactProjection class
std::function<FieldVector<field_type, dim> (const FieldVector<field_type,dim>)> contactDirection =
[](const FieldVector<field_type,dim> pos) {
    FieldVector<field_type,dim> dir(0);
    dir[dim-1] = -1;
    return dir;
};

bool refineCondition(const FieldVector<field_type,dim>& pos) {
    return 20 > fabs(pos[2]);   // Das Knie
}

int main (int argc, char *argv[]) try
{
    // Some types that I need
    typedef BCRSMatrix<FieldMatrix<field_type, dim, dim> > OperatorType;
    typedef BlockVector<FieldVector<field_type, dim> >     VectorType;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("het2bcontact.parset", parameterSet);


    // read solver settings
    const int minLevel         = parameterSet.get<int>("minLevel");
    const int maxLevel         = parameterSet.get<int>("maxLevel");
    const int numIt            = parameterSet.get<int>("numIt");
    const int nu1              = parameterSet.get<int>("nu1");
    const int nu2              = parameterSet.get<int>("nu2");
    const int mu               = parameterSet.get<int>("mu");
    const field_type tolerance     = parameterSet.get<field_type>("tolerance");
    const field_type baseTolerance = parameterSet.get<field_type>("baseTolerance");
    const bool paramBoundaries = parameterSet.get<int>("paramBoundaries");

    // read problem settings
    std::string path                 = parameterSet.get<std::string>("path");
    std::string resultPath                 = parameterSet.get<std::string>("resultPath");
    std::string object0Name          = parameterSet.get<std::string>("gridFile0");
    std::string object1Name          = parameterSet.get<std::string>("gridFile1");
    std::string parFile0             = parameterSet.get<std::string>("parFile0");
    std::string parFile1             = parameterSet.get<std::string>("parFile1");
    std::string dirichletFile0       = parameterSet.get<std::string>("dirichletFile0");
    std::string dirichletFile1       = parameterSet.get<std::string>("dirichletFile1");
    std::string dirichletValuesFile0 = parameterSet.get<std::string>("dirichletValuesFile0");
    std::string dirichletValuesFile1 = parameterSet.get<std::string>("dirichletValuesFile1");
    std::string obsFilename          = parameterSet.get<std::string>("obsFilename");
    field_type obsDistance               = parameterSet.get<field_type>("obsDistance");
    field_type scaling                   = parameterSet.get<field_type>("scaling");

    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    typedef UGGrid<dim> GridType0;
    typedef YaspGrid<dim,EquidistantOffsetCoordinates<field_type,dim> > GridType1;
    std::unique_ptr<GridType0> gridPtr0;

    if (paramBoundaries) {
        //AmiraMeshReader<GridType0>::read(grid0, path + object0Name, path + parFile0);
        gridPtr0 = AmiraMeshReader<GridType0>::read(path + object0Name, PSurfaceBoundary<dim-1>::read(path + parFile0));
    } else {
        gridPtr0 = AmiraMeshReader<GridType0>::read(path + object0Name);
    }

    GridType0& grid0 = *gridPtr0;
    grid0.setRefinementType(GridType0::COPY);

    //std::array<int,dim> N = {20,20,5};
    //FieldVector<field_type,dim> L(-240);
    //L[dim-1] = -88;
    //FieldVector<field_type,dim> H(80);
    // H[dim-1] = -8;
    std::array<int,dim> N = {{3,3,3}};
    FieldVector<field_type,dim> L(0);
    L[dim-1] = -1;
    FieldVector<field_type,dim> H(1);
    H[dim-1] = 0;

    GridType1 grid1(L,H,N);

    // ////////////////////////////////////////
    //   Read Dirichlet values
    // ////////////////////////////////////////
    std::array<VectorType, 2> coarseDirichletValues;
    coarseDirichletValues[0].resize(grid0.size(0, dim));
    coarseDirichletValues[1].resize(grid1.size(0, dim));
    AmiraMeshReader<GridType0>::readFunction(coarseDirichletValues[0], path + dirichletValuesFile0);
    coarseDirichletValues[1] = 0;

    // Scale Dirichlet values
    coarseDirichletValues[0] *= scaling;

    // setup Dirichlet boundary patches
    BoundaryPatch<GridType0::LevelGridView> coarseDirichletBoundary0(grid0.levelGridView(0));
    readBoundaryPatch<GridType0>(coarseDirichletBoundary0,dirichletFile0);

    BitSetVector<1> coarseDirichletNodes;

    // Set lower face of cube as Dirichlet boundary
    GridType1::Codim<dim>::LevelIterator vIt    = grid1.lbegin<dim>(0);
    GridType1::Codim<dim>::LevelIterator vEndIt = grid1.lend<dim>(0);

    for (; vIt!=vEndIt; ++vIt) {
        int index     = grid1.levelIndexSet(0).index(*vIt);
        field_type zCoord = vIt->geometry().corner(0)[2];
        coarseDirichletNodes[index] = (zCoord < -47.99);
    }

    BoundaryPatch<GridType1::LevelGridView> coarseDirichletBoundary1(grid1.levelGridView(0),coarseDirichletNodes);

    // //////////////////////////////////////
    //   Refine uniformly until minlevel
    // //////////////////////////////////////
    for (int i=0; i<minLevel; i++) {
        grid0.globalRefine(1);
        grid1.globalRefine(1);

        if (paramBoundaries)
            improveGrid(grid0, 10);

    }

    // read field describing the obstacles
    BitSetVector<1> obsField;
    readBitField(obsField, grid0.size(0, dim), path + obsFilename);
    auto obsPatch = std::make_shared<BoundaryPatch<GridType0::LevelGridView> >(grid0.levelGridView(0), obsField);
    auto mortarPatch = std::make_shared<BoundaryPatch<GridType1::LevelGridView> >(grid1.levelGridView(0), true);

    std::array<VectorType, 2> rhs;
    std::array<VectorType, 2> x;
    x[0].resize(grid0.size(dim));
    x[1].resize(grid1.size(dim));

    // Initial solution
    x[0] = 0;
    x[1] = 0;

    while (true) {

        int toplevel = std::max(grid0.maxLevel(),grid1.maxLevel());

        typedef BoundaryPatch<GridType0::LeafGridView> LeafBoundaryPatch0;
        LeafBoundaryPatch0 dirichletBoundary0;
        BoundaryPatchProlongator<GridType0>::prolong(coarseDirichletBoundary0,dirichletBoundary0);

        typedef BoundaryPatch<GridType1::LeafGridView> LeafBoundaryPatch1;
        LeafBoundaryPatch1 dirichletBoundary1;
        BoundaryPatchProlongator<GridType1>::prolong(coarseDirichletBoundary1,dirichletBoundary1);

        std::array<BitSetVector<dim>, 2> dirichletNodes;
        dirichletNodes[0].resize(grid0.size(dim));
        for (size_t j=0; j<dirichletNodes[0].size(); j++)
            dirichletNodes[0][j] = dirichletBoundary0.containsVertex(j);

        dirichletNodes[1].resize(grid1.size(dim));
        for (size_t j=0; j<dirichletNodes[1].size(); j++)
            dirichletNodes[1][j] = dirichletBoundary1.containsVertex(j);

        std::array<VectorType, 2> dirichletValues;
        sampleOnBitField(grid0,coarseDirichletValues[0], dirichletValues[0], dirichletNodes[0]);
        sampleOnBitField(grid1, coarseDirichletValues[1], dirichletValues[1], dirichletNodes[1]);


        // ////////////////////////////////////////////////////////////
        //    Create rhs vectors
        // ////////////////////////////////////////////////////////////

        rhs[0].resize(grid0.size(dim));
        rhs[1].resize(grid1.size(dim));

        // Set right hand side vectors
        for (int i=0; i<2; i++) {

            rhs[i] = 0;

            for (size_t j=0; j<rhs[i].size(); j++)
                for (int k=0; k<dim; k++) {
                    if (dirichletNodes[i][j][k])
                        x[i][j][k] = dirichletValues[i][j][k];

                }

        }

        // make dirichlet bitfields containing dirichlet information for both grids
        BitSetVector<dim> totalDirichletNodes(rhs[0].size() + rhs[1].size());

        int offset = dirichletValues[0].size();

        for (size_t j=0; j<dirichletValues[0].size(); j++)
            totalDirichletNodes[j] = dirichletNodes[0][j];

        for (size_t j=0; j<dirichletValues[1].size(); j++)
            totalDirichletNodes[offset + j]  = dirichletNodes[1][j];

        // ////////////////////////////////////////////////////
        //   Assemble separate linear elasticity problems
        // ////////////////////////////////////////////////////

        // First the bone
        typedef P1NodalBasis<GridType0::LeafGridView,field_type> P1Basis;
        P1Basis p1NodalBasis(grid0.leafGridView());
        OperatorAssembler<P1Basis,P1Basis> p1Assembler(p1NodalBasis, p1NodalBasis);

        StVenantKirchhoffAssembler<GridType0, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> p1LocalAssembler(E, nu);
        OperatorType stiffnessMatrix0;

        p1Assembler.assemble(p1LocalAssembler, stiffnessMatrix0);

        // then the foundation
        typedef Q1NodalBasis<GridType1::LeafGridView,field_type> Q1Basis;
        Q1Basis q1NodalBasis(grid1.leafGridView());
        OperatorAssembler<Q1Basis,Q1Basis> q1Assembler(q1NodalBasis, q1NodalBasis);

        StVenantKirchhoffAssembler<GridType1, Q1Basis::LocalFiniteElement, Q1Basis::LocalFiniteElement> q1LocalAssembler(mortarE, mortarNu);
        OperatorType stiffnessMatrix1;

        q1Assembler.assemble(q1LocalAssembler, stiffnessMatrix1);

        std::array<const OperatorType*, 2> submat;
        submat[0] = &stiffnessMatrix0;
        submat[1] = &stiffnessMatrix1;

        //printmatrix(std::cout, *submat[0], "stiffness", "--");
        // Assemble contact problem
        std::shared_ptr<ContactProjection<LeafBoundaryPatch0, LeafBoundaryPatch1> > projection;
#if EXPLICIT_CONTACT_DIRECTIONS
        projection = std::shared_ptr<AnalyticProjection<LeafBoundaryPatch0, LeafBoundaryPatch1> >(contactDirection);
#else
        projection = std::make_shared<NormalProjection<LeafBoundaryPatch0, LeafBoundaryPatch1> >();
#endif


        HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType> contactAssembler;

        contactAssembler.setup(grid0, grid1,
                               obsPatch, mortarPatch,
                               obsDistance,
                               CouplingPairBase::CONTACT, projection);

        OperatorType bilinearForm;
        contactAssembler.assembleJacobian(submat, bilinearForm);

        VectorType totalX, totalRhs;

        contactAssembler.nodalToTransformed(x, totalX);

        contactAssembler.concatenateVectors(rhs, totalRhs);

        // //////////////////////////////////////////////////////
        //    Lengthen obstacle vector, to formally account for both
        //    bodies, even though the second one doesn't actually have obstacles
        // //////////////////////////////////////////////////////

        BitSetVector<dim> hasObstacle(grid0.size(dim) + grid1.size(dim), false);

        for (int j=0; j<grid0.size(dim); j++)
            if(contactAssembler.contactCoupling_->nonmortarBoundary().containsVertex(j))
                hasObstacle[j] = true;

        // /////////////////////////////////////
        //   Create a solver
        // /////////////////////////////////////

        // First create a base solver
#ifdef HAVE_IPOPT
        QuadraticIPOptSolver<OperatorType,VectorType> baseSolver(baseTolerance, 100, Solver::QUIET);
#endif

        // Make pre and postsmoothers
        ProjectedBlockGSStep<OperatorType, VectorType> presmoother, postsmoother;

        NonSmoothNewtonMGStep<OperatorType, VectorType> multigridStep(bilinearForm, totalX, totalRhs);

        multigridStep.setMGType(mu, nu1, nu2);
        multigridStep.setIgnore(totalDirichletNodes);
        multigridStep.setBaseSolver(baseSolver);
        multigridStep.setSmoother(presmoother, postsmoother);
        multigridStep.setHasObstacle(hasObstacle);
        multigridStep.setObstacles(contactAssembler.obstacles_);

        // Create the transfer operators
        std::vector<NonSmoothNewtonContactTransfer<VectorType> > mgTransfers(toplevel);
        for (size_t i=0; i<mgTransfers.size(); i++) {
            mgTransfers[toplevel-i-1].setup(grid0, grid1, i,
                    contactAssembler.contactCoupling_->mortarLagrangeMatrix(),
                    contactAssembler.localCoordSystems_,
                    *contactAssembler.contactCoupling_->nonmortarBoundary().getVertices());
        }

        multigridStep.setTransferOperators(mgTransfers);

        EnergyNorm<OperatorType, VectorType> energyNorm(multigridStep);

        ::LoopSolver<VectorType> solver(multigridStep,
                                           numIt,
                                           tolerance,
                                           energyNorm,
                                           Solver::FULL);

        // /////////////////////////////
        //    Solve !
        // /////////////////////////////
        solver.preprocess();

        solver.solve();

        totalX = multigridStep.getSol();

        contactAssembler.postprocess(totalX, x);

        // ////////////////////////////////////////////////////////////////////////
        //    Leave adaptation loop if maximum number of levels has been reached
        // ////////////////////////////////////////////////////////////////////////

        if (grid0.maxLevel() == maxLevel)
            break;

        // ////////////////////////////////////////////////////
        //    Estimate error per element and refine
        // ////////////////////////////////////////////////////

        ZienkiewiczZhuEstimator<GridType0,VectorType> estimator(grid0, E, nu);
        //HierarchicContactEstimator<GridType0> estimator(grid0, E, nu);

        BlockVector<FieldVector<field_type,1> > errorPerElement;
        estimator.estimate(x[0], errorPerElement);

        // Compute average error
        field_type etaBar = 1;
        field_type averageError = etaBar * std::sqrt((x[0].two_norm2() + errorPerElement.two_norm2()) / grid0.leafIndexSet().size(0));

        // Mark elements for refinement
        unsigned int markedElements = 0;

        for (const auto& e : elements(grid0.leafGridView())) {

            if (errorPerElement[grid0.leafIndexSet().index(e)] > averageError) {
                markedElements++;
                grid0.mark(1,e);
            }

        }

        std::cout << markedElements << " elements of grid0 were marked for refinement." << std::endl;

        // ////////////////////////////////////////////////////
        //   Refine grid
        // ////////////////////////////////////////////////////
        GridFunctionAdaptor<P1Basis> adaptorP1(p1NodalBasis,true,true);

        grid0.preAdapt();
        grid0.adapt();
        grid0.postAdapt();

        p1NodalBasis.update();
        adaptorP1.adapt(x[0]);

        if (paramBoundaries)
            improveGrid(grid0, 10);

        GridFunctionAdaptor<Q1Basis> adaptorQ1(q1NodalBasis,true,true);

        grid1.preAdapt();
        grid1.adapt();
        grid1.postAdapt();

        q1NodalBasis.update();
        adaptorQ1.adapt(x[1]);

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        std::cout << "  Grid: 0   Level: " << grid0.maxLevel()
                  << "   vertices: " << grid0.size(grid0.maxLevel(), dim)
                  << "   elements: " << grid0.size(toplevel+1, 0) << std::endl;
        std::cout << "  Grid: 1   Level: " << toplevel+1
                  << "   vertices: " << grid1.size(toplevel+1, dim)
                  << "   elements: " << grid1.size(toplevel+1, 0) << std::endl;
        std::cout << "########################################################" << std::endl;

    }

    // Compute the stress
    BlockVector<FieldVector<field_type,1> > stress0, stress1;

    {
        // make grid function pointer
        typedef P1NodalBasis<GridType0::LeafGridView,field_type> P1Basis;
        P1Basis p1Basis(grid0.leafGridView());
        auto dispFunc = std::make_shared<BasisGridFunction<P1Basis,VectorType> >(p1Basis,x[0]);

        // make P0 basis for cell data
        typedef P0Basis<GridType0::LeafGridView,field_type> P0Basis;
        P0Basis p0Basis(grid0.leafGridView());

        VonMisesStressAssembler<GridType0,P0Basis::LocalFiniteElement> stressAssembler(E,nu,dispFunc);
        FunctionalAssembler<P0Basis> assembler(p0Basis);
        assembler.assemble(stressAssembler,stress0);
    }

    {
        // make grid function pointer
        typedef P1NodalBasis<GridType1::LeafGridView,field_type> P1Basis;
        P1Basis p1Basis(grid1.leafGridView());
        auto dispFunc = std::make_shared<BasisGridFunction<P1Basis,VectorType> >(p1Basis,x[1]);

        // make P0 basis for cell data
        typedef P0Basis<GridType1::LeafGridView,field_type> P0Basis;
        P0Basis p0Basis(grid1.leafGridView());

        VonMisesStressAssembler<GridType1,P0Basis::LocalFiniteElement> stressAssembler(mortarE,mortarNu,dispFunc);
        FunctionalAssembler<P0Basis> assembler(p0Basis);
        assembler.assemble(stressAssembler,stress1);
    }

    // Output result
    LeafAmiraMeshWriter<GridType0> amiramesh0;
    amiramesh0.addLeafGrid(grid0,true);
    amiramesh0.addVertexData(x[0], grid0.leafGridView());
    amiramesh0.addCellData(stress0, grid0.leafGridView());
    amiramesh0.write(resultPath + "0resultGrid");

    LeafAmiraMeshWriter<GridType1> amiramesh1;
    amiramesh1.addLeafGrid(grid1,false);
    amiramesh1.addVertexData(x[1], grid1.leafGridView());
    amiramesh1.addCellData(stress1, grid1.leafGridView());
    amiramesh1.write(resultPath + "1resultGrid");


} catch (Exception e) {

    std::cout << e << std::endl;

 }
