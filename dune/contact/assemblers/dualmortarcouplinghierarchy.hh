// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_DUAL_MORTAR_COUPLING_HIERARCHY_HH
#define DUNE_CONTACT_ASSEMBLERS_DUAL_MORTAR_COUPLING_HIERARCHY_HH

#include <memory>

#include <dune/common/bitsetvector.hh>

#include <dune/istl/bdmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>

#include <dune/contact/assemblers/dualmortarcoupling.hh>

namespace Dune {
namespace Contact {

/** \brief Assembles the mmg transfer operator for two-body contact
 */
template<class field_type, class GridType0, class GridType1=GridType0>
class DualMortarCouplingHierarchy : public DualMortarCoupling<field_type,GridType0,GridType1> {

    // /////////////////////
    // private types
    // /////////////////////

    enum {dim = GridType0::dimension};

    enum {dimworld = GridType0::dimensionworld};

    using Base = DualMortarCoupling<field_type,GridType0,GridType1>;
    using typename Base::MatrixBlock;
    using typename Base::MatrixType;
    using typename Base::ctype;

    using typename Base::LevelBoundaryPatch0;
    using typename Base::LevelBoundaryPatch1;

    using typename Base::LeafBoundaryPatch0;
    using typename Base::LeafBoundaryPatch1;


    typedef Dune::GridGlue::GridGlue<Dune::GridGlue::Codim1Extractor<typename GridType0::LevelGridView>,
                                Dune::GridGlue::Codim1Extractor<typename GridType1::LevelGridView> > GlueType;
  public:

    using Base::grid0_;
    using Base::grid1_;
    using Base::weakObstacle_;
    using Base::nonmortarBoundary;
    using Base::mortarBoundary;

    DualMortarCouplingHierarchy(field_type overlap = 1e-2, field_type coveredArea = 1.0 - 1e-2)
        : Base(overlap, coveredArea)
    {}

    DualMortarCouplingHierarchy(const GridType0& grid0, const GridType1& grid1,
                                field_type overlap = 1e-2, field_type coveredArea = 1.0 - 1e-2)
        : Base(grid0, grid1, overlap, coveredArea)
    {}

    /** \brief Sets up the contact coupling operator on all grid levels */
    void setup();

    /** \brief Get the number of levels in the grid hierarchy. */
    size_t numLevels() const {return mortarLagrangeMat_.size();}

    /** \brief Assemble the nonmortar matrix. */
    void assembleNonmortarLagrangeMatrices();

    /** \brief Mark the overlaps that are relevant. Overlaps are called relevant if they belong
     *         to a base grid boundary segment which is completely covered by overlaps.
     *
     *  \param gridGlue A grid-glue object that contains all computed overlaps.
     *  \param relevantOverlaps Reference to a vector of bits used to save which overlaps are relevant.
     */
    void markRelevantOverlaps(const GlueType& gridGlue, Dune::BitSetVector<1>& relevantOverlaps);

    /** \brief Assemble the dual mortar coupling matrix by condensing a nodal dg mortar matrix.
     *
     *  \param dgMatrix The nodal dg mortar coupling matrix of a given level.
     *  \param level    The level of which to compute the dual mortar matrix for.
     */
    void condenseDGMatrix(const Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> >& dgMatrix, int level);

    /** \brief Get the mortar matrix of the finest level. */
    const MatrixType& mortarLagrangeMatrix() const {
        return mortarLagrangeMat_.back();
    }

    /** \brief Get the mortar matrix of a level.
     *
     *  \param i The level to get the mortar matrix for.
     */
    const MatrixType& mortarLagrangeMatrix(size_t i) const {
        return mortarLagrangeMat_[i];
    }

    /** \brief Get the nonmortar matrix of a level.
     *
     *  \param i The level to get the mortar matrix for.
     */
    const Dune::BDMatrix<MatrixBlock>& nonmortarLagrangeMatrix(size_t i) const {
        return nonmortarLagrangeMat_[i];
    }

    /** \brief Setup leaf nonmortar and mortar patches. */
    void setupContactPatch(const LevelBoundaryPatch0& coarseNonmortar,
                           const LevelBoundaryPatch1& coarseMortar) {

        Base::setupContactPatch(coarseNonmortar,coarseMortar);

        nonmortarBoundaryHierarchy_.resize(coarseNonmortar.gridView().grid().maxLevel()+1);
        nonmortarBoundaryHierarchy_[0] = coarseNonmortar;

        mortarBoundaryHierarchy_.resize(coarseMortar.gridView().grid().maxLevel()+1);
        mortarBoundaryHierarchy_[0] = coarseMortar;

        BoundaryPatchProlongator<GridType0>::prolong(nonmortarBoundaryHierarchy_);
        BoundaryPatchProlongator<GridType1>::prolong(mortarBoundaryHierarchy_);
    }

    /** \brief Return the leafnonmortar boundary. */
    const LevelBoundaryPatch0& nonmortarBoundary(int i) const {
        return nonmortarBoundaryHierarchy_[i];
    }

    /** \brief Return the leafnonmortar boundary. */
    const LevelBoundaryPatch1& mortarBoundary(int i) const {
        return mortarBoundaryHierarchy_[i];
    }

    // /////////////////////////////////////////////
    //   Data members
    // /////////////////////////////////////////////

private:
    /** \brief The matrix coupling mortar side and Lagrange multiplyers */
    std::vector<MatrixType> mortarLagrangeMat_;

    /** \brief The diagonal matrix coupling nonmortar side and Lagrange multiplyers */
    std::vector<Dune::BDMatrix<MatrixBlock> > nonmortarLagrangeMat_;

    /** \brief For each dof a bit specifying whether the dof carries an obstacle or not */
    std::vector<LevelBoundaryPatch0> nonmortarBoundaryHierarchy_;

    /** \brief The mortar boundary. */
    std::vector<LevelBoundaryPatch1> mortarBoundaryHierarchy_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "dualmortarcouplinghierarchy.cc"

#endif
