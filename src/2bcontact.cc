// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#include <memory>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/timer.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/amirameshreader.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/solvers.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/mmgstep.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>

#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>
#include <dune/fufem/functions/coarsegridfunctionwrapper.hh>
#include <dune/fufem/utilities/dirichletbcassembler.hh>
#include <dune/fufem/utilities/gridconstruction.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif

#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/solvers/nsnewtonmgstep.hh>
#include <dune/contact/solvers/contacttransfer.hh>
#include <dune/contact/solvers/contactobsrestrict.hh>
#include <dune/contact/solvers/contacttransferoperatorassembler.hh>
#include <dune/contact/solvers/nsnewtoncontacttransfer.hh>
#include <dune/contact/assemblers/twobodyassembler.hh>
#include <dune/contact/common/markcontactarea.hh>

// The grid dimension
const int dim = 3;

// The data type used for the solution values
typedef double field_type;

using namespace Dune;
using namespace Dune::Contact;

//#define EXPLICIT_CONTACT_DIRECTIONS
std::function<FieldVector<field_type, dim> (const FieldVector<field_type,dim>)> contactDirection =
[](const FieldVector<field_type,dim> pos) {
    FieldVector<field_type,dim> dir(0);
    dir[dim-1] = -1;
    return dir;
};

int main (int argc, char *argv[]) try
{

    Dune::MPIHelper::instance(argc, argv);

    // Some types that I need
    using BlockType = FieldMatrix<field_type, dim, dim>;
    using MatrixType = BCRSMatrix<BlockType>;
    using VectorType = BlockVector<FieldVector<field_type, dim> >;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("2bcontact.parset", parameterSet);

    // read solver settings
    const int minLevel              = parameterSet.get<int>("minLevel");
    const int maxLevel              = parameterSet.get<int>("maxLevel");
    const int numIt                 = parameterSet.get<int>("numIt");
    const int nu1                   = parameterSet.get<int>("nu1");
    const int nu2                   = parameterSet.get<int>("nu2");
    const int mu                    = parameterSet.get<int>("mu");
    const field_type tolerance          = parameterSet.get<field_type>("tolerance");
    const field_type baseTolerance      = parameterSet.get<field_type>("baseTolerance");
    const bool paramBoundaries      = parameterSet.get<int>("paramBoundaries");
    const int  nonsmoothNewtonMG    = parameterSet.get<int>("nonsmoothNewtonMG");
    const field_type refinementFraction = parameterSet.get<field_type>("refinementFraction");
    const bool useImproveGrid       = parameterSet.get("useImproveGrid", int(1));
    const bool uniformlyRefineContactNeighborhood = parameterSet.get("uniformlyRefineContactNeighborhood", int(0));

    // read problem settings
    std::string path                 = parameterSet.get<std::string>("path");
    std::string resultPath           = parameterSet.get("resultPath", "./");
    std::string object0Name          = parameterSet.get<std::string>("gridFile0");
    std::string object1Name          = parameterSet.get<std::string>("gridFile1");
    std::string dirichletFile0       = parameterSet.get<std::string>("dirichletFile0");
    std::string dirichletFile1       = parameterSet.get<std::string>("dirichletFile1");
    std::string dirichletValuesFile0 = parameterSet.get<std::string>("dirichletValuesFile0");
    std::string dirichletValuesFile1 = parameterSet.get<std::string>("dirichletValuesFile1");
    std::string obsFilename          = parameterSet.get<std::string>("obsFilename");
    field_type obsDistance               = parameterSet.get<field_type>("obsDistance");
    field_type scaling                   = parameterSet.get("scaling", field_type(1));
    const field_type E0                   = parameterSet.get<field_type>("E0");
    const field_type E1                   = parameterSet.get<field_type>("E1");
    const field_type nu                  = parameterSet.get("nu", field_type(0.3));

    // optional problem settings
    std::string parFile0;
    std::string parFile1;

    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    typedef UGGrid<dim> GridType;
    typedef BoundaryPatch<GridType::LevelGridView> LevelBoundaryPatch;
    typedef BoundaryPatch<GridType::LeafGridView> LeafBoundaryPatch;

    std::array<std::unique_ptr<GridType>, 2> grids;
#if HAVE_AMIRAMESH
    if (paramBoundaries) {
        parFile0 = parameterSet.get<std::string>("parFile0");
        parFile1 = parameterSet.get<std::string>("parFile1");
        grids[0] = AmiraMeshReader<GridType>::read(path + object0Name, PSurfaceBoundary<dim-1>::read(path + parFile0));
        grids[1] = AmiraMeshReader<GridType>::read(path + object1Name, PSurfaceBoundary<dim-1>::read(path + parFile1));
    } else {
        grids[0] = AmiraMeshReader<GridType>::read(path + object0Name);
        grids[1] = AmiraMeshReader<GridType>::read(path + object1Name);
    }
#else
    // if no Amira is found create a box and a die
    ParameterTree configBox;
    configBox["lowerCorner"] = "0 0 0";
    configBox["upperCorner"] = "4 9 3";
    configBox["elements"] = "4 8 2";
    configBox["tetrahedral"] = "1";
    grids[0] = GridConstruction<GridType, dim>::createCuboid(configBox);

    ParameterTree configDie;
    configDie["center"] = "-0.7 4.5 6.2";
    configDie["thickness"] = "0.2";
    configDie["length"]= "5.2";
    configDie["innerRadius"]= "3";
    configDie["fromAngle"] = "3.1415";
    configDie["toAngle"] = "6.283";
    configDie["nElementRing"] = "25";
    configDie["nElementLength"] = "10";
    configDie["closeTube"] = "0";
    configDie["axis"] = "0";
    configDie["tetrahedra"] = "1";
    configDie["parameterizedBoundary"] = "0";
    grids[1] = GridConstruction<GridType, dim>::createTubeSegment(configDie);
#endif

    std::array<VectorType, 2> coarseDirichletValues;
    coarseDirichletValues[0].resize(grids[0]->size(0, dim));
    coarseDirichletValues[1].resize(grids[1]->size(0, dim));
#if HAVE_AMIRAMESH
    AmiraMeshReader<GridType>::readFunction(coarseDirichletValues[0], path + dirichletValuesFile0);
    AmiraMeshReader<GridType>::readFunction(coarseDirichletValues[1], path + dirichletValuesFile1);
#else
#warning You have to set Dirichlet values manually or get Amira!
    coarseDirichletValues[0] = 0;
    coarseDirichletValues[1] = 0;
#endif
    // Scale Dirichlet values
    coarseDirichletValues[0] *= scaling;
    coarseDirichletValues[1] *= scaling;

    std::array<BitSetVector<dim>, 2> coarseDirichletNodes;
#if HAVE_AMIRAMESH
    readBitField(coarseDirichletNodes[0],grids[0]->size(0,dim), path + dirichletFile0);
    readBitField(coarseDirichletNodes[1],grids[1]->size(0,dim), path + dirichletFile1);
#else
#warning Dirichlet boundary is set to the whole boundary
    for (size_t i=0; i < 2; i++) {
      BoundaryPatch<GridType::LevelGridView> allPatch(grids[i]->levelGridView(0), true);
      DirichletBCAssembler<GridType>::inflateBitField(*allPatch.getVertices(), coarseDirichletNodes[i]);
    }
#endif



    grids[0]->setRefinementType(GridType::COPY);
    grids[1]->setRefinementType(GridType::COPY);

    // refine uniformly until minlevel
    for (int j=0; j<2; j++)
        for (int i=0; i<minLevel; i++) {
            grids[j]->globalRefine(1);

            if (paramBoundaries && useImproveGrid)
                improveGrid(*grids[j], 10);
    }

    // read field describing the obstacles
    BitSetVector<1> obsField;
    readBitField(obsField, grids[0]->size(0, dim), path + obsFilename);
    auto obsPatch = std::make_shared<LevelBoundaryPatch>(grids[0]->levelGridView(0), obsField);

    auto mortarPatch = std::make_shared<LevelBoundaryPatch>(grids[1]->levelGridView(0), true);

    if (parameterSet.hasKey("mortarFilename")) {
        readBitField(obsField, grids[1]->size(0, dim), path + parameterSet.get<std::string>("mortarFilename"));
        mortarPatch->setup(grids[1]->levelGridView(0), obsField);
    }

    std::array<VectorType, 2> rhs;
    std::array<VectorType, 2> x;

    for (size_t i=0; i<2; i++) {
        x[i].resize(grids[i]->size(dim));

        // Initial solution
        x[i] = 0;
    }

    // The global fe bases
    using P1Basis =  DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1> >;

    std::array<P1Basis*,2> p1Basis;

    while (true) {
        std::cout<<"Starting Refinement loop!\n";

        int toplevel = std::max(grids[0]->maxLevel(),grids[1]->maxLevel());

        std::array<BitSetVector<dim>, 2> dirichletNodes;
        std::array<VectorType, 2> dirichletValues;

        // Prolongate dirichlet values to the fine grid
        std::array<LeafBoundaryPatch, 2> dirichletBoundary;
        for (size_t i=0; i<2; i++) {

            DirichletBCAssembler<GridType>::assembleDirichletBC(*grids[i],coarseDirichletNodes[i],
                                                                coarseDirichletValues[i],
                                                                dirichletNodes[i],dirichletValues[i]);

            p1Basis[i] = new P1Basis(grids[i]->leafGridView());

            BitSetVector<1> leafNodes;
            DirichletBCAssembler<GridType>::inflateBitField(dirichletNodes[i],leafNodes);
            dirichletBoundary[i].setup(grids[i]->leafGridView(),leafNodes);
        }

        // ////////////////////////////////////////////////////////////
        //    Create rhs vectors
        // ////////////////////////////////////////////////////////////


        // Set right hand side vectors
        for (int i=0; i<2; i++) {

            rhs[i].resize(grids[i]->size(dim));
            rhs[i] = 0;

            for (size_t j=0; j<rhs[i].size(); j++)
                for (int k=0; k<dim; k++) {
                    if (dirichletNodes[i][j][k])
                        x[i][j][k] = dirichletValues[i][j][k];
                }

        }

        // make dirichlet bitfields containing dirichlet information for both grids
        int size = rhs[0].size() + rhs[1].size();

        BitSetVector<dim> totalDirichletNodes(size);

        for (size_t i=0; i<dirichletNodes[0].size(); i++)
            for (int j=0; j<dim; j++)
                totalDirichletNodes[i][j] = dirichletNodes[0][i][j];

        int offset = rhs[0].size();
        for (size_t i=0; i<dirichletNodes[1].size(); i++)
            for (int j=0; j<dim; j++)
                totalDirichletNodes[offset + i][j] = dirichletNodes[1][i][j];

        // Assemble separate linear elasticity problems
        std::array<MatrixType,2> stiffnessMatrix;
        std::array<const MatrixType*, 2> submat;

        for (size_t i=0; i<2; i++) {
            OperatorAssembler<P1Basis,P1Basis> globalAssembler(*p1Basis[i],*p1Basis[i]);

            double s = (i==0) ? E0 : E1;
            StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(s, nu);

            globalAssembler.assemble(localAssembler, stiffnessMatrix[i]);

            submat[i] = &stiffnessMatrix[i];
        }

        // Assemble contact problem
        std::shared_ptr<ContactProjection<LeafBoundaryPatch> > contactProjection;
#if EXPLICIT_CONTACT_DIRECTIONS
        contactProjection = std::make_shared_ptr<AnalyticProjection<LeafBoundaryPatch> >(contactDirection);
#else
        contactProjection = std::make_shared<NormalProjection<LeafBoundaryPatch> >();
#endif

        TwoBodyAssembler<GridType, VectorType> contactAssembler(*grids[0], *grids[1],
                               obsPatch,
                               mortarPatch,
                               obsDistance,
                               CouplingPairBase::CONTACT,
                               contactProjection,
                               (nonsmoothNewtonMG!=1));

        contactAssembler.setup();

        MatrixType bilinearForm;
        contactAssembler.assembleJacobian(submat, bilinearForm);

        // ////////////////////////////////////////////////////////////////////////////
        //   Turn the initial solutions from the nodal basis to the transformed basis
        // ////////////////////////////////////////////////////////////////////////////

        VectorType totalX, totalRhs;

        contactAssembler.nodalToTransformed(x, totalX);
        contactAssembler.concatenateVectors(rhs, totalRhs);

        // ////////////////////////////////////////////////
        //   Create new tnnmg solver
        // ////////////////////////////////////////////////

        // First create a base solver
#ifdef HAVE_IPOPT
        QuadraticIPOptSolver<MatrixType,VectorType> baseSolver(baseTolerance, 100, NumProc::QUIET);
#else
        ProjectedBlockGSStep<MatrixType, VectorType> baseSolverStep;

        EnergyNorm<MatrixType, VectorType> baseEnergyNorm(baseSolverStep);

        Dune::Solvers::LoopSolver<VectorType> baseSolver(baseSolverStep,
                                                            baseIt,
                                                            baseTolerance,
                                                            baseEnergyNorm,
                                                            Solver::QUIET);

        // Dummy obstacles.  This is not nice
        BitSetVector<1> dummyHasObstacle(size,false);
        baseSolverStep.hasObstacle_ = &dummyHasObstacle;
#endif

        // Make pre and postsmoothers
        std::shared_ptr<ProjectedBlockGSStep<MatrixType, VectorType> > linearSmoother;

        MultigridStep<MatrixType, VectorType>* multigridStep;
        switch (nonsmoothNewtonMG) {
            case 0:
                multigridStep = new MonotoneMGStep<MatrixType, VectorType>(bilinearForm, totalX, totalRhs);
                break;
            case 1:
                multigridStep = new NonSmoothNewtonMGStep<MatrixType, VectorType>(bilinearForm, totalX, totalRhs);
                break;
        }

        multigridStep->setMGType(mu, nu1, nu2);
        multigridStep->setIgnore(totalDirichletNodes);
        multigridStep->setBaseSolver(baseSolver);
        multigridStep->setSmoother(linearSmoother);

        if (nonsmoothNewtonMG == 1){
            dynamic_cast<NonSmoothNewtonMGStep<MatrixType, VectorType>*>(multigridStep)->setHasObstacle(contactAssembler.totalHasObstacle_);
            dynamic_cast<NonSmoothNewtonMGStep<MatrixType, VectorType>*>(multigridStep)->setObstacles(contactAssembler.totalObstacles_);
        } else {
            dynamic_cast<MonotoneMGStep<MatrixType, VectorType>*>(multigridStep)->setHasObstacles(contactAssembler.totalHasObstacle_);
            dynamic_cast<MonotoneMGStep<MatrixType, VectorType>*>(multigridStep)->setObstacles(contactAssembler.totalObstacles_);
            dynamic_cast<MonotoneMGStep<MatrixType, VectorType>*>(multigridStep)->setObstacleRestrictor(ContactObsRestriction<VectorType>());
        }

        // Create the transfer operators
        std::vector<std::shared_ptr<TruncatedDenseMGTransfer<VectorType> > > mgTransfers(toplevel);

        if (nonsmoothNewtonMG == 1) {

            for (size_t i=0; i<mgTransfers.size(); i++) {
                auto transfer = std::make_shared<NonSmoothNewtonContactTransfer<VectorType> >();
                transfer->setup(*grids[0], *grids[1], i,
                        contactAssembler.contactCoupling_[0]->mortarLagrangeMatrix(),
                        contactAssembler.localCoordSystems_,
                        *contactAssembler.contactCoupling_[0]->nonmortarBoundary().getVertices());
                mgTransfers[toplevel-i-1] = std::dynamic_pointer_cast<TruncatedDenseMGTransfer<VectorType> >(transfer);
            }
        } else if (nonsmoothNewtonMG == 0){

            std::vector<ContactMGTransfer<VectorType>* > transfers(toplevel);
            for (size_t i=0; i<transfers.size(); i++)
                transfers[i]= new ContactMGTransfer<VectorType>;

            ContactTransferOperatorAssembler<VectorType,GridType>::assembleOperatorHierarchy(
                    *dynamic_cast<TwoBodyAssembler<GridType,VectorType>*>(&contactAssembler),transfers);

            for (size_t i=0; i<transfers.size(); i++) {
                auto sharedTransferPtr = std::shared_ptr<ContactMGTransfer<VectorType> >(transfers[i]);
                mgTransfers[i] = std::dynamic_pointer_cast<TruncatedDenseMGTransfer<VectorType> >(sharedTransferPtr);
            }
        }

        multigridStep->setTransferOperators(mgTransfers);

        EnergyNorm<MatrixType, VectorType> energyNorm(*multigridStep);

        ::LoopSolver<VectorType> solver(*multigridStep,
                // IPOpt doesn't like to be started at the solution, so make sure we really
                // call it only once when it is the only solver
                (toplevel==0) ? 1 : numIt,
                tolerance,
                energyNorm,
                Solver::FULL);

        // solver.historyBuffer_ = "tmp";
        // ///////////////////////////
        //   Compute solution
        // ///////////////////////////

        solver.preprocess();

        solver.solve();

        totalX = multigridStep->getSol();

        // cleanup
        delete(multigridStep);

        contactAssembler.postprocess(totalX, x);

        // ////////////////////////////////////////////////////////////////////////
        //    Leave adaptation loop if maximum number of levels has been reached
        // ////////////////////////////////////////////////////////////////////////

        if (toplevel == maxLevel)
            break;

        // ////////////////////////////////////////////////////
        //    Estimate error per element
        // ////////////////////////////////////////////////////
#ifdef HAVE_IPOPT
        //using P2Basis = DuneFunctionsBasis<Dune::Functions::PQkNodalBasis<GridType::LeafGridView,2> >;
        using P2Basis = P2NodalBasis<GridType::LeafGridView,field_type>;
        //using P2Basis = P2HierarchicalBasis<GridType::LeafGridView, field_type>;
        std::vector<std::shared_ptr<P2Basis> > p2Bases(2);
        p2Bases[0] = std::make_shared<P2Basis>(grids[0]->leafGridView());
        p2Bases[1] = std::make_shared<P2Basis>(grids[1]->leafGridView());

        HierarchicContactEstimator<P1Basis, P2Basis> estimator(*grids[0], *grids[1]);
        estimator.setupContactCoupling(0,0,1,obsPatch,mortarPatch,obsDistance,CouplingPairBase::CONTACT);

        using P2Lfe = P2Basis::LocalFiniteElement;
        using P2LocalStiffness = StVenantKirchhoffAssembler<GridType,P2Lfe,P2Lfe>;
        auto localStiffness0 = std::make_shared<P2LocalStiffness>(E0,nu);
        auto localStiffness1 = std::make_shared<P2LocalStiffness>(E1,nu);
        std::vector<std::shared_ptr<LocalOperatorAssembler<GridType, P2Lfe, P2Lfe, BlockType> > > localStiffnesses(2);
        localStiffnesses[0] = localStiffness0; localStiffnesses[1] = localStiffness1;
       // = {localStiffness, localStiffness};

        std::vector<std::shared_ptr<RefinementIndicator<GridType> > > refinementIndicator(2);
        refinementIndicator[0] = std::make_shared<RefinementIndicator<GridType> >(*grids[0]);
        refinementIndicator[1] = std::make_shared<RefinementIndicator<GridType> >(*grids[1]);

        std::vector<std::shared_ptr<LeafBoundaryPatch> > dirichletBoundaries =
                  {stackobject_to_shared_ptr(dirichletBoundary[0]), stackobject_to_shared_ptr(dirichletBoundary[1])};

        auto xFunc0 = ::Functions::makeFunction(*p1Basis[0],x[0]);
        auto xFunc1 = ::Functions::makeFunction(*p1Basis[1],x[1]);
        std::vector<std::shared_ptr<BasisGridFunction<P1Basis,VectorType> > > xFunctions =
            {stackobject_to_shared_ptr(xFunc0), stackobject_to_shared_ptr(xFunc1)};

        estimator.estimate(xFunctions,
                dirichletBoundaries,
                refinementIndicator,
                p2Bases,
                localStiffnesses);

        // ////////////////////////////////////////////////////////
        //   Refine the grids and transfer the current solution
        // ////////////////////////////////////////////////////////

        std::vector<GridType*> adaptiveGridVector(2);
        adaptiveGridVector[0] = grids[0].get();
        adaptiveGridVector[1] = grids[1].get();
        FractionalMarkingStrategy<GridType>::mark(refinementIndicator, adaptiveGridVector, refinementFraction);
#else
        std::cout << "No adaptive refinement without IPOpt installed.  Refining uniformly." << std::endl;
        for (size_t i = 0; i < 2; ++i)
          for (const auto& e : elements(grids[j]->leafGridView()))
            grids[i]->mark(1, e);
#endif

        // mark possible contact area for refinement
        if (uniformlyRefineContactNeighborhood)
            MarkContactArea::markPossibleContactArea(*grids[0], *grids[1], obsDistance);

        for (size_t i=0; i<2; i++) {

            GridFunctionAdaptor<P1Basis> adaptor(*p1Basis[i],true,true);

            grids[i]->preAdapt();
            grids[i]->adapt();
            grids[i]->postAdapt();

            p1Basis[i]->update();
            adaptor.adapt(x[i]);

            if (paramBoundaries && useImproveGrid)
                improveGrid(*grids[i], 10);
        }

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        for (size_t i=0; i<2; i++) {
            std::cout << "  Grid: "<<i<<" Max level: " << grids[i]->maxLevel()
                << "   vertices: " << grids[i]->size(grids[i]->maxLevel(), dim)
                << "   elements: " << grids[i]->size(grids[i]->maxLevel(), 0) << std::endl;
        }
        std::cout << "########################################################" << std::endl;

        for (size_t i=0; i<2; i++)
            delete(p1Basis[i]);
    }

    // Output result
    std::string writer = parameterSet.get("writer","Vtk");
    if (writer=="Amira") {
#if HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType> amiramesh;
        amiramesh.addLeafGrid(*grids[0],true);
        amiramesh.addVertexData(x[0], grids[0]->leafGridView());
        amiramesh.write(resultPath+"0resultGrid",1);

        LeafAmiraMeshWriter<GridType> amiramesh2;
        amiramesh2.addLeafGrid(*grids[1],true);
        amiramesh2.addVertexData(x[1], grids[1]->leafGridView());
        amiramesh2.write(resultPath+"1resultGrid",1);
#else
#warning Cannot write results in Amira format without Amiramesh
#endif
    } else {

        VTKWriter<GridType::LeafGridView> vtkWriter0(grids[0]->leafGridView());
        auto displacement0 = std::make_shared<VTKBasisGridFunction<P1Basis,VectorType> >(*p1Basis[0], x[0], "Displacement");
        vtkWriter0.addVertexData(displacement0);
        vtkWriter0.write(resultPath + "0resultGrid");

        VTKWriter<GridType::LeafGridView> vtkWriter1(grids[1]->leafGridView());
        auto displacement1 = std::make_shared<VTKBasisGridFunction<P1Basis,VectorType> >(*p1Basis[1], x[1], "Displacement");
        vtkWriter1.addVertexData(displacement1);
        vtkWriter1.write(resultPath + "1resultGrid");
    }
} catch (Exception& e) {

    std::cout << e.what() << std::endl;

}
