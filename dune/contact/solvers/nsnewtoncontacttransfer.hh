#ifndef NONSMOOTH_NEWTON_CONTACT_TRANSFER_HH
#define NONSMOOTH_NEWTON_CONTACT_TRANSFER_HH

#include <vector>
#include <dune/common/bitsetvector.hh>
#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>
#include <dune/solvers/transferoperators/truncatedcompressedmgtransfer.hh>

#include <dune/contact/assemblers/largedeformationcontactassembler.hh>

namespace Dune {
namespace Contact {

/** \brief Multigrid restriction and prolongation operator for contact problems using the TNNMG method.
 *
 * Currently only works for first-order Lagrangian elements!
 *
 *   This class sets up the multigrid transfer operator for n-body contact
 *   problems.  It first constructs the standard prolongation matrices for the
 *   grids and then combines them into a single matrix.  Then, this matrix
 *   is modified using the mortar transformation basis of Wohlmuth, Krause: "Monotone
 *   Methods on Nonmatching Grids for Nonlinear Contact Problems". In the TNNMG method
 *   the nonlinear obstacle problem is only solved on the finest level using a smoother.
 *   As coarse grid correction a linear multigrid step is applied which is later projected
 *   back onto the admissible set. Consequently the mortar basis is only needed on the finest
 *   level. This transfer operator thus additionally to the restriction and truncation,
 *   transformes the mortar basis back to the nodal basis on the finest level and otherwise
 *   just does standard restriction/prolongation. On the finest level the prolongation matrix
 *   is of the form

 *   \f[ \begin{pmatrix}
 *        (I_l^{l+1})_{ii} & (I_l^{l+1})_{im}       & (I_l^{l+1})_{is} \\
 *        0                & (I_l^{l+1})_{mm}       & 0 \\
 *        0                & -M_{l+1}(I_l^{l+1})_{mm} & (I_l^{l+1})_{ss}
 *       \end{pmatrix} \f].
 *
 *   where \f[ I_l^{l+1} \f] denote the standard transfer operators
 *     and \f[ M_{l+1} \f] denotes the weighted mortar matrix
 */
template<class VectorType>
class NonSmoothNewtonContactTransfer : public TruncatedDenseMGTransfer<VectorType> {

    enum {blocksize = VectorType::block_type::dimension};

    typedef typename VectorType::field_type field_type;

    typedef FieldMatrix<field_type, blocksize, blocksize> MatrixBlock;

    typedef BCRSMatrix<MatrixBlock> MatrixType;

    typedef std::vector<MatrixBlock> CoordSystemVector;
    using This = NonSmoothNewtonContactTransfer<VectorType>;

public:

    typedef typename MultigridTransfer<VectorType>::OperatorType OperatorType;

    /** \brief Setup transfer operator for a one-body contact problem.
     *
     *  \param grid The grid.
     *  \param colevel The co-level of the fine grid level to which the transfer operator belongs
     *  \param fineLocalCoordSystems A vector containing the local coordinate systems (householder transformations)
    */
    template <class GridType>
    void setup(const GridType& grid,
               int colevel,
               const CoordSystemVector& fineLocalCoordSystems);

    /** \brief Setup transfer operator for a two-body contact problem.
     *
     *  \param grid0 The nonmortar grid.
     *  \param grid1 The mortar grid.
     *  \param colevel The co-level of the fine grid level to which the transfer operator belongs
     *  \param mortarTransferOperator The weighted mortar matrix
     *  \param fineLocalCoordSystems A vector containing the local coordinate systems (householder transformations)
     *  \param fineHasObstacle  Bitfield determining which fine grid doFs have an obstacle.
     *
    */
    template <class GridType0, class GridType1>
    void setup(const GridType0& grid0, const GridType1& grid1,
               int colevel,
               const MatrixType& mortarTransferOperator,
               const CoordSystemVector& fineLocalCoordSystems,
               const BitSetVector<1>& fineHasObstacle);

    /** \brief Setup transfer operator for a N-body contact problem.
     *
     *  \param grids The involved grids
     *  \param colevel The co-level of the fine grid level to which the transfer operator belongs
     *  \param mortarTransferOperator The weighted mortar matrices of the couplings
     *  \param fineLocalCoordSystems A vector containing the local coordinate systems (householder transformations) of all grids
     *  \param fineHasObstacle  Bitfields determining for each coupling which fine grid nodes belong to the nonmortar boundary.
     *  \param gridIdx  For each coupling store the indices of the nonmortar and mortar grid.
    */
    template <class GridType>
    void setup(const std::vector<const GridType*>& grids, int colevel,
               const std::vector<const MatrixType*>& mortarTransferOper,
               const CoordSystemVector& fineLocalCoordSystems,
               const std::vector<const BitSetVector<1>*>& fineHasObstacle,
               const std::vector<std::array<int,2> >& gridIdx);

    template <class GridType>
    static void setupHierarchy(std::vector<std::shared_ptr<NonSmoothNewtonContactTransfer<VectorType> > >& mgTransfers,
                const std::vector<const GridType*> grids,
                const std::vector<const MatrixType*> mortarTransferOperator,
                const CoordSystemVector& fineLocalCoordSystems,
                const std::vector<const BitSetVector<1>*>& fineHasObstacle,
                const std::vector<std::array<int,2> >& gridIdx);



    template <class GridType, class ThisPtr>
    static void setupHierarchy(std::vector<ThisPtr>& mgTransfers,
                           std::shared_ptr<LargeDeformationContactAssembler<GridType,VectorType> > contactAssembler);
protected:
    /** \brief Combine all the standard prolongation and mortar operators to one big matrix.
     *
     *  \param submat The standard transfer operators
     *  \param mortarTransferOperator The weighted mortar matrices of the couplings
     *  \param fineLocalCoordSystems A vector containing the local coordinate systems (householder transformations) of all grids
     *  \param fineHasObstacle  Bitfields determining for each coupling which fine grid nodes belong to the nonmortar boundary.
     *  \param gridIdx  For each coupling store the indices of the nonmortar and mortar grid.

    */
    void combineSubMatrices(const std::vector<const MatrixType*>& submat,
                            const std::vector<const MatrixType*>& mortarTransferOperator,
                            const CoordSystemVector& fineLocalCoordSystems,
                            const std::vector<const BitSetVector<1>*>& fineHasObstacle,
                            const std::vector<std::array<int,2> >& gridIdx);

};

} /* namespace Contact */
} /* namespace Dune */

#include "nsnewtoncontacttransfer.cc"

#endif
