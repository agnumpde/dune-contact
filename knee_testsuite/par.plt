set title 'Knee with parametrization'
set xlabel 'Iteration'
set ylabel 'Error'
set logscale y

plot 'par_mmg_cont/statistics' using 1:3 with lines title "Contact problem"                    , \
     'par_mmg_lin/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \
     'par_mmg_can/statistics'  using 1:3 with lines title "Linear problem, nodal basis"        , \
     'par_gmg_cont/statistics' using 1:3 with lines title "Graeser multigrid"      

set output "par.eps"
set terminal postscript eps color
replot

set output "par.fig"
set terminal fig color
replot